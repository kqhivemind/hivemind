import { format } from 'date-fns';
import { CABINET_POSITIONS, POSITIONS_BY_ID } from 'util/constants';

const getPlayerBlessMaiden = event => [event.values[2] == 'Blue' ? CABINET_POSITIONS.BLUE_QUEEN : CABINET_POSITIONS.GOLD_QUEEN];

const eventTypes = {
  berryDeposit: { description: 'Berry deposited', playerIndexes: [2], locationIndexes: [0, 1] },
  berryKickIn: { description: 'Berry kicked in', playerIndexes: [2], locationIndexes: [0, 1] },
  blessMaiden: { description: 'Gate reserved', getPlayers: getPlayerBlessMaiden, locationIndexes: [0, 1] },
  cabinetOffline: { description: 'Cabinet offline', playerIndexes: [], locationIndexes: [] },
  cabinetOnline: { description: 'Cabinet online', playerIndexes: [], locationIndexes: [] },
  carryFood: { description: 'Picked up berry', playerIndexes: [0], locationIndexes: [] },
  disconnect: { description: 'Disconnected', playerIndexes: [], locationIndexes: [] },
  gameend: { description: 'End of game', playerIndexes: [], locationIndexes: [] },
  gamestart: { description: 'Start of game', playerIndexes: [], locationIndexes: [] },
  getOffSnail: { description: 'Got off snail', playerIndexes: [3], locationIndexes: [0, 1] },
  getOnSnail: { description: 'Got on snail', playerIndexes: [2], locationIndexes: [0, 1] },
  glance: { description: 'Bumped', playerIndexes: [2, 3], locationIndexes: [0, 1] },
  playerKill: { description: 'Killed', playerIndexes: [2, 3], locationIndexes: [0, 1] },
  reloadMatch: { description: 'Reload match', playerIndexes: [], locationIndexes: [] },
  reserveMaiden: { description: 'Entered gate', playerIndexes: [2], locationIndexes: [0, 1] },
  snailEat: { description: 'Snail began eating', playerIndexes: [2, 3], locationIndexes: [0, 1] },
  snailEscape: { description: 'Escaped snail', playerIndexes: [2], locationIndexes: [0, 1] },
  spawn: { description: 'Spawned', playerIndexes: [0], locationIndexes: [] },
  unreserveMaiden: { description: 'Exited gate', playerIndexes: [3], locationIndexes: [0, 1] },
  useMaiden: { description: 'Used gate', playerIndexes: [3], locationIndexes: [0, 1] },
  victory: { description: 'Victory', playerIndexes: [], locationIndexes: [] },
};

export default function GameEvent({ timestamp, eventType, values, game }) {
  this.game = game;
  this.timestamp = timestamp;
  this.eventType = eventType;
  this.values = values;

  if (eventTypes[this.eventType]) {
    const eventType = eventTypes[this.eventType];
    this.description = eventType.description;
    this.location = eventType.locationIndexes.map(i => this.values[i]);
    if (eventType.getPlayers) {
      this.players = eventType.getPlayers(this);
    } else {
      this.players = eventType.playerIndexes.map(i => POSITIONS_BY_ID[this.values[i]]);
    }
  } else {
    this.description = this.eventType,
    this.location = [];
    this.players = [];
  }

  this.formatTimestamp = () => {
    if (new Date(this.timestamp) >= new Date(this.game.startTime)) {
      return format(new Date(this.timestamp) - new Date(this.game.startTime), 'mm:ss.SSS');
    } else {
      return '-' + format(new Date(this.game.startTime) - new Date(this.timestamp), 'mm:ss.SSS');
    }
  };

  return this;
};

