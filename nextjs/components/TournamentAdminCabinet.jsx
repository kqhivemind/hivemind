import React, { useState, useEffect, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import { Card, Grid, Typography, CircularProgress, Button } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiTimer, mdiMap, mdiFruitGrapes, mdiPencil } from '@mdi/js';
import clsx from 'clsx';

import { MAP_NAMES } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { formatUTC } from 'util/dates';
import { useTournamentAdmin } from '@/components/tournament-admin';
import TournamentMatchForm from 'components/forms/TournamentMatchForm';

const useStyles = makeStyles(theme => ({
  gridItem: {
    '&:not(.online)': {
      opacity: 0.7,
    },
  },
  card: {
    margin: theme.spacing(0),
    cursor: 'pointer',
    boxShadow: 'none',
    backgroundColor: 'white',
  },
  row: {
    padding: theme.spacing(1, 2),

    alignItems: 'center',
    '&:first-child': {
      borderWidth: 0,
    },
  },
  teamRow: {
    borderColor: theme.palette.divider,
    borderStyle: 'solid',
    borderWidth: '0 0 1px',
    padding: 0,
    '&:last-child': {
      borderBottom: 0,
    },
    '&:before': {
      content: '""',
      width: '1.75rem',
      height: '1.75rem',
      marginLeft: '1rem',
      display: 'block',
    },
  },
  blue: {
    '&:before': {
      background: theme.gradients.blue.light,
    },
  },
  gold: {
    '&:before': {
      background: theme.gradients.gold.light,
    },
  },
  cabinetNameRow: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.gold.light3,
    '.online &': {
      backgroundColor: theme.palette.gold.light3,
    },
  },
  cabinetName: {
    fontWeight: 'bold',
  },
  gameStats: {
    backgroundColor: theme.palette.grey['800'],
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    padding: theme.spacing(1.5, 2),
  },
  teamScore: {
    flexGrow: 0,
    flexBasis: '100px',
    borderLeft: `1px solid ${theme.palette.divider}`,
    textAlign: 'right',
    padding: theme.spacing(1.5, 2),
  },
  subtitleItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    background: theme.palette.grey['800'],
    color: 'white',
    '& svg': {
      width: '24px',
      height: '24px',
      verticalAlign: 'middle',
    },
  },
}));

export default function TournamentAdminCabinet({ cabinet }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const [activeMatch, setActiveMatch] = useState(undefined);
  const [online, setOnline] = useState(cabinet.clientStatus === 'online');
  const [stats, setStats] = useState(null);
  const [formOpen, setFormOpen] = useState(false);
  const { tournament, wsGameState, teamsById, hideInactiveCabs } = useTournamentAdmin();
  const classes = useStyles();

  useEffect(() => {
    const hash = router.asPath.split('#')[1];
    if (!formOpen && cabinet.id === parseInt(hash)) {
      setFormOpen(true);
    }
  }, [router.asPath]);

  const visible = useMemo(() => {
    if (formOpen || !hideInactiveCabs) return true;
    if (tournament?.assignedCabinets?.length > 0 && cabinet?.id) {
      return tournament.assignedCabinets.includes(cabinet.id);
    }

    return online;
  }, [formOpen, hideInactiveCabs, tournament?.assignedCabinets, cabinet?.id, online]);

  const allTeams = {};
  for (const team of tournament.teams) {
    allTeams[team.id] = team;
  }

  const finishMatch = async () => {
    let response = await axios.get(`/api/tournament/match/${activeMatch.id}/`);
    const data = response.data;

    data.isComplete = true;
    data.activeCabinet = null;

    response = await axios.put(`/api/tournament/match/${activeMatch.id}/`, data);

    setFormOpen(false);
    loadActiveMatch();
  };

  wsGameState.onJsonMessage(message => {
    if (message.type == 'match' && message.cabinetId == cabinet.id) {
      loadActiveMatch();
    }

    if (message.type == 'cabinetOnline' && message.cabinetId == cabinet.id) {
      setOnline(true);
    }

    if (message.type == 'cabinetOffline' && message.cabinetId == cabinet.id) {
      setOnline(false);
    }
  });

  const onOpenStats = () => {
    wsStats.sendJsonMessage({ type: 'subscribe', cabinet_id: cabinet.id });
  };

  const wsStats = useWebSocket('/ws/ingame_stats', { onOpen: onOpenStats });
  wsStats.onJsonMessage(message => {
    setStats(message);
  });

  const handleClick = () => {
    router.replace(`${router.asPath.split('#')[0]}#${cabinet.id}`);
  };

  const loadActiveMatch = () => {
    axios
      .get(`/api/tournament/match/`, { params: { activeCabinetId: cabinet.id } })
      .then(response => {
        if (response.data?.count > 0) {
          setActiveMatch(response.data.results[0]);
        } else {
          setActiveMatch({
            tournament: tournament.id,
            activeCabinet: cabinet.id,
            blueScore: 0,
            goldScore: 0,
          });
        }
      });
  };

  const handleClose = () => {
    router.replace(router.asPath.split('#')[0]);
    setFormOpen(false);
  };

  useEffect(() => {
    if (activeMatch === undefined) {
      loadActiveMatch();
    }
  }, []);

  if (activeMatch === undefined) {
    return <CircularProgress />;
  }

  const blueTeamName = activeMatch?.blueTeam ? allTeams[activeMatch.blueTeam]?.name : '(no game)';
  const goldTeamName = activeMatch?.goldTeam ? allTeams[activeMatch.goldTeam]?.name : '(no game)';

  const dialogActions = activeMatch?.id && (
    <Button variant="outlined" color="primary" onClick={finishMatch}>
      Match Complete
    </Button>
  );

  if (!visible) {
    return <></>;
  }

  return (
    <Grid item xs={12} md={6} className={clsx(classes.gridItem, { online })}>
      <Card className={classes.card} onClick={handleClick}>
        <Grid container direction="column" className={classes.container}>
          <Grid item className={clsx(classes.row, classes.cabinetNameRow)}>
            <Typography className={classes.cabinetName}>{cabinet.displayName}</Typography>
            {online && (
              <div className="w-5 h-5">
                <Icon path={mdiPencil} />
              </div>
            )}
          </Grid>
          <Grid item container direction="row" className={clsx(classes.row, classes.gameStats)}>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiMap} /> {MAP_NAMES[stats?.mapName] ?? stats?.mapName}
            </Grid>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiTimer} /> {formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss')}
            </Grid>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiFruitGrapes} /> {stats?.berriesRemaining}
            </Grid>
          </Grid>

          <Grid
            item
            container
            direction="row"
            className={clsx(classes.row, classes.teamRow, classes.blue)}
          >
            <Grid item className={classes.teamName}>
              {blueTeamName}
            </Grid>
            <Grid item className={classes.teamScore}>
              {activeMatch?.blueScore ?? ''}
            </Grid>
          </Grid>

          <Grid
            item
            container
            direction="row"
            className={clsx(classes.row, classes.teamRow, classes.gold)}
          >
            <Grid item className={classes.teamName}>
              {goldTeamName}
            </Grid>
            <Grid item className={classes.teamScore}>
              {activeMatch?.goldScore ?? ''}
            </Grid>
          </Grid>
        </Grid>
      </Card>

      <TournamentMatchForm
        tournament={tournament}
        cabinet={cabinet}
        activeMatch={activeMatch}
        open={formOpen}
        onClose={handleClose}
        dialogActions={dialogActions}
        stats={stats}
      />
    </Grid>
  );
}
