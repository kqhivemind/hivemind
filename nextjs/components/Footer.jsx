export default function Footer() {

  return (
    <footer className='w-full footer pt-2 px-4 pb-2 bg-gradient-to-b from-light-dirtTop to-light-dirtBottom border-0 border-t-8 border-light-grass border-solid'>
      <div className='max-w-[1400px] mx-auto md:px-24 md:justify-evenly md:justify-items-center grid grid-cols-2 md:grid-cols-3 gap-4'>
        <div className='col-span-2 md:col-span-1'>
          <h2 className='text-bold'>About</h2>
            <ul className='list-none pl-0'>
              <li><p>HiveMind is a free open-source stat tracker for <a className='text-secondary-main hover:text-secondary-dark no-underline' href='https://killerqueenarcade.com/'>Killer Queen Arcade</a>.</p></li>
              <li><a href='/about'>About HiveMind</a></li>
              <li><a href='https://shop.kqhivemind.com'>Store</a></li>
              <li><a href='/privacy'>Privacy Policy</a></li>
              
            </ul>
        </div>
        <div>
          <h2 className='text-bold'>Help</h2>
            <ul className='list-none pl-0'>
              <li><a href='/wiki'>Wiki</a></li>
              <li><a href='/scene-request'>Request a New Scene</a></li>
              
            </ul>
        </div>
        <div>
          <h2 className='text-bold'>Community</h2>
            <ul className='list-none pl-0'>
              <li><a href='https://discord.gg/kqa'>Discord</a></li>
              <li><a href='https://gitlab.com/kqhivemind/hivemind'>GitLab</a></li>
              <li><a href='https://www.patreon.com/kqhivemind'>Support Us on Patreon!</a></li>
            </ul>
        </div>
      </div>
    </footer>
  );
}
