import React, { useState } from 'react';
import { Button, Menu, MenuItem, Divider } from '@material-ui/core';
import { bindMenu, bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';

import { PERMISSION_TYPES, PERMISSION_NAMES } from '@/util/constants';
import { getAxios } from '@/util/axios';
import { DeleteConfirmDialog } from '@/components/DeleteButton';
import { useScenePermissions } from './context';

export function EditMenu({ menuState, onSelectPermission, onDelete }) {
  return (
    <>
      <Menu getContentAnchorEl={null} {...bindMenu(menuState)}>
        {[PERMISSION_TYPES.ADMIN, PERMISSION_TYPES.TOURNAMENT].map(value => (
          <MenuItem key={value} value={value} onClick={() => onSelectPermission(value)}>
            <div>
              Change to <b>{PERMISSION_NAMES[value]}</b>
            </div>
          </MenuItem>
        ))}

        <Divider />

        <MenuItem onClick={onDelete}>Delete</MenuItem>
      </Menu>
    </>
  );
}

export function EditButton({ permission }) {
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const { setUserPermissions } = useScenePermissions();
  const axios = getAxios({ authenticated: true });
  const menuState = usePopupState({ popupId: `permission-edit-${permission.id}` });

  const openDeleteDialog = () => {
    menuState.close();
    setDeleteDialogOpen(true);
  };

  const confirmText = (
    <>
      Remove <b>{PERMISSION_NAMES[permission.permission]}</b> permission from{' '}
      <b>{permission.user.name}</b>?
    </>
  );

  const onSelectPermission = value => {
    axios.patch(`/api/user/permission/${permission.id}/`, { permission: value }).then(() => {
      setUserPermissions(val =>
        val.map(i => (i.id === permission.id ? { ...i, permission: value } : i)),
      );
      menuState.close();
    });
  };

  const onDelete = () => {
    axios.delete(`/api/user/permission/${permission.id}/`).then(() => {
      setUserPermissions(val => val.filter(i => i.id !== permission.id));
      menuState.close();
    });
  };

  return (
    <>
      <Button variant="outlined" color="secondary" {...bindTrigger(menuState)}>
        Edit
      </Button>
      <EditMenu onDelete={openDeleteDialog} {...{ menuState, onSelectPermission }} />
      <DeleteConfirmDialog
        open={deleteDialogOpen}
        confirmText={confirmText}
        handleClose={() => setDeleteDialogOpen(false)}
        handleConfirm={onDelete}
      />
    </>
  );
}
