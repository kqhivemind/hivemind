import React from 'react';

import { PERMISSION_NAMES } from '@/util/constants';
import { Subtitle } from '@/components/subtitle';
import DeleteButton from '@/components/DeleteButton';
import { getAxios } from '@/util/axios';
import { useScenePermissions } from './context';
import { EditButton } from './edit-button';
import styles from './table.module.css';

export function ScenePermissionsRow({ row }) {
  const axios = getAxios({ authenticated: true });
  const { setUserPermissions } = useScenePermissions();

  const deleteRow = () => {};

  return (
    <div className={styles.permissionRow}>
      <div className={styles.image}>{row.user.image && <img src={row.user.image} />}</div>

      <div className={styles.name}>
        {row.user.name} {row.user.scene && ` [${row.user.scene}]`}
      </div>
      <div className={styles.type}>{PERMISSION_NAMES[row.permission]}</div>
      <div className={styles.icons}>
        <EditButton permission={row} />
      </div>
    </div>
  );
}

export function ScenePermissionsTable() {
  const { userPermissions } = useScenePermissions();

  if (!userPermissions) return null;

  return (
    <>
      <Subtitle title="Permissions" />

      <div className={styles.permissionTable}>
        {userPermissions.map(row => (
          <ScenePermissionsRow key={row.id} row={row} />
        ))}
      </div>
    </>
  );
}
