import React from 'react';
import { format } from 'date-fns';

import { PERMISSION_NAMES } from '@/util/constants';
import { Subtitle } from '@/components/subtitle';
import { useScenePermissions } from './context';
import { InviteForm } from './invite-form';
import styles from './invites-table.module.css';

export function PermissionInviteRow({ invite }) {
  const { setSelectedInvite } = useScenePermissions();

  const onClick = () => {
    setSelectedInvite(invite);
  };

  return (
    <div className={styles.inviteRow} onClick={onClick}>
      <div className={styles.token}>{invite.token}</div>
      <div className={styles.permission}>{PERMISSION_NAMES[invite.permission]}</div>
      <div className={styles.createTimestamp}>
        Created at {format(new Date(invite.createdTimestamp), 'hh:mm aa')}
      </div>
    </div>
  );
}

export function PermissionInvitesTable() {
  const { invites } = useScenePermissions();

  return (
    <>
      <Subtitle title="Invites" buttons={<InviteForm />} />

      <div className={styles.invitesTable}>
        {!(invites?.length > 0) && (
          <div className={styles.noResults}>
            There are no open invites for permissions on this scene.
          </div>
        )}
        {invites?.map(invite => (
          <PermissionInviteRow key={invite.id} invite={invite} />
        ))}
      </div>
    </>
  );
}
