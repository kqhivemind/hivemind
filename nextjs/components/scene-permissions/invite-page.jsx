import React, { useState, useMemo } from 'react';
import { mdiAccount, mdiCity, mdiShieldKey } from '@mdi/js';
import { Button, CircularProgress } from '@material-ui/core';

import LoginButtons from '@/components/LoginButtons';
import Title from '@/components/Title';
import { SummaryTable, SummaryTableRow } from '@/components/tables/SummaryTable';
import { useGet } from '@/util/api';
import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { PERMISSION_NAMES } from '@/util/constants';
import styles from './invite-page.module.css';

export function PermissionInvitePage({ token }) {
  const { data: invite, error } = useGet(`/api/user/permission-invite/by-token/${token}/`);
  const { user, reloadUser } = useAuth();
  const axios = getAxios({ authenticated: true });
  const [loading, setLoading] = useState(false);
  const [complete, setComplete] = useState(false);

  const onClick = () => {
    axios
      .post(`/api/user/permission-invite/${invite.id}/accept/`, { token: invite.token })
      .then(response => {
        setLoading(false);
        setComplete(true);
      });
  };

  const status = useMemo(() => {
    if (loading) return <CircularProgress />;
    if (complete) return <div>Permission added.</div>;

    return (
      <Button variant="outlined" color="primary" onClick={onClick}>
        Accept Permission
      </Button>
    );
  }, [loading, complete, invite]);

  if (error) {
    return <div className={styles.error}>This invite does not exist or has expired.</div>;
  }

  if (!user?.id) {
    return (
      <>
        <LoginButtons>Log in to continue</LoginButtons>
      </>
    );
  }

  return (
    <div className="space-y-4">
      <Title title="Accept Permission" />

      <SummaryTable>
        <SummaryTableRow icon={mdiAccount} label="Your Name">
          <b>{user.name}</b>
        </SummaryTableRow>

        <SummaryTableRow icon={mdiCity} label="Scene">
          <b>{invite?.scene?.displayName}</b>
        </SummaryTableRow>

        <SummaryTableRow icon={mdiShieldKey} label="Permission">
          {invite && PERMISSION_NAMES[invite.permission]}
        </SummaryTableRow>

        <SummaryTableRow>{status}</SummaryTableRow>
      </SummaryTable>
    </div>
  );
}
