import React from 'react';
import clsx from 'clsx';
import { FullScreen } from 'react-full-screen';

import { CABINET_POSITIONS } from '@/util/constants';
import { useClipboard } from './context';
import { Sprite } from './sprite';
import { Snail } from './snail';
import styles from './clipboard.module.css';

export function ClipboardMap() {
  const { map, fullscreen } = useClipboard();

  const onDragOver = evt => {
    evt.preventDefault();
  };

  return (
    <FullScreen handle={fullscreen}>
      <div
        className={clsx(styles.container, { fullscreen: fullscreen.active })}
        onDragOver={onDragOver}
      >
        <img className={styles.background} src={map.image} alt={`${map.displayName} Map`} />

        {Object.values(CABINET_POSITIONS).map(pos => (
          <Sprite pos={pos} key={pos.ID} />
        ))}

        <Snail map={map} />
      </div>
    </FullScreen>
  );
}
