import React from 'react';
import { useClipboard } from './context';
import { IconButton } from '@material-ui/core';
import { Icon } from '@mdi/react';
import { mdiFullscreen } from '@mdi/js';

import styles from './clipboard.module.css';

export function ClipboardButtons() {
  const { fullscreen } = useClipboard();

  return (
    <div className={styles.buttons}>
      <IconButton aria-label="fullscreen" onClick={fullscreen.enter}>
        <Icon path={mdiFullscreen} size={1} />
      </IconButton>
    </div>
  );
}
