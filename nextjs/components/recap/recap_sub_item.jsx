import { Typography, Grid } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiHexagonMultipleOutline } from '@mdi/js';

import styles from './Recap.module.css';

export default function RecapSubItem({ children }) {
  return (
    <Grid item xs={12} className={styles.gridItem}>
      <div className={styles.subItem}>
        <Icon path={mdiHexagonMultipleOutline} size={1} className={styles.subItemIcon} />
        {children}
      </div>
    </Grid>
  );
}
