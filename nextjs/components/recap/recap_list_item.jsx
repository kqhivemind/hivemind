import { Typography, ListItem } from '@material-ui/core';

import styles from './Recap.module.css';

export default function RecapListItem({ name, value, backgroundColor, ...params }) {
  return (
    <ListItem className={styles.listItem} {...params}>
      <Typography className={styles.listItemName}>{name}</Typography>
      <Typography className={styles.listItemValue}>{value.toLocaleString()}</Typography>
    </ListItem>
  );
}
