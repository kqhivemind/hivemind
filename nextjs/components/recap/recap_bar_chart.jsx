import { Grid, Typography } from '@material-ui/core';
import { BarChart, CartesianGrid, XAxis, YAxis, Bar, Tooltip, ResponsiveContainer } from 'recharts';
import clsx from 'clsx';

import SceneColorBox from 'components/SceneColorBox';

import styles from './Recap.module.css';

export default function RecapBarChart({
  title,
  data,
  labelKey,
  stacks,
  stackLabelKey,
  stackColorKey,
}) {
  const stacksById = {};
  for (const stack of stacks) {
    stacksById[stack.id] = stack;
  }

  const formatTooltip = (value, name, props) => {
    return [
      <>
        <SceneColorBox color={props.fill} />
        <span className={styles.tooltipLabel}>{stacksById[name][stackLabelKey]}:</span>
        <span className={styles.tooltipValue}>{value.toLocaleString()}</span>
      </>,
    ];
  };

  return (
    <Grid
      item
      xs={12}
      className={clsx(styles.gridItem, styles.chartContainer, styles.barChartContainer)}
    >
      {title && <Typography className={styles.listTitle}>{title}</Typography>}

      <ResponsiveContainer width="100%" height="100%">
        <BarChart data={data}>
          <CartesianGrid />
          <XAxis dataKey={labelKey} />
          <YAxis />
          <Tooltip formatter={formatTooltip} />

          {stacks
            .sort((a, b) => a[stackLabelKey].localeCompare(b[stackLabelKey]))
            .map(stack => (
              <Bar key={stack.id} dataKey={stack.id} stackId="a" fill={stack[stackColorKey]} />
            ))}
        </BarChart>
      </ResponsiveContainer>
    </Grid>
  );
}
