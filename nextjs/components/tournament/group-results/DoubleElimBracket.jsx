import { Grid, Typography } from '@material-ui/core';

import { getAxios } from 'util/axios';
import { Bracket, BracketMatch, BracketRound } from 'components/tournament/bracket';
import { MATCH_TYPE } from 'util/constants';
import styles from './Bracket.module.css';
import { matchOrder } from '../util/matchOrder';
import { useGroupResults } from './context';

export function DoubleElimBracket({ bracket, tournament }) {
  const axios = getAxios();
  const { matches } = useGroupResults();

  const bracketMatches = bracket.matches.map(m => matches[m.id] ?? m);

  const teamNames = {};
  for (const team of tournament.teams) {
    teamNames[team.id] = team.name;
  }

  const placements = {};
  for (const match of bracketMatches) {
    if (match.winnerToMatch) {
      placements[match.winnerToMatch] ??= {};
      placements[match.winnerToMatch][match.winnerToTeam] = `Winner of #${match.matchNum}`;
    }

    if (match.loserToMatch) {
      placements[match.loserToMatch] ??= {};
      placements[match.loserToMatch][match.loserToTeam] = `Loser of #${match.matchNum}`;
    }
  }

  const winnersMatches = bracketMatches.filter(m => m.matchType === MATCH_TYPE.WINNERS);
  const winnersRounds = [];
  for (const match of winnersMatches.sort((a, b) => a.id - b.id)) {
    if (!winnersRounds[match.roundNum - 1]) {
      winnersRounds[match.roundNum - 1] = {
        num: match.roundNum,
        name: match.roundName,
        matches: [],
      };
    }

    winnersRounds[match.roundNum - 1].matches.push(match);
  }

  if (winnersRounds.length === 0) return;

  winnersRounds.reverse();

  const firstRound = winnersRounds[0];
  const firstRoundSize = 2 ** (Math.ceil(Math.log2(winnersMatches.length)) - 1);
  const numByes = firstRoundSize - firstRound.matches.length;

  if (numByes > 0) {
    const firstRoundOrder = matchOrder([...Array(firstRoundSize).keys()]);
    firstRoundOrder.map((matchNum, idx) => {
      if (matchNum < numByes) {
        firstRound.matches.splice(idx, 0, {
          id: `bye${matchNum}`,
          isBye: true,
        });
      }
    });
  }

  const losersMatches = bracketMatches.filter(m => m.matchType === MATCH_TYPE.LOSERS);
  const losersRounds = [];
  for (const match of losersMatches.sort((a, b) => a.id - b.id)) {
    if (!losersRounds[match.roundNum - 1]) {
      losersRounds[match.roundNum - 1] = {
        num: match.roundNum,
        name: match.roundName,
        matches: [],
      };
    }

    losersRounds[match.roundNum - 1].matches.push(match);
  }

  losersRounds.reverse();

  const grandFinalsRounds = [
    {
      num: 1,
      name: 'First Round',
      matches: bracketMatches.filter(m => m.matchType === MATCH_TYPE.FIRST_FINAL),
    },
    {
      num: 2,
      name: 'Second Round (if necessary)',
      matches: bracketMatches.filter(m => m.matchType === MATCH_TYPE.SECOND_FINAL),
    },
  ];

  return (
    <Grid container className={styles.container} direction="column">
      <Grid item className={styles.titleContainer}>
        <Typography className={styles.title} variant="h3">
          {bracket.name}
        </Typography>
      </Grid>

      <Grid item className={styles.bracket}>
        <Bracket name="Grand Finals">
          {grandFinalsRounds.map(roundEntry => (
            <BracketRound key={roundEntry.num} name={roundEntry.name}>
              {roundEntry.matches.map(match => (
                <BracketMatch
                  key={match.id}
                  href={`/tournament-match/${match.id}`}
                  matchNum={match.matchNum}
                  blueTeam={teamNames[match.blueTeam] ?? placements[match.id]?.blue ?? ''}
                  blueScore={match.blueScore ?? 0}
                  blueIsTeam={Boolean(match.blueTeam)}
                  goldTeam={teamNames[match.goldTeam] ?? placements[match.id]?.gold ?? ''}
                  goldScore={match.goldScore ?? 0}
                  goldIsTeam={Boolean(match.goldTeam)}
                  isComplete={match.isComplete}
                  isActive={Boolean(match.activeCabinet)}
                  isBye={match.isBye}
                />
              ))}
            </BracketRound>
          ))}
        </Bracket>

        <Bracket name="Championship Bracket">
          {winnersRounds.map(roundEntry => (
            <BracketRound key={roundEntry.num} name={roundEntry.name}>
              {roundEntry.matches.map(match => (
                <BracketMatch
                  key={match.id}
                  href={`/tournament-match/${match.id}`}
                  matchNum={match.matchNum}
                  blueTeam={teamNames[match.blueTeam] ?? placements[match.id]?.blue ?? ''}
                  blueScore={match.blueScore ?? 0}
                  blueIsTeam={Boolean(match.blueTeam)}
                  goldTeam={teamNames[match.goldTeam] ?? placements[match.id]?.gold ?? ''}
                  goldScore={match.goldScore ?? 0}
                  goldIsTeam={Boolean(match.goldTeam)}
                  isComplete={match.isComplete}
                  isActive={Boolean(match.activeCabinet)}
                  isBye={match.isBye}
                />
              ))}
            </BracketRound>
          ))}
        </Bracket>

        <Bracket name="Elimination Bracket">
          {losersRounds.map(roundEntry => (
            <BracketRound key={roundEntry.num} name={roundEntry.name}>
              {roundEntry.matches.map(match => (
                <BracketMatch
                  key={match.id}
                  href={`/tournament-match/${match.id}`}
                  matchNum={match.matchNum}
                  blueTeam={teamNames[match.blueTeam] ?? placements[match.id]?.blue ?? ''}
                  blueScore={match.blueScore ?? 0}
                  blueIsTeam={Boolean(match.blueTeam)}
                  goldTeam={teamNames[match.goldTeam] ?? placements[match.id]?.gold ?? ''}
                  goldScore={match.goldScore ?? 0}
                  goldIsTeam={Boolean(match.goldTeam)}
                  isComplete={match.isComplete}
                  isActive={Boolean(match.activeCabinet)}
                  isBye={match.isBye}
                />
              ))}
            </BracketRound>
          ))}
        </Bracket>
      </Grid>
    </Grid>
  );
}
