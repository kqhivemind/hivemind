import React, { createContext, useContext, useEffect, useState, useMemo } from 'react';

import { getAxios } from '@/util/axios';
import { useWebSocket } from '@/util/websocket';

export const GroupResultsContext = createContext({});

export function GroupResultsProvider({ tournament, children }) {
  const [matches, setMatches] = useState({});
  const websocket = useWebSocket(`/ws/tournament-relay/${tournament.id}`);
  const axios = getAxios();

  websocket.onJsonMessage(message => {
    if (message?.type === 'match' && message?.data?.id) {
      setMatches(val => ({ ...val, [message.data.id]: message.data }));
    }
  });

  const ctx = {
    tournament,
    matches,
  };

  return <GroupResultsContext.Provider value={ctx}>{children}</GroupResultsContext.Provider>;
}

export function useGroupResults() {
  return useContext(GroupResultsContext);
}
