// Order of teams/matches in a single-elimination bracket
export function matchOrder(matches) {
  if (matches.length <= 1) return matches;

  const left = [];
  const right = [];

  matches.forEach((match, i) => {
    if (Math.floor((i + 1) / 2) % 2 === 0) {
      left.push(match);
    } else {
      right.push(match);
    }
  });

  return [...matchOrder(left), ...matchOrder(right)];
}
