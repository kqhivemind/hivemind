import { Link, Grid, Typography } from '@material-ui/core';
import clsx from 'clsx';

import styles from './BracketMatch.module.css';

export function BracketMatch({
  href,
  matchNum,
  blueTeam,
  blueScore,
  blueIsTeam,
  goldTeam,
  goldScore,
  goldIsTeam,
  isComplete,
  isBye,
  isActive,
  className,
}) {
  return (
    <Link
      href={href}
      className={clsx(styles.link, { [styles.isBye]: isBye, [styles.isActive]: isActive })}
    >
      <Grid container direction="row" className={clsx(styles.container, className)}>
        <Grid item className={styles.matchNum}>
          {matchNum}
        </Grid>

        <Grid item container direction="column" className={styles.teams}>
          <Grid
            item
            container
            direction="row"
            className={clsx(styles.row, styles.blueRow, {
              winner: isComplete && blueScore > goldScore,
              isTeam: blueIsTeam,
            })}
          >
            <Grid item className={clsx(styles.teamName, styles.blueTeamName)}>
              <Typography className={styles.text}>{blueTeam}</Typography>
            </Grid>
            <Grid item className={clsx(styles.score, styles.blueScore)}>
              <Typography className={styles.text}>{blueScore}</Typography>
            </Grid>
          </Grid>

          <Grid
            item
            container
            direction="row"
            className={clsx(styles.row, styles.goldRow, {
              winner: isComplete && goldScore > blueScore,
              isTeam: goldIsTeam,
            })}
          >
            <Grid item className={clsx(styles.teamName, styles.goldTeamName)}>
              <Typography className={styles.text}>{goldTeam}</Typography>
            </Grid>
            <Grid item className={clsx(styles.score, styles.goldScore)}>
              <Typography className={styles.text}>{goldScore}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Link>
  );
}
