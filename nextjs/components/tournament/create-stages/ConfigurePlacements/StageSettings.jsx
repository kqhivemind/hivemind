import { makeStyles } from '@material-ui/core/styles';
import { Grid, Switch, TextField } from '@material-ui/core';

import { STAGE_TYPE } from 'util/constants';
import styles from './StageSettings.module.css';
import { useCreateStages } from '../Context';
import { List, ListItem } from '../Components';
import { CurrentStageContext, useCurrentStage } from './Context';

export function StageSettingsInput({ title, helperText, valueKey, renderField, getValue }) {
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();

  const onChange = evt => {
    setStages(val => {
      for (const row of val) {
        if (row.key === stage.key) {
          row[valueKey] = getValue(evt.target);
        }
      }

      return [...val];
    });
  };

  return (
    <Grid item container spacing={2} direction="row" className={styles.inputRow}>
      <Grid item container direction="column" xs={10} className={styles.label}>
        <Grid item className={styles.title}>
          {title}
        </Grid>
        <Grid item className={styles.helperText}>
          {helperText}
        </Grid>
      </Grid>
      <Grid item xs={2} className={styles.inputCell}>
        {renderField({ value: stage[valueKey], onChange, fullWidth: true })}
      </Grid>
    </Grid>
  );
}

export function StageSettingsNumberField({ title, helperText, valueKey }) {
  return (
    <StageSettingsInput
      title={title}
      helperText={helperText}
      valueKey={valueKey}
      renderField={props => (
        <TextField fullWidth type="number" className={styles.numericInput} {...props} />
      )}
      getValue={target => target.valueAsNumber}
    />
  );
}

export function StageSettingsSwitch({ title, helperText, valueKey }) {
  const stage = useCurrentStage();
  return (
    <StageSettingsInput
      title={title}
      helperText={helperText}
      valueKey={valueKey}
      renderField={props => <Switch checked={Boolean(props.value)} {...props} />}
      getValue={target => Boolean(!stage[valueKey])}
    />
  );
}

function RoundsPerMatchInput() {
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();

  return <></>;
}

export function StageSettings() {
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();

  return (
    <List droppable={false} name="Stage Settings">
      <StageSettingsNumberField
        title="Rounds per Match"
        helperText="The match will end when this number of rounds have been played. Set to 0 to disable."
        valueKey="roundsPerMatch"
      />

      <StageSettingsNumberField
        title="Wins per Match"
        helperText="The match will end when one team has won this many rounds. Set to 0 to disable."
        valueKey="winsPerMatch"
      />

      {stage.stageType === STAGE_TYPE.ROUND_ROBIN && (
        <>
          <StageSettingsNumberField
            title="Matches per Opponent"
            helperText="Number of times each team will play each other team."
            valueKey="matchesPerOpponent"
          />

          <StageSettingsSwitch
            title="Add Tiebreaker"
            helperText="If the match is tied after reaching the maximum number of games to play, add another game."
            valueKey="addTiebreaker"
          />
        </>
      )}

      <StageSettingsSwitch
        title="Auto-Warmup"
        helperText="Automatically enable a warm-up game for a queued match if either team has not played yet in this stage."
        valueKey="autoWarmup"
      />
    </List>
  );
}
