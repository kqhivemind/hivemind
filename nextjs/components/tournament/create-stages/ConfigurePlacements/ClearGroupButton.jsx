import Icon from '@mdi/react';
import { mdiDeleteSweep } from '@mdi/js';

import styles from './ConfigurePlacements.module.css';
import { CurrentStageContext, useCurrentStage } from './Context';
import { useCreateStages } from '../Context';

export function ClearGroupButton({ group }) {
  const { setStages } = useCreateStages();
  const stage = useCurrentStage();

  const clearGroup = () => {
    setStages(val => {
      const currentStage = val.filter(i => i.key === stage.key)[0];
      const currentGroup = currentStage.groups[group.idx];

      while (currentGroup.placements.length > 0) {
        removeFromGroup({ stage: currentStage, groupIdx: group.idx, placeIdx: 0 });
      }

      return [...val];
    });
  };

  return (
    <Icon
      title="Remove All from Group"
      className={styles.menuIcon}
      onClick={clearGroup}
      path={mdiDeleteSweep}
      size={1}
    />
  );
}
