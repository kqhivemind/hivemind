import { useContext, createContext, useState, useRef, useEffect } from 'react';
import { Button, Grid, Typography, Menu, MenuItem, Switch, TextField } from '@material-ui/core';
import { bindMenu, bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';
import Icon from '@mdi/react';
import {
  mdiArrowLeft,
  mdiArrowRight,
  mdiMenuUp,
  mdiMenuDown,
  mdiDeleteSweep,
  mdiPencil,
} from '@mdi/js';
import MenuIcon from '@material-ui/icons/Menu';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import clsx from 'clsx';
import ordinal from 'ordinal';

import { STAGE_TYPE } from 'util/constants';
import { useTournamentAdmin } from '@/components/tournament-admin';
import { useCreateStages } from '../Context';
import { Step, PlaceType } from '../Constants';
import { List, ListItem } from '../Components';
import styles from './ConfigurePlacements.module.css';
import { CurrentStageContext, useCurrentStage } from './Context';
import { StageSettings } from './StageSettings';
import { CustomScheduleGroupTable } from './CustomSchedule';
import { GroupTableTitle } from './GroupTableTitle';
import { ClearGroupButton } from './ClearGroupButton';
import { EditGroupNameButton } from './EditGroupNameButton';

function addTeamToGroup({ stage, tournament, teamId, groupIdx, placeIdx }) {
  const group = stage.groups[groupIdx];
  const placement = {
    placeType: PlaceType.Team,
    placeId: parseInt(teamId),
    name: tournament.teams.filter(t => t.id === parseInt(teamId)).shift().name,
    key: `${PlaceType.Team}-${teamId}`,
  };

  group.placements.splice(placeIdx ?? group.placements.length, 0, placement);
  stage.teamsIncluded.add(parseInt(teamId));
}

function addPrevStagePlaceToGroup({ stage, stages, placeId, groupIdx, placeIdx }) {
  const group = stage.groups[groupIdx];

  const [prevStageId, prevGroupId, prevPlaceId] = placeId.split('.');
  const prevStage = stages.filter(i => i.key === parseInt(prevStageId))[0];
  const prevGroup = prevStage.groups[parseInt(prevGroupId)];
  const prevPlace = `${ordinal(parseInt(prevPlaceId))} Place`;
  const name = `${prevGroup.name} - ${prevPlace}`;

  const placement = {
    placeType: PlaceType.PrevStage,
    placeId,
    name,
    key: `${PlaceType.PrevStage}-${placeId}`,
  };

  group.placements.splice(placeIdx ?? group.placements.length, 0, placement);
  prevStage.placesIncluded.add(placeId);
}

function removeFromGroup({ stage, stages, groupIdx, placeIdx }) {
  const group = stage.groups[groupIdx];
  const place = group.placements[placeIdx];

  if (place.placeType === PlaceType.Team) {
    stage.teamsIncluded.delete(parseInt(place.placeId));
  }

  if (place.placeType === PlaceType.PrevStage) {
    const [prevStageId, prevGroupId, prevPlaceId] = place.placeId.split('.');
    const prevStage = stages.filter(i => i.key === parseInt(prevStageId))[0];
    prevStage.placesIncluded.delete(place.placeId);
  }

  group.placements.splice(placeIdx, 1);
}

function GroupNameInput({ group }) {
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();

  const onChange = evt => {
    setStages(val => {
      for (const row of val) {
        if (row.key === stage.key) {
        }
      }

      return [...val];
    });
  };

  return <Typography variant="h3">{group.name}</Typography>;
}

function TeamTableMenu() {
  const menuState = usePopupState({ popupId: 'team-table-menu' });
  const { tournament } = useTournamentAdmin();
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();

  const teamIsAssigned = id => stage.teamsIncluded.has(id);

  const assignAll = groupIdx => {
    setStages(val => {
      const currentStage = val.filter(i => i.key === stage.key)[0];
      for (const team of tournament.teams.filter(t => !teamIsAssigned(t.id))) {
        addTeamToGroup({
          stage: currentStage,
          tournament,
          teamId: team.id,
          groupIdx,
        });
      }

      menuState.close();
      return [...val];
    });
  };

  const assignRandom = () => {
    setStages(val => {
      const currentStage = val.filter(i => i.key === stage.key)[0];
      const shuffledTeamIds = tournament.teams
        .filter(t => !teamIsAssigned(t.id))
        .map(t => ({ id: t.id, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(t => t.id);

      shuffledTeamIds.map((teamId, idx) => {
        addTeamToGroup({
          stage: currentStage,
          tournament,
          teamId,
          groupIdx: idx % currentStage.groups.length,
        });
      });

      menuState.close();
      return [...val];
    });
  };

  return (
    <>
      <MenuIcon className={styles.menuIcon} {...bindTrigger(menuState)} />
      <Menu
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        transformOrigin={{ vertical: 'top', horizontal: 'center' }}
        className={styles.menu}
        {...bindMenu(menuState)}
      >
        {stage.groups.map((group, idx) => (
          <MenuItem key={group.idx} className={styles.menuItem} onClick={() => assignAll(idx)}>
            Assign All to {group.name}
          </MenuItem>
        ))}
        {stage.groups.length > 1 && (
          <MenuItem className={styles.menuItem} onClick={assignRandom}>
            Assign Randomly
          </MenuItem>
        )}
      </Menu>
    </>
  );
}

function TeamTable() {
  const { tournament } = useTournamentAdmin();
  const { stages, getAssignableTeams } = useCreateStages();
  const stage = useCurrentStage();
  const teams = getAssignableTeams();

  if (teams.length === 0) return null;

  return (
    <List id="teams" name="Teams" icons={<TeamTableMenu />}>
      {teams.map((team, idx) => (
        <ListItem name={team.name} key={team.id} id={`${PlaceType.Team}-${team.id}`} index={idx} />
      ))}
    </List>
  );
}

function PreviousStageGroupTableMenu({ prevStage, group }) {
  const { stages, setStages, getAssignablePlaces } = useCreateStages();
  const stage = useCurrentStage();
  const menuState = usePopupState({ popupId: `prev-stage-menu-${prevStage.key}` });

  const placements = getAssignablePlaces(prevStage, group);

  const assignAll = groupIdx => {
    setStages(val => {
      const currentStage = val.filter(i => i.key === stage.key)[0];
      for (const place of placements) {
        addPrevStagePlaceToGroup({
          stage: currentStage,
          stages,
          placeId: place.id,
          groupIdx,
        });
      }

      menuState.close();
      return [...val];
    });
  };

  return (
    <>
      <MenuIcon className={styles.menuIcon} {...bindTrigger(menuState)} />
      <Menu
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        transformOrigin={{ vertical: 'top', horizontal: 'center' }}
        className={styles.menu}
        {...bindMenu(menuState)}
      >
        {stage.groups.map((group, idx) => (
          <MenuItem key={group.idx} className={styles.menuItem} onClick={() => assignAll(idx)}>
            Assign All to {group.name}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
}

function PreviousStageGroupTable({ prevStage, group }) {
  const { getAssignablePlaces } = useCreateStages();

  const placements = getAssignablePlaces(prevStage, group);
  if (placements.length === 0) return null;

  return (
    <List
      id={`${PlaceType.PrevStage}-${prevStage.key}-${group.idx}`}
      name={group.name}
      icons={<PreviousStageGroupTableMenu prevStage={prevStage} group={group} />}
    >
      {placements.map((place, idx) => (
        <ListItem
          name={place.name}
          index={idx}
          key={place.id}
          id={`${PlaceType.PrevStage}-${place.id}`}
        />
      ))}
    </List>
  );
}

function PreviousStageTables({ prevStage }) {
  return (
    <>
      {prevStage.groups.map(group => (
        <PreviousStageGroupTable key={group.idx} prevStage={prevStage} group={group} />
      ))}
    </>
  );
}

function PreviousStages() {
  const { stages, placementStageIdx } = useCreateStages();
  const stage = useCurrentStage();

  return stages
    .slice(0, placementStageIdx)
    .map(stage => <PreviousStageTables key={stage.key} prevStage={stage} />);
}

function GroupTable({ group }) {
  const stage = useCurrentStage();

  if (stage.stageType === STAGE_TYPE.CUSTOM) {
    return <CustomScheduleGroupTable group={group} />;
  }

  return (
    <List
      id={`group-${group.idx}`}
      name={<GroupTableTitle group={group} />}
      icons={<ClearGroupButton group={group} />}
    >
      {group.placements.map((place, idx) => (
        <ListItem name={place.name} key={place.key} id={place.key} place={place} index={idx} />
      ))}
    </List>
  );
}

function ConfigureStagePlacements() {
  const { tournament } = useTournamentAdmin();
  const { setStages } = useCreateStages();
  const stage = useCurrentStage();
  const numGroups = stage.stageType === STAGE_TYPE.ROUND_ROBIN ? stage.numGroups : 1;

  const onDragEnd = result => {
    const [placeType, placeId] = result.draggableId.split('-');
    const [srcType, srcId] = result.source.droppableId.split('-');

    setStages(val => {
      const currentStage = val.filter(i => i.key === stage.key)[0];

      if (srcType === 'group') {
        removeFromGroup({
          stage: currentStage,
          stages: val,
          groupIdx: srcId,
          placeIdx: result.source.index,
        });
      }

      if (!result.destination) return [...val];
      const [destType, destId] = result.destination.droppableId.split('-');

      if (destType === 'group') {
        if (placeType === PlaceType.Team) {
          addTeamToGroup({
            stage: currentStage,
            tournament,
            teamId: placeId,
            groupIdx: parseInt(destId),
            placeIdx: result.destination.index,
          });
        }

        if (placeType === PlaceType.PrevStage) {
          addPrevStagePlaceToGroup({
            stage: currentStage,
            stages: val,
            placeId,
            groupIdx: parseInt(destId),
            placeIdx: result.destination.index,
          });
        }
      }

      if (destType === 'newMatch') {
        const [groupIdx, team] = destId.split('.');
        const group = currentStage.groups[parseInt(groupIdx)];

        const match = { key: group.matches.length + 1 };

        const name = tournament.teams.filter(t => t.id === parseInt(placeId)).shift().name;
        if (team === 'blue') match.blueTeam = name;
        if (team === 'gold') match.goldTeam = name;

        group.matches.push(match);
      }

      return [...val];
    });
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Grid container spacing={2}>
        <Grid item container direction="column" xs={12} md={6}>
          <TeamTable />
          <PreviousStages />
        </Grid>

        <Grid item container direction="column" xs={12} md={6}>
          <StageSettings />
          {stage.groups.map(group => (
            <GroupTable group={group} key={group.idx} />
          ))}
        </Grid>
      </Grid>
    </DragDropContext>
  );
}

function ToggleAssignedButton() {
  const { placementStageIdx, showAssigned, setShowAssigned } = useCreateStages();

  if (placementStageIdx === 0) return null;

  return (
    <Button variant="outlined" color="primary" onClick={() => setShowAssigned(v => !v)}>
      {showAssigned ? 'Hide Assigned' : 'Show Assigned'}
    </Button>
  );
}

function PrevStepButton() {
  const { setStep, placementStageIdx, setPlacementStageIdx } = useCreateStages();

  const prevStep = () => {
    if (placementStageIdx > 0) {
      setPlacementStageIdx(placementStageIdx - 1);
    } else {
      setStep(Step.CreateStages);
    }
  };

  return (
    <Button
      variant="outlined"
      color="primary"
      startIcon={<Icon path={mdiArrowLeft} size={1} />}
      onClick={prevStep}
    >
      Back
    </Button>
  );
}

function NextStepButton() {
  const { stages, setStep, placementStageIdx, setPlacementStageIdx, setStandings } =
    useCreateStages();

  const nextStep = () => {
    if (placementStageIdx < stages.length - 1) {
      setPlacementStageIdx(placementStageIdx + 1);
    } else {
      const standings = [];
      const stagesReversed = [...stages].reverse();
      for (const stage of stagesReversed) {
        for (const group of stage.groups) {
          for (const idx of [...group.placements.keys()]) {
            const placeId = `${stage.key}.${group.idx}.${idx + 1}`;
            if (stages.filter(s => s.placesIncluded.has(placeId)).length === 0) {
              stage.placesIncluded.add(placeId);
              standings.push({ id: placeId, name: `${group.name} - ${ordinal(idx + 1)} Place` });
            }
          }
        }
      }

      setStandings(standings);
      setStep(Step.SetFinalStandings);
    }
  };

  return (
    <Button
      variant="outlined"
      color="primary"
      endIcon={<Icon path={mdiArrowRight} size={1} />}
      onClick={nextStep}
    >
      Next
    </Button>
  );
}

export function ConfigurePlacements({}) {
  const { stages, placementStageIdx } = useCreateStages();
  const stage = stages[placementStageIdx];

  return (
    <CurrentStageContext.Provider value={stage}>
      <Typography variant="h2">Configure Placements: {stage.stageName}</Typography>
      <ConfigureStagePlacements />

      <Grid container direction="row" justifyContent="space-between">
        <Grid item spacing={1}>
          <PrevStepButton />
          <ToggleAssignedButton />
        </Grid>

        <Grid item spacing={1}>
          <NextStepButton />
        </Grid>
      </Grid>
    </CurrentStageContext.Provider>
  );
}
