import { Button, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiPlaylistPlus } from '@mdi/js';
import { mdiArrowLeft, mdiArrowRight } from '@mdi/js';
import { DragDropContext } from 'react-beautiful-dnd';
import ordinal from 'ordinal';

import styles from './SetFinalStandings.module.css';
import { useCreateStages } from './Context';
import { Step, PlaceType } from './Constants';
import { List, ListItem } from './Components';

function addToStandings({ standings, stages, placeId, placeIdx }) {
  const [prevStageId, prevGroupId, prevPlaceId] = placeId.split('.');
  const prevStage = stages.filter(i => i.key === parseInt(prevStageId))[0];
  const prevGroup = prevStage.groups[parseInt(prevGroupId)];
  const prevPlace = `${ordinal(parseInt(prevPlaceId))} Place`;
  const name = `${prevGroup.name} - ${prevPlace}`;

  const placement = {
    placeType: PlaceType.PrevStage,
    placeId,
    name,
    key: `${PlaceType.PrevStage}-${placeId}`,
  };

  standings.splice(placeIdx ?? standings.length, 0, placement);
  prevStage.placesIncluded.add(placeId);
}

function removeFromStandings({ standings, stages, placeId, placeIdx }) {
  const [prevStageId, prevGroupId, prevPlaceId] = placeId.split('.');
  const prevStage = stages.filter(i => i.key === parseInt(prevStageId))[0];

  standings.splice(placeIdx, 1);
  prevStage.placesIncluded.delete(placeId);
}

function AddAllButton({ prevStage, group }) {
  const { stages, setStandings, getAssignablePlaces } = useCreateStages();

  const placements = getAssignablePlaces(prevStage, group);

  const assignAll = () => {
    setStandings(val => {
      for (const place of placements) {
        addToStandings({
          standings: val,
          stages,
          placeId: place.id,
        });
      }

      return [...val];
    });
  };

  return (
    <Icon
      title="Add All to Final Standings"
      className={styles.menuIcon}
      onClick={assignAll}
      path={mdiPlaylistPlus}
      size={1}
    />
  );
}

function PreviousStageGroupTable({ prevStage, group }) {
  const { getAssignablePlaces } = useCreateStages();

  const placements = getAssignablePlaces(prevStage, group);
  if (placements.length === 0) return null;

  return (
    <List
      id={`${PlaceType.PrevStage}-${prevStage.key}-${group.idx}`}
      name={group.name}
      icons={<AddAllButton prevStage={prevStage} group={group} />}
    >
      {placements.map((place, idx) => (
        <ListItem
          name={place.name}
          index={idx}
          key={place.id}
          id={`${PlaceType.PrevStage}-${place.id}`}
        />
      ))}
    </List>
  );
}

function PreviousStageTables({ prevStage }) {
  return prevStage.groups.map(group => (
    <PreviousStageGroupTable key={group.idx} prevStage={prevStage} group={group} />
  ));
}

function PreviousStages() {
  const { stages } = useCreateStages();

  return stages.map(stage => <PreviousStageTables key={stage.key} prevStage={stage} />);
}

function ClearStandingsButton() {}

function StandingsTable() {
  const { standings } = useCreateStages();

  return (
    <List id="standings" name="Final Standings" icons={<ClearStandingsButton />}>
      {standings.map((place, idx) => (
        <ListItem
          name={place.name}
          id={`${PlaceType.PrevStage}-${place.id}`}
          key={place.id}
          place={place}
          index={idx}
        />
      ))}
    </List>
  );
}

function SetFinalStandingsTables() {
  const { stages, setStandings } = useCreateStages();

  const onDragEnd = result => {
    setStandings(val => {
      const [placeType, placeId] = result.draggableId.split('-');
      const [srcType, srcId] = result.source.droppableId.split('-');

      if (srcType === 'standings') {
        removeFromStandings({
          standings: val,
          stages,
          placeId,
          placeIdx: result.source.index,
        });
      }

      if (!result.destination) return [...val];
      const [destType, destId] = result.destination.droppableId.split('-');

      if (destType === 'standings') {
        addToStandings({
          standings: val,
          stages,
          placeId,
          placeIdx: result.destination.index,
        });
      }

      return [...val];
    });
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Grid container spacing={2}>
        <Grid item container direction="column" xs={12} md={6}>
          <PreviousStages />
        </Grid>

        <Grid item container direction="column" xs={12} md={6}>
          <StandingsTable />
        </Grid>
      </Grid>
    </DragDropContext>
  );
}

function PrevStepButton() {
  const { setStep, stages, setPlacementStageIdx } = useCreateStages();

  const prevStep = () => {
    setPlacementStageIdx(stages.length - 1);
    setStep(Step.ConfigurePlacements);
  };
  return (
    <Button
      variant="outlined"
      color="primary"
      startIcon={<Icon path={mdiArrowLeft} size={1} />}
      onClick={prevStep}
    >
      Back
    </Button>
  );
}

function NextStepButton() {
  const { setStep } = useCreateStages();

  const nextStep = () => {
    setStep(Step.AwaitConfirm);
  };

  return (
    <Button
      variant="outlined"
      color="primary"
      endIcon={<Icon path={mdiArrowRight} size={1} />}
      onClick={nextStep}
    >
      Next
    </Button>
  );
}

export function SetFinalStandings({}) {
  return (
    <>
      <Typography variant="h2">Configure Final Tournament Standings</Typography>

      <SetFinalStandingsTables />

      <Grid container direction="row" justifyContent="space-between">
        <PrevStepButton />
        <NextStepButton />
      </Grid>
    </>
  );
}
