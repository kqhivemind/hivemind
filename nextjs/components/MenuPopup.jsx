import React from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Divider,
  Link,
  Menu,
  MenuItem as MuiMenuItem,
} from '@material-ui/core';
import { bindMenu } from 'material-ui-popup-state/hooks';
import Icon from '@mdi/react';
import { mdiPatreon } from '@mdi/js';

import { logOut, useUser } from 'util/auth';
import LoginButtons from 'components/LoginButtons';

const useStyles = makeStyles(theme => ({
  menu: {},
  item: {},
  link: {
    color: 'black',
    padding: theme.spacing(1, 4),
  },
  linkIcon: {
    verticalAlign: 'middle',
    marginRight: theme.spacing(1),
    color: theme.palette.text.secondary,
  },
  loginButtons: {
    padding: theme.spacing(1, 4),
  },
}));

export function MenuItem({ href, onClick, icon, children }) {
  const classes = useStyles();
  console.log(href);
  return (
    <MuiMenuItem className={classes.item}>
      <Link className={classes.link} href={href} onClick={onClick}>
        {icon && <Icon className={classes.linkIcon} path={icon} size={1} />}

        {children}
      </Link>
    </MuiMenuItem>
  );
}

export default function MenuPopup({ menuState }) {
  const classes = useStyles();
  const router = useRouter();
  const user = useUser();

  const onLogoutClick = evt => {
    evt.preventDefault();
    logOut();
    router.reload();
  };

  return (
    <Menu
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      transformOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={classes.menu}
      {...bindMenu(menuState)}
    >
      {!user?.id && (
        <MuiMenuItem className={classes.item}>
          <Box className={classes.loginButtons}>
            <LoginButtons onSuccess={router.reload} />
          </Box>
        </MuiMenuItem>
      )}

      {user?.id && <MenuItem href={`/user/${user.id}`}>User Profile</MenuItem>}

      {user?.permissions?.some(p => p.permission === 'admin') && (
        <MenuItem href="/devices">Manage Client Devices</MenuItem>
      )}

      <Divider />

      {user?.id && (
        <MenuItem href="#" onClick={onLogoutClick}>
          Log Out
        </MenuItem>
      )}

    </Menu>
  );
}
