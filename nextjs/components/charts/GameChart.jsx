import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  container: {
    height: '200px',
  },
  title: {
    width: '30px',
    writingMode: 'vertical-rl',
    transform: 'rotate(180deg)',
    textAlign: 'center',
  },
  chart: {
    background: 'white',
    flexGrow: 1,
    flexBasis: 0,
    borderRadius: '5px'
  },
}));

export default function GameChart({ title, children }) {
  const classes = useStyles();

  return (
    <Grid container direction="row" className={classes.container}>
      <Grid item className={classes.title}>
        <Typography>{title}</Typography>
      </Grid>

      <Grid item className={classes.chart}>
        {children}
      </Grid>
    </Grid>
  );
}

export function formatTime(totalSeconds) {
  var minutes = Math.floor(totalSeconds / 60);
  var seconds = String(Math.floor(totalSeconds % 60)).padStart(2, "0");
  return `${minutes}:${seconds}`;
}

export function getBaseOptions(game) {
  return {
    plugins: {
      title: {display: false},
      legend: {display: false},
      tooltip: {
        callbacks: {
          title: () => '',
          label: item => item.raw.text,
        },
      },
    },
    animation: {duration: 0},
    maintainAspectRatio: false,
    scales: {
      x: {
        display: true,
        type: 'linear',
        min: 0,
        max: game.lengthSec,
        scaleLabel: {
          display: false,
          labelString: 'Seconds',
        },
        ticks: {
          callback: formatTime,
        },
      },
    },
  };
}

