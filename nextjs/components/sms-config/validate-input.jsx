import React, { useState, useRef, useEffect } from 'react';
import { Button, IconButton, TextField } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiDelete, mdiEmailSync } from '@mdi/js';

import { useSMSConfig } from './context';
import styles from './sms-config.module.css';

export function ValidateInput() {
  const [token, setToken] = useState('');
  const { config, submitValidate, resend, remove } = useSMSConfig();
  const inputRef = useRef(null);

  useEffect(() => {
    if (inputRef?.current) {
      inputRef.current.focus();
    }
  }, [inputRef?.current]);

  const onKeyDown = evt => {
    if (evt?.key === 'Enter') {
      submitValidate(token);
    }
  };

  return (
    <div className={styles.container}>
      <div>
        <span>
          Enter the 6-digit number that was sent to <b>{config.phoneNumber}</b>.
        </span>
        <div className={styles.iconButtons}>
          <IconButton onClick={resend}>
            <Icon path={mdiEmailSync} size={1} title="Send again" />
          </IconButton>

          <IconButton onClick={remove}>
            <Icon path={mdiDelete} size={1} title="Remove" />
          </IconButton>
        </div>
      </div>

      <div>
        <TextField
          value={token}
          onChange={e => setToken(e.target.value)}
          onKeyDown={onKeyDown}
          autoFocus={true}
        />
        <Button variant="outlined" color="primary" onClick={() => submitValidate(token)}>
          Validate
        </Button>
      </div>
    </div>
  );
}
