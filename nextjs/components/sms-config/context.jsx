import React, { createContext, useContext, useEffect, useState, useMemo } from 'react';
import { toast } from 'react-toastify';

import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';

export const SMSConfigContext = createContext({});

export function SMSConfigProvider({ children }) {
  const axios = getAxios({ authenticated: true });
  const { user } = useAuth();

  const [config, setConfig] = useState(null);

  const loadConfig = () => {
    axios
      .get('/api/notifications/notification-config/', {
        params: { userId: user.id },
      })
      .then(response => {
        if (response.data.results) {
          setConfig(response.data.results[0]);
        }
      });
  };

  useEffect(() => {
    if (user?.id) {
      loadConfig();
    }
  }, [user?.id]);

  const submitPhoneNumber = value => {
    axios
      .post('/api/notifications/notification-config/', {
        phoneNumber: value,
        user: user?.id,
      })
      .then(response => {
        setConfig(response.data);
      });
  };

  const submitValidate = value => {
    axios
      .put(`/api/notifications/notification-config/${config.id}/validate/`, { token: value })
      .then(response => {
        if (response.data.success) {
          toast.success('Phone number validated.');
          loadConfig();
        } else {
          toast.error(`Could not validate: ${response.data.error ?? 'an unknown error occurred'}`);
        }
      });
  };

  const resend = evt => {
    evt?.preventDefault();
    axios.put(`/api/notifications/notification-config/${config.id}/resend/`).then(response => {
      toast.success('Message sent.');
    });
  };

  const remove = evt => {
    evt?.preventDefault();
    axios.delete(`/api/notifications/notification-config/${config.id}/`).then(response => {
      setConfig(null);
    });
  };

  const update = values => {
    axios.patch(`/api/notifications/notification-config/${config.id}/`, values).then(response => {
      setConfig(response.data);
    });
  };

  const ctx = {
    config,
    setConfig,
    submitPhoneNumber,
    submitValidate,
    resend,
    remove,
    update,
  };

  return <SMSConfigContext.Provider value={ctx}>{children}</SMSConfigContext.Provider>;
}

export function useSMSConfig() {
  return useContext(SMSConfigContext);
}
