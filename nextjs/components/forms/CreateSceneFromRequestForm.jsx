import React, { useState } from 'react';
import { MenuItem, Typography, Link, CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import { format } from 'date-fns';
import { mdiAccount, mdiCalendar, mdiComment, mdiEmail } from '@mdi/js';

import Form from './Form';
import List from './List';
import Field, { Divider, Select, Switch, TextField } from '../fields/Field';
import { SummaryTable, SummaryTableRow } from 'components/tables/SummaryTable';
import { UserConsumer, isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { PERMISSION_TYPES } from 'util/constants';

const SceneSchema = Yup.object().shape({
  name: Yup.string().max(20, 'Too long'),
  displayName: Yup.string().max(30, 'Too long'),
  backgroundColor: Yup.string().matches(/^\#[0-9a-f]{6}/, 'Must be a hex color.'),
});

const CreateSceneFromRequestForm = UserConsumer(({ sceneRequestId }) => {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const [sceneRequest, setSceneRequest] = useState(null);
  const [email, setEmail] = useState(null);
  const scene = {
    name: sceneRequest?.name,
    displayName: sceneRequest?.displayName,
    backgroundColor: '',
  };

  if (sceneRequest === null) {
    axios.get(`/api/user/scene-request/${sceneRequestId}/`).then(response => {
      setSceneRequest(response.data);
    });
  }

  if (email === null) {
    if (sceneRequest !== null) {
      axios.get(`/api/user/user/${sceneRequest.user.id}/email/`).then(response => {
        setEmail(response.data.email);
      });
    }

    return (<CircularProgress />);
  }

  const props = {
    dialog: false,
    canEdit: scene => true,
    dialogTitle: `Editing Scene: ${scene.displayName}`,
    canEdit: async () => true,
    object: scene,
    validationSchema: SceneSchema,
    url: `/api/user/scene-request/${sceneRequest.id}/approve/`,
    method: 'POST',
    saveButtonText: 'Create Scene',
    successText: data => `${data.displayName} has been created!`,
    onSave: result => {
      router.push(`/scene/${result.name}`);
    },
  };

  return (
    <Form {...props}>
      <SummaryTable>
        <SummaryTableRow icon={mdiCalendar} label="Date Submitted">
          <Typography>{format(new Date(sceneRequest.submitDate), 'MMM d - hh:mm aa')}</Typography>
        </SummaryTableRow>
        <SummaryTableRow icon={mdiAccount} label="User Name">
          <Typography>{sceneRequest.userName}</Typography>
        </SummaryTableRow>
        <SummaryTableRow icon={mdiEmail} label="User Email">
          <Typography>{email}</Typography>
        </SummaryTableRow>
        <SummaryTableRow icon={mdiComment} label="Comments">
          <Typography>{sceneRequest.comments}</Typography>
        </SummaryTableRow>
      </SummaryTable>

      <Divider />

      <Field name="name" label="Short Name" component={TextField} />
      <Field name="displayName" label="Display Name" component={TextField} />
      <Field name="backgroundColor" label="Background Color" component={TextField} />
    </Form>
  );
});

export default CreateSceneFromRequestForm;
