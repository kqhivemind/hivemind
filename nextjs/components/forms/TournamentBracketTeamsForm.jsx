import {
  Button,
  Card,
  CardContent,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import GroupIcon from '@material-ui/icons/Group';
import { useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import { getAxios } from 'util/axios';
import Form from './Form';

const useStyles = makeStyles(theme => ({
  card: {
    height: '100%',
    '& .MuiCardContent-root': {
      height: '100%',
    },
  },
  list: {
    height: '100%',
  },
}));

export function TeamCard({ team, ...props }) {
  return (
    <Draggable draggableId={`${team.id}`} {...props}>
      {(provided, snapshot) => (
        <ListItem
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemIcon>
            <GroupIcon />
          </ListItemIcon>
          <ListItemText primary={team.name} />
        </ListItem>
      )}
    </Draggable>
  );
}

export default function TournamentBracketTeamsForm({ bracket, tournament, onClose, ...props }) {
  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [selectedTeams, setSelectedTeams] = useState([]);
  const availableTeams = tournament.teams.filter(team => !selectedTeams.includes(team));

  const onDragEnd = result => {
    if (!result.destination) {
      return;
    }

    const newTeamList = [...selectedTeams];

    if (result.source.droppableId === 'selected') {
      newTeamList.splice(result.source.index, 1);
    }

    const team = tournament.teams.filter(team => `${team.id}` === result.draggableId)[0];

    if (result.destination.droppableId === 'selected') {
      newTeamList.splice(result.destination.index, 0, team);
    }

    setSelectedTeams(newTeamList);
  };
  const moveAll = () => {
    return e => {
      setSelectedTeams(availableTeams);
    };
  };

  const formProps = {
    buttonText: 'Add Teams to Stage',
    dialogTitle: `Add Teams to Stage: ${bracket.name}`,
    canEdit: () => true,
    object: { teams: selectedTeams },
    method: 'PUT',
    url: `/api/tournament/bracket/${bracket.id}/set-teams/`,
    getPostData: {
      teams: values => selectedTeams.map(i => i.id),
    },
  };

  return (
    <Form className={classes.form} {...formProps}>
      <Grid container spacing={1}>
        <DragDropContext onDragEnd={onDragEnd}>
          <Grid item xs={6}>
            <Card className={classes.card}>
              <CardContent>
                <div className="flex items-center justify-between">
                  <Typography variant="h6">Available Teams</Typography>
                  <Button variant="outlined" color="secondary" onClick={moveAll()}>
                    Move All To Stage
                  </Button>
                </div>
                <Droppable droppableId="available">
                  {(provided, snapshot) => (
                    <List ref={provided.innerRef} className={classes.list}>
                      {availableTeams.map((team, idx) => (
                        <TeamCard key={team.id} team={team} index={idx} />
                      ))}
                      {provided.placeholder}
                    </List>
                  )}
                </Droppable>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={6}>
            <Card className={classes.card}>
              <CardContent>
                <div className="flex items-center justify-between">
                  <Typography variant="h6">Selected Teams</Typography>
                  <Button variant="outlined" color="" onClick={moveAll()}>
                    Remove All
                  </Button>
                </div>
                <Droppable droppableId="selected">
                  {(provided, snapshot) => (
                    <List ref={provided.innerRef} className={classes.list}>
                      {selectedTeams.map((team, idx) => (
                        <TeamCard key={team.id} team={team} index={idx} />
                      ))}
                      {provided.placeholder}
                    </List>
                  )}
                </Droppable>
              </CardContent>
            </Card>
          </Grid>
        </DragDropContext>
      </Grid>
    </Form>
  );
}
