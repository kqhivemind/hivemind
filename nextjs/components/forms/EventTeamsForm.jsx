import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress, List, ListItem, ListItemText, ListItemIcon, Card, CardContent,
         Grid, Typography, RootRef } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';

import Form from './Form';
import Field, { Autocomplete, TextField } from '../fields/Field';
import { getAxios } from 'util/axios';
import { isAdminOf } from 'util/auth';

const useStyles = makeStyles(theme => ({
}));

export function TeamCard({ team, onChangeName }) {
  const [name, setName] = useState(team.name);
  const onChange = evt => {
    setName(evt.target.value);
    onChangeName(evt.target.value);
  };

  return (
    <Grid item xs={12} md={6} lg={4}>
      <Card>
        <CardContent>
          <TextField fullWidth name="name" label="Team Name" value={name} onChange={onChange} />

          <Droppable droppableId={team.droppableId}>
            {(provided, snapshot) => (
              <List ref={provided.innerRef}>
                {team.players.map((player, idx) => (
                  <Draggable
                    key={player.id}
                    draggableId={`player-${player.id}`}
                    index={idx}
                  >

                    {(provided, snapshot) => (
                      <ListItem
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >

                        <ListItemIcon>
                          <PersonIcon />
                        </ListItemIcon>
                        <ListItemText primary={player.name} />
                      </ListItem>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </List>
            )}
          </Droppable>
        </CardContent>
      </Card>
    </Grid>
  );
}

export default function EventTeamsForm({ event }) {
  const axios = getAxios({ authenticated: true });
  const [allPlayers, setAllPlayers] = useState(null);
  const [teams, setTeams] = useState(null);

  const onDragEnd = result => {
    if (!result.destination) {
      return;
    }

    let sourceTeam, destTeam;
    for (const team of teams) {
      if (team.droppableId == result.source.droppableId) {
        sourceTeam = team;
      }

      if (team.droppableId == result.destination.droppableId) {
        destTeam = team;
      }
    }

    if (!sourceTeam || !destTeam) {
      return;
    }

    const [player] = sourceTeam.players.splice(result.source.index, 1);
    destTeam.players.splice(result.destination.index, 0, player);

    setTeams(teams);
  };

  const onSave = async () => {
    for (const team of teams) {
      let response = await axios.get(`/api/league/team/${team.id}/`);
      const data = response.data;

      data.name = team.name;
      data.players = team.players.map(player => player.id);

      response = await axios.put(`/api/league/team/${team.id}/`, data);
    }
  };

  const onChangeName = (teamId, value) => {
    for (const team of teams) {
      if (team.id == teamId) {
        team.name = value;
      }
    }

    setTeams(teams);
  };

  const props = {
    buttonText: 'Edit Teams',
    dialogTitle: 'Editing Event Teams',
    canEdit: event => isAdminOf(event.season.scene.id),
    object: event,
    onSave,
    getPostData: {
      season: values => values.season.id,
    },
  };

  if (allPlayers === null) {
    axios.getAllPages('/api/league/player/', { params: { sceneId: event.season.scene.id } }).then(setAllPlayers);
  }

  if (teams === null) {
    const teamsCopy = [...event.standings]
          .map(entry => entry.team)
          .sort((a, b) => a.name.localeCompare(b.name));

    for (const team of teamsCopy) {
      team.droppableId = `team-${team.id}`
    }

    setTeams(teamsCopy);

    return (
      <Form {...props}>
        <CircularProgress />
      </Form>
    );
  }

  return (
    <Form {...props}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Grid container spacing={1}>
          {teams.map(team => (
            <TeamCard key={team.id} team={team} onChangeName={value => { onChangeName(team.id, value) }} />
          ))}
        </Grid>
      </DragDropContext>
    </Form>
  );
}


