import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  MenuItem,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import DragIcon from '@material-ui/icons/DragIndicator';
import { mdiPencil } from '@mdi/js';
import clsx from 'clsx';
import Field, { Select, Switch, TextField } from 'components/fields/Field';
import camelCase from 'lodash/camelCase';
import startCase from 'lodash/startCase';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import * as Yup from 'yup';
import Form, { useForm } from './Form';
import List from './List';

const FIELD_TYPES = ['text', 'multi_choice', 'single_choice', 'checkbox'];
const TournamentSchema = Yup.object().shape({
  tournament: Yup.string().required('Tournament is required'),
  fieldDescription: Yup.string().required('Field description is required'),
  isRequired: Yup.bool()
    .default(false)
    .transform(v => Boolean(v)),
  fieldType: Yup.string().oneOf(FIELD_TYPES),
  choices: Yup.array(),
});
const handleDelete = async ({ id }, cb) => {
  const axios = getAxios({ authenticated: true });
  await axios.delete(`/api/tournament/player-info-field/${id}/`);
  cb();
};
const DeleteQuestion = ({ question }) => {
  const [open, setOpen] = useState(false);
  const router = useRouter();

  const handleSuccess = () => {
    router.replace(router.asPath);
    // setOpen(false);
  };
  return (
    <>
      <IconButton
        className="text-gray-400 hover:text-gray-500 bg-transparent"
        onClick={() => setOpen(true)}
      >
        <DeleteIcon />
      </IconButton>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Are you sure you want to delete this question?
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {question.fieldDescription}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="neutral" variant="outlined">
            Nevermind
          </Button>
          <Button
            onClick={() => handleDelete(question, handleSuccess)}
            color=""
            variant="contained"
            className="bg-red-500 text-white"
          >
            Confirm Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default function TournamentConfigureRegistrationForm({
  title,
  question,
  tournament,
  onSave,
  index,
}) {
  const [loading, setLoading] = useState(false);
  const [editing, setEditing] = useState(!Boolean(question.id));
  return (
    <Draggable draggableId={question.id.toString()} index={index}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          className={clsx(
            'flex flex-wrap md:flex-nowrap gap-4 items-center justify-between border-gray-300 rounded px-2 border border-solid pl-0',
            snapshot.isDragging && 'bg-gray-100 shadow-xl',
          )}
        >
          <div
            {...provided.dragHandleProps}
            className="rounded-l border-0 border-r border-solid border-gray-300 text-gray-500 bg-gray-100 flex items-center justify-center text-bold text-xl px-4 h-full "
          >
            <DragIcon className="text-lg" title={index + 1} />
          </div>
          <div className="font-bold text-lg flex-grow order-2 md:order-1 basis-full md:basis-0">
            {question.fieldSlug || question.fieldDescription}
          </div>
          <div className="md:order-2 order-1 inline-flex">
            <AddRegistrationQuestionForm
              dialog={true}
              dialogTitle={`Edit Question ${question.order}`}
              showButton={true}
              buttonIconPath={mdiPencil}
              question={question}
              tournament={tournament}
              loading={loading}
              onSave={() => {
                setLoading(false);
                setEditing(false);
                onSave();
              }}
              onCancel={() => {
                setLoading(false);
                setEditing(false);
              }}
            />

            <DeleteQuestion question={question} />
          </div>
        </div>
      )}
    </Draggable>
  );
}

export const AddRegistrationQuestionForm = ({
  question,
  tournament,
  loading = false,
  onCancel = () => null,
  onSave = () => null,
  children,
  ...props
}) => {
  const router = useRouter();

  const [questionData, setQuestionData] = useState({
    fieldDescription: '',
    fieldType: 'text',
    isRequired: false,
    choices: [],
    tournament: '',
    order: 0,
    ...question,
    tournament: tournament.id,
  });
  const defaultProps = {
    dialog: false,
    canEdit: questionData => isAdminOf(tournament?.scene?.id),
    object: questionData,
    validationSchema: TournamentSchema,
    url: `/api/tournament/player-info-field/${questionData?.id ? questionData.id + '/' : ''}`,
    method: questionData?.id ? 'PUT' : 'POST',
    getPostData: {
      values: values => values,
    },
    onSave: async (result, formik) => {
      await formik.resetForm({ values: result });
      await router.replace(router.asPath);
      onSave();
    },
    className: 'overflow-visible',
    initialData: questionData,
    ...props,
  };

  return (
    <Form hideSaveButton={true} {...defaultProps}>
      {({ formik }) => {
        return (
          <>
            <div>
              <div className="grid grid-cols-1  items-center gap-8">
                <Field
                  name="fieldSlug"
                  label="Optional Short Internal Reference Name"
                  component={TextField}
                  noGrid
                  className="col-span-5"
                />
                <Field
                  name="fieldDescription"
                  label="Question"
                  component={TextField}
                  noGrid
                  className="col-span-5"
                />
                <div className="col-span-4 flex gap-8 items-center">
                  <Field
                    name="fieldType"
                    label="Type"
                    component={Select}
                    noGrid
                    className="col-span-2"
                  >
                    {FIELD_TYPES.map(type => (
                      <MenuItem key={type} value={type}>
                        {startCase(camelCase(type))}
                      </MenuItem>
                    ))}
                  </Field>
                  <Field name="isRequired" label="Required" component={Switch} />
                </div>
              </div>

              <MultipleChoiceForm type={formik.values.fieldType} questionData={questionData} />
              <input type="hidden" name="order" value={questionData.order} />
              {questionData?.id && (
                <>
                  <input type="hidden" name="id" value={questionData.id} />
                  <span className="absolute bottom-4 left-4 text-sm text-gray-400">
                    Question internal name: {questionData.fieldName}
                  </span>
                </>
              )}
            </div>
            {!props?.dialog && (
              <div className="flex items-center gap-4 mt-6">
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  size="large"
                  onClick={formik.handleSubmit}
                >
                  {loading ? 'Saving...' : 'Save'}
                </Button>
                <Button
                  variant="outlined"
                  color="default"
                  type="submit"
                  size="large"
                  className=""
                  onClick={() => onCancel()}
                >
                  Cancel
                </Button>
              </div>
            )}
          </>
        );
      }}
    </Form>
  );
};

export const MultipleChoiceForm = ({ questionData, type }) => {
  const [choices, setChoices] = useState(questionData.choices || []);
  const formik = useForm();
  useEffect(() => {
    formik.values['choices'] = choices;
  }, [choices]);
  let titleNote;
  switch (type) {
    case 'multi_choice':
      titleNote = 'People will be able to select one or more';
      break;
    case 'single_choice':
      titleNote = 'People will be able to select one or more';
      break;
    default:
      return null;
  }
  return (
    <div className="mt-4 -mb-4">
      <List
        className="flex flex-col "
        title={
          <span className="text-lg">
            Choices <span className="text-xs text-gray-500 ml-4">{titleNote}</span>
          </span>
        }
        items={choices}
        fields={[
          {
            name: 'label',
            required: true,
          },
        ]}
        getItemText={item => item}
        noItemsText="Add choices to this field"
        onAdd={(v, { values }) => setChoices([...choices, values.label])}
        onDelete={value => setChoices(choices.filter(choice => choice !== value))}
      />
      <input type="hidden" name="choices" value={choices} />
    </div>
  );
};
