import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Chip,
  InputAdornment,
  Link,
  MenuItem,
  Typography,
} from '@material-ui/core';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';
import Icon from '@mdi/react';
import { mdiAccount } from '@mdi/js';

import Form, { getDefaultErrorMessage } from './Form';
import List from './List';
import TournamentBracketTeamsForm from './TournamentBracketTeamsForm';
import Field, { TextField, Switch, Select, Conditional, OrderedChoices } from '../fields/Field';
import DeleteButton from 'components/DeleteButton';
import InlineCode from 'components/InlineCode';
import { TOURNAMENT_LINK_TYPES, STAGE_TYPE, AUTO_WARMUP } from 'util/constants';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { useStageTiebreakerOptions } from '@/hooks';

const TournamentBracketSchema = Yup.object().shape({
  name: Yup.string().required('Stage name is required').max(100, 'Too long'),
  roundsPerMatch: Yup.number().default(0),
  winsPerMatch: Yup.number().default(0),
  queueTimer: Yup.number().min(0).max(120),
});

export default function TournamentBracketForm({ bracket, tournament, onClose, ...props }) {
  const axios = getAxios({ authenticated: true });
  const [valid, setValid] = useState(false);
  const [isValidating, setIsValidating] = useState(false);
  const [errors, setErrors] = useState(null);
  const router = useRouter();
  const tiebreakerOptions = useStageTiebreakerOptions();

  const handleSubmitNew = async (values, form) => {
    const letters = [...Array(26).keys()].map(i => String.fromCharCode(65 + i));

    for (const idx of Array(values.numGroups).keys()) {
      const thisStageValues = { ...values };
      if (values.numGroups > 1) {
        thisStageValues.name = `${values.name} Group ${letters[idx] ?? idx + 1}`;
      }

      try {
        await axios({ method: 'POST', url: '/api/tournament/bracket/', data: thisStageValues });
      } catch (err) {
        toast.error(getDefaultErrorMessage(err.response?.data));
        formik.setErrors(err.response?.data);
      }
    }
  };

  const formProps = {
    buttonText: bracket.id ? 'Edit' : 'Create Stage',
    dialogTitle: bracket.id ? `Editing Tournament Stage: ${bracket.name}` : 'Create New Stage',
    canEdit: bracket => isAdminOf(tournament.scene.id),
    object: bracket,
    validationSchema: TournamentBracketSchema,
    url: bracket.id ? `/api/tournament/bracket/${bracket.id}/` : undefined,
    onSave: bracket.id ? undefined : handleSubmitNew,
    method: bracket.id ? 'PUT' : 'POST',
    enableReinitialize: true,
    getPostData: {
      linkedOrg: values => (values.linkedOrg === '' ? null : values.linkedOrg),
    },
    onClose,
    ...props,
  };

  const validateBracket = () => {
    setIsValidating(true);
    axios.put(`/api/tournament/bracket/${bracket.id}/validate/`, bracket).then(response => {
      setIsValidating(false);
      if (response.data.success) {
        setValid(true);
      } else {
        toast.error(response.data?.errors?.join('\n'));
        setErrors(response.data.errors);
      }
    });
  };

  const deleteBracket = () => {
    axios.delete(`/api/tournament/bracket/${bracket.id}/`).then(response => {
      toast(`${bracket.name} deleted.`);
      onClose();
    });
  };

  if (bracket.id) {
    formProps.dialogActions = (
      <>
        {(tournament.linkType == TOURNAMENT_LINK_TYPES.HIVEMIND ||
          (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE && bracket.isValid)) &&
          tournament.teams?.length > 0 && (
            <TournamentBracketTeamsForm
              bracket={bracket}
              tournament={tournament}
              onClose={onClose}
            />
          )}

        <DeleteButton onConfirm={deleteBracket} confirmText={`Delete "${bracket.name}"?`} />
      </>
    );
  }

  return (
    <Form {...formProps}>
      <Field
        name="name"
        label="Stage Name"
        component={TextField}
        helperText="Required. A clear but concise name recommended for display on stream overlays."
      />

      {tournament.linkType === TOURNAMENT_LINK_TYPES.HIVEMIND && (
        <Field name="stageType" label="Stage Type" component={Select}>
          <MenuItem value={STAGE_TYPE.ROUND_ROBIN}>Round Robin (Group Stage)</MenuItem>
          <MenuItem value={STAGE_TYPE.SINGLE_ELIM}>Single Elimination</MenuItem>
          <MenuItem value={STAGE_TYPE.DOUBLE_ELIM}>Double Elimination</MenuItem>
          <MenuItem value={STAGE_TYPE.LADDER}>Ladder</MenuItem>
          <MenuItem value={STAGE_TYPE.CUSTOM}>Custom (Manually Scheduled)</MenuItem>
        </Field>
      )}

      <Conditional
        test={v =>
          tournament.linkType !== TOURNAMENT_LINK_TYPES.HIVEMIND ||
          v.stageType === STAGE_TYPE.ROUND_ROBIN
        }
      >
        <Field
          name="roundsPerMatch"
          label="Rounds per Match"
          component={TextField}
          type="number"
          helperText="The match will end when this number of rounds have been played. Set to 0 to disable."
        />
      </Conditional>

      <Field
        name="winsPerMatch"
        label="Wins per Match"
        component={TextField}
        type="number"
        helperText="The match will end when one team has won this many rounds. Set to 0 to disable. (Best of 3 = 2 wins, Best of 5 = 3 wins, Best of 7 = 4 wins, etc.)"
      />

      {tournament.linkType === TOURNAMENT_LINK_TYPES.HIVEMIND && (
        <Conditional test={v => v.stageType === STAGE_TYPE.ROUND_ROBIN}>
          <Field
            name="matchesPerOpponent"
            label="Matches per Opponent"
            component={TextField}
            type="number"
            helperText="Each team will play each other team this many times."
          />
        </Conditional>
      )}
      <Conditional
        test={v =>
          tournament.linkType !== TOURNAMENT_LINK_TYPES.HIVEMIND ||
          v.stageType === STAGE_TYPE.ROUND_ROBIN
        }
      >
        <Field
          name="addTiebreaker"
          label="Add Match Tiebreaker"
          component={Switch}
          helperText="If a match is tied after reaching the maximum number of games to play, add another game."
        />
      </Conditional>

      <Conditional
        test={v =>
          tournament.linkType === TOURNAMENT_LINK_TYPES.HIVEMIND &&
          v.stageType === STAGE_TYPE.SINGLE_ELIM
        }
      >
        <Field
          name="thirdPlaceMatch"
          label="Add Third-Place Match"
          component={Switch}
          helperText="Add a third-place match between the losers of the semifinals."
        />
      </Conditional>

      <Field
        name="queueTimer"
        label="Match Queue Timer"
        component={TextField}
        type="number"
        helperText="After a match ends, the next available match in queue will be assigned to the cab after this many seconds (0-120)."
        InputProps={{
          endAdornment: <InputAdornment position="end">seconds</InputAdornment>,
        }}
      />
      <Field
        name="autoWarmup"
        label="Warm-Up Games"
        component={Select}
        helperText="Automatically enable warm-ups for a queued match. First Set enables only if either team has not yet played in this stage. Can be manually overridden."
      >
        <MenuItem value={AUTO_WARMUP.NEVER}>Never</MenuItem>
        <MenuItem value={AUTO_WARMUP.FIRST_SET}>First Set</MenuItem>
        <MenuItem value={AUTO_WARMUP.ALWAYS}>Always</MenuItem>
      </Field>

      {tournament.linkType === TOURNAMENT_LINK_TYPES.HIVEMIND && !bracket.id && (
        <Field
          name="numGroups"
          label="Number of Groups"
          component={TextField}
          type="number"
          helperText="Number of groups to create with these settings. If set to more than one, 'Group <X>' will be appended to the name of each stage."
        />
      )}

      {tournament.linkType === TOURNAMENT_LINK_TYPES.HIVEMIND && (
        <Conditional test={v => v.stageType === STAGE_TYPE.ROUND_ROBIN}>
          <Field
            name="stageTiebreakers"
            label="Stage Tiebreaker Order"
            component={OrderedChoices}
            choices={tiebreakerOptions}
          />
        </Conditional>
      )}

      {tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE && (
        <>
          <Typography variant="h2">Challonge Settings</Typography>
          <Field
            name="reportAsSets"
            label="Report Results as Sets"
            component={Switch}
            helperText="If selected, the results will be reported to Challonge as a series of 1-0 sets. Select this if awarding points per game/set win."
          />
          <Field
            name="linkedOrg"
            label="Challonge Organization"
            component={TextField}
            disabled={bracket.isValid || valid}
            helperText="Required if the tournament is hosted by an organization. Leave blank otherwise."
          />
          <Field
            name="linkedBracketId"
            label="Challonge ID"
            component={TextField}
            disabled={bracket.isValid || valid}
            helperText={`The part of the URL after "challonge.com/"`}
          />
          <Field
            name="randomizeSides"
            label="Randomize Sides"
            component={Switch}
            helperText="If selected, teams will be randomly assigned to blue or gold. If not selected, the first team listed in Challonge will be assigned to blue."
          />

          {!bracket.isValid && !valid && (
            <Card variant="outlined">
              <CardContent>
                <Typography variant="h2" className="mt-0 mb-2">
                  Challonge Validation
                </Typography>
                <Typography>
                  In order to retrieve brackets/teams and automatically report scores, please
                  complete validation.
                </Typography>
                <Typography className="mt-4">
                  In the Challonge tournament under "Settings" &gt; "Advanced Options" &gt;
                  "Permissions" &gt; "Share admin access", add the user:&nbsp;
                  <Chip
                    href="https://challonge.com/users/kq_hivemind"
                    component="a"
                    variant="outlined"
                    size="small"
                    clickable
                    label="kq_hivemind"
                    icon={<Icon path={mdiAccount} size={1} />}
                  />
                </Typography>
                <Typography className="mt-4">
                  Once the Challonge ID is entered, click "Save" below then confirm that this
                  tournament belongs to you by adding the following validation code into your
                  tournament's description and clicking "Validate": &nbsp;
                  {bracket.linkToken && bracket.linkedBracketId && (
                    <InlineCode>{bracket.linkToken}</InlineCode>
                  )}
                </Typography>

                {/*
                {errors && errors.map((error, idx) => <Typography key={idx}>{error}</Typography>)}
                */}
              </CardContent>
              <CardActions className="justify-center">
                <Button
                  variant="contained"
                  color="primary"
                  onClick={validateBracket}
                  disabled={isValidating || !bracket.linkToken}
                >
                  Validate
                </Button>
              </CardActions>
            </Card>
          )}

          {valid && (
            <Card variant="outlined">
              <CardContent>
                <Typography>
                  Validation successful. You can remove the validation code from your Challonge
                  tournament description.
                </Typography>
              </CardContent>
            </Card>
          )}
        </>
      )}
    </Form>
  );
}

TournamentBracketForm.propTypes = {
  bracket: PropTypes.object.isRequired,
  tournament: PropTypes.object.isRequired,
};
