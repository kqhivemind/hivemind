import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Paper,
  Typography,
} from '@material-ui/core';
import clsx from 'clsx';
import { CheckboxGroup } from 'components/fields/Checkbox';
import LoginButtons from 'components/LoginButtons';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { getAxios } from 'util/axios';
import * as Yup from 'yup';

import SuccessIcon from '@material-ui/icons/CheckCircle';
import AlertIcon from '@material-ui/icons/Warning';
import { SceneSelect } from 'components/fields/SceneSelect';
import MarkdownDisplay from 'components/MarkdownDisplay';
import RegistrationClosedMessage, {
  displayRegistrationClosedMessage,
} from 'components/RegistrationClosedMessage';
import SceneBadge from 'components/SceneBadge';
import { Remark } from 'react-remark';
import { formatUTC } from 'util/dates';
import { useAuth } from '@/util/auth';
import Field, { ImageUploadField, imageURLRegex, Switch, TextField } from '../fields/Field';
import Form, { useForm } from './Form';
import RegisterTeamForm from './RegisterTeamForm';
import RegistrationQuestionsFields from './RegistrationQuestionsFields';

const PlayerSchema = (tournament, questions) => {
  const additionalSchema = {};
  questions?.forEach(q => {
    if (q.fieldType === 'multi_choice') {
      additionalSchema[q.fieldName] = q.isRequired
        ? Yup.array().required('Field is required')
        : Yup.array();
    } else if (q.fieldType === 'checkbox') {
      additionalSchema[q.fieldName] = q.isRequired
        ? Yup.bool()
            .oneOf([true], 'Must activate this switch to continue')
            .required('This field is required')
        : Yup.bool();
    } else {
      additionalSchema[q.fieldName] = q.isRequired
        ? Yup.string().required('Field is required')
        : Yup.string();
    }
  });
  const imageSchema = Yup.string()
    .trim()
    .matches(
      /^data:([a-z]+\/[a-z0-9-+.]+(;[a-z-]+=[a-z0-9-]+)?)?(;base64)?,([a-z0-9!$&',()*+;=\-._~:@/?%\s]*)$/i,
      'Must be a valid data URI',
    );
  const imageURLSchema = Yup.string().trim().matches(imageURLRegex, 'Must be a valid URL');

  return Yup.object().shape({
    name: Yup.string().required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    scene: Yup.string().required('Please select a scene'),
    pronouns: Yup.string(),
    doNotDisplay: Yup.boolean().nullable(),
    tidbit: Yup.string(),
    image: Yup.lazy(value => {
      switch (typeof value) {
        case 'string':
          return /^data/.test(value)
            ? tournament.requirePlayerPhoto
              ? imageSchema.required()
              : imageSchema
            : tournament.requirePlayerPhoto
            ? imageURLSchema.required()
            : imageURLSchema;
        case 'undefined':
        default:
          return tournament.requirePlayerPhoto
            ? Yup.mixed().required('A photo is required')
            : Yup.mixed();
      }
    }),
    ...additionalSchema,
  });
};

export function PayButton({ label, playerId, callback }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const formik = useForm();
  if (!formik.values.paymentOptions) return null;
  const checkout = async e => {
    e.preventDefault();
    const pid = playerId;
    const player = await formik.submitForm();
    if (!formik?.isValid) return;
    const quantities = formik.values?.paymentQuantities;
    const prices = formik.values?.paymentPrices;
    const paymentOptions = formik.values?.paymentOptions?.map(id => {
      const payObj = {
        paymentOption: id,
        quantity: quantities[id],
      };
      if (prices[id]) payObj.custom_price = prices[id];
      return payObj;
    });
    const response = await axios.put(`/api/tournament/player/${pid ?? player.id}/begin-checkout/`, {
      paymentOptions,
    });
    callback?.();
    router.push(response.data.url);
  };
  return (
    <Button
      variant="contained"
      color="secondary"
      className="w-auto"
      onClick={checkout}
      disabled={formik.isSubmitting || formik.values?.paymentOptions?.length === 0}
      size="large"
    >
      {label || 'Save and Continue to Payment'}
    </Button>
  );
}
export function SaveAndPayButton({ playerId, hasPaid, callback, tournament }) {
  const formik = useForm();
  const { stripeConfigured } = tournament;

  const submit = async () => {
    const success = await formik.submitForm();
    if (success) callback();
  };

  return (
    <>
      {!hasPaid && stripeConfigured && <PayButton playerId={playerId} callback={callback} />}
      <Button
        variant={hasPaid ? 'contained' : 'outlined'}
        color="secondary"
        className="w-auto"
        onClick={submit}
        disabled={formik.isSubmitting}
        size="large"
      >
        {'Save Info'}
      </Button>
    </>
  );
}

export function MenuItemCheckbox({ name, value }) {
  const formik = useForm();

  return <Checkbox checked={Boolean(formik.values[name].includes(value))} />;
}

export function PaymentSummary({ tournament, forSaleItems }) {
  const { values } = useForm();
  const paymentOptions = values['paymentOptions'] || [];
  const quantities = values['paymentQuantities'] || {};
  const prices = values['paymentPrices'] || {};
  return (
    <>
      <div className="bg-gray-200 rounded p-4 text-xl flex justify-between mt-4">
        <span>Subtotal Price:</span>
        <span className="font-bold ml-8">
          {forSaleItems
            .filter(p => paymentOptions.includes(p.id.toString()))
            .reduce((prev, curr) => {
              const price = curr.price ?? prices?.[curr.id];
              if (!Number(parseFloat(price))) return prev;
              return prev + parseFloat(price) * (quantities?.[curr.id] ?? 1);
            }, 0)
            .toLocaleString('en-US', { style: 'currency', currency: 'USD' })}
        </span>
      </div>
    </>
  );
}

const DisplayField = ({ name, value, className }) => {
  return (
    <div className={clsx('space-y-px', className)}>
      <div className="font-mono uppercase text-xs text-gray-600 tracking-wider">
        <Remark
          rehypeReactOptions={{
            components: {
              p: props => <span className="my-0 inline-block" {...props} />,
              a: props => <a target="_blank" {...props} />,
            },
          }}
        >
          {name}
        </Remark>
      </div>
      <div className="col-span-2">{value || '--'}</div>
    </div>
  );
};
const PaymentSection = ({ heading, forSaleItems, tournament }) => {
  const formik = useForm();
  return (
    <>
      <section id="payment-info">
        <div>
          {heading || <h3>Payment</h3>}
          {tournament.paymentMessage && (
            <div className="my-6 p-3 bg-gold-light4 rounded">
              <MarkdownDisplay>{tournament.paymentMessage}</MarkdownDisplay>
            </div>
          )}
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8 items-start">
            <Field name="paymentOptions" component={CheckboxGroup} multiple={true} noGrid>
              <div className="space-y-1 w-full mt-2">
                {forSaleItems.map(forSaleItem => (
                  <FormControlLabel
                    className="flex items-center checkbox-label"
                    key={forSaleItem.id}
                    control={<Checkbox name="paymentOptions" value={Number(forSaleItem.id)} />}
                    label={
                      <span className="flex justify-between flex-grow ml-auto w-full">
                        <span>{forSaleItem.name}</span>
                        {formik.values?.paymentOptions?.includes(forSaleItem.id.toString()) && (
                          <span className="ml-auto inline-flex gap-2 items-center">
                            <input
                              type="number"
                              name={`paymentQuantities.${forSaleItem.id}`}
                              defaultValue={1}
                              min={1}
                              className="border border-solid border-gray-400 rounded flex-grow-0 w-10 [&::-webkit-inner-spin-button]:opacity-100 pl-1.5 self-stretch"
                            />
                            <span>&times;</span>
                          </span>
                        )}
                        <span className=" ml-2 w-20 font-bold text-right justify-end text-base inline-flex">
                          {'$'}
                          {forSaleItem.price ? (
                            <span className="">{forSaleItem.price}</span>
                          ) : (
                            <input
                              name={`paymentPrices.${forSaleItem.id}`}
                              className="bg-transparent border-0 border-b-2 border-solid border-gray-800 text-base w-full m-0 block h-full placeholder:font-normal placeholder:text-xs font-bold text-right"
                              type="text"
                              pattern="^\d+"
                              placeholder="Enter Amt"
                            />
                          )}
                        </span>
                      </span>
                    }
                  ></FormControlLabel>
                ))}
              </div>
            </Field>
            <PaymentSummary tournament={tournament} forSaleItems={forSaleItems} />
          </div>
        </div>
      </section>
    </>
  );
};
export default function TournamentRegistrationForm({
  tournament,
  scenes,
  questions,
  forSaleItems,
}) {
  const { user, userLoading } = useAuth();
  const axios = getAxios({ authenticated: true });

  const [loading, setLoading] = useState(false);
  const [initialData, setInitialData] = useState(null);
  const [changeTeam, setChangeTeam] = useState(false);
  const [editing, setEditing] = useState(false);
  const showTeamSelection = !initialData?.id || changeTeam;
  const enablePayment = forSaleItems?.length > 0 && tournament.stripeConfigured;
  const getInitialData = async () => {
    try {
      let response = await axios.get(`/api/tournament/tournament/${tournament.id}/registration/`);
      const data = { ...response.data };

      data.image ||= user?.image || '';
      data.name ||= user?.name;
      data.streamName ||= user?.streamName;
      data.scene ||= user?.scene || '';
      data.pronouns ||= user?.pronouns || '';
      data.email ||= user?.permissions?.[0]?.user || '';
      data.doNotDisplay ||= user?.doNotDisplay || false;
      data.paymentQuantities = {};
      data.paymentPrices = {};
      forSaleItems.forEach(item => {
        data.paymentPrices[`${item.id}`] = null;
        data.paymentQuantities[`${item.id}`] = 1;
      });
      questions.forEach(question => {
        switch (question.fieldType) {
          case 'checkbox':
            data[question.fieldName] ||= false;
            break;
          case 'multi_choice':
            data[question.fieldName] ||= [];
            break;
          default:
            data[question.fieldName] ||= '';
            break;
        }
      });

      if (data.team) {
        response = await axios.get(`/api/tournament/team/${data.team}/`);
        data.team = response.data;
      }
      setInitialData(data);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (!userLoading) {
      getInitialData();
    }
  }, [userLoading]);

  const props = {
    url: `/api/tournament/tournament/${tournament.id}/register/`,
    method: 'PUT',
    dialog: false,
    object: initialData,
    buttonText: tournament.id ? 'Edit Registration' : 'Register',
    validationSchema: PlayerSchema(tournament, questions),
    postProcessData: {
      image: async data => (imageURLRegex.test(data?.image) ? undefined : data?.image),
    },
    getPostData: {
      team: async data => data?.team?.id ?? data.team,
    },

    onSave: getInitialData,
    className: 'overflow-visible',
  };

  if (!user?.id) {
    return (
      <>
        <h3>Log in to register for this tournament.</h3>
        <LoginButtons onSuccess={getInitialData} />
      </>
    );
  }

  if (
    displayRegistrationClosedMessage(tournament.registrationCloseDate, tournament.allowRegistration)
  )
    return (
      <RegistrationClosedMessage
        closeDate={tournament.registrationCloseDate}
        allowRegistration={tournament.allowRegistration}
      />
    );

  if (initialData === null) {
    return <CircularProgress />;
  }

  return (
    <Form
      {...props}
      hideSaveButton={true}
      initialValues={{
        ...initialData,
      }}
    >
      {initialData?.id && !editing ? (
        <div>
          {tournament.acceptsPayments && !initialData?.paid && (
            <div className="mb-8  border-2 border-secondary bg-gray-100 border-solid border-gray-800 rounded  p-6 space-y-6">
              <PaymentSection
                heading={
                  <h3 className="-mt-6 -mx-6 p-6 text-white font-bold bg-gray-800 text-xl">
                    Payment is required before{' '}
                    <b>{formatUTC(tournament.registrationCloseDate, 'MMM d, yyyy')}</b>
                  </h3>
                }
                forSaleItems={forSaleItems}
                tournament={tournament}
              />

              <PayButton playerId={initialData?.id} label="Pay now" />
            </div>
          )}

          <div className="grid lg:grid-cols-3 grid-cols-1 items-start">
            <div className="lg:col-span-2 flex flex-col gap-6 border-2 border-solid border-blue-light2 rounded-lg shadow-2xl p-6 relative z-20 bg-white lg:-rotate-1">
              <header className="flex gap-4 justify-between items-stretch">
                <div className="bg-gray-300 font-semibold px-3 pt-2.5 pb-1.5 flex-grow rounded">
                  {tournament.name} - Official Participant
                </div>
                <Button
                  variant="outlined"
                  color="secondary"
                  size="small"
                  onClick={() => setEditing(true)}
                >
                  Edit
                </Button>
              </header>
              <div className="grid grid-cols-1 lg:grid-cols-8 gap-4">
                {initialData?.image && (
                  <div className="lg:col-span-3 overflow-hidden rounded-lg">
                    <img src={initialData.image} className="w-full h-full object-cover" />
                  </div>
                )}
                <div className="lg:col-span-5 first:lg:col-span-8 space-y-3">
                  <DisplayField
                    name="Name"
                    value={
                      <span className="text-3xl font-bold font-['Roboto_Slab']">
                        {initialData.name}
                      </span>
                    }
                  />
                  <DisplayField name="Team" value={initialData?.team?.name} />
                  <div className="grid grid-cols-2 gap-8">
                    <DisplayField
                      name="Pronouns"
                      value={
                        initialData?.pronouns ? (
                          <span className="inline-block bg-emerald-200 text-xs font-mono h-6 px-2 pt-1 text-center rounded-sm  -my-0.5">
                            {initialData.pronouns}
                          </span>
                        ) : (
                          '--'
                        )
                      }
                    />
                    <DisplayField
                      name="Scene"
                      value={
                        <SceneBadge
                          scene={scenes && scenes.find(scene => scene.name === initialData.scene)}
                        />
                      }
                    />
                  </div>
                  <DisplayField name="Tidbit" value={initialData.tidbit} />
                </div>
              </div>
            </div>
            <div
              className={clsx(
                '-order-1 -rotate-1 rounded-t text-center  relative z-10',
                'lg:order-2  lg:rotate-3 lg:rounded-r lg:rounded-tl-none  shadow-lg',
                'lg:my-8 lg:mx-0 lg:-ml-8 lg:p-12 lg:pb-12 lg:pl-16 lg:left-0',
                'mx-4 -left-1.5 -mb-8 p-8 pb-16 ',
                initialData?.paid
                  ? '  bg-gradient-to-br from-blue-light5 to-blue-light3'
                  : '  bg-gradient-to-br from-gold-light5 to-gold-light3',
              )}
            >
              {initialData?.paid ? (
                <SuccessIcon className="mx-auto mb-1 text-5xl text-blue-dark1" />
              ) : (
                <AlertIcon className="mx-auto mb-1 text-5xl text-gold-dark5" />
              )}
              <h2 className="mb-4 my-0 leading-tight">
                You are registered {initialData?.paid ? 'and paid in full' : 'but not paid'}
              </h2>

              {initialData?.paid && (
                <>
                  <DisplayField
                    name="Total Paid"
                    value={
                      <>
                        <span className="text-lg">
                          {initialData?.price &&
                            (parseFloat(initialData?.price) / 100).toLocaleString('en-US', {
                              style: 'currency',
                              currency: 'USD',
                            })}
                        </span>
                        {initialData?.registrationTime && (
                          <span className="block text-xs">
                            {new Date(initialData?.registrationTime).toLocaleString('en-US')}
                          </span>
                        )}
                      </>
                    }
                  />
                  {initialData?.selectedPaymentOptions?.length > 0 && (
                    <DisplayField
                      className="mt-4"
                      name="Line Items"
                      value={
                        <ul className="p-0 m-0 list-none">
                          {initialData?.selectedPaymentOptions?.map(po => (
                            <li className="text-xs font-mono">
                              {po.paymentOption.name} - {po.quantity} &times;{' '}
                              {parseFloat(po.paymentOption?.price || po.customPrice).toLocaleString(
                                'en-US',
                                {
                                  style: 'currency',
                                  currency: 'USD',
                                },
                              )}
                            </li>
                          ))}
                        </ul>
                      }
                    />
                  )}
                </>
              )}
            </div>
          </div>
          <div className="border-2 border-solid border-gray-200 rounded-lg shadow-none p-6 relative z-20 bg-white mt-8">
            <h4 className="mt-0 my-6 text-xl">Registration Details</h4>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
              <DisplayField name="Email" value={initialData.email} />

              <DisplayField
                name="Allow picture on stream?"
                value={initialData.doNotDisplay ? 'No' : 'Yes'}
              />

              {questions?.map(({ fieldDescription, fieldName, fieldType }) => (
                <DisplayField
                  key={`df-${fieldName}`}
                  name={fieldDescription}
                  value={
                    fieldType === 'multi_choice'
                      ? initialData[fieldName]?.map(choice => <div>&bull; {choice}</div>)
                      : initialData[fieldName]
                  }
                />
              ))}
            </div>
          </div>
        </div>
      ) : (
        <>
          <Typography variant="h2">Register for {tournament.name}</Typography>

          {tournament.registrationCloseDate && (
            <Paper
              className="border-solid border border-gold-light1 bg-gold-light4 p-6 my-6"
              elevation={0}
            >
              <div>
                Registration will close on{' '}
                <b>{formatUTC(tournament.registrationCloseDate, 'MMM d, yyyy')}</b>. Your info can
                be edited at any time until registration is closed.
              </div>
            </Paper>
          )}
          {tournament.registrationMessage && (
            <div>
              <MarkdownDisplay>{tournament.registrationMessage}</MarkdownDisplay>
            </div>
          )}
          <section id="personal-info">
            <h3>Your Info</h3>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
              <div className="space-y-8">
                <Field
                  name="name"
                  label="Your Display Name"
                  component={TextField}
                  required
                  helperText="Displays on stream and may be used by commentators"
                />
                <Field component={TextField} name="pronouns" label="Pronouns" />
              </div>
              <div>
                <Field
                  name="image"
                  label="Picture"
                  component={ImageUploadField}
                  required={tournament.requirePlayerPhoto}
                  helperText="Upload a picture of yourself. May be used on stream."
                  noGrid
                />
                <Field
                  name="doNotDisplay"
                  label="Please don't show my picture on stream"
                  component={Switch}
                  noGrid
                />
              </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4 md:gap-8 mt-8">
              <Field name="email" label="Email" type="email" component={TextField} />
              <SceneSelect scenes={scenes} />
            </div>

            <Field
              name="tidbit"
              label="Interesting tidbit about yourself"
              component={TextField}
              multiline
              minRows={3}
            />
          </section>

          {questions?.length > 0 && (
            <section id="additional-info">
              <h3>Additional Info:</h3>
              <div className="grid grid-cols-1  gap-x-8 mt-8 gap-y-6">
                <RegistrationQuestionsFields questions={questions} initialData={initialData} />
              </div>
            </section>
          )}

          <section id="team-info" className="mt-8">
            <div className="flex justify-between items-center">
              <h3>Your Team</h3>
              {!showTeamSelection && (
                <Button
                  variant="outlined"
                  color="secondary"
                  onClick={() => setChangeTeam(true)}
                  className="inline-block"
                >
                  Change Team Selection
                </Button>
              )}
            </div>

            {initialData?.id && initialData?.team && (
              <div className="text-xl text-center font-bold p-12 rounded bg-gradient-to-br from-blue-light5 to-blue-light3 color-mo mb-4">
                {initialData.team.name}
              </div>
            )}

            {!showTeamSelection ? (
              <div>
                {!initialData?.team && (
                  <div className="text-lg text-center p-12 rounded bg-gray-200 mb-4">
                    You are registered as a free agent.
                  </div>
                )}
              </div>
            ) : (
              <RegisterTeamForm tournament={tournament} />
            )}
          </section>

          {enablePayment && !initialData?.paid && (
            <PaymentSection forSaleItems={forSaleItems} tournament={tournament} />
          )}

          <div className="flex  flex-col md:flex-row gap-4 mt-12">
            <SaveAndPayButton
              tournament={tournament}
              playerId={initialData?.id}
              hasPaid={initialData?.paid}
              callback={() => {
                setChangeTeam(false);
                setEditing(false);
              }}
            />
            {editing && (
              <Button variant="outlined" color="default" onClick={() => setEditing(false)}>
                Cancel
              </Button>
            )}
          </div>
        </>
      )}
    </Form>
  );
}
