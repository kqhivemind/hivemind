import React, { useState } from 'react';
import { MenuItem, Grid } from '@material-ui/core';
import * as Yup from 'yup';

import Form, { useForm } from './Form';
import Field, { Select, TextField } from '../fields/Field';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import { isAdminOf } from 'util/auth';

const TournamentMatchSchema = Yup.object().shape({
  bracket: Yup.number().required('Tournament stage must be selected'),
  blueTeam: Yup.number(),
  goldTeam: Yup.number(),
  blueScore: Yup.number(),
  goldScore: Yup.number(),
});

export default function TournamentMatchForm({ tournament, cabinet, activeMatch, ...props }) {
  const formProps = {
    dialogTitle: cabinet.displayName,
    showButton: false,
    object: activeMatch,
    validationSchema: TournamentMatchSchema,
    url: activeMatch?.id ? `/api/tournament/match/${activeMatch.id}/` : '/api/tournament/match/',
    method: activeMatch?.id ? 'PUT' : 'POST',
    canEdit: overlay => isAdminOf(tournament.scene.id),
    enableReinitialize: true,
    reloadOnSave: false,
    ...props,
  };

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.MANUAL) {
    return (
      <Form {...formProps}>
        <Grid container direction="row" spacing={2}>
          <Grid item xs={12}>
            <Field name="bracket" label="Stage" component={Select}>
              {tournament?.brackets.map(bracket => (
                <MenuItem key={bracket.id} value={bracket.id}>
                  {bracket.name}
                </MenuItem>
              ))}
            </Field>
          </Grid>

          <Grid item xs={12} md={6}>
            <Field name="blueTeam" label="Blue Team" component={Select}>
              {tournament?.teams &&
                tournament.teams.map(team => (
                  <MenuItem key={team.id} value={team.id}>
                    {team.name}
                  </MenuItem>
                ))}
            </Field>
            <Field name="blueScore" label="Blue Score" component={TextField} type="number" />
          </Grid>
          <Grid item xs={12} md={6}>
            <Field name="goldTeam" label="Gold Team" component={Select}>
              {tournament?.teams &&
                tournament.teams.map(team => (
                  <MenuItem key={team.id} value={team.id}>
                    {team.name}
                  </MenuItem>
                ))}
            </Field>
            <Field name="goldScore" label="Gold Score" component={TextField} type="number" />
          </Grid>
        </Grid>
      </Form>
    );
  }

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
    return <></>;
  }
}
