import React, { useState } from 'react';
import { MenuItem, Typography, Link, Grid, List as MuiList, ListItem } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import fileDownload from 'js-file-download';
import { useRouter } from 'next/router';

import Form from './Form';
import List from './List';
import Field, { Divider, Select, Switch, TextField } from '../fields/Field';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { PERMISSION_TYPES } from 'util/constants';

const CabinetSchema = Yup.object().shape({
  name: Yup.string().max(20, 'Too long'),
  displayName: Yup.string().max(30, 'Too long'),
});

export default function CabinetForm({ cabinetId, scene }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const [timezones, setTimezones] = useState(null);
  const [cabinet, setCabinet] = useState(null);

  // Re-fetch cabinet data as an authenticated user
  if (cabinet === null) {
    if (cabinetId === null) {
      setCabinet({ scene, timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone });
    } else {
      axios.get(`/api/game/cabinet/${cabinetId}`).then(response => {
        const data = response.data;
        axios.get(`/api/game/scene/${data.scene}/`).then(response => {
          data.scene = response.data;
          setCabinet(data);
        });
      });
    }

    return <></>;
  }

  const downloadClientConfig = event => {
    const config = {
      cabinets: [
        {
          sceneName: cabinet.scene.name,
          cabinetName: cabinet.name,
          token: cabinet.token,
          url: 'ws://kq.local:12749',
        },
      ],
      servers: [
        {
          name: 'HiveMind',
          url: 'wss://kqhivemind.com/ws/stats_listener/v3',
        },
      ],
    };

    fileDownload(JSON.stringify(config, null, 2), 'config.json');
    event.preventDefault();
  };

  const props = {
    buttonText: cabinetId ? 'Edit' : 'Create New',
    dialogTitle: cabinetId ? `Editing Cabinet: ${cabinet.displayName}` : 'Create New Cabinet',
    canEdit: cabinet => isAdminOf(scene.id),
    object: cabinet,
    validationSchema: CabinetSchema,
    url: cabinetId ? `/api/game/cabinet/${cabinet.id}/` : '/api/game/cabinet/',
    method: cabinetId ? 'PUT' : 'POST',
    getPostData: {
      scene: values => scene.id,
    },
    onSave: result => {
      if (cabinetId) {
        router.reload();
      } else {
        router.push(`/cabinet/${scene.name}/${result.name}`);
      }
    },
  };

  if (timezones === null) {
    axios.get('/api/game/cabinet/timezones/').then(response => {
      setTimezones(response.data.results);
    });
  }

  return (
    <Form {...props}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="name"
            label="Short Name"
            component={TextField}
            helperText="Lowercase letters and numbers only. Must be unique within your scene."
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="displayName" label="Display Name" component={TextField} />
        </Grid>
      </Grid>
      <Field
        name="description"
        label="Description/Info"
        component={TextField}
        multiline
        rows={10}
      />
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="timeZone" label="Time Zone" component={Select}>
            {timezones !== null &&
              timezones.map(timezone => (
                <MenuItem key={timezone} value={timezone}>
                  {timezone}
                </MenuItem>
              ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          {cabinetId && (
            <Field
              name="token"
              label="Client Token"
              component={TextField}
              InputProps={{ readOnly: true }}
              withCopyButton
            />
          )}
        </Grid>
        <Grid item xs={12} md={6}>
          {cabinetId && (
            <Field name="twitchChannelName" label="Twitch Channel Name" component={TextField} />
          )}
        </Grid>
      </Grid>

      {cabinetId && (
        <>
          <Divider />

          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <Typography variant="h3" className="mb-4">
                Client Configuration
              </Typography>
              <MuiList>
                <ListItem>
                  <Link href="#" onClick={downloadClientConfig}>
                    Download Client Configuration
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/wiki/Basic_Client_Setup">Cabinet Setup Instructions</Link>
                </ListItem>
              </MuiList>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="h3" className="mb-4">
                QR Codes
              </Typography>
              <MuiList>
                <ListItem>
                  <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}/qr/onepage`}>
                    QR code for sign-in page
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}/qr/nfc`}>
                    NFC card registration QR codes
                  </Link>
                </ListItem>
              </MuiList>
            </Grid>
          </Grid>

          <Divider />

          <Typography variant="h3" className="mb-4">
            Player Sign-In
          </Typography>

          <Grid container>
            <Grid item xs={12} md={6}>
              <Field name="allowQrSignin" label="Allow QR Sign-In" component={Switch} />
            </Grid>
            <Grid item xs={12} md={6}>
              <Field name="allowNfcSignin" label="Allow NFC Sign-In" component={Switch} />
            </Grid>
          </Grid>
        </>
      )}
    </Form>
  );
}

CabinetForm.propTypes = {
  scene: PropTypes.object,
  cabinetId: PropTypes.number,
};

CabinetForm.defaultProps = {
  scene: null,
  cabinetId: null,
};
