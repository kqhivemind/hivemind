import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import { useState } from 'react';
import * as Yup from 'yup';

import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import Field, { DateField, Radio, RadioGroup, Switch, TextField, SlugField } from '../fields/Field';
import Form from './Form';

import styles from './TournamentCreateForm.module.css';

const useStyles = makeStyles(theme => ({
  loading: {
    cursor: 'wait',
  },
  payOptionsContainer: {
    padding: theme.spacing(1, 2, 2),
    backgroundColor: '#f3f3f3',
    borderRadius: '5px',
    display: 'flex',
    flexDirection: 'column',
  },
}));

const TournamentSchema = Yup.object().shape({
  name: Yup.string().required('Tournament name is required'),
  date: Yup.string()
    .nullable(true)
    .transform(v => v || null)
    .required('Tournament date is required'),
  description: Yup.string(),
  isActive: Yup.bool()
    .default(false)
    .transform(v => Boolean(v)),
});

export default function TournamentForm({ tournament }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const props = {
    buttonText: tournament.id ? 'Edit' : 'Create New',
    buttonVariant: tournament.id ? 'contained' : 'outlined',
    dialogTitle: tournament.id ? `Editing Tournament: ${tournament.name}` : 'Create New Tournament',
    canEdit: tournament => isAdminOf(tournament.scene.id),
    object: tournament,
    validationSchema: TournamentSchema,
    url: tournament.id
      ? `/api/tournament/tournament/${tournament.id}/`
      : '/api/tournament/tournament/',
    method: tournament.id ? 'PUT' : 'POST',
    getPostData: {
      scene: values => values.scene.id,
    },
    onSave: result => {
      if (!tournament.id) {
        router.push(`/tournament/${result.id}/admin`);
      }
    },
    showDeleteButton: false,
    className: clsx({ [classes.loading]: loading }),
  };

  return (
    <Form {...props}>
      <div className={styles.container}>
        <div className={styles.itemFull}>
          <Field name="name" label="Tournament Name" component={TextField} noGrid />
        </div>

        <div className={styles.itemFull}>
          <Field
            name="slug"
            label="Tournament Slug/URL Path"
            component={SlugField}
            basedOn="name"
            baseUrl={`${window.location.origin}/t/${tournament.scene.name}/`}
            helperText="Must be unique within your scene. Tournament URL will be: "
            noGrid
          />
        </div>

        <Field
          className={styles.item}
          name="date"
          label="Date"
          component={DateField}
          containerClass="my-0"
          noGrid
        />

        {tournament.id && (
          <Field
            className={styles.item}
            name="isActive"
            label="Active"
            component={Switch}
            containerClass="my-0"
          />
        )}
      </div>
      <Field name="description" label="Description" component={TextField} multiline rows={5} />

      {!tournament.id && (
        <Field name="linkType" label="Tournament Integration" component={RadioGroup}>
          <Radio
            value={TOURNAMENT_LINK_TYPES.MANUAL}
            label="Manual"
            helperText="Team names are entered manually. Tournament operators select the teams that are playing."
          />
          <Radio
            value={TOURNAMENT_LINK_TYPES.CHALLONGE}
            label="Challonge"
            helperText="Team names and matches are retrieved from the Challonge API. Tournament operators select the match that is being played. Scores are reported to Challonge automatically."
          />
          <Radio
            value={TOURNAMENT_LINK_TYPES.HIVEMIND}
            label="HiveMind"
            helperText="Matches are generated using HiveMind's tournament system."
          />
        </Field>
      )}
    </Form>
  );
}
