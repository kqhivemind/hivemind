import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';

import Form from './Form';
import List from './List';
import Field, { TextField } from '../fields/Field';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { PERMISSION_TYPES } from 'util/constants';

const SceneSchema = Yup.object().shape({
  displayName: Yup.string().max(30, 'Too long'),
});

export default function SceneForm({ scene }) {
  const axios = getAxios({ authenticated: true });
  const [admins, setAdmins] = useState(null);
  const isAdmin = isAdminOf(scene.id);
  const props = {
    buttonText: 'Edit',
    buttonVariant: 'contained',
    dialogTitle: `Editing Scene: ${scene.displayName}`,
    canEdit: scene => isAdminOf(scene.id),
    object: scene,
    validationSchema: SceneSchema,
    url: `/api/game/scene/${scene.id}/`,
  };

  return (
    <Form {...props}>
      <Field name="displayName" label="Display Name" component={TextField} />
      <Field
        name="description"
        label="Description/Info"
        component={TextField}
        multiline
        rows={10}
      />
    </Form>
  );
}

SceneForm.propTypes = {
  scene: PropTypes.shape({
    displayName: PropTypes.string.isRequired,
  }).isRequired,
};
