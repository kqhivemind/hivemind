import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  Switch as MuiSwitch,
} from '@material-ui/core';

export default function Switch({ name, label, formik, helperText, value, ...props }) {
  delete props.fullWidth;

  const checked = value ?? Boolean(formik.values[name]);
  const control = <MuiSwitch {...{ name, checked, ...props }} />;

  return (
    <FormControl>
      <FormControlLabel control={control} label={label} />
      {helperText && (
        <FormHelperText className={props.error && 'text-red-500'}>{helperText}</FormHelperText>
      )}
    </FormControl>
  );
}
