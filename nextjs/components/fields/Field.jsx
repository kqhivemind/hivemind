import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import React from 'react';

import { useForm } from '../forms/Form';
import Autocomplete from './Autocomplete';
import CabinetSelect from './CabinetSelect';
import Conditional from './Conditional';
import DateField from './DateField';
import Divider from './Divider';
import ImageUploadField, { imageURLRegex } from './ImageUploadField';
import OrderedChoices from './OrderedChoices';
import { Radio, RadioGroup } from './Radio';
import Select from './Select';
import SlugField from './SlugField';
import Switch from './Switch';
import TextField from './TextField';

export const HiddenField = args => <input type="hidden" {...args} />;

const useStyles = makeStyles(theme => ({
  fieldContainer: {
    margin: theme.spacing(2, 0),
    '&:first-child': {
      marginTop: 0,
    },
  },
}));

export default function Field({ name, component, noGrid, containerClass, multiple, ...props }) {
  const classes = useStyles();
  const formik = useForm();
  const componentProps = {
    name: name,
    fullWidth: true,
    ...props,
  };

  if (formik.values[name] === undefined) {
    formik.values[name] = multiple ? [] : '';
  }
  const TheField = React.createElement(component, {
    formik,
    name,
    value: formik.values[name],
    error: formik.touched[name] && formik.errors[name] ? true : undefined,
    helperText:
      formik.touched[name] && formik.errors[name] ? formik.errors[name] : componentProps.helperText,
    onChange: formik.handleChange,
    multiple,
    ...componentProps,
  });
  return noGrid ? (
    TheField
  ) : (
    <Grid item className={classes.fieldContainer + ' ' + containerClass}>
      {TheField}
    </Grid>
  );
}

Field.propTypes = {
  name: PropTypes.string.isRequired,
  component: PropTypes.elementType.isRequired,
};

export {
  Autocomplete,
  CabinetSelect,
  Conditional,
  DateField,
  Divider,
  ImageUploadField,
  OrderedChoices,
  Radio,
  RadioGroup,
  Select,
  SlugField,
  Switch,
  TextField,
  imageURLRegex,
};
