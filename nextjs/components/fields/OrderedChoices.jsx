import React, { createContext, useContext } from 'react';
import clsx from 'clsx';
import { IconButton } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiPlusCircleOutline, mdiMinusCircleOutline, mdiDragVertical } from '@mdi/js';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import styles from './OrderedChoices.module.css';
import { useForm } from '@/components/forms/Form';

const OrderedChoicesContext = createContext({});
const useOrderedChoices = () => useContext(OrderedChoicesContext);

export function UnselectedChoice({ choice }) {
  const { name, value, setFieldValue } = useOrderedChoices();

  const onClick = () => {
    setFieldValue(name, [...value, choice.name]);
  };

  return (
    <div className={styles.choice}>
      <div className={styles.text}>
        <div className={styles.title}>{choice.title}</div>
        <div className={styles.description}>{choice.description}</div>
      </div>

      <IconButton onClick={onClick} aria-label="Add to tiebreaker list">
        <Icon path={mdiPlusCircleOutline} size={1} />
      </IconButton>
    </div>
  );
}

export function SelectedChoice({ choice, index }) {
  const { name, value, setFieldValue } = useOrderedChoices();

  const onClick = () => {
    setFieldValue(
      name,
      value.filter(v => v !== choice.name),
    );
  };

  return (
    <Draggable draggableId={choice.name} index={index}>
      {(provided, snapshot) => (
        <div className={styles.choice} ref={provided.innerRef} {...provided.draggableProps}>
          <div className={styles.dragHandle} {...provided.dragHandleProps}>
            <Icon path={mdiDragVertical} size={1} />
          </div>

          <div className={styles.text}>
            <div className={styles.title}>{choice.title}</div>
            <div className={styles.description}>{choice.description}</div>
          </div>

          <IconButton onClick={onClick} aria-label="Add to tiebreaker list">
            <Icon path={mdiMinusCircleOutline} size={1} />
          </IconButton>
        </div>
      )}
    </Draggable>
  );
}

export default function OrderedChoices({ name, label, choices, className, ...props }) {
  const { values, setFieldValue } = useForm();
  const value = values[name];
  const onDragEnd = result => {
    if (result?.source?.index !== undefined && result?.destination?.index !== undefined) {
      const newValue = [...value];
      const moving = newValue.splice(result.source.index, 1);
      newValue.splice(result.destination.index, 0, ...moving);
      setFieldValue(name, newValue);
    }
  };

  const contextValue = { name, label, value, setFieldValue, choices, onDragEnd };

  return (
    <OrderedChoicesContext.Provider value={contextValue}>
      <div className={clsx(styles.orderedChoices, className)}>
        <div className={styles.fieldLabel}>{label}</div>

        <div className={clsx(styles.contents)}>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId={`droppable-${name}`}>
              {(provided, snapshot) => (
                <div className={styles.selected} ref={provided.innerRef}>
                  {values[name].map((choiceName, idx) => (
                    <SelectedChoice
                      key={choiceName}
                      choice={choices.find(i => i.name === choiceName)}
                      index={idx}
                    />
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>

          <div className={styles.unselected}>
            {choices
              .filter(v => !values[name].includes(v.name))
              .map(choice => (
                <UnselectedChoice key={choice.name} choice={choice} />
              ))}
          </div>
        </div>
      </div>
    </OrderedChoicesContext.Provider>
  );
}
