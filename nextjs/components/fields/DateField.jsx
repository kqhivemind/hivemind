import PropTypes from 'prop-types';

import TextField from './TextField';

export default function DateField({ ...props }) {
  return (
    <TextField type="date" InputLabelProps={{ shrink: true }} {...props} />
  );
}

DateField.propTypes = {
};
