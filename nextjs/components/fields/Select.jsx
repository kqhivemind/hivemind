import { FormControl, FormHelperText, InputLabel, Select as MuiSelect } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'components/forms/Form';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  label: {
    transform: 'translate(12px, 10px) scale(0.75)',
    background: 'white',
  },
}));

export default function Select({ label, helperText, name, ...props }) {
  const classes = useStyles();
  const formik = useForm();
  return (
    <FormControl fullWidth>
      <InputLabel variant="outlined" className={classes.label}>
        {label}
      </InputLabel>
      <MuiSelect variant="outlined" name={name} {...props} />
      <FormHelperText className={formik.touched[name] && formik.errors[name] && 'text-red-500'}>
        {helperText}
      </FormHelperText>
    </FormControl>
  );
}

Select.propTypes = {
  label: PropTypes.node.isRequired,
};
