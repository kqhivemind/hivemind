import {
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  FormLabel,
  Checkbox as MuiCheckbox,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useContext } from 'react';

import { useForm } from '../forms/Form';

export const CheckboxContext = React.createContext({});

const useStyles = makeStyles(theme => ({
  checkboxGroup: {
    display: 'flex',
    flexDirection: 'row',
    gap: '24px',
  },
  formLabel: {
    color: theme.palette.text.primary,
  },
  formControl: {
    flexBasis: 0,
    flexGrow: 1,
    margin: theme.spacing(1.5, 0),
    padding: theme.spacing(1),
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '4px',
    cursor: 'pointer',
    '&.selected': {
      backgroundColor: theme.palette.highlight.main,
    },
  },
  label: {
    margin: theme.spacing(0, 0, 1, 0),
    textTransform: 'uppercase',
    '& .MuiTypography-root': {
      fontSize: '.875rem',
    },
    '&.selected .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  checkbox: {
    display: 'none',
  },
}));

export function CheckboxGroup({ name, label, children, error, fullWidth, helperText, ...props }) {
  const classes = useStyles();

  return (
    <FormControl fullWidth={fullWidth} error={error}>
      <FormLabel className={classes.formLabel}>{label}</FormLabel>
      <FormHelperText>{helperText}</FormHelperText>

      <FormGroup className={classes.checkboxGroup} name={name} {...props}>
        <CheckboxContext.Provider value={name}>{children}</CheckboxContext.Provider>
      </FormGroup>
    </FormControl>
  );
}

CheckboxGroup.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  error: PropTypes.bool,
  fullWidth: PropTypes.bool,
  helperText: PropTypes.string,
};

CheckboxGroup.defaultProps = {
  children: <></>,
  error: false,
  fullWidth: true,
  helperText: '',
};

export function Checkbox({ value, children, fullWidth, helperText, ...props }) {
  const classes = useStyles();
  const formik = useForm();
  const checkboxGroupName = useContext(CheckboxContext);
  const selected = formik.values[checkboxGroupName].includes(value);

  const onClick = e => {
    formik.setFieldValue(checkboxGroupName, value);
  };

  const control = <MuiCheckbox className={clsx({ selected })} value={value} />;

  return (
    <FormControl className={clsx({ selected })} onChange={onClick}>
      <FormControlLabel
        value={value}
        className={clsx('flex items-center checkbox-label', { selected })}
        control={control}
        {...props}
      />
      <FormHelperText className={classes.helperText}>{helperText}</FormHelperText>
    </FormControl>
  );
}
