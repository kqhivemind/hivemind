import NamedPlayerStatsTable from './NamedPlayerStatsTable';

export default function TournamentTeamStatsTable({ team }) {
  return <NamedPlayerStatsTable stats={team.players} />
}
