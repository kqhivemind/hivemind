import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Table, { TableRow, TableCell } from 'components/tables/Table';
import TournamentMatchEditForm from 'components/forms/TournamentMatchEditForm';
import { useTournamentAdmin } from '@/components/tournament-admin';

export function TournamentQueueMatch({ match, isEditable }) {
  const { teamsById } = useTournamentAdmin();
  const { tournament } = useTournamentAdmin();
  const [isOpen, setIsOpen] = useState(false);

  const getTeamName = teamId => {
    if (teamId && teamsById[teamId] && teamsById[teamId].name) return teamsById[teamId].name;
    return '(unknown team)';
  };

  const onClick = () => {
    if (isEditable) {
      setIsOpen(true);
    }
  };

  return (
    <>
      <TableRow key={match.id} onClick={onClick}>
        <TableCell>{getTeamName(match.blueTeam)}</TableCell>
        <TableCell>{match.roundName}</TableCell>
        <TableCell>{getTeamName(match.goldTeam)}</TableCell>
      </TableRow>
      {isEditable && (
        <TournamentMatchEditForm
          tournament={tournament}
          match={match}
          isOpen={isOpen}
          onClose={() => setIsOpen(false)}
        />
      )}
    </>
  );
}

export default function TournamentQueueTable({ title, queue, isEditable }) {
  const { queues } = useTournamentAdmin();

  const columnHeaders = ['Blue', 'Round', 'Gold'];

  const props = {
    title,
    columnHeaders,
  };

  if (queue === undefined) return <></>;

  return (
    <Table {...props}>
      {queue?.matchList?.map(match => (
        <TournamentQueueMatch {...{ match, isEditable }} />
      ))}
    </Table>
  );
}

TournamentQueueTable.defaultProps = {
  title: 'Cabinet Queue',
};
