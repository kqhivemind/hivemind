import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, TEAM_NAMES, WIN_CONDITIONS } from 'util/constants';
import { formatGameTime } from 'util/dates';
import { getAxios } from 'util/axios';
import Table, { TableRow, TableCell } from './Table';

const useStyles = makeStyles(theme => ({
  loading: {
    cursor: 'wait',
  },
  row: {
    backgroundColor: theme.palette.background.default,
  },
  blue: {
    background: theme.gradients.blue.light,
    '& .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  gold: {
    background: theme.gradients.gold.light,
    '& .MuiTypography-root, & .MuiTableCell-root': {
      fontWeight: 'bold',
    },
  },
  unavailable: {
    backgroundColor: theme.palette.action.disabled,
  },
}));

export default function TournamentGameSelector({ tournament, cabinet, activeMatch }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [games, setGames] = useState(null);
  const [loading, setLoading] = useState(false);

  const loadGames = async () => {
    const response = await axios.get(`/api/game/game/recent/`, {
      params: { cabinetId: cabinet.id },
    });
    setGames(response.data.results);
  };

  useEffect(() => {
    loadGames();
  }, [tournament, activeMatch]);

  const canEdit = game =>
    activeMatch?.id &&
    !loading &&
    (!game.tournamentMatch || game.tournamentMatch == activeMatch.id);

  const handleClick = async game => {
    if (!canEdit(game)) {
      return;
    }

    setLoading(true);

    const response = await axios.get(`/api/tournament/match/${activeMatch.id}/`);
    const match = response.data;

    const adj = game.tournamentMatch == activeMatch.id ? -1 : 1;
    const newMatchValues = {};

    if (game.winningTeam == BLUE_TEAM) newMatchValues.blueScore = match.blueScore + adj;
    if (game.winningTeam == GOLD_TEAM) newMatchValues.goldScore = match.goldScore + adj;

    await axios.patch(`/api/tournament/match/${activeMatch.id}/`, newMatchValues);

    const newMatchID = game.tournamentMatch == activeMatch.id ? null : activeMatch.id;
    await axios.patch(`/api/game/game/${game.id}/`, { tournamentMatch: newMatchID });

    await loadGames();
    setLoading(false);
  };

  const columnHeaders = ['', '', ''];

  const getClassName = game => {
    return clsx(classes.row, {
      [classes.blue]:
        activeMatch.id && game.tournamentMatch == activeMatch.id && game.winningTeam == BLUE_TEAM,
      [classes.gold]:
        activeMatch.id && game.tournamentMatch == activeMatch.id && game.winningTeam == GOLD_TEAM,
      [classes.unavailable]: game.tournamentMatch && game.tournamentMatch != activeMatch.id,
    });
  };

  return (
    <Table
      title={`Recent Games on ${cabinet.displayName}`}
      showColumnHeaders={false}
      columnHeaders={columnHeaders}
      isLoading={games === null}
      className={clsx({ [classes.loading]: loading })}
    >
      {games !== null &&
        games.slice(0, 5).map(game => (
          <TableRow
            key={game.id}
            className={getClassName(game)}
            onClick={canEdit(game) ? () => handleClick(game) : undefined}
          >
            <TableCell>{game.mapName}</TableCell>
            <TableCell>{formatGameTime(game)}</TableCell>
            <TableCell>{TEAM_NAMES[game.winningTeam] ?? game.winningTeam}</TableCell>
            <TableCell>{WIN_CONDITIONS[game.winCondition] ?? game.winCondition}</TableCell>
          </TableRow>
        ))}
    </Table>
  );
}