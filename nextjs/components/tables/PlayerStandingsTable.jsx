import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import Table from './Table';

export default function PlayerStandingsTable({ title, season, data }) {
  const columnHeaders = ['Player Name', 'Points', 'Rounds', 'Matches'];
  const getRowCells = row => [
    row.playerName,
    row.points,
    `${row.roundsWon}-${row.roundsLost}`,
    `${row.matchesWon}-${row.matchesLost}`,
  ];

  const rowLink = row => `/player/${row.playerId}`;
  const subtitleLink = row => `/season/${row.id}`;

  return (
    <Table title={title} columnHeaders={columnHeaders} data={data} getRowCells={getRowCells} isLoading={false} link={rowLink} subtitleLink={subtitleLink} />
  );
}

PlayerStandingsTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array.isRequired,
};

PlayerStandingsTable.defaultProps = {
  title: 'Player Standings',
};
