import PaginatedTable from './PaginatedTable';
import { SceneTableDisplayName } from './SceneTable';
import { TableRow, TableCell } from './Table';

function UserTournamentTableRow({ row }) {
  return (
    <TableRow href={`/tournament-player/${row.playerId}`}>
      <TableCell>{row.name}</TableCell>
      <TableCell>{row.date}</TableCell>
      <SceneTableDisplayName scene={row.scene} />
      <TableCell>{row.team?.name}</TableCell>
    </TableRow>
  );
}

export default function UserTournamentTable({ user }) {
  const columnHeaders = ['Tournament', 'Date', 'Location', 'Team Name'];

  return (
    <PaginatedTable
      title="Tournaments"
      tableSubtitle="Only tournaments with player registration through HiveMind will be included here."
      columnHeaders={columnHeaders}
      url={`/api/user/user/${user.id}/tournaments/`}
      rowsPerPage={20}
      rowElement={UserTournamentTableRow}
    />
  );
}
