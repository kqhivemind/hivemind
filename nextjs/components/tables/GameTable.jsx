import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import Icon from '@mdi/react';
import PropTypes from 'prop-types';
import { format, parse, parseISO } from 'date-fns';
import clsx from 'clsx';
import { TEAM_NAMES, WIN_CONDITIONS } from 'util/constants';
import { getAxios } from 'util/axios';
import PaginatedTable from './PaginatedTable';
import { mdiFruitGrapes, mdiSword } from '@mdi/js';
import SnailIcon from '../SnailIcon';

const useStyles = makeStyles(theme => ({
  topRow: {},
  location: {
    fontSize: '14px',
  },
  bottomRow: {
    justifyContent: 'space-between',
  },
  date: {
    fontSize: '15px',
    fontWeight: 'bold',
  },
  result: {
    fontSize: '14px',
    textAlign: 'right',
    '& span': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
  },
  icon: {
    height: '1.25rem',
    width: '1.25rem',
    display: 'block',
  },
}));

const winConditionIcons = {
  Military: <Icon className={clsx('block h-5 w-5 icon')} path={mdiSword} />,
  Economic: <Icon className={clsx('icon')} path={mdiFruitGrapes} />,
  Snail: <SnailIcon className={clsx('icon')} />,
};
export function GameMeta({ row }) {
  const classes = useStyles();
  return (
    <div className={clsx(classes.result, '')}>
      <div className="flex gap-6 items-center justify-end add-between-dots">
        <div className="flex-grow">{row.mapName}</div>
        <div className="font-bold">
          {format(parseISO(row.endTime) - parseISO(row.startTime), 'm:ss')}
        </div>
        <div className="flex basis-6 justify-center">
          <div className="block w-6 h-6">
            {winConditionIcons[WIN_CONDITIONS[row.winCondition] ?? row.winCondition]}
          </div>
        </div>
        <div
          className={`basis-4 w-4 h-4 block  ${
            (TEAM_NAMES[row.winningTeam] ?? row.winningTeam) === 'Blue'
              ? 'bg-blue-main'
              : 'bg-gold-main'
          }`}
        ></div>
      </div>
    </div>
  );
}
export default function GameTable({
  title,
  columnHeaders,
  getRowCells,
  filters,
  cellClassNames,
  showCabName,
  afterDateSlot,
  bottomRightSlot,
  rowsPerPage,
}) {
  const classes = useStyles();
  const rowLink = row => `/game/${row.id}`;
  if (getRowCells === null) {
    getRowCells = row => [
      <>
        <Grid container direction="row" className={clsx(classes.bottomRow, 'text-gray-700')}>
          <Grid item className="flex items-center gap-6">
            <Typography className={classes.date}>
              {format(new Date(row.endTime), 'MMM d - hh:mm aa')}
            </Typography>
            {afterDateSlot && afterDateSlot(row)}
          </Grid>
          <Grid item className="flex-grow">
            <GameMeta row={row} />
          </Grid>
        </Grid>
        {showCabName && (
          <Grid
            container
            direction="row"
            className={clsx(
              classes.topRow,
              'gap-8 border-0 border-t border-solid border-gray-200 pt-1 mt-1',
            )}
          >
            <Grid item>
              <Typography className={classes.location}>
                {row.scene.displayName} - {row.cabinet.displayName}
              </Typography>
            </Grid>
            {bottomRightSlot && (
              <Grid item className="flex-grow text-right">
                {bottomRightSlot(row)}
              </Grid>
            )}
          </Grid>
        )}
      </>,
    ];
  }

  return (
    <PaginatedTable
      title={title}
      columnHeaders={columnHeaders}
      showColumnHeaders={false}
      getRowCells={getRowCells}
      link={rowLink}
      cellClassNames={cellClassNames}
      url="/api/game/game/recent/"
      filters={filters}
      emptyText="No games found."
      rowsPerPage={rowsPerPage}
    />
  );
}

GameTable.propTypes = {
  title: PropTypes.string,
  filters: PropTypes.shape({
    cabinetId: PropTypes.number,
    sceneId: PropTypes.number,
  }),
  cellClassNames: PropTypes.array,
  showCabName: PropTypes.bool,
  rowsPerPage: PropTypes.number,
};

GameTable.defaultProps = {
  title: 'Recent Games',
  filters: {},
  columnHeaders: ['Location'],
  getRowCells: null,
  cellClassNames: null,
  showCabName: true,
  rowsPerPage: 20,
};
