import React, { useEffect, useState, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Box, Typography, Link } from '@material-ui/core';
import { format } from 'date-fns';

import { getAxios } from '@/util/axios';
import Table, { TableRow, TableCell, TableSubtitle } from './Table';
import { formatStatValue, formatStatValueShort } from '@/util/formatters';

const useStyles = makeStyles(theme => ({
  label: {},
  numeric: {
    ...theme.mixins.tableCell.numeric(theme),
    width: '130px',
  },
  numericLong: {
    ...theme.mixins.tableCell.numeric(theme),
    width: '300px',
  },
}));

export default function UserStatsSummaryTable({ user, stats, title, titleButton, className }) {
  const axios = getAxios();
  const [cabinet, setCabinet] = useState(null);
  const classes = useStyles();
  const session = user?.lastSession;

  useEffect(() => {
    if (session?.cabinet) {
      axios.get(`/api/game/cabinet/${session.cabinet}/`).then(response => {
        setCabinet(response.data);
      });
    }
  }, [session?.cabinet]);

  const lastSessionStats = useMemo(() => {
    if (!user?.lastSessionStats) return undefined;

    const sessionStats = {};
    for (const row of user.lastSessionStats) {
      sessionStats[row.name] = row.value;
    }

    return sessionStats;
  }, [user?.lastSessionStats]);

  const subtitle = useMemo(() => {
    if (!session) return undefined;

    return (
      'Last session: ' +
      format(new Date(user.lastSession.startTime), 'MMM d, yyyy') +
      ' at ' +
      (user.lastSession.cabinet ? cabinet?.displayName ?? '...' : 'multiple cabinets')
    );
  }, [session]);

  const formatValue = row => formatStatValue(row.value, row.formatting);
  const columnHeaders = session ? ['Stat', 'Last Session', 'All Time'] : ['Label', 'Value'];
  const getRowCells = row => [row.label, formatStatValue(row.value, row.formatting)];

  const cellClassNames = session
    ? [classes.label, classes.numeric, classes.numeric]
    : [classes.label, classes.numericLong];

  return (
    <Table
      title={title}
      titleButton={titleButton}
      tableSubtitle={subtitle}
      columnHeaders={columnHeaders}
      showColumnHeaders={session}
      cellClassNames={cellClassNames}
      className={className}
    >
      {stats?.map(row => (
        <TableRow key={row.label}>
          <TableCell className={classes.label}>{row.label}</TableCell>
          {session ? (
            <>
              <TableCell className={classes.numeric}>
                {formatStatValueShort(lastSessionStats[row.name], row.formatting)}
              </TableCell>
              <TableCell className={classes.numeric}>
                {formatStatValueShort(row.value, row.formatting)}
              </TableCell>
            </>
          ) : (
            <TableCell className={classes.numericLong}>
              {formatStatValue(row.value, row.formatting)}
            </TableCell>
          )}
        </TableRow>
      ))}
    </Table>
  );
}

UserStatsSummaryTable.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  stats: PropTypes.array.isRequired,
};

UserStatsSummaryTable.defaultProps = {
  title: 'Total Stats',
  subtitle: null,
};
