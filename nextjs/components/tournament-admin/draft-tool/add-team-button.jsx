import { Button } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiPlus } from '@mdi/js';

import { useDraft } from './context';

export function AddTeamButton() {
  const { addTeam } = useDraft();

  return (
    <Button
      variant="outlined"
      color="primary"
      startIcon={<Icon path={mdiPlus} size={1} />}
      onClick={addTeam}
    >
      Add Team
    </Button>
  );
}
