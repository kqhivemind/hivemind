import { Button } from '@material-ui/core';

import { useDraft } from './context';

export function FinalizeDraftButton() {
  const { finalizeDraft } = useDraft();

  return (
    <Button variant="outlined" color="primary" onClick={finalizeDraft}>
      Finalize Draft
    </Button>
  );
}
