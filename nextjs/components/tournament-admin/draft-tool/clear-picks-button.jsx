import React, { useState } from 'react';
import { Button } from '@material-ui/core';

import { useDraft } from './context';
import { DeleteConfirmDialog } from '@/components/DeleteButton';

export function ClearPicksButton() {
  const { clearPicks } = useDraft();
  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);

  const openDeleteConfirm = () => {
    setDeleteConfirmOpen(true);
  };

  const handleClose = () => {
    setDeleteConfirmOpen(false);
  };

  const handleConfirm = () => {
    clearPicks();
    handleClose();
  };

  return (
    <>
      <Button variant="outlined" color="secondary" onClick={openDeleteConfirm}>
        Clear Picks
      </Button>
      <DeleteConfirmDialog
        confirmText="Are you sure you want to clear all selections?"
        open={deleteConfirmOpen}
        {...{ handleClose, handleConfirm }}
      />
    </>
  );
}
