import React, { useState } from 'react';

import Form from '@/components/forms/Form';
import Field, { Switch, TextField } from '@/components/fields/Field';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';

export function CabinetGroupForm({ queue, open }) {
  const { tournament, cabinets } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });

  const defaultSelected = {};
  for (const cabId of queue.cabinets) {
    defaultSelected[cabId] = true;
  }

  const [selectedCabs, setSelectedCabs] = useState(defaultSelected);

  const props = {
    buttonText: queue?.id ? 'Edit' : 'Create New',
    dialogTitle: queue?.id ? 'Editing Cabinet Group' : 'Creating New Cabinet Group',
    canEdit: () => true,
    object: queue,
    method: queue?.id ? 'PATCH' : 'PUT',
    url: queue?.id
      ? `/api/tournament/queue/${queue.id}/`
      : `/api/tournament/tournament/${tournament.id}/create-queue/`,
    onSave: response => {
      const selectedCabsList = Object.keys(selectedCabs).filter(id => selectedCabs[id]);
      axios.put(`/api/tournament/queue/${response.id}/set-cabinets/`, {
        cabinets: selectedCabsList,
      });
    },
    reloadOnSave: false,
  };

  const toggleCab = cabinetId => {
    setSelectedCabs(v => ({ ...v, [cabinetId]: !v[cabinetId] }));
  };

  return (
    <Form {...props}>
      <Field name="name" label="Name" component={TextField} />
      {cabinets?.map(cabinet => (
        <Field
          key={cabinet.id}
          name={`cabinet.${cabinet.id}`}
          label={cabinet.displayName}
          component={Switch}
          value={selectedCabs[cabinet.id]}
          onChange={() => toggleCab(cabinet.id)}
        />
      ))}
    </Form>
  );
}
