import React, { useEffect, useState } from 'react';
import { MenuItem } from '@material-ui/core';

import Field, {
  Conditional,
  Radio,
  RadioGroup,
  Select,
  TextField,
} from '@/components/fields/Field';
import { getAxios } from '@/util/axios';
import { TEAM_TYPE } from '@/util/constants';
import { useTournamentAdmin } from '../context';

export function RegisterTeamForm({ tournament }) {
  const axios = getAxios({ authenticated: true });
  const [allTeams, setAllTeams] = useState(null);

  useEffect(() => {
    axios
      .getAllPages(`/api/tournament/team/`, { params: { tournamentId: tournament.id } })
      .then(setAllTeams);
  }, []);

  return (
    <>
      <Field
        name="teamType"
        label="Team Selection"
        component={RadioGroup}
        helperText="Create a team or join an existing team. You can change this later."
      >
        <Radio
          value={TEAM_TYPE.FREE_AGENT}
          label="Free Agent"
          helperText="Complete registration without joining a team."
        />
        <Radio
          value={TEAM_TYPE.CREATE_TEAM}
          label="Create a Team"
          helperText="Create a new team and allow other players to join when they register."
        />
        {allTeams?.length > 0 && (
          <Radio
            value={TEAM_TYPE.JOIN_TEAM}
            label="Join a Team"
            helperText="Join a team that has already been created."
          />
        )}
      </Field>

      <Conditional test={v => v.teamType === TEAM_TYPE.JOIN_TEAM}>
        <Field name="team" label="Team to Join" component={Select}>
          {allTeams !== null &&
            allTeams?.map(team => (
              <MenuItem key={team.id} value={team.id}>
                {team.name}
              </MenuItem>
            ))}
        </Field>
      </Conditional>

      <Conditional test={v => v.teamType === TEAM_TYPE.CREATE_TEAM}>
        <Field name="teamName" label="Team Name" component={TextField} />
      </Conditional>
    </>
  );
}
