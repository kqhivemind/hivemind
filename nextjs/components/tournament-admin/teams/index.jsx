import React, { useEffect, useRef, useState } from 'react';
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  IconButton,
  MenuItem,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  mdiAccountCheckOutline,
  mdiAccountEditOutline,
  mdiAccountMultiplePlusOutline,
  mdiAccountPlusOutline,
  mdiCheckCircle,
  mdiClose,
  mdiDelete,
  mdiPencil,
} from '@mdi/js';
import Icon from '@mdi/react';
import clsx from 'clsx';
import { toast } from 'react-toastify';

import styles from './Teams.module.css';
import DeleteButton from '@/components/DeleteButton';
import Field, { Select, TextField } from '@/components/fields/Field';
import Form, { useForm } from '@/components/forms/Form';
import TournamentPlayerAdminForm from './player-admin-form';
import { ImageForm } from './image-form';
import { getAxios } from '@/util/axios';
import { SIGN_IN_ACTIONS } from '@/util/constants';
import { useWebSocket } from '@/util/websocket';
import { useTournamentAdmin } from '../context';

function TeamRowButton({ name, team, isSelected, handleTeamSelect }) {
  return (
    <li
      className={clsx(styles.teamRow, 'border border-solid border-gray-400 rounded py-2 px-4', {
        'bg-blue-light3 shadow-md border-blue-main': isSelected,
      })}
      onClick={() => handleTeamSelect()}
    >
      {name}
    </li>
  );
}

function RenameableTeamTitle({ tournament, team, successCallback }) {
  const axios = getAxios({ authenticated: true });
  const renameTeam = async () => {
    const teamInfo = { tournament: tournament.id, name: inputRef.current.value };
    const response = await axios.put(`/api/tournament/team/${team.id}/`, teamInfo);

    if (response.status === 200) {
      successCallback(response.data);
      setIsEditing(false);
    }
  };
  const [isEditing, setIsEditing] = useState(false);
  const inputRef = useRef(null);

  return (
    <div className="inline-flex items-center">
      {isEditing ? (
        <div className="border border-solid border-gray-300 bg-white rounded p-2 focus-within:border-blue-main">
          <input
            ref={inputRef}
            type="text"
            name="renameTeam"
            defaultValue={team.name}
            className="border-0 focus:outline-none"
            autoFocus
            onKeyDown={e => e.key === 'Enter' && renameTeam()}
          />
          <IconButton onClick={() => renameTeam()} className="-my-2 text-emerald-600">
            <Icon path={mdiCheckCircle} size={0.8} />
          </IconButton>
          <IconButton onClick={() => setIsEditing(!isEditing)} className="-my-2 -mr-2 -ml-2">
            <Icon path={mdiClose} size={0.8} />
          </IconButton>
        </div>
      ) : (
        <>
          <span>{team?.name}</span>{' '}
          <IconButton onClick={() => setIsEditing(!isEditing)}>
            <Icon path={mdiPencil} size={0.8} />
          </IconButton>
        </>
      )}
    </div>
  );
}

export function FormContents({ ...props }) {
  const axios = getAxios({ authenticated: true });
  const formik = useForm();

  const { tournament } = useTournamentAdmin();

  const [cabinets, setCabinets] = useState(null);
  const [teams, setTeams] = useState(tournament.teams);
  const [players, setPlayers] = useState(null);
  const [selectedTeam, setSelectedTeam] = useState(null);
  const [registeringPlayer, setRegisteringPlayer] = useState(null);
  const isSelected = team => team?.id == selectedTeam?.id;

  const ws = useWebSocket('/ws/signin');

  const loadPlayers = async (team, refetch = true) => {
    const params = { tournamentId: tournament.id };

    if (team) params.teamId = team.id;
    const response = await axios.getAllPages(`/api/tournament/player/`, {
      params,
    });
    setPlayers(team ? response : response.filter(p => !p.team));
  };

  const handleTeamSelect = async team => {
    setSelectedTeam(team);
    setPlayers(null);
    loadPlayers(team);
  };

  const addTeam = async () => {
    const newTeam = { tournament: tournament.id, name: formik.values.newTeamName };
    const response = await axios.post('/api/tournament/team/', newTeam);

    setTeams([...teams, response.data]);
    await handleTeamSelect(response.data);

    formik.setFieldValue('newTeamName', '');
  };

  const addPlayer = async () => {
    const newPlayer = {
      tournament: tournament.id,
      team: selectedTeam?.id,
      name: formik.values.newPlayerName,
    };
    const response = await axios.post('/api/tournament/player/', newPlayer);

    setPlayers([...players, response.data]);

    formik.setFieldValue('newPlayerName', '');
  };

  const deleteTeam = async team => {
    await axios.delete(`/api/tournament/team/${team.id}/`);
    handleTeamSelect(null);
    setTeams(teams => teams.filter(t => t.id !== team.id));
    toast.success(`Deleted team ${team.name}.`);
  };

  useEffect(() => {
    if (selectedTeam === null) {
      loadPlayers(null);
    }
  }, []);
  useEffect(() => {
    if (cabinets === null) {
      axios
        .getAllPages('/api/game/cabinet/', { params: { sceneId: tournament.scene.id } })
        .then(setCabinets);
    }
  }, [cabinets]);
  return (
    <>
      <section id="teams">
        <div className="grid grid-cols-2 gap-8 items-start">
          <div>
            <h3>Team Sheet</h3>
            <ul className="list-none p-0 flex flex-wrap gap-2">
              <TeamRowButton
                name="Free Agents"
                team={null}
                isSelected={selectedTeam === null}
                handleTeamSelect={() => handleTeamSelect(null)}
              />
              {teams
                .sort((a, b) => a.name.localeCompare(b.name))
                .map(team => (
                  <TeamRowButton
                    key={team.id}
                    team={team}
                    name={team.name}
                    isSelected={isSelected(team)}
                    handleTeamSelect={() => handleTeamSelect(team)}
                  />
                ))}
            </ul>
          </div>

          <div>
            <h3>Add New Team</h3>
            <div className="flex gap-2">
              <Field
                component={TextField}
                name="newTeamName"
                label="Enter Team Name"
                noGrid
                size="small"
                onKeyPress={evt => {
                  if (evt.key == 'Enter') addTeam();
                }}
              />
              <Button
                onClick={addTeam}
                variant="outlined"
                color="secondary"
                className="flex-shrink-0"
              >
                <Icon path={mdiAccountMultiplePlusOutline} size={1} className="mr-2" />
                <span>Add&nbsp;Team</span>
              </Button>
            </div>
          </div>
        </div>
      </section>
      <section className="bg-gray-100 rounded p-6">
        <div className="flex items-center justify-between">
          {selectedTeam ? (
            <>
              <h3 className="my-0">
                <RenameableTeamTitle
                  team={selectedTeam}
                  tournament={tournament}
                  successCallback={async data => {
                    const updatedTeams = teams.filter(t => t.id !== data.id);
                    setTeams([...updatedTeams, data]);
                    await handleTeamSelect(data);
                  }}
                />
              </h3>
              <DeleteButton
                onConfirm={() => deleteTeam(selectedTeam)}
                buttonText="Delete Team"
                confirmText={`Delete "${selectedTeam.name}"?`}
                className="ml-auto"
              />
            </>
          ) : (
            <h3 className="my-0">Free Agents</h3>
          )}
        </div>

        {selectedTeam && (
          <div>
            <ImageForm team={selectedTeam} setTeams={setTeams} />
          </div>
        )}

        <div>
          {players === null && <CircularProgress />}

          <ul className="grid grid-cols-2 gap-x-6 gap-y-3 list-none p-0 mb-6">
            {players?.map(player => (
              <li
                key={player.id}
                className={clsx('flex justify-between items-center rounded px-4 bg-gray-300 ', {
                  selected: registeringPlayer?.id === player.id,
                })}
              >
                <div>{player.name}</div>
                <div className="inline-flex gap-1 items-center">
                  {player.checkInTime !== null && (
                    <IconButton>
                      <Icon path={mdiAccountCheckOutline} size={1} />
                    </IconButton>
                  )}

                  <TournamentPlayerAdminForm
                    player={player}
                    team={selectedTeam}
                    buttonIconPath={mdiAccountEditOutline}
                  />
                  <DeletePlayerButton
                    player={player}
                    onSuccess={() => setPlayers(players.filter(p => p.id !== player.id))}
                  />
                </div>
              </li>
            ))}
          </ul>

          <div className="flex gap-2 w-1/2 pr-3">
            <Field
              component={TextField}
              name="newPlayerName"
              label="Add New Player"
              noGrid
              size="small"
              onKeyPress={evt => {
                if (evt.key == 'Enter') addPlayer();
              }}
            />
            <Button
              onClick={addPlayer}
              variant="outlined"
              color="secondary"
              className="flex-shrink-0"
            >
              <Icon path={mdiAccountPlusOutline} size={1} className="mr-2" />
              <span>Add&nbsp;Player</span>
            </Button>
          </div>
        </div>
      </section>
    </>
  );
}

function DeletePlayerButton({ player, onSuccess }) {
  const [open, setOpen] = useState(false);
  const axios = getAxios({ authenticated: true });
  const deletePlayer = async player => {
    await axios.delete(`/api/tournament/player/${player.id}/`);
    onSuccess();
    setOpen(false);
    toast.success(`Deleted player ${player.name}.`);
  };
  return (
    <>
      <IconButton onClick={() => setOpen(true)}>
        <Icon path={mdiDelete} size={1} />
      </IconButton>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className="shadow-sm"
      >
        <DialogTitle id="alert-dialog-title">
          Are you sure you want to delete this player?
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            This will completely remove them and their data from the tournament
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="neutral" variant="outlined">
            Nevermind
          </Button>
          <Button
            onClick={() => deletePlayer(player)}
            color=""
            variant="contained"
            className="bg-red-500 text-white"
          >
            Confirm Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

function Teams({ ...props }) {
  const { tournament } = useTournamentAdmin();

  if (!tournament) {
    return <CircularProgress />;
  }

  const formData = {
    newTeamName: '',
    newPlayerName: '',
    cabinet: '',
    readerId: 'registration',
  };

  return (
    <Form open={true} dialog={false} object={formData} hideSaveButton={true} {...props}>
      <FormContents />
    </Form>
  );
}

Teams.title = 'Team & Player Management';
export { Teams };
