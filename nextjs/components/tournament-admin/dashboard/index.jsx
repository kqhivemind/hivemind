import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Divider,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';

import { useAuth } from '@/util/auth';
import Title from '@/components/Title';
import TournamentMatchListTable from '@/components/tables/TournamentMatchListTable';
import TournamentTeamTable from '@/components/tables/TournamentTeamTable';
import TournamentQueueTable from '@/components/tables/TournamentQueueTable';
import { PERMISSION_TYPES } from '@/util/constants';

import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';
import { TournamentRefereeCabinet } from './referee-cabinet';

const validPermissions = [PERMISSION_TYPES.ADMIN, PERMISSION_TYPES.TOURNAMENT];

export function Dashboard() {
  const { tournament, cabinets, queues, reloadTournament, hideInactiveCabs, setHideInactiveCabs } =
    useTournamentAdmin();
  const { hasAnyPermission } = useAuth();

  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  const cabinetVisible = cabinet => {
    if (!hideInactiveCabs) return true;

    if (tournament?.assignedCabinets?.length > 0) {
      return tournament.assignedCabinets.includes(cabinet?.id);
    }

    return cabinet?.isOnline;
  };

  return (
    <>
      <Subtitle title={'Dashboard'} />
      <div className="space-y-0 py-4 px-4 pb-2 bg-gray-100 rounded-lg">
        <div className="flex justify-between items-center">
          <Typography variant="h2" className="m-0">
            Cabinets
          </Typography>
          {hasAnyPermission(tournament.scene.id, validPermissions) && (
            <>
              <Button
                variant="outlined"
                color="default"
                size="small"
                onClick={() => setHideInactiveCabs(i => !i)}
              >
                {hideInactiveCabs ? 'Show' : 'Hide'} Inactive Cabinets
              </Button>
            </>
          )}
        </div>
        <Grid container direction="row" spacing={4}>
          {cabinets.map(cabinet => (
            <TournamentRefereeCabinet cabinet={cabinet} />
          ))}

          <Grid item xs={12}>
            <Divider />
          </Grid>

          <Grid item xs={12} className="m-0">
            <Typography variant="h2" className="m-0 mt-2">
              Queues
            </Typography>
          </Grid>

          {queues?.map(queue => (
            <Grid item xs={12} md={6}>
              <TournamentQueueTable title={queue.name} queue={queue} isEditable={true} />
            </Grid>
          ))}
        </Grid>
      </div>
    </>
  );
}
