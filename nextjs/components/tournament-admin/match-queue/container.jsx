import { DragDropContext } from 'react-beautiful-dnd';
import { toast } from 'react-toastify';

import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';

export function TournamentQueueContainer({ children }) {
  const axios = getAxios({ authenticated: true });
  const { queues, matches, setQueue, tournament } = useTournamentAdmin();

  const onDragEnd = result => {
    if (!result.destination) return;

    const matchId = parseInt(result.draggableId);
    const match = matches?.filter(m => m.id === matchId).shift();
    const [srcType, srcId] = result.source.droppableId.split('-');
    const [destType, destId] = result.destination.droppableId.split('-');

    if (srcType === 'cabinet' && destType !== 'cabinet') {
      axios
        .patch(`/api/tournament/match/${matchId}/`, { activeCabinet: null })
        .catch(err => toast.error('Could not unassign match.'));
      axios.put(`/api/tournament/match/${matchId}/stop/`).catch(() => {});
    }

    if (destType === 'cabinet') {
      axios
        .patch(`/api/tournament/match/${matchId}/`, { activeCabinet: parseInt(destId) })
        .catch(err => toast.error('Could not assign match.'))
        .then(response => {
          if (response.data.success) {
            axios.put(`/api/tournament/match/${matchId}/start/`).catch(() => {});
          } else {
            toast.error(response.data.error);
          }
        });
    }

    // move within same queue
    if (srcType === 'queue' && destType === 'queue' && srcId === destId) {
      const queue = queues.filter(q => q.id === parseInt(srcId)).shift();
      queue.matchList.splice(result.source.index, 1);
      queue.matchList.splice(result.destination.index, 0, match);

      setQueue(queue);

      axios
        .put(`/api/tournament/queue/${srcId}/set-queue/`, {
          matches: queue.matchList.map(m => m.id),
        })
        .then(response => setQueue(response.data))
        .catch(err => toast.error('Could not remove match from queue.'));

      return;
    }

    // move between two queues - wait for remove, then add to new one
    if (srcType === 'queue' && destType === 'queue') {
      const srcQueue = queues.filter(q => q.id === parseInt(srcId)).shift();
      const destQueue = queues.filter(q => q.id === parseInt(destId)).shift();

      srcQueue.matchList.splice(result.source.index, 1);
      destQueue.matchList.splice(result.destination.index, 0, match);

      setQueue(srcQueue);
      setQueue(destQueue);

      axios
        .put(`/api/tournament/queue/${srcId}/set-queue/`, {
          matches: srcQueue.matchList.map(m => m.id),
        })
        .then(response => {
          setQueue(response.data);
          axios
            .put(`/api/tournament/queue/${destId}/set-queue/`, {
              matches: destQueue.matchList.map(m => m.id),
            })
            .then(response => setQueue(response.data))
            .catch(err => toast.error('Could not add match to queue.'));
        })
        .catch(err => toast.error('Could not remove match from queue.'));

      return;
    }

    if (srcType === 'queue') {
      const srcQueue = queues.filter(q => q.id === parseInt(srcId)).shift();
      srcQueue.matchList.splice(result.source.index, 1);

      setQueue(srcQueue);

      axios
        .put(`/api/tournament/queue/${srcId}/set-queue/`, {
          matches: srcQueue.matchList.map(m => m.id),
        })
        .then(response => setQueue(response.data))
        .catch(err => toast.error('Could not remove match from queue.'));
    }

    if (destType === 'queue') {
      const destQueue = queues.filter(q => q.id === parseInt(destId)).shift();
      destQueue.matchList.splice(result.destination.index, 0, match);

      setQueue(destQueue);

      axios
        .put(`/api/tournament/queue/${destId}/set-queue/`, {
          matches: destQueue.matchList.map(m => m.id),
        })
        .then(response => setQueue(response.data))
        .catch(err => toast.error('Could not add match to queue.'));
    }
  };

  return <DragDropContext onDragEnd={onDragEnd}>{children}</DragDropContext>;
}
