import { Fragment, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CircularProgress, Grid, Menu, MenuItem, Typography } from '@material-ui/core';
import { Droppable } from 'react-beautiful-dnd';
import { bindMenu, bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import { toast } from 'react-toastify';

import styles from './MatchQueue.module.css';
import { getAxios } from '@/util/axios';
import { TOURNAMENT_LINK_TYPES } from '@/util/constants';
import { useTournamentAdmin } from '../context';
import { TournamentQueueMatch } from './match';

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: 'white',
  },
  row: {
    padding: theme.spacing(1, 2),
    alignItems: 'center',
    '&:first-child': {
      borderWidth: 0,
    },
  },
  stageNameRow: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.gold.light3,
    '.online &': {
      backgroundColor: theme.palette.gold.light3,
    },
  },
  stageName: {
    fontWeight: 'bold',
  },
  menuIcon: {
    cursor: 'pointer',
  },
}));

export function AssignAllMenu({ bracket, menuState, filteredMatches }) {
  const axios = getAxios({ authenticated: true });
  const { cabinets, matches, queues, visibleQueues, setQueue } = useTournamentAdmin();

  const assignAll = queueId => {
    const queue = queues.filter(q => q.id === queueId).shift();
    queue.matchList = queue.matchList.concat(filteredMatches);

    axios
      .put(`/api/tournament/queue/${queue.id}/set-queue/`, {
        matches: queue.matchList.map(m => m.id),
      })
      .catch(err => toast.error('Could not assign matches to queue.'));

    setQueue(queue);
    menuState.close();
  };

  return (
    <Menu
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      transformOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={styles.menu}
      {...bindMenu(menuState)}
    >
      {visibleQueues.map(queue => (
        <MenuItem key={queue.id} className={styles.menuItem} onClick={() => assignAll(queue.id)}>
          Assign All to {queue.name}
        </MenuItem>
      ))}
    </Menu>
  );
}

export function TournamentQueueBracket({ bracket, queuedIds }) {
  const menuState = usePopupState({ popupId: `assign-all-menu-${bracket.id}` });
  const { tournament, matches } = useTournamentAdmin();

  const filteredMatches = useMemo(() => {
    if (matches === null) return null;

    const filtered = matches.filter(
      m => m.bracket === bracket.id && !queuedIds.has(m.id) && !m.activeCabinet && !m.isComplete,
    );

    filtered.sort((a, b) => { return (a.matchNum - b.matchNum || a.id - b.id) });

    return filtered;
  }, [matches, tournament.linkType, bracket.id, queuedIds]);

  if (matches === null) return <></>;

  return (
    <Fragment key={bracket.id}>
      <Grid key={bracket.id} item className={clsx(styles.row, styles.stageNameRow)}>
        <Typography className={styles.stageName}>{bracket.name}</Typography>
        <MenuIcon className={styles.menuIcon} {...bindTrigger(menuState)} />
        <AssignAllMenu menuState={menuState} bracket={bracket} filteredMatches={filteredMatches} />
      </Grid>

      {filteredMatches.map((match, idx) => (
        <TournamentQueueMatch key={match.id} className={styles.row} match={match} index={idx} />
      ))}
    </Fragment>
  );
}

export function TournamentQueueMatchList({}) {
  const { tournament, queues } = useTournamentAdmin();
  const queuedIds = new Set();

  if (queues === null) {
    return <CircularProgress />;
  }

  for (const queue of queues) {
    for (const match of queue.matchList) {
      queuedIds.add(match.id);
    }
  }

  return (
    <Grid item>
      <Card className={styles.card}>
        <Droppable droppableId="unassigned">
          {(provided, snapshot) => (
            <Grid container ref={provided.innerRef} direction="column" className={styles.container}>
              {tournament?.brackets.map(bracket => (
                <TournamentQueueBracket key={bracket.id} bracket={bracket} queuedIds={queuedIds} />
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </Card>
    </Grid>
  );
}
