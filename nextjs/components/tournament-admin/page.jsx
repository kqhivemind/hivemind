import React, { useMemo } from 'react';
import { Breadcrumbs, Button, CircularProgress, Grid, Link, Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import Title from 'components/Title';
import { TOURNAMENT_LINK_TYPES, PERMISSION_TYPES } from '@/util/constants';
import { useAuth } from '@/util/auth';

import styles from './Page.module.css';
import { TournamentAdminProvider, useTournamentAdmin } from './context';
import { MenuIcon } from './menu';
import { BasicSettings } from './basic-settings';
import { Templates } from './templates';
import { ConfigureRegistration } from './configure-registration';
import { Links } from './links';
import { Teams } from './teams';
import { DraftTool } from './draft-tool';
import { RegisteredList } from './registered-list';
import { Stages } from './stages';
import { TeamPlacement } from './team-placement';
import { PaymentProcessor } from './payment-processor';
import { Cabinets } from './cabinets';
import { Videos } from './videos';
import { Dashboard } from './dashboard';
import { CurrentMatches } from './current-matches';
import { EditMatchResults } from './edit-match-results';
import { MatchQueue } from './match-queue';

export function TournamentAdminPage() {
  const { tournament, reloadTournament } = useTournamentAdmin();
  const { user, userLoading, hasAnyPermission } = useAuth();
  const router = useRouter();

  const pages = {
    null: BasicSettings,
    templates: Templates,
    'configure-registration': ConfigureRegistration,
    links: Links,
    teams: Teams,
    'draft-tool': DraftTool,
    'registered-list': RegisteredList,
    stages: Stages,
    'team-placement': TeamPlacement,
    'payment-processor': PaymentProcessor,
    cabinets: Cabinets,
    videos: Videos,
    dashboard: Dashboard,
    'current-matches': CurrentMatches,
    'edit-match-results': EditMatchResults,
    'match-queue': MatchQueue,
  };

  const adminOnly = [PERMISSION_TYPES.ADMIN];
  const tournamentOfficial = [PERMISSION_TYPES.ADMIN, PERMISSION_TYPES.TOURNAMENT];

  const requiredPermissions = {
    null: adminOnly,
    templates: adminOnly,
    'configure-registration': adminOnly,
    links: adminOnly,
    teams: adminOnly,
    'draft-tool': adminOnly,
    'registered-list': adminOnly,
    stages: adminOnly,
    'team-placement': adminOnly,
    'payment-processor': adminOnly,
    cabinets: adminOnly,
    videos: adminOnly,
    dashboard: tournamentOfficial,
    'current-matches': tournamentOfficial,
    'edit-match-results': tournamentOfficial,
    'match-queue': tournamentOfficial,
  };

  const [componentName, ...queryParams] = router?.query?.component ?? [null, []];
  const PageObject = pages[componentName] ?? null;

  if (!tournament) {
    return <CircularProgress />;
  }

  const requiredPermission = requiredPermissions[componentName];
  if (!requiredPermission || !hasAnyPermission(tournament?.scene?.id, requiredPermission)) {
    return userLoading ? <CircularProgress /> : <div>Not Authorized</div>;
  }

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/t/${tournament.sceneName}/${tournament.slug}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title
        className={styles.title}
        title={tournament?.name ?? 'Tournament Admin'}
        form={<MenuIcon />}
      />

      {PageObject && <PageObject queryParams={queryParams} />}
    </div>
  );
}
