import { useState } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import Icon from '@mdi/react';
import { mdiMenuUp, mdiMenuDown, mdiDragVertical } from '@mdi/js';
import clsx from 'clsx';

import styles from './team-placement.module.css';
import { useTeamPlacement } from './context';

export function List({ id, name, children, icons, className, droppable = true } = {}) {
  const [collapsed, setCollapsed] = useState(false);

  const toggleCollapsed = () => setCollapsed(v => !v);

  return (
    <Grid container direction="column" className={clsx(styles.list, className)}>
      <Grid
        item
        container
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        className={clsx(styles.listTitle, { collapsed })}
      >
        <Grid item>{name}</Grid>
        <Grid item>
          {!collapsed && icons}
          <Icon
            className={styles.menuIcon}
            onClick={toggleCollapsed}
            path={collapsed ? mdiMenuUp : mdiMenuDown}
            size={1}
          />
        </Grid>
      </Grid>

      {!collapsed && droppable && (
        <Droppable droppableId={id}>
          {(provided, snapshot) => (
            <Grid
              item
              container
              direction="column"
              ref={provided.innerRef}
              className={styles.listBody}
            >
              {children}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      )}

      {!collapsed && !droppable && (
        <Grid item container direction="column" className={styles.listBody}>
          {children}
        </Grid>
      )}
    </Grid>
  );
}

export function ListItem({
  id,
  index,
  name,
  buttons,
  draggable = true,
  showPlaceNum = false,
} = {}) {
  if (draggable) {
    return (
      <Draggable draggableId={id} index={index}>
        {(provided, snapshot) => (
          <Grid
            item
            container
            ref={provided.innerRef}
            className={styles.listItem}
            {...provided.draggableProps}
          >
            <Grid item className={styles.dragHandle} {...provided.dragHandleProps}>
              <Icon path={mdiDragVertical} size={1} />
            </Grid>

            {showPlaceNum && <span className={styles.placeNum}>{index + 1}. </span>}
            <span className={styles.listText}>{name}</span>

            {buttons && (
              <Grid item className={styles.buttons}>
                {buttons}
              </Grid>
            )}
          </Grid>
        )}
      </Draggable>
    );
  }

  return (
    <Grid item className={styles.listItem}>
      <span className={styles.listText}>{name}</span>

      {buttons && (
        <Grid item className={styles.buttons}>
          {buttons}
        </Grid>
      )}
    </Grid>
  );
}
