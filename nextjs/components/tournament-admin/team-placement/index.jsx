import React, { useState } from 'react';
import { Divider } from '@material-ui/core';

import { STAGE_TYPE } from '@/util/constants';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';
import { TeamPlacementTable } from './table';
import { TeamPlacementProvider } from './context';
import { StageSelect } from './stage-select';
import { SaveButton } from './save-button';
import styles from './team-placement.module.css';

export function TeamPlacement({}) {
  const { tournament } = useTournamentAdmin();

  if (!(tournament?.brackets?.length > 0)) {
    return (
      <div>
        Teams cannot be assigned yet because stages have not been created. Select "Stages" from the
        tournament admin menu to create at least one stage.
      </div>
    );
  }

  if (!(tournament?.teams.length > 0)) {
    return (
      <div>
        Teams cannot be assigned yet because none have been created. Select "Teams" from the
        tournament admin menu to create teams for this tournament.
      </div>
    );
  }

  return (
    <TeamPlacementProvider>
      <Subtitle title="Team Placement" buttons={<SaveButton />} />
      <StageSelect />

      <Divider />

      <TeamPlacementTable />
    </TeamPlacementProvider>
  );
}
