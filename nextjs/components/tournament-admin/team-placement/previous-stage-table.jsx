import React, { useState, useMemo } from 'react';
import ordinal from 'ordinal';
import { IconButton } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiPlusCircleOutline } from '@mdi/js';

import { List, ListItem } from './list';
import { PlaceType } from './constants';
import { PreviousStageTableMenu } from './previous-stage-table-menu';
import { useTeamPlacement } from './context';

export function AddPlaceButton({ prevStage, place }) {
  const { setPlacements, setChanged } = useTeamPlacement();

  const prevPlace = `${ordinal(parseInt(place.place))} Place`;
  const placeId = `${prevStage.id}.${place.place}`;
  const name = `${prevStage.name} - ${prevPlace}`;

  const placement = {
    placeType: PlaceType.PrevStage,
    srcBracket: parseInt(prevStage.id),
    srcPlace: parseInt(place.place),
    placeId,
    name,
    key: `${PlaceType.PrevStage}-${placeId}`,
  };

  const onClick = () => {
    setPlacements(val => [...val, placement]);
    setChanged(true);
  };

  return (
    <IconButton onClick={onClick} aria-label="Add to stage">
      <Icon path={mdiPlusCircleOutline} size={1} />
    </IconButton>
  );
}

export function PreviousStageTable({ prevStage }) {
  const { placements, placementsByStage } = useTeamPlacement();
  const stagePlacements = useMemo(() => {
    if (placementsByStage && placementsByStage[prevStage?.id]) {
      return placementsByStage[prevStage.id]
        .map((p, idx) => ({ ...p, place: idx + 1 }))
        .filter(
          p1 =>
            placements.filter(p2 => p2.srcBracket === prevStage.id && p2.srcPlace === p1.place)
              .length === 0,
        );
    }
  }, [placements, placementsByStage, prevStage?.id]);

  if (!stagePlacements?.length) return null;

  return (
    <List
      id={`${PlaceType.PrevStage}-${prevStage.id}`}
      name={prevStage.name}
      icons={<PreviousStageTableMenu prevStage={prevStage} />}
    >
      {stagePlacements.map(sp => (
        <ListItem
          key={sp.id}
          name={`${ordinal(sp.place)} Place`}
          value={sp.place}
          index={sp.place}
          draggable={false}
          buttons={<AddPlaceButton prevStage={prevStage} place={sp} />}
          id={`${PlaceType.PrevStage}-${prevStage.id}.${sp.place}`}
        />
      ))}
    </List>
  );
}
