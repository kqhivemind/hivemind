import React from 'react';
import { Grid, Link, Typography } from '@material-ui/core';

import { TournamentForm } from './form';
import { useTournamentAdmin } from '../context';

function BasicSettings() {
  const { tournament, reloadTournament } = useTournamentAdmin();

  return (
    <>
      <TournamentForm tournament={tournament} />

      <Grid container direction="row" spacing={4}>
        <Grid item xs={12} md={6}></Grid>
        <Grid item xs={12} md={6}></Grid>
      </Grid>
    </>
  );
}

BasicSettings.title = 'Basic Settings';
export { BasicSettings };
