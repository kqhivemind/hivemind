import React, { useMemo } from 'react';
import clsx from 'clsx';
import { Switch } from '@material-ui/core';

import styles from './Whiteboard.module.css';
import DeleteButton from '@/components/DeleteButton';
import { getAxios } from '@/util/axios';
import { useWhiteboard } from './context';

export function ToggleActiveButton({ signup }) {
  const axios = getAxios({ authenticated: true });
  const { session, bluePlayerIds, goldPlayerIds } = useWhiteboard();

  const addSignup = () => {
    axios.put(`/api/whiteboard/session/${session.id}/add-player/`, {
      signupId: signup.id,
    });
  };

  const removeSignup = () => {
    axios.put(`/api/whiteboard/session/${session.id}/remove-player/`, {
      signupId: signup.id,
    });
  };

  const toggleReady = () => (signup.isReady ? removeSignup() : addSignup());

  const isInCurrentMatch = useMemo(() => {
    return bluePlayerIds?.has(signup.playerId) || goldPlayerIds?.has(signup.playerId);
  }, [signup, bluePlayerIds, goldPlayerIds]);

  if (isInCurrentMatch) {
    return (
      <DeleteButton
        className={clsx(styles.button, styles.deleteButton)}
        onConfirm={removeSignup}
        buttonText="Remove"
        confirmText={
          <>
            <div>Remove {signup.user.name} from queue?</div>
            <div>
              <b>This will regenerate the current match.</b>
            </div>
          </>
        }
      />
    );
  }

  return (
    <>
      <span className={clsx(styles.readyText, { active: signup.isReady })}>
        {signup.isReady ? 'Ready' : 'Not Ready'}
      </span>
      <Switch checked={signup.isReady} onClick={toggleReady} />
    </>
  );
}
