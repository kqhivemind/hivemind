import React from 'react';

import styles from './Whiteboard.module.css';
import LoginButtons from '@/components/LoginButtons';
import { useAuth } from '@/util/auth';
import { WhiteboardProvider } from './context';
import { SessionStatus } from './session_status';
import { PlayerStatus } from './player_status';
import { CurrentMatch } from './current_match';
import { SessionAdminButton } from './session_admin_button';

export function Whiteboard({ session }) {
  const { user, reloadUser } = useAuth();

  return (
    <WhiteboardProvider session={session}>
      <div className={styles.page}>
        <div className={styles.mainPageStatus}>
          <SessionStatus />
          <SessionAdminButton />

          {user?.id ? (
            <PlayerStatus />
          ) : (
            <div className={styles.signIn}>
              Log in to add yourself to the queue. <LoginButtons onSuccess={reloadUser} />
            </div>
          )}
        </div>

        <CurrentMatch />
      </div>
    </WhiteboardProvider>
  );
}
