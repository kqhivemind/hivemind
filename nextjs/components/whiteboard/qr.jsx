import React, { useState, useEffect } from 'react';
import QRCode from 'qrcode';
import clsx from 'clsx';

import styles from './Whiteboard.module.css';
import { useWhiteboard } from './context';

export function QR() {
  const [dataUrl, setDataUrl] = useState(null);
  const { session } = useWhiteboard();

  if (!session?.id) return null;

  const url = `${window.location.origin}/whiteboard/${session.id}`;

  useEffect(() => {
    QRCode.toDataURL(url).then(setDataUrl);
  }, [url]);

  return (
    <div className={styles.qrContainer}>
      <div className={clsx(styles.text, styles.headerText)}>Scan to join this session</div>

      <div className={styles.qrImage}>{dataUrl && <img src={dataUrl} />}</div>

      <div className={styles.text}>
        Or visit <a href={url}>{url}</a>
      </div>
    </div>
  );
}
