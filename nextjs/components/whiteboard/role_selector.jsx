import React from 'react';
import clsx from 'clsx';
import Icon from '@mdi/react';

import { useForm } from '@/components/forms/Form';
import Field, { Switch } from '@/components/fields/Field';
import styles from './Whiteboard.module.css';

export function RoleSelector({ name, label, iconPath, ...props }) {
  const form = useForm();
  const active = form.values[name];

  const onClick = () => {
    form.setFieldValue(name, !active);
  };

  return (
    <div className={clsx(styles.roleSelector, { active })} {...{ onClick }} {...props}>
      <div className={styles.icon}>
        <Icon path={iconPath} size={2} />
      </div>
      <div className={styles.label}>{label}</div>
      <div className={styles.roleSelectorSwitch}>
        <Field name={name} value={active} component={Switch} />
      </div>
    </div>
  );
}
