import React, { useMemo } from 'react';
import Icon from '@mdi/react';

import styles from './Whiteboard.module.css';
import { useAuth } from '@/util/auth';
import { useWhiteboard } from './context';
import { TogglePauseButton } from './toggle_pause_button';
import { ROLES, roleIcon } from './constants';

export function SessionStatus() {
  const { isAdminOf } = useAuth();
  const { scene, session, status, currentMatch, playerList } = useWhiteboard();

  const statusText = useMemo(() => {
    if (!session?.id) return 'No session is currently active.';
    if (!status?.isActive) return 'Session has ended.';
    if (status.isPaused) return 'Session is paused.';
    if (currentMatch === null)
      return 'Session is active. Games will begin once enough players are ready.';
    if (session.isLastSet) return 'Session will end after the current set.';
    return 'Session is active.';
  }, [session, status, currentMatch]);

  const countByRole = useMemo(() => {
    if (!playerList) return null;

    return playerList
      .filter(p => p.isReady)
      .reduce((acc, player) => {
        for (const role of player.roles) {
          acc[role] = (acc[role] || 0) + 1;
        }

        return acc;
      }, {});
  }, [playerList]);

  return (
    <>
      <div className={styles.sessionStatus}>{statusText}</div>

      {isAdminOf(scene.id) && status?.isActive && (
        <div className={styles.sessionStatus}>
          <TogglePauseButton />
        </div>
      )}

      {status?.playersQueued !== undefined && (
        <div className={styles.sessionStatus}>
          <b>{status.playersQueued}</b> players are currently signed up to play.
        </div>
      )}

      {countByRole && (
        <div className={styles.countByRole}>
          {ROLES.map(role => (
            <div className={styles.role}>
              <Icon path={roleIcon[role]} size={1} />
              {countByRole[role] ?? 0}
            </div>
          ))}
        </div>
      )}
    </>
  );
}
