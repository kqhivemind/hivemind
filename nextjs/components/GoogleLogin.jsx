import React, { useEffect, useRef } from 'react';
import getConfig from 'next/config';
import { setAuthTokens } from 'axios-jwt';

import { getAxios } from 'util/axios';
import { useAuth } from 'util/auth';

export function useScript(url, onLoad) {
  useEffect(() => {
    const script = document.createElement('script');

    script.src = url;
    script.onload = onLoad;

    document.head.appendChild(script);

    return () => {
      document.head.removeChild(script);
    };
  }, [url, onLoad]);
}

export default function GoogleLogin({ onSuccess, onFailure }) {
  const axios = getAxios();
  const googleSignInButton = useRef(null);
  const { reloadUser } = useAuth();
  const { publicRuntimeConfig } = getConfig();

  const onSignIn = async ({ ...props }) => {
    const response = await axios.post('/api/user/exchange-token/google-oauth2/', { ...props });

    await setAuthTokens({
      accessToken: response.data.accessToken,
      refreshToken: response.data.refreshToken,
    });

    reloadUser();
  };

  if (!publicRuntimeConfig?.OAUTH_CLIENT_ID) {
    return <></>;
  }

  useScript('https://accounts.google.com/gsi/client', () => {
    window.google.accounts.id.initialize({
      client_id: publicRuntimeConfig?.OAUTH_CLIENT_ID,
      callback: onSignIn,
    });
    window.google.accounts.id.renderButton(googleSignInButton.current, {
      theme: 'outline',
      size: 'medium',
      text: 'signin',
    });
  });

  return <div className="google-sign-in" ref={googleSignInButton}></div>;
}
