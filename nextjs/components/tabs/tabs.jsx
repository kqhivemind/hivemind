import Icon from '@mdi/react';
import clsx from 'clsx';
import { createContext, useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import styles from './tabs.module.css';

const TabContext = createContext(null);

export function TabSection({ name, title, isDefault, icon, onSelect, children }) {
  const { addTab, selectedTab } = useContext(TabContext);

  useEffect(() => {
    addTab({ name, title, isDefault, icon, onSelect });
  }, [name, title, isDefault, icon]);

  if (selectedTab === name) {
    return <>{children}</>;
  }

  return null;
}

export function TabHeader() {
  const { tabs, selectedTab, onTabClick } = useContext(TabContext);

  return (
    <div className={styles.tabHeader}>
      {tabs.map(tab => (
        <div
          onClick={() => onTabClick(tab.name)}
          key={tab.name}
          className={clsx(styles.tab, { [styles.selected]: selectedTab === tab.name })}
        >
          {tab.icon && <Icon path={tab.icon} className={styles.icon} />}
          <span className={styles.tabTitle}>{tab.title}</span>
        </div>
      ))}
    </div>
  );
}

export function Tabs({ hashnav, children }) {
  const router = useRouter();
  const [tabs, setTabs] = useState([]);
  const [selectedTab, setSelectedTab] = useState(router?.asPath.split('#')[1] ?? null);
  const [onSelectHooks, setOnSelectHooks] = useState({});

  const onTabClick = name => {
    setSelectedTab(name);

    if (hashnav) {
      router.replace(`${router.asPath.split('#')[0]}#${name}`, null, { shallow: true });
    }

    if (onSelectHooks[name]) {
      onSelectHooks[name]();
    }
  };

  const tabContext = {
    tabs,
    selectedTab,
    addTab: ({ name, title, isDefault, icon, onSelect }) => {
      setTabs(val => {
        if (val.filter(t => t.name === name).length > 0) {
          return val;
        }

        if (isDefault) {
          setSelectedTab(s => {
            if (s === null) {
              return name;
            }

            return s;
          });
        }

        if (onSelect) {
          setOnSelectHooks(v => ({ ...v, [name]: onSelect }));
        }

        return [...val, { name, title, icon }];
      });
    },
    onTabClick,
  };

  return (
    <TabContext.Provider value={tabContext}>
      <TabHeader />

      {children}
    </TabContext.Provider>
  );
}
