import React from 'react';

import { LoginButton } from './login-button';
import { LinkAccount } from './link-account';
import Icon from './discord.svg';

const params = {
  provider: 'discord',
  url: 'https://discord.com/oauth2/authorize',
  scope: 'identify email',
  Icon,
  iconColor: '#5865F2',
  clientIdConfig: 'DISCORD_CLIENT_ID',
};

export function DiscordLogin() {
  return <LoginButton title="Log in with Discord" {...params} />;
}

export function LinkDiscord() {
  return (
    <LinkAccount
      title="Connect your Discord account"
      connectedText="Discord account connected."
      {...params}
    />
  );
}
