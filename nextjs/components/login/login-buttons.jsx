import React, { useState } from 'react';
import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';

import { GoogleLogin } from './google';
import { DiscordLogin } from './discord';
import { TwitchLogin } from './twitch';
import styles from './login-buttons.module.css';

export function LoginButtons({ className, children }) {
  const [open, setOpen] = useState(false);
  const onClick = evt => {
    evt.preventDefault();
    evt.stopPropagation();
    setOpen(true);
  };

  return (
    <>
      <a className={className} href="#" onClick={onClick}>
        {children ?? <>Log in</>}
      </a>

      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>Log in to HiveMind</DialogTitle>
        <DialogContent>
          <div className={styles.loginButtons}>
            <GoogleLogin />
            <DiscordLogin />
            <TwitchLogin />
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
}
