import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import PlayerNames from './default/PlayerNames';
import TeamNameBox from './default/TeamNameBox';
import EventName from 'overlay/components/EventName';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '1920px',
    height: '1080px',
    background: 'black',
  },
  cameraBox: {
    position: 'absolute',
    top: '200px',
    width: '864px',
    height: '486px',
  },
  blueBox: {
    left: '48px',
    backgroundColor: theme.palette.blue.light5,
    boxShadow: `0 0 15px 10px ${theme.palette.blue.light3}7f`,
  },
  goldBox: {
    left: '1004px',
    backgroundColor: theme.palette.gold.light5,
    boxShadow: `0 0 15px 10px ${theme.palette.gold.light3}7f`,
  },
  teamName: {
    position: 'absolute',
    top: '866px',
    width: '864px',
  },
  teamNameBlue: {
    left: '48px',
  },
  teamNameGold: {
    left: '1004px',
  },
  players: {
    position: 'absolute',
    top: '716px',
    width: '864px',
    height: '100px',
  },
  playersBlue: {
    left: '48px',
  },
  playersGold: {
    left: '1004px',
  },
}));

export default function DefaultOverlay({ }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, "player-cams")}>
      <div className={clsx(classes.cameraBox, classes.blueBox, "blue-box")}></div>
      <div className={clsx(classes.cameraBox, classes.goldBox, "gold-box")}></div>

      <PlayerNames team={BLUE_TEAM} className={clsx(classes.players, classes.playersBlue, "players", "blue")} />
      <PlayerNames team={GOLD_TEAM} className={clsx(classes.players, classes.playersGold, "players", "gold")} />

      <TeamNameBox team={BLUE_TEAM} className={clsx(classes.teamName, classes.teamNameBlue, "team-name", "blue")} />
      <TeamNameBox team={GOLD_TEAM} className={clsx(classes.teamName, classes.teamNameGold, "team-name", "gold")} />
    </div>
  );
}

DefaultOverlay.themeProps = {
  name: 'default',
  size: { w: 1920, h: 1080 },
  blueCameraWindow: { w: 864, h: 486, x: 48, y: 200 },
  goldCameraWindow: { w: 864, h: 486, x: 1004, y: 200 },
};
