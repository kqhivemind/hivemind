import React from 'react';
import clsx from 'clsx';
import { format, parseISO } from 'date-fns';
import { mdiSword, mdiSnail, mdiFruitGrapes } from '@mdi/js';
import Icon from '@mdi/react';

import styles from '../Wildflower.module.css';

const winConditionIcon = {
  military: mdiSword,
  economic: mdiFruitGrapes,
  snail: mdiSnail,
};

export default function GameResult({ className, game, idx, ...props }) {
  if (!game) {
    return <></>;
  }

  return (
    <div className={clsx(styles.gameResult, 'game-result', game.winningTeam)}>
      <div className={clsx(styles.gameNumber, 'game-number')}>{idx + 1}</div>

      <div className={clsx(styles.winTypeIndicator, 'win-type-indicator')}>
        {winConditionIcon[game.winCondition] && <Icon path={winConditionIcon[game.winCondition]} />}
      </div>

      <div className={clsx(styles.mapName, 'map-name')}>{game.mapName}</div>

      <div className={clsx(styles.gameTime, 'game-time')}>
        {format(parseISO(game.endTime) - parseISO(game.startTime), 'm:ss')}
      </div>
    </div>
  );
}
