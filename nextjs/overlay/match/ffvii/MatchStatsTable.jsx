import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { formatTime } from 'util/dates';
import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';

const useStyles = makeStyles(theme => ({
  table: {
    width: '100%',
    margin: '0 auto',
    padding: '12px 24px',
    display: 'flex',
    flexDirection: 'column',

    // Standard border config
    border: '4px groove white',
    boxShadow: 'inset 0 0 4px #000000aa',
    borderRadius: '8px',
    color: 'white',
    textShadow: '1.5px 1.5px #000000',
    background: 'rgba(0,0,0,0.8)',
    // End standard border config

    fontWeight: 'bold',
    fontFamily: 'Reactor7, sans-serif',
  },
}));

export default function MatchSummary({ className }) {
  const classes = useStyles();
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div className={clsx(classes.table, className)}>
      <div className={classes.headerRow}>
        <PlayerIconsRow className={'flex'} />
        <StatsRow title="K/D" getValue={stat => `${stat.kills}-${stat.deaths}`} />
        <StatsRow
          title="Military K/D"
          getValue={stat => `${stat.militaryKills}-${stat.militaryDeaths}`}
        />
        <StatsRow title="Queen Kills" statKey="queenKills" />
        <StatsRow title="Bump Assists" statKey="bumpAssists" />
        <StatsRow
          title="Warrior Uptime"
          getValue={stat => formatTime(stat.warriorUptime)}
          defaultText=""
        />
        <StatsRow title="Berries Deposited" statKey="berries" />
        <StatsRow
          title="Snail Distance"
          getValue={stat => `${stat.snailDistancePct}%`}
          showForQueen={false}
        />
      </div>
    </div>
  );
}
