import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import {
  BLUE_TEAM,
  CABINET_POSITIONS,
  GOLD_TEAM,
  isQueen,
  POSITIONS_BY_TEAM,
} from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    marginBottom: '15px',
    alignItems: 'center',
  },
  cell: {
    textAlign: 'center',
    flexGrow: 1,
    flexBasis: 0,
    fontWeight: 'bold',
  },
  stats: {},
  title: {},
}));

export default function StatsRow({
  className,
  title,
  statKey,
  getValue,
  defaultText,
  showForQueen,
  ...props
}) {
  const classes = useStyles();
  const match = useMatchStats();
  const settings = useOverlaySettings();

  if (!match?.stats?.byPlayer) {
    return <></>;
  }

  const statsByPlayer = {};
  for (const pos of Object.values(CABINET_POSITIONS)) {
    statsByPlayer[pos.ID] = {};
    for (const [key, values] of Object.entries(match.stats.byPlayer)) {
      statsByPlayer[pos.ID][key] = values[pos.ID] || defaultText;
    }
  }

  const getStatValue = pos => {
    if (!showForQueen && isQueen(pos)) {
      return '';
    }

    if (getValue) {
      return getValue(statsByPlayer[pos.ID]);
    } else {
      return statsByPlayer[pos.ID][statKey];
    }
  };

  return (
    <div className={clsx(classes.row, className)}>
      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.blue, 'text-2xl py-1')}>
          {getStatValue(pos)}
        </div>
      ))}

      <div
        className={clsx(classes.cell, classes.title, 'basis-[300px] flex-grow-0  text-xl bb5-font')}
      >
        {title}
      </div>

      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM]].reverse().map(pos => (
        <div
          key={pos.ID}
          className={clsx(classes.cell, classes.gold, classes.stats, 'text-2xl py-1')}
        >
          {getStatValue(pos)}
        </div>
      ))}
    </div>
  );
}

StatsRow.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  statKey: PropTypes.string,
  getValue: PropTypes.func,
  defaultText: PropTypes.string,
  showForQueen: PropTypes.bool,
};

StatsRow.defaultProps = {
  defaultText: '0',
  showForQueen: true,
};
