import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { formatTime } from 'util/dates';
import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';

const useStyles = makeStyles(theme => ({
  table: {
    width: '90%',
    margin: '1px auto',
    padding: '12px 24px',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,

    color: '#000',
    // textShadow: '#1A1A1A 1px 1px',

    borderRadius: '8px',
    border: '4px solid transparent',
    background: 'white',
    boxShadow: '0 3px px rgba(0,0,0,.5)',
  },
}));

export default function MatchSummary({ className }) {
  const classes = useStyles();
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div className={clsx(classes.table, className, 'bb5-stat-font')}>
      <PlayerIconsRow className={'flex'} />
      <StatsRow title="K/D" getValue={stat => `${stat.kills}-${stat.deaths}`} />
      <StatsRow
        title="Military K/D"
        getValue={stat => `${stat.militaryKills}-${stat.militaryDeaths}`}
      />
      <StatsRow title="Queen Kills" statKey="queenKills" />
      <StatsRow title="Bump Assists" statKey="bumpAssists" />
      <StatsRow
        title="Warrior Uptime"
        getValue={stat => formatTime(stat.warriorUptime)}
        defaultText=""
      />
      <StatsRow title="Berries Deposited" statKey="berries" />
      <StatsRow
        title="Snail Distance"
        getValue={stat => `${stat.snailDistancePct}%`}
        showForQueen={false}
      />
    </div>
  );
}
