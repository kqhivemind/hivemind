import clsx from 'clsx';
import { useMatchStats } from 'overlay/MatchStats';
import GameResult from './GameResult';

export default function GameListTable({ className }) {
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div id="matchSummaryGameList" className={clsx('flex flex-col gap-2 flex-grow', className)}>
      {match?.games &&
        match.games
          .sort((a, b) => new Date(a.endTime) - new Date(b.endTime))
          .map((game, idx) => (
            <>
              <GameResult
                className="flex-grow flex items-center"
                key={game.id}
                idx={idx}
                game={game}
              />
            </>
          ))}
    </div>
  );
}
