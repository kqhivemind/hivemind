import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import GameListTable from './wildflower/GameListTable';
import MatchSummary from './wildflower/MatchSummary';
import MatchStatsTable from './wildflower/MatchStatsTable';

import styles from './Wildflower.module.css';

export default function WildflowerOverlay({ dark = false }) {
  const settings = useOverlaySettings();
  const stats = useMatchStats();

  return (
    <>
      <div
        id="matchSummaryOverlay"
        className={clsx(styles.container, { active: stats?.visible, dark })}
      >
        <MatchSummary className={clsx(styles.matchSummary)} />
        <GameListTable className={clsx(styles.gameListTable)} />
        <MatchStatsTable className={clsx(styles.matchStatsTable)} />
      </div>
    </>
  );
}

WildflowerOverlay.themeProps = {
  name: 'wildflower',
  displayName: 'Wildflower (Light)',
  description: `Wildflower match stats theme`,
};
