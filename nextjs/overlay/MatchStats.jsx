import React, { createContext, useState, useContext } from 'react';
import PropTypes from 'prop-types';

import { OVERLAY_SCENES } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { useOverlaySettings } from 'overlay/Settings';
import { useSceneController } from 'overlay/SceneController';

export const MatchStatsContext = createContext({});

export function MatchStatsProvider({ autoHide, children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const [stats, setStats] = useState(null);

  const ws = useWebSocket('/ws/gamestate');

  const loadMatch = async id => {
    setStats({});

    let response = await axios.get(`/api/tournament/match/${id}/`);
    const match = response.data;

    if (match.error) {
      return;
    }

    const responses = await Promise.all([
      axios.get(`/api/tournament/match/${id}/stats/`),
      axios.get(`/api/tournament/team/${match.blueTeam}/`),
      axios.get(`/api/tournament/team/${match.goldTeam}/`),
      axios.getAllPages(`/api/game/game/`, { params: { tournamentMatchId: id } }),
    ]);

    match.stats = responses.shift().data;
    match.blueTeam = responses.shift().data;
    match.goldTeam = responses.shift().data;
    match.games = responses.shift();

    match.loadedTime = Date.now();
    match.nextMatch = {};

    const winner = match.blueScore > match.goldScore ? 'blue' : 'gold';
    const loser = match.blueScore < match.goldScore ? 'blue' : 'gold';

    if (match.winnerToMatch) {
      response = await axios.get(`/api/tournament/match/${match.winnerToMatch}/`);
      match.nextMatch[winner] = response.data.roundName;
    }

    if (match.loserToMatch) {
      response = await axios.get(`/api/tournament/match/${match.loserToMatch}/`);
      match.nextMatch[loser] = response.data.roundName;
    }

    setStats(match);
  };

  ws.onJsonMessage(
    message => {
      if (message.type == 'matchend' && message.cabinetId == settings?.cabinet?.id) {
        loadMatch(message.matchId);
      }
    },
    [settings?.cabinet?.id],
  );

  if (settings !== null && stats === null) {
    axios
      .get(`/api/game/game/recent/`, { params: { cabinetId: settings.cabinet.id } })
      .then(response => {
        for (const game of response.data.results) {
          if (game.tournamentMatch) {
            loadMatch(game.tournamentMatch);
            return;
          }
        }

        setStats({});
      });
  }

  const value = {
    ...stats,
    scene,
    visible: scene === OVERLAY_SCENES.MATCH_SUMMARY || scene === null,
  };

  return <MatchStatsContext.Provider value={value}>{children}</MatchStatsContext.Provider>;
}

MatchStatsProvider.propTypes = {
  autoHide: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
};

export function useMatchStats() {
  return useContext(MatchStatsContext);
}
