import React, { createContext, createElement, useEffect, useState, useContext } from 'react';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { getIngameTheme } from 'overlay/ingame/themes';
import { getPostgameTheme } from 'overlay/postgame/themes';
import { getMatchTheme } from 'overlay/match/themes';
import { getMatchPreviewTheme } from 'overlay/matchPreview/themes';
import { getPlayerCamsTheme } from 'overlay/playerCams/themes';

export const OverlaySettingsContext = createContext({});

async function loadOverlay(id) {
  const axios = getAxios();

  let response = await axios.get(`/api/overlay/overlay/${id}/`);
  const overlay = response.data;

  overlay.themes = {
    ingame: getIngameTheme(overlay.ingameTheme),
    postgame: getPostgameTheme(overlay.postgameTheme),
    match: getMatchTheme(overlay.matchTheme),
    matchPreview: getMatchPreviewTheme(overlay.matchPreviewTheme),
    playerCams: getPlayerCamsTheme(overlay.playerCamsTheme),
  },

  response = await axios.get(`/api/game/cabinet/${overlay.cabinet}/`);
  overlay.cabinet = response.data;

  response = await axios.get(`/api/game/scene/${overlay.cabinet.scene}/`);
  overlay.cabinet.scene = response.data;

  overlay.isTeamOnLeft = team => team == (overlay.goldOnLeft ? GOLD_TEAM : BLUE_TEAM);

  return overlay;
}

export function OverlaySettingsProvider({ id, children }) {
  const [overlay, setOverlay] = useState(null);
  const ws = useWebSocket('/ws/gamestate');

  ws.onJsonMessage(message => {
    if (message.type == 'overlaysettings' && message.overlayId == id) {
      setOverlay(v => ({
        ...v,
        blueTeam: message.blueTeam,
        blueScore: message.blueScore,
        goldTeam: message.goldTeam,
        goldScore: message.goldScore,
        showPlayers: message.showPlayers,
        showScore: message.showScore,
        matchWinMax: message.matchWinMax,
      }));
    }
  });

  useEffect(() => {
    if (overlay === null) {
      loadOverlay(id).then(setOverlay);
    }
  }, []);

  return (
    <OverlaySettingsContext.Provider value={overlay}>
      {children}
    </OverlaySettingsContext.Provider>
  );
}

export function IngameOverlay({ ...props }) {
  const settings = useOverlaySettings();

  if (settings?.themes?.ingame) {
    return createElement(settings.themes.ingame, props);
  } else {
    return (<></>);
  }
}

export function PostgameOverlay({ ...props }) {
  const settings = useOverlaySettings();

  if (settings?.themes?.postgame) {
    return createElement(settings.themes.postgame, props);
  } else {
    return (<></>);
  }
}

export function MatchOverlay({ ...props }) {
  const settings = useOverlaySettings();

  if (settings?.themes?.match) {
    return createElement(settings.themes.match, props);
  } else {
    return (<></>);
  }
}

export function MatchPreviewOverlay({ ...props }) {
  const settings = useOverlaySettings();

  if (settings?.themes?.matchPreview) {
    return createElement(settings.themes.matchPreview, props);
  } else {
    return (<></>);
  }
}

export function PlayerCamsOverlay({ ...props }) {
  const settings = useOverlaySettings();

  if (settings?.themes?.playerCams) {
    return createElement(settings.themes.playerCams, props);
  } else {
    return (<></>);
  }
}

export function useOverlaySettings() {
  return useContext(OverlaySettingsContext);
}
