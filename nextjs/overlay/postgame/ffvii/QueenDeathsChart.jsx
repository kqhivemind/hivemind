import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';
import BaseQueenDeathsChart from 'components/charts/BaseQueenDeathsChart';

const useStyles = makeStyles(theme => ({
}));

export default function QueenDeathsChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  if (!stats?.queenDeathData) return null;

  return (
    <ChartContainer title="Queen Deaths" className={className}>
      {stats?.warriorData && (
        <BaseQueenDeathsChart game={stats} datasetProps={{ pointRadius: 0, borderWidth: 4 }} />
      )}
    </ChartContainer>
  );
}
