import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';
import BaseSnailChart from 'components/charts/BaseSnailChart';

const useStyles = makeStyles(theme => ({
}));

export default function SnailChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <ChartContainer title="Snail Progress" className={className}>
      {stats?.snailData && (
        <BaseSnailChart
          game={stats}
          datasetProps={{ pointRadius: 0, borderWidth: 4 }}
          lightBlueColor={"#6CE3D1"}
          darkBlueColor={"#6CE3D1"}
          lightGoldColor={"#D79938"}
          darkGoldColor={"#D79938"}
        />
      )}
    </ChartContainer>
  );
}
