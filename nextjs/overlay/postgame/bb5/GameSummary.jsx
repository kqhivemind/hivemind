import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import GameLinkQrCode from '../default/GameLinkQrCode';

const useStyles = makeStyles(theme => ({
  victoryImage: {
    width: '4vw',
    height: '4vw',
    marginLeft: '0.5vw',
  },
  winDescription: {
    fontSize: '2.5vw',

    fontWeight: 'bold',
    textAlign: 'center',
    borderBottom: '1px solid #4f4f4f',
    marginBottom: '1vw',
  },
  gameInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  gameInfoEntry: {
    fontSize: '1vw',
  },
  summaryRows: {
    flexGrow: 1,
  },
}));

export default function GameSummary({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  if (stats === null) {
    return <></>;
  }

  return (
    // <div className={clsx('flex items-center', className)}>
    <div
      className={clsx(
        classes.textSection,
        'text-section py-[1.66666667vw] pl-8 pr-3 flex  justify-between items-end ',
      )}
    >
      <div
        className={clsx(
          'font-bold flex-grow',
          'win-description bb-sans text-transparent  tracking-wider text-[3vw] leading-[0.9] bg-clip-text',
          stats.winningTeam === 'blue'
            ? 'bg-gradient-to-b from-blue-500 to-blue-800'
            : 'bg-gradient-to-b from-gold-main from-10% to-50% to-gold-dark3',
        )}
      >
        {stats?.winningTeamDisplay ?? '???'} {stats.winConditionDisplay} Victory
      </div>
      <div className={clsx('game-info font-bold flex flex-col engraved-light-text text-right')}>
        <div className={clsx(classes.gameInfoEntry, 'game-info-entry')}>
          {stats.length} on {stats.map}
        </div>

        <div className={clsx(classes.gameInfoEntry, 'game-info-entry')}>
          Game ID: {stats.gameId}
        </div>
      </div>
      <GameLinkQrCode className="-mt-[0.6vw] -mb-[0.9vw]" />
    </div>
    // </div>
  );
}
