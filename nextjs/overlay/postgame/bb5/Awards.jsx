import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import { usePostgameStats } from 'overlay/PostgameStats';
import AwardRow from '../default/AwardRow';

const useStyles = makeStyles(theme => ({
  placeholder: {
    opacity: 0,
  },
  row: {
    opacity: 0,
    transition: 'opacity 1.5s',
    position: 'absolute',
    width: 'calc(100% - 2vw)',
    top: '1vw',
    '&.visible': {
      opacity: 1,
    },
  },
}));

export default function Awards({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (stats?.loadedTime) {
        setCounter(Math.floor((Date.now() - stats.loadedTime) / 5000));
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [stats]);

  if (!(stats?.awards?.length > 0)) {
    return <></>;
  }

  return (
    <div className={clsx('grid place-content-center relative h-20 ', className)}>
      <>
        {stats.awards.map((award, idx) => (
          <AwardRow
            key={award.title}
            label={award.title}
            players={award.players}
            teams={award.teams}
            className={clsx(
              classes.row,
              { visible: counter % stats.awards.length === idx },
              ' [&>:first-child]:ml-0 max-w-[525px]',
            )}
          />
        ))}
      </>
    </div>
  );
}

Awards.propTypes = {
  className: PropTypes.string,
};

Awards.defaultProps = {
  className: null,
};
