import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import Bb5StyleHeader from '../ingame/bb5/Bb5StyleHeader';
import Awards from './bb5/Awards';
import GameSummary from './bb5/GameSummary';
import TeamStatsRow from './bb5/TeamStatsRow';
import TopPlayersRow from './bb5/TopPlayersRow';

const useStyles = makeStyles(theme => ({
  container: {
    opacity: 0,
    width: '98vw',
    height: '54.25vw',
    position: 'absolute',
    left: 0,
    top: 0,
    margin: '1vw',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 'unset',
    '&.active': {
      opacity: 1,
    },
  },
  boxes: {
    borderWidth: '0px',
    borderStyle: 'solid',
    borderRadius: '5px',
    mixBlendMode: 'hard-light',
  },
  gameSummary: {},
  blueWin: {
    backgroundColor: theme.palette.blue.light4,
    background: `radial-gradient(at left top, ${theme.palette.blue.light4}, ${theme.palette.blue.light1})`,
  },
  goldWin: {
    backgroundColor: theme.palette.gold.light5,
    background: `radial-gradient(at left top, ${theme.palette.gold.light2}, ${theme.palette.gold.dark3})`,
  },
  gameSummary: {
    flexGrow: 0,
  },
  charts: {
    width: '50vw',
    flexBasis: '50vw',
    marginRight: '1vw',
  },
  rightSide: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = usePostgameStats();
  const theme = useTheme();

  const boxClasses = clsx('box', classes.boxes, {
    [classes.blueWin]: stats?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: stats?.winningTeam == GOLD_TEAM,
  });

  return (
    <>
      <Bb5StyleHeader />

      <div
        id="postGameOverlay"
        className={clsx(
          'aspect-video w-screen flex ',
          'background relative font-sans',
          'transition-opacity duration-500 ease-out [&.active]:opacity-100 opacity-0',
          {
            active: stats?.visible,
          },
        )}
      >
        <div
          className={clsx(
            'aspect-video w-[1540px] top-[212px] left-0 absolute overflow-hidden rounded-lg',
            'transition-all duration-500 ease-out [.active>&]:opacity-100 opacity-0',
            classes[stats?.map],
          )}
        >
          <div
            className={clsx('relative h-full object-cover animate-slab-down mx-auto', boxClasses)}
          >
            <div
              className={clsx(
                classes.rightSide,
                'right-side absolute inset-16 justify-start bg-white shadow-lg rounded-lg px-12',
              )}
            >
              <GameSummary className={clsx(classes.gameSummary, 'game-summary', boxClasses)} />

              <div className="grid grid-cols-[650px,auto] gap-8 flex-grow">
                <div
                  className={clsx(
                    classes.statsBox,
                    'top-players flex-grow grid grid-cols-1 gap-x-[2vw]   p-[1.25vw]',
                  )}
                >
                  {stats !== null && (
                    <>
                      <TopPlayersRow label="Most Kills" value={stats.mostKills} />
                      <TopPlayersRow label="Most Military Kills" value={stats.mostMilitaryKills} />
                      <TopPlayersRow label="Most Deaths" value={stats.mostDeaths} />
                      <TopPlayersRow label="Most Berries" value={stats.mostBerries} />
                      <TopPlayersRow
                        label="Most Snail Distance"
                        from={stats.byPlayer?.snailMeters}
                        format={v => `${v} m`}
                      />
                      <TopPlayersRow label="Best Warrior Uptime" value={stats.mostWarriorUptime} />
                      <TopPlayersRow label="Most Bump Assists" from={stats.byPlayer?.bumpAssists} />
                    </>
                  )}
                  <div className="flex-shrink-0 mt-8 ">
                    <Awards className={clsx(classes.statsBox, 'awards m-0   bb5-font ')} />
                  </div>
                </div>
                <div
                  className={clsx(
                    'team-stats grid grid-cols-2 justify-between bb5-font pb-14 max-h-full relative left-12',
                  )}
                >
                  {stats !== null && (
                    <>
                      <TeamStatsRow label="Kills" value={stats.kills} />
                      <TeamStatsRow label="Military Kills" value={stats.militaryKills} />
                      <TeamStatsRow label="Berries" value={stats.berries} />
                      <TeamStatsRow label="Gate Control" value={stats.gateControl} />
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

DefaultOverlay.themeProps = {
  name: 'bb5',
  displayName: 'BB5 Theme',
  description: `A responsive post-game overlay, designed to sit atop the game footage.`,
};
