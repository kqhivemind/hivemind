import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import TeamStatsRow from './TeamStatsRow';

const useStyles = makeStyles(theme => ({
  container: {
    marginBottom: '1px',

    borderRadius: "8px",
    border: "4px solid transparent",
    background: "linear-gradient(to right, #040404, #040404) content-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
    padding: 0,

    position: "relative",

    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",
  }
}));

export default function TeamStats({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <div className={clsx(classes.container, className)}>
      {(stats !== null) && (
        <>
          <TeamStatsRow label="Kills" value={stats.kills} />
          <TeamStatsRow label="Military Kills" value={stats.militaryKills} />
          <TeamStatsRow label="Berries" value={stats.berries} />
          <TeamStatsRow label="Gate Control" value={stats.gateControl} />
        </>
      )}
    </div>
  );
}
