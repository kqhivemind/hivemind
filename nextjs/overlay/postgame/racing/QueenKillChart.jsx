import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';
import BaseWarriorChart from 'components/charts/BaseWarriorChart';

const useStyles = makeStyles(theme => ({
}));

export default function WarriorChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <ChartContainer title="Warriors Up" className={className}>
      {stats?.warriorData && (
        <BaseWarriorChart game={stats} datasetProps={{ pointRadius: 0, borderWidth: 4 }} />
      )}
    </ChartContainer>
  );
}
