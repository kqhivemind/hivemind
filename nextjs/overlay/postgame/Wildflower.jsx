import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';

import { usePostgameStats } from 'overlay/PostgameStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import Awards from './wildflower/Awards';
import Charts from './wildflower/Charts';
import GameSummary from './wildflower/GameSummary';
import TeamStats from './wildflower/TeamStats';
import TopPlayers from './wildflower/TopPlayers';

const useStyles = makeStyles(theme => ({
  background: {
    opacity: 0,
    width: '100vw',
    height: '56.25vw',
    position: 'absolute',
    left: 0,
    top: 0,
    transition: 'opacity 1s',
    '&.active': {
      opacity: 1,
    },
  },
  container: {
    opacity: 0,
    width: '98vw',
    height: '54.25vw',
    position: 'absolute',
    left: 0,
    top: 0,
    margin: '1vw',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 'unset',
    '&.active': {
      opacity: 1,
    },
  },
  boxes: {
    borderWidth: '1px',
    borderStyle: 'solid',
    borderRadius: '5px',
    backgroundImage: 'url("/static/wildflower/background.png")',
    border: '1px solid #afafaf',
    boxShadow: `0 0 15px 3px #0000007f`,
    backgroundBlendMode: 'luminosity',
  },
  gameSummary: {},
  blueWin: {
    backgroundColor: theme.palette.blue.dark3,
  },
  goldWin: {
    backgroundColor: theme.palette.gold.dark3,
  },
  gameSummary: {
    flexGrow: 0,
  },
  charts: {
    width: '50vw',
    flexBasis: '50vw',
    marginRight: '1vw',
  },
  rightSide: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = usePostgameStats();
  const theme = useTheme();

  const boxClasses = clsx('box', classes.boxes, {
    [classes.blueWin]: stats?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: stats?.winningTeam == GOLD_TEAM,
  });

  return (
    <>
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500&display=swap"
          rel="stylesheet"
        />
      </Head>

      <div
        id="postGameOverlay"
        className={clsx(classes.background, 'background', { active: stats?.visible })}
      >
        <div
          className={clsx(classes.container, classes[stats?.map], 'container', {
            active: stats?.visible,
          })}
        >
          <Charts className={clsx(classes.charts, 'charts', 'box', boxClasses)} />

          <div className={clsx(classes.rightSide, 'right-side')}>
            <GameSummary className={clsx(classes.gameSummary, 'game-summary', boxClasses)} />
            <Awards className={clsx(classes.statsBox, 'awards', boxClasses)} />
            <TeamStats className={clsx(classes.statsBox, 'team-stats', boxClasses)} />
            <TopPlayers className={clsx(classes.statsBox, 'top-players', boxClasses)} />
          </div>
        </div>
      </div>
    </>
  );
}

DefaultOverlay.themeProps = {
  name: 'wildflower',
  displayName: 'Wildflower',
  description: `A postgame theme to match the Wildflower in-game theme.`,
};
