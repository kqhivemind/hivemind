import clsx from 'clsx';
import medievalClasses from '../../ingame/medieval/medieval.module.css';
const cleanValues = numberOrString => {
  if (typeof numberOrString === 'number') return numberOrString;
  return parseFloat(numberOrString.replace('%', ''));
};
export default function TeamStatsRow({ label, value, className }) {
  if (!value) {
    return null;
  }
  const blueValue = cleanValues(value.blue);
  const goldValue = cleanValues(value.gold);

  const max = Math.max(blueValue, goldValue);
  return (
    <div
      className={clsx(
        'flex flex-col gap-[.25vw] pt-[1vw] bg-white/50 p-[1vw] pb-[.1vw] rounded',
        className,
        medievalClasses.ridgeBorder,
      )}
    >
      <div className="grid grid-cols-2 items-end flex-grow h-[4vw] w-[8vw] gap-[0.5vw] mx-auto">
        <StatBar
          label="Kills"
          value={blueValue}
          meterValue={blueValue / max}
          className={'blue text-blue-500'}
        />
        <StatBar
          label="Kills"
          value={goldValue}
          meterValue={goldValue / max}
          className={'blue text-gold-light1'}
        />
      </div>
      <div className={clsx('label font-bold text-[1.15vw] text-center')}>{label}</div>
    </div>
  );
}

const StatBar = ({ meterValue, value, className }) => {
  return (
    <div
      className={clsx(
        'value',
        'relative h-full flex flex-col',
        medievalClasses.engravedBox,
        className,
      )}
    >
      <span
        className={clsx('flex-grow bg-current mix-blend-multiply origin-bottom')}
        style={{ transform: `scaleY(${meterValue})` }}
      ></span>
      <span className="engraved-light-text  z-10 text-center font-bold text-[1.65vw] bg-current mix-blend-multiply">
        <span className="text-black">{value ?? ''}</span>
      </span>
    </div>
  );
};
