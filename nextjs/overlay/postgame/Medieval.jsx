import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import medievalClasses from '../ingame/medieval/medieval.module.css';

import { usePostgameStats } from 'overlay/PostgameStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM } from 'util/constants';
import Awards from './default/Awards';
import GameSummary from './medieval/GameSummary';
import TeamStatsRow from './medieval/TeamStatsRow';
import TopPlayersRow from './medieval/TopPlayersRow';

const useStyles = makeStyles(theme => ({
  container: {
    opacity: 0,
    width: '98vw',
    height: '54.25vw',
    position: 'absolute',
    left: 0,
    top: 0,
    margin: '1vw',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 'unset',
    '&.active': {
      opacity: 1,
    },
  },
  boxes: {
    borderWidth: '0px',
    borderStyle: 'solid',
    borderRadius: '5px',
    mixBlendMode: 'hard-light',
  },
  gameSummary: {},
  blueWin: {
    backgroundColor: theme.palette.blue.light4,
  },
  goldWin: {
    backgroundColor: theme.palette.gold.light5,
  },
  gameSummary: {
    flexGrow: 0,
  },
  charts: {
    width: '50vw',
    flexBasis: '50vw',
    marginRight: '1vw',
  },
  rightSide: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = usePostgameStats();
  const theme = useTheme();

  const boxClasses = clsx('box', classes.boxes, {
    [classes.blueWin]: stats?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: stats?.winningTeam == GOLD_TEAM,
  });

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
        <link
          href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap"
          rel="stylesheet"
        ></link>
        <style type="text/css">
          {`
           .font-medieval { font-family: 'MedievalSharp', sans-serif; letter-spacing: 2px; }
           .carved-text { color: #898989; text-shadow: 1px 1px 3px black, 1px 1px 0px black, -1px -1px 0px white, -2px -2px 1px rgba(255,255,255,.5); }
           .engraved-text { color: #444; text-shadow: -1px -1px 3px rgba(0,0,0,.8), -1px -1px 0px black, 1px 1px 0px white, 2px 2px 3px rgba(255,255,255,0.5); }
           .engraved-light-text { text-shadow: -1px -1px 1px rgba(0,0,0,.8), -1px -1px 0px black, 1px 1px 0px white; }
           .shadow-text { text-shadow: 1px 1px 2px black; }
          `}
        </style>
      </Head>

      <div
        id="postGameOverlay"
        className={clsx(
          'aspect-video w-screen flex ',
          'background relative font-sans',
          'transition-opacity ease-out [&.active]:opacity-100 opacity-0',
          {
            active: stats?.visible,
          },
        )}
      >
        <div
          className={clsx(
            'aspect-video w-[75vw] top-[3.125vw] left-[12.5vw] absolute overflow-hidden',
            classes[stats?.map],
          )}
        >
          <div className="relative h-full object-cover animate-slab-down pl-[19.79166667vw] pr-[1.25vw]">
            <div className="absolute top-0 left-0 w-[19.79166667vw]">
              <img
                src={`/static/medieval/victory-banner-${stats.winCondition}-${stats.winningTeam}.webp`}
                className="w-full h-full"
              />
            </div>
            <div className={clsx(classes.rightSide, 'right-side justify-start')}>
              <GameSummary className={clsx(classes.gameSummary, 'game-summary', boxClasses)} />

              <div className="flex">
                <div
                  className={clsx(
                    classes.statsBox,
                    'top-players flex-grow grid grid-cols-1 gap-x-[2vw] justify-around bg-white/50 p-[1.25vw]',
                    medievalClasses.ridgeBorder,
                  )}
                >
                  {stats !== null && (
                    <>
                      <TopPlayersRow label="Most Kills" value={stats.mostKills} />
                      <TopPlayersRow label="Most Military Kills" value={stats.mostMilitaryKills} />
                      <TopPlayersRow label="Most Deaths" value={stats.mostDeaths} />
                      <TopPlayersRow label="Most Berries" value={stats.mostBerries} />
                      <TopPlayersRow
                        label="Most Snail Distance"
                        from={stats.byPlayer?.snailMeters}
                        format={v => `${v} m`}
                      />
                      <TopPlayersRow label="Best Warrior Uptime" value={stats.mostWarriorUptime} />
                      <TopPlayersRow label="Most Bump Assists" from={stats.byPlayer?.bumpAssists} />
                    </>
                  )}
                </div>
                <div
                  className={clsx(
                    classes.statsBox,
                    'team-stats grid grid-cols-2 justify-between engraved-light-text',
                  )}
                >
                  {stats !== null && (
                    <>
                      <TeamStatsRow label="Kills" value={stats.kills} />
                      <TeamStatsRow label="Military Kills" value={stats.militaryKills} />
                      <TeamStatsRow label="Berries" value={stats.berries} />
                      <TeamStatsRow label="Gate Control" value={stats.gateControl} />
                    </>
                  )}
                </div>
              </div>
              <Awards
                className={clsx(
                  classes.statsBox,
                  'awards m-0  p-[.5vw] bg-white/50 engraved-light-text',

                  medievalClasses.ridgeBorder,
                )}
              />
            </div>
            <img
              src="/static/medieval/slab.webp"
              className="absolute -z-10 inset-0 aspect-video w-full h-full "
            />
          </div>
        </div>
        <div
          id="winnerGlow"
          className={clsx(
            'winner-glow bg-no-repeat bg-cover aspect-[406/725] w-[21.14583333vw] absolute bottom-0 mix-blend-overlay',
            stats.winningTeam === 'blue' ? 'left-0' : '-scale-x-100 right-0',
            'transition-opacity ease-out [&.active]:opacity-100 opacity-0',
            {
              active: stats?.visible,
            },
          )}
          style={{
            backgroundImage: `url(/static/medieval/${stats?.winningTeam}-winner-glow.webp)`,
          }}
        ></div>
        <img
          src="/static/medieval/tears.gif"
          className={clsx(
            'absolute top-[994px] left-1/2 z-20 ',
            stats?.winningTeam === LEFT_TEAM ? 'translate-x-[103px]' : '-translate-x-[103px]',
          )}
        ></img>
      </div>
    </>
  );
}

DefaultOverlay.themeProps = {
  name: 'medieval',
  displayName: 'Medieval',
  description: `A responsive post-game overlay, designed to sit atop the game footage.`,
};
