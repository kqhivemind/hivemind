import React from 'react';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import TopPlayersRow from './TopPlayersRow';

export default function TeamStats({ className }) {
  const stats = usePostgameStats();

  return (
    <div className={className}>
      {stats !== null && (
        <>
          <TopPlayersRow label="Most Kills" value={stats.mostKills} />
          <TopPlayersRow label="Most Military Kills" value={stats.mostMilitaryKills} />
          <TopPlayersRow label="Most Deaths" value={stats.mostDeaths} />
          <TopPlayersRow label="Most Berries" value={stats.mostBerries} />
          <TopPlayersRow
            label="Most Snail Distance"
            from={stats.byPlayer?.snailMeters}
            format={v => `${v} m`}
          />
          <TopPlayersRow label="Best Warrior Uptime" value={stats.mostWarriorUptime} />
          <TopPlayersRow label="Most Bump Assists" from={stats.byPlayer?.bumpAssists} />
        </>
      )}
    </div>
  );
}
