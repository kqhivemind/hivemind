import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { POSITIONS_BY_ID } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  label: {
    fontFamily: 'Oswald',
    flexGrow: 3,
    flexBasis: 0,
    fontSize: '1.5vw',
    fontWeight: 'bold',
    lineHeight: '2vw',
    whiteSpace: 'nowrap',
    margin: '0 1vw',
  },
  icons: {
    flexGrow: 2,
    flexBasis: 0,
    textAlign: 'right',
  },
  icon: {
    height: '2vw',
    width: '2vw',
    marginLeft: '5px',
  },
  teamIcon: {
    height: '2vw',
    width: '2vw',
    marginLeft: '5px',
    border: '3px solid #ffffff99',
    display: 'inline-block',
    '&.blue': {
      backgroundColor: theme.palette.blue.dark2,
    },
    '&.gold': {
      backgroundColor: theme.palette.gold.dark2,
    },
  },
}));

export default function AwardRow({ label, players, teams, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>
      <div className={classes.label}>{label}</div>
      <div className={classes.icons}>
        {players &&
          players.map(id => (
            <img key={id} src={POSITIONS_BY_ID[id].ICON} className={classes.icon} />
          ))}
        {teams && teams.map(id => <div key={id} className={clsx(classes.teamIcon, id)}></div>)}
      </div>
    </div>
  );
}
