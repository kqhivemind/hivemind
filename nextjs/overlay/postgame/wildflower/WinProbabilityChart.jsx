import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';
import BaseWinProbabilityChart from 'components/charts/BaseWinProbabilityChart';

const useStyles = makeStyles(theme => ({
}));

export default function WinProbabilityChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  if (!stats?.winProbability?.length) return null;

  return (
    <ChartContainer title="Win Probability" className={className}>
      <BaseWinProbabilityChart game={stats} datasetProps={{ pointRadius: 0 }} />
    </ChartContainer>
  );
}
