import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import TwitchChat from 'overlay/components/TwitchChat';

const useStyles = makeStyles(theme => ({
  twitchChat: {
    fontFamily: 'Oswald',
    fontSize: '18px',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    '& .chat-line': {
      marginTop: '3px',
    },
    '& .username': {
      fontWeight: 'bold',
      marginRight: '10px',
      color: 'white',
    },
    '& .chat-text': {
      color: 'white',
    },
  },
}));

export default function TwitchChatBox({ className }) {
  const classes = useStyles();
  const settings = useOverlaySettings();

  if (!settings?.showChat) {
    return <></>;
  }

  return <TwitchChat className={clsx(className, classes.twitchChat)} />;
}
