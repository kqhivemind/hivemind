import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Icon from '@mdi/react';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: '10px',
  },
  icon: {
    verticalAlign: 'middle',
    height: '32px',
    color: '#dfdfdf',
    margin: '0 8px',
  },
  text: {
    fontFamily: 'Oswald',
    fontSize: '24px',
    color: '#dfdfdf',
    verticalAlign: 'middle',
  },
}));

export default function TextWithIcon({ path, className, children }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)}>
      <Icon className={clsx(classes.icon, 'icon')} path={path} />
      <div className={clsx(classes.text, 'text')}>{children}</div>
    </div>
  );
}
