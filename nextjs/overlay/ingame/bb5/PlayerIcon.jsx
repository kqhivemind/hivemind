import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import PlayerStatusSprite from 'overlay/components/PlayerStatusSprite';
import { GOLD_TEAM } from 'util/constants';
import {useMatch} from "@/overlay/Match";

const useStyles = makeStyles(theme => ({
  icon: {
    height: '70px',
    "&.warmup": { // Warmup shrinks the queen sprite
      height: "55px"
    },

    width: '48px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center calc(100% - 4px)',
    backgroundImage: ({ position }) =>
      `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}@4x.png)`,
    backgroundSize: ({ position }) => (position.POSITION == 'queen' ? '55px 57px' : '30px 39px'),
    transform: ({ position }) => position.TEAM == GOLD_TEAM && 'scaleX(-1)',
    '&.warrior': {
      backgroundImage: ({ position }) =>
        `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}_warrior@4x.png)`,
      backgroundSize: '43.5px 51px',
      backgroundPositionX: 'calc(100% + 3px)'
    },
    '&.speed': {
      backgroundImage: ({ position }) =>
        `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}_speed@4x.png)`,
      backgroundSize: '39px 51px',
      backgroundPositionX: 'calc(50% - 4.5px)',
    },
    '&.speed.warrior': {
      backgroundImage: ({ position }) =>
        `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}_speed_warrior@4x.png)`,
      backgroundSize: '52.5px 63px',
      backgroundPositionX: 'calc(100% + 3px)',
    },
    "&.queen.warmup": {
      backgroundSize: "47px 48px",
    },
  },
}));

export default function PlayerIcon({ position }) {
  const classes = useStyles({ position });
  const match = useMatch();

  // If it's a warmup, the queen icon shrinks to make room for the banner
  if (match?.isWarmUp) {
    return <PlayerStatusSprite className={clsx(classes.icon, 'player-sprite', 'warmup')} position={position}/>;
  } else {
    return <PlayerStatusSprite className={clsx(classes.icon, 'player-sprite')} position={position}/>;
  }
}
