import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Head from 'next/head';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { useOverlaySettings } from 'overlay/Settings';
import StatsSection from './wildflower-dark/StatsSection';
import PlayerNames from './wildflower-dark/PlayerNames';
import PlayerStats from './default/PlayerStats';
import TeamNameBox from './wildflower-dark/TeamNameBox';
import HiveMindLogo from './default/HiveMindLogo';
import GameTimeBox from './wildflower-dark/GameTimeBox';
import TwitchChat from './wildflower-dark/TwitchChat';
import BerriesRemainingBox from './wildflower-dark/BerriesRemainingBox';
import KillFeed from './default/KillFeed';
import CabinetName from 'overlay/components/CabinetName';
import EventName from 'overlay/components/EventName';
import CurrentTime from 'overlay/components/CurrentTime';

const useStyles = makeStyles(theme => ({
  overlay: {
    width: '1920px',
    height: '1080px',
    background: 'url("/static/wildflower/background-dark.png")',
    backgroundColor: 'black',
  },
  sidebar: {
    position: 'absolute',
    width: '480px',
    height: '1080px',
    top: '0px',
    left: '1440px',
  },
  infoBox: {
    position: 'absolute',
    bottom: '0px',
    left: '1440px',
    width: '480px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  bottomSection: {
    position: 'absolute',
    top: '810px',
    height: '270px',
    left: '0px',
    width: '1440px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    gap: '10px',
  },
  bottombar: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  teamName: {
    position: 'absolute',
    color: 'white',
    left: '8px',
    width: '464px',
  },
  teamNameBlue: {
    top: '277px',
  },
  teamNameGold: {
    top: '598px',
  },
  twitchChat: {
    position: 'absolute',
    top: '660px',
    height: '310px',
    width: '100%',
  },
  bottomInfoBox: {
    position: 'absolute',
    bottom: '10px',
    width: '480px',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  bottomInfo: {
    fontFamily: 'Oswald',
    fontSize: '22px',
    color: '#dfdfdf',
  },
  bottomInfoRow: {
    display: 'flex',
    flexDirection: 'row',
    width: '500px',
    justifyContent: 'space-evenly',
  },
}));

export default function WildflowerDarkOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <div className={clsx(classes.overlay, 'ingame-overlay')}>
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500&display=swap"
          rel="stylesheet"
        />
      </Head>

      <div className={clsx(classes.sidebar, 'sidebar')}>
        <TeamNameBox className={clsx(classes.teamName, classes.teamNameBlue)} team={BLUE_TEAM} />
        <TeamNameBox className={clsx(classes.teamName, classes.teamNameGold)} team={GOLD_TEAM} />

        <div className={clsx(classes.infoBox, 'info-box')}></div>

        <TwitchChat className={clsx(classes.twitchChat, 'twitch-chat')} />

        <div className={clsx(classes.bottomInfoBox, 'bottom-info-box')}>
          <EventName className={classes.bottomInfo} />
          <CabinetName className={classes.bottomInfo} />
          <CurrentTime className={classes.bottomInfo} />
        </div>
      </div>

      <div className={clsx(classes.bottomSection, 'bottom-section')}>
        <PlayerNames team={leftTeam} />

        <div className={clsx(classes.bottombar, 'bottom-bar')}>
          <StatsSection />
          <div className={clsx(classes.bottomInfoRow, 'bottom-info-row')}>
            <GameTimeBox />
            <BerriesRemainingBox />
          </div>
        </div>

        <PlayerNames team={rightTeam} />
      </div>
    </div>
  );
}

WildflowerDarkOverlay.themeProps = {
  name: 'wildflower-dark',
  displayName: 'Wildflower (Dark)',
  description: `A theme with a horizontal stats display and room to include stream chat.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1920, h: 1080, x: 0, y: 0 },
  gameCaptureWindow: { w: 1440, h: 810, x: 0, y: 0 },
  blueCameraWindow: { w: 464, h: 261, x: 1448, y: 8 },
  goldCameraWindow: { w: 464, h: 261, x: 1448, y: 329 },
};
