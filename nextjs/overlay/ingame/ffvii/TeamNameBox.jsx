import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM } from 'util/constants';
import { useOverlaySettings } from 'overlay/Settings';
import TeamName from 'overlay/components/TeamName';
import TeamScore from 'overlay/components/TeamScore';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'row' : 'row-reverse'),
    flexGrow: '1',
    flexBasis: '0',
    backgroundImage: ({ team }) => team == BLUE_TEAM ? "linear-gradient(#000099, #000033)" : "linear-gradient(#cc9933, #996633)",
    boxShadow: 'inset 0 0 4px #000000aa',
    border: '4px groove white',
    borderRadius: '8px',
  },
  section: {
    overflow: 'hidden',
  },
  teamName: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'left' : 'right'),
    margin: '0 5px',
  },
  score: {
    textAlign: 'center',
    flexBasis: '20px',
    flexGrow: 0,
  },
}));

export default function TeamNameBox({ team, className }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ team, settings });
  const match = useMatch();
  const colorOnChange = { higher: '#550', duration: 1200 };

  return (
    <div className={clsx(classes.container, className, 'team-name-container')}>
      <div className={clsx(classes.teamName, classes.section, 'team-name')}>
        <TeamName team={team} />
      </div>
      {match.showScore && (
        <div className={clsx(classes.score, classes.section, 'score')}>
          <TeamScore team={team} colorOnChange={colorOnChange} />
        </div>
      )}
    </div>
  );
}

TeamNameBox.propTypes = {
  team: PropTypes.string.isRequired,
  className: PropTypes.string,
};
