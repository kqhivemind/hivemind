import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';

import StatValue from 'overlay/components/StatValue';
import QueenKillIconContainer from 'overlay/components/QueenKillIconContainer';
import { POSITIONS_BY_TEAM, BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '2px solid #ffffff19',
  },
  cell: {
    textAlign: 'center',
    padding: '3px 0 1px 0',
    display: 'flex',
    flexDirection: 'row',
    width: '48px',
    justifyContent: 'center',
  },
  value: {
  },
  valueText: {
    color: 'white',
  },
  title: {
    width: '70px',
    textTransform: 'uppercase',
    color: 'white',
  },
  queenKillContainer: {
    color: 'white',
  },
}));

export default function StatsRow({ className, title, queenKillIcon, statKey, ...props }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className, statKey + '-row')}>
      <Head>
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/reactor7/webfont.css`} />
      </Head>

      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} statKey={statKey} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer position={pos} className={classes.queenKillContainer} element={queenKillIcon} />
          )}
        </div>
      ))}

      <div className={clsx(classes.cell, classes.title, 'stat-title', 'stat-title-' + statKey)}>{title}</div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} statKey={statKey} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer position={pos} className={classes.queenKillContainer} element={queenKillIcon} />
          )}
        </div>
      ))}

    </div>
  );
}
