import React from 'react';
import Head from 'next/head';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: ({ team }) => team == GOLD_TEAM ? 'row-reverse' : 'row',
    marginRight: ({ team }) => team == BLUE_TEAM && '1px',
    marginLeft: ({ team }) => team == GOLD_TEAM && '1px',
    backgroundImage: ({ team }) => team == BLUE_TEAM ? "linear-gradient(#000099, #000033)" : "linear-gradient(#cc9933, #996633)",
    width: "468px",
    height: "54px",
    alignSelf: "end",

    // Standard border config
    border: "4px groove white",
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: "8px",
    color: "white",
    letterSpacing: "0.05em",
    // End standard border config

  },
  scoreCounter: {
    display: 'flex',
    flexDirection: ({ team }) => team == GOLD_TEAM ? 'row-reverse' : 'row',
    padding: "2px 2px",
  },
  marker: {
    height: "42px",

    // Default settings for middles
    width: "48px",
    padding: "6px 9px",
    "&.blue": {
      backgroundImage: "url(/static/ffvii/game-counters/blue_open_open.png)",
    },
    "&.gold": {
      backgroundImage: "url(/static/ffvii/game-counters/gold_open_open.png)",
    },
    // end middle default settings

    "&:only-child:first-child": {
      width: "42px",
      padding: "6px",
      "&.blue": {
        backgroundImage: "url(/static/ffvii/game-counters/blue_closed_closed.png)",
      },
      "&.gold": {
        padding: "6px",
        backgroundImage: "url(/static/ffvii/game-counters/gold_closed_closed.png)",
      },
    },

    "&:first-child": {
      width: "45px",
      "&.blue": {
        padding: "6px",
        backgroundImage: "url(/static/ffvii/game-counters/blue_closed_open.png)",
      },
      "&.gold": {
        padding: "6px 9px",
        backgroundImage: "url(/static/ffvii/game-counters/gold_open_closed.png)",
      },
    },

    "&:last-child": {
      width: "45px",
      "&.blue": {
        padding: "6px 9px",
        backgroundImage: "url(/static/ffvii/game-counters/blue_open_closed.png)",
      },
      "&.gold": {
        padding: "6px",
        backgroundImage: "url(/static/ffvii/game-counters/gold_closed_open.png)",
      },
    },
  },
  markerEmpty: {
  },
  markerFilled: {
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    fontFamily: 'OPTIEngeEtienne, Final Fantasy, Roboto, sans-serif',
    textTransform: 'uppercase',
    color: 'white',
    fontSize: '32px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    padding: "0 8px",
    textOverflow: 'ellipsis',
    letterSpacing: "1px",
    textAlign: ({ team }) => team == GOLD_TEAM ? 'left' : 'right',
  },
}));

export default function TeamSection({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();

  const MarkerFilled = () => (
    <div
      className={clsx(classes.marker, classes.markerFilled, team)}
    ><img
      src={`/static/ffvii/game-counters/${team}_materia.png`}
    /></div>
  );

  const MarkerEmpty = () => (
    <div
      className={clsx(classes.marker, classes.markerEmpty, team)}
    />
  );

  return (
    <div className={clsx(classes.container, className)}>
      <Head>
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/final_fantasy/stylesheet.css`} />
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/enge-etienne/stylesheet.css`} />
      </Head>

      <div className={clsx(classes.scoreCounter, "score-counter")}>
        <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
      </div>

      <div className={clsx(classes.teamName, "team-name")}>
        <TeamName className={classes.teamName} team={team} />
      </div>
    </div>
  );
}
