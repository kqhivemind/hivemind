import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';
import SnailPercentageRow from './SnailPercentageRow';
import {useMatch} from "@/overlay/Match";

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  container: {
    backgroundImage: "linear-gradient(#cc3399, #993399)",

    // TODO(chris): I'm fairly certain this can be refactored into a global style, but that's where my React knowledge runs out
    // Standard border config
    border: "4px groove white",
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: "8px",
    color: "white",
    fontSize: '18px',
    fontWeight: 'bold',
    textShadow: '1.5px 1.5px #000000',
    // End standard border config
  },
  warmup: {
    fontSize: "96px",
    textAlign: "center",
  },
}));

export default function StatsSection({ className }) {
  const classes = useStyles();
  const match = useMatch();

  if (match?.isWarmUp) {
    return (
      <div className={clsx(classes.container, className)}>
        <PlayerIconsRow className={clsx(classes.row, 'header-row')}/>
        <div className={clsx(classes.warmup, className)}>
          WARMUP
        </div>
      </div>
    );
  }

  const queenKillIcon = (
    <span>&#8224;</span>
  );

  return (
    <div className={clsx(classes.container, className)}>
      <PlayerIconsRow className={clsx(classes.row, 'header-row')} />
      <StatsRow title="Kills" statKey="kills" colorOnChange={{ higher: '#0f0', duration: 1200 }} queenKillIcon={queenKillIcon} />
      <StatsRow title="Deaths" statKey="deaths" colorOnChange={{ higher: '#f00', duration: 1200 }} />
      <StatsRow title="Berries" statKey="totalBerries" colorOnChange={{ higher: '#0f0', duration: 1200 }} />
      <SnailPercentageRow />
      <SnailPercentageRow useEstimatedDistance={true} />
    </div>
  );
}
