import React from 'react';
import clsx from 'clsx';
import Icon from '@mdi/react';
import { mdiSword } from '@mdi/js';
import { motion, AnimatePresence, LayoutGroup } from 'framer-motion';
import { partition } from 'lodash';

import { POSITIONS_BY_ID } from 'util/constants';
import { useGameStats } from 'overlay/GameStats';
import { formatUTC } from 'util/dates';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  killFeed: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 10,
    display: 'flex',
    justifyContent: 'space-between',
    maxHeight: '200px',
    overflow: 'hidden',
    '& .assists': {
      marginTop: '18px',
      position: 'absolute',
      left: '55px',
    },
    '& .kill-feed-entry': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
      padding: '4px',
      // backgroundColor: 'rgba(0,0,0,.2)',
      position: 'relative',
      width: '138px',
      flex: '0 0 138px',
      animation: '$bloom 250ms forwards 1 ease-in',

      '&::after' : {
        content: '""',
        position: 'absolute',
        inset: '0 -2px',
        backgroundColor: 'rgba(106,0,0,.7)',
        transform: 'skew(-15deg)',
        zIndex: -1,
        animation: '$bloom 250ms forwards 1 ease-in',
        mixBlendMode: 'multiply',

      },
      '& .kill-time': {
        color: 'white',
        textAlign: 'right',
        marginRight: '8px',
        width: '40px',
      },
      '& .player-icon': {
        height: '20px',
        width: '20px',
        margin: '0 5px',
        '&.assist': {
          height: '12px',
          width: '12px',
          margin: '0 2px',
        },
      },
      '& .sword-icon': {
        color: 'white',
        transform: 'scaleX(-1)',
      },
    },
  },
  '@keyframes bloom': {
    '0%': {
      filter: 'blur(4px) brightness(5)',
    },
    '100%': {
      filter: 'blur(0px) brightness(1)',
    },
  },
  killFeedSide: {
    display: 'flex',
    gap: '8px',
  },
  killFeedRight: {
    flexDirection: 'row-reverse',
    '& .kill-feed-entry::after': {
      transform: 'skew(15deg)'
    }
  }
}));


export default function KillFeed({ className, flipTeams }) {
  const classes = useStyles();
  const stats = useGameStats();

  const [goldKills, blueKills ] = partition(stats?.killFeed, (kill) => kill.killer % 2);

  const rightTeamKills = flipTeams ? blueKills : goldKills;
  const leftTeamKills = flipTeams ? goldKills : blueKills;


  const killFeedEntry = (kill, isOnLeft = true) => (
    <motion.div
      layout
      key={kill.id}
      className="kill-feed-entry"
      positionTransition
      initial={{ opacity: 0, x: isOnLeft ? -50 : 50, scale: 1.25, '--brightness': 3, '--blur': 10 }}
      animate={{ opacity: 1, x: 0, scale: 1, '--brightness': 1,'--blur': 0 }}
      exit={{ opacity: 0, x: isOnLeft ? -50 : 50, transition: { duration: 0.5 } }}
    >
      {kill.gameTime && (
        <div className="kill-time">{formatUTC(kill.gameTime * 1000, 'm:ss')}</div>
      )}
      <div className="assists">
        {kill.assists.map(assist => (
          <img key={assist} className="player-icon assist" src={POSITIONS_BY_ID[assist].ICON} />
        ))}
      </div>
      <img className="player-icon killer" src={POSITIONS_BY_ID[kill.killer]?.ICON} />
      <Icon className="sword-icon" path={mdiSword} size={0.85} />
      <img className="player-icon victim" src={POSITIONS_BY_ID[kill.victim]?.ICON} />
    </motion.div>);

  return (
    <div className={clsx(className, classes.killFeed, "kill-feed")}>
      <div className={clsx(classes.killFeedSide, "kill-feed-left")}>
          <LayoutGroup>
        <AnimatePresence initial={false}>

        {leftTeamKills?.map(kill => killFeedEntry(kill))}
        </AnimatePresence>
          </LayoutGroup>
      </div>
      <div className={clsx(classes.killFeedSide, classes.killFeedRight, "kill-feed-right")}>
          <LayoutGroup>
        <AnimatePresence initial={false}>
        {rightTeamKills?.map(kill => killFeedEntry(kill, false))}
        </AnimatePresence>
          </LayoutGroup>
      </div>
    </div>
  );
}
