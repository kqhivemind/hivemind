import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from 'overlay/components/PlayerIcon';
import PlayerName from 'overlay/components/PlayerName';
import PlayerNamesBlock from 'overlay/components/PlayerNamesBlock';

const useStyles = makeStyles(theme => ({
  block: {

  },
  title: {
    width: '160px',
    color: 'white',

    fontFamily: 'jaf-mashine',
    fontSize: '15px',
  },
  position: {
    display: 'flex',
    width: '160px',
    margin: '5px 0',
    // flexDirection: ({ team }) => (team == GOLD_TEAM ? 'row-reverse' : 'row'),
    gap: '4px'
  },
  icon: {
    height: '20px',
    width: '20px',
    margin: '0 1px',
    opacity: 0.3,
    '&.active': {
      opacity: 1,
    },
  },
  name: {
    color: 'white',
    display: 'block',
    flexBasis: 0,
    flexGrow: 1,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    fontFamily: 'Roboto',
    fontSize: '14px',
    textOverflow: 'ellipsis'
    // textAlign: ({ team }) => (team == GOLD_TEAM ? 'right' : 'left'),
  },
}));

export default function PlayerNames({ team, className }) {
  const classes = useStyles({ team });

  return (
    <PlayerNamesBlock className={clsx('player-names', className)}>


      {POSITIONS_BY_TEAM[team].map(pos => (
        <div className={clsx(classes.position, 'position')} key={pos.ID}>
          <PlayerIcon className={clsx(classes.icon, 'icon')} position={pos} />
          <PlayerName className={clsx(classes.name, 'name')} position={pos} />
        </div>
      ))}

    </PlayerNamesBlock>
  );
}
