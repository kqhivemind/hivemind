import { useMatch } from '@/overlay/Match';
import { BLUE_TEAM, GOLD_TEAM } from '@/util/constants';
import clsx from 'clsx';

export default function TeamImages({ classNameOverride }) {
  const match = useMatch();

  const { teamImages } = match;
  return (
    <div
      className={clsx(
        classNameOverride ||
          'absolute inset-[21px] top-[568px] flex justify-between flex-start -z-10',
      )}
    >
      <div className="w-[180px] h-[180px] object-contain">
        <img
          src={teamImages?.[BLUE_TEAM] ?? `/static/medieval/blue-crest.png`}
          className="block h-full w-full object-contain"
        />
      </div>
      <div className="w-[180px] h-[180px] object-contain">
        <img
          src={teamImages?.[GOLD_TEAM] ?? `/static/medieval/gold-crest.png`}
          className="block h-full w-full object-contain"
        />
      </div>
    </div>
  );
}
