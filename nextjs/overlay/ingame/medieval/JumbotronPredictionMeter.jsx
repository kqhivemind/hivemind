import clsx from 'clsx';
import { useGameStats } from 'overlay/GameStats';
// const interpolateValue = val => (val <= 0.9 && val >= 0.1 ? val * 1.5 + 0.05 : val);
const interpolateValue = val => (val - 0.15) * 1.15;
const lerp = (x, y, a) => x * (1 - a) + y * a;
export default function JumbotronPredictionMeter({ flipTeams }) {
  const stats = useGameStats();
  const { prediction } = stats ?? { prediction: 0.5 };
  const [bluePrediction, goldPrediction] = [prediction, 1 - prediction];
  const fillClasses = ['fill-blue-dark1', 'fill-gold-dark3'];
  const [leftClass, rightClass] = flipTeams ? fillClasses.reverse() : fillClasses;

  const leftValue = flipTeams ? goldPrediction : bluePrediction;
  const rightValue = flipTeams ? bluePrediction : goldPrediction;

  return (
    <div className="w-[404px] h-[26px] grid grid-cols-[142px_120px_142px]  absolute z-[30] bottom-[106px] left-1/2 -translate-x-1/2 mix-blend-multiply ">
      <div className="-scale-x-100">
        <TeamMeterBar predictionValue={leftValue} fillClass={leftClass} />
      </div>
      <span></span>
      <TeamMeterBar predictionValue={rightValue} fillClass={rightClass} />
    </div>
  );
}

const TeamMeterBar = ({ predictionValue, fillClass }) => (
  <svg viewBox="0 0 142 26" xmlns="http://www.w3.org/2000/svg" className="w-[142px] h-[26px] ">
    <defs>
      <clipPath id="predictionClip">
        <path
          d="M0.552368 25.9985C4.10681 17.5055 6.21311 8.84344 6.83438 0.120117H135.076L142.552 13.0593L135.076 25.9985H0.552368Z"
          fill="black"
        />
      </clipPath>
    </defs>
    <g transform="" clipPath="url(#predictionClip)">
      <rect
        width="142"
        height="22"
        x="0"
        y="2"
        className={clsx(fillClass, 'transition-transform duration-500 ease-in-out')}
        style={{
          transform: `scale(${lerp(0.15, 1, interpolateValue(predictionValue))},1) `,
        }}
      />
    </g>
  </svg>
);
