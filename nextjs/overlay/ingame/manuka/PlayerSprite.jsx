import { makeStyles } from '@material-ui/core/styles';

import { useOverlaySettings } from 'overlay/Settings';
import PlayerStatusSprite from 'overlay/components/PlayerStatusSprite';

const useStyles = makeStyles(theme => ({
  sprite: {
    height: '78px',
    width: '84px',
    position: 'relative',
    zIndex: 2,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center 100%',
    backgroundImage: ({ position }) =>
      `url(/static/sprites/${position.TEAM}_${position.POSITION}.png)`,
    backgroundSize: '40px 52px',
    transform: ({ position }) => `translate(${position.TEAM == 'blue' ? '16px, 0' : '-16px, 0'})`,
    '&.warrior': {
      backgroundImage: ({ position }) =>
        `url(/static/sprites/${position.TEAM}_${position.POSITION}_warrior.png)`,
      backgroundSize: '84px 74px',
      transform: ({ position }) =>
        `translate(${position.TEAM == 'blue' ? '12px, 6px' : '-8px ,10px'})`,
      zIndex: 10,
    },
    '&.warrior.speed:after': {
      height: '74px',
      width: '84px',
      position: 'absolute',

      backgroundImage: ({ position }) =>
        `url(/static/sprites/${position.TEAM}_${position.POSITION}_warrior.png)`,
      backgroundSize: '84px 74px',
      transform: ({ position }) =>
        position.TEAM === 'blue'
          ? 'translate(-30px, 6px) scale(1.95, 1) skew(-9deg)'
          : 'translate(42px, 26px) scale(2.5, 1) skew(9deg)',
    },
    '&.queen': {
      backgroundSize: '84px 78px',
      transform: ({ position }) =>
        `translate(${position.TEAM == 'blue' ? '12px, 10px' : '-14px, 10px'})`,
    },
    '&.speed:not(.warrior)': {
      backgroundImage: ({ position }) =>
        `url(/static/sprites/${position.TEAM}_${position.POSITION}_run.png)`,
      backgroundSize: '40px 52px',
      transform: ({ position }) => `translate(${position.TEAM == 'blue' ? '14px, 0' : '-14px, 0'})`,
      transformStyle: 'preserve-3d',
    },
    '&.speed:after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      height: '52px',
      width: '40px',
      zIndex: -1,
      backgroundPosition: 'center 100%',
      backgroundImage: ({ position }) =>
        `url(/static/sprites/${position.TEAM}_${position.POSITION}_run.png)`,
      backgroundSize: '40px 52px',
      margin: 0,
      transform: ({ position }) =>
        position.TEAM === 'blue'
          ? 'translate(0px, 26px) scale(2.5, 1) skew(-9deg)'
          : 'translate(42px, 26px) scale(2.5, 1) skew(9deg)',
      filter: 'blur(2px) brightness(1.5)',
      mixBlendMode: 'overlay',
    },
  },
}));

export default function PlayerSprite({ position }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ position, settings });

  return <PlayerStatusSprite className={classes.sprite} position={position} />;
}
