export const statKeys = [
  'militaryKills',
  'militaryDeaths',
  'totalBerries',
  'workerKills',
  'workerDeaths',
  'snailDistance',
];
export const queenIgnoreStats = ['workerDeaths', 'totalBerries', 'snailDistance'];
export const STAT_EMOJIS = {
  militaryKills: '⚔️',
  militaryDeaths: '☠️',
  totalBerries: '🍇',
  workerKills: '🗡️',
  workerDeaths: '💀',
  snailDistance: '🐌',
};
export const STAT_LEGEND = {
  militaryKills: 'military',
  militaryDeaths: 'military',
  totalBerries: 'berries',
  workerKills: 'drone',
  workerDeaths: 'drone',
  snailDistance: 'dist',
};
