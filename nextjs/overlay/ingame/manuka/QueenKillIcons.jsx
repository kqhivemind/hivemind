import { makeStyles } from '@material-ui/core/styles';
import { mdiCrown } from '@mdi/js';
import Icon from '@mdi/react';
import clsx from 'clsx';

import { GOLD_TEAM } from 'util/constants';
import QueenKillIconContainer from './QueenKillIconContainer';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    height: '20px',
    width: '68px',
    position: 'absolute',
    top: '-.75rem',
    left: '-3px', //({ position }) => (position.TEAM == GOLD_TEAM ? '3px' : '37px'),
    alignItems: 'center',
    zIndex: 30,
  },
  icon: {
    color: ({ position }) => (position.TEAM == GOLD_TEAM ? 'var(--blue)' : 'var(--gold)'),
    height: '24px',
    filter: 'brightness(1.25)',
    animation: '$queenKillFx 400ms 1 forwards  cubic-bezier(.68,-0.55,.27,1.55)',
  },
  '@keyframes queenKillFx': {
    '0%': {
      opacity: 0,
      transform: 'scale(5) rotate(-8deg)',
    },
    '100%': {
      opacity: 1,
      transform: 'scale(1) translateY(0) rotate(0)',
    },
  },
}));

export default function QueenKillIcons({ position, side }) {
  const classes = useStyles({ position });

  return (
    <QueenKillIconContainer
      className={clsx(classes.container, 'queen-kills')}
      position={position}
      side={side}
      element={<Icon className={classes.icon} path={mdiCrown} />}
    />
  );
}
