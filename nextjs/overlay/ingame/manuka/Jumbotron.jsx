import BerriesRemainingBox from './BerriesRemainingBox';
import GameTimeBox from './GameTimeBox';
import JumbotronPredictionMeter from './JumbotronPredictionMeter';
import QueenEggTracker from './QueenEggTracker';
import SnailCompletion from './SnailCompletion';

export default function Jumbotron({ flipTeams }) {
  return (
    <div
      id="jumbotron"
      className="general-game-stats absolute z-50 top-1 left-1/2 -translate-x-1/2 flex flex-col justify-center items-center"
    >
      <svg
        version="1.1"
        id="Layer_1"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 140 80"
        height="80"
        width="140"
        className="relative -top-2 -z-10 custom-color "
      >
        <defs>
          <mask id="predictionMask">
            <path id="predictionMeterOutline" fill="white" d="m108.1 72 6.9-12H25l6.9 12z" />
          </mask>
        </defs>
        <path
          id="jumbotronColorBase"
          fill="currentColor"
          d="M139.7 31.9 122 1.3c-.5-.8-1.3-1.3-2.2-1.3H20.2c-.9 0-1.8.5-2.2 1.3L.3 31.9c-.5.8-.5 1.8 0 2.6l25.6 44.3c.5.8 1.3 1.3 2.3 1.3h83.7c.9 0 1.8-.5 2.3-1.3l25.6-44.3c.3-.9.3-1.8-.1-2.6z"
        />
        <path
          id="topGlass"
          className="mix-blend-screen fill-[#c2c2c2]"
          d="M137.4 33.1 119.8 2.6H20.2L2.6 33.1z"
        />
        <path
          id="bottomGlass"
          className="mix-blend-screen fill-[#adadad]"
          d="m111.9 77.4 25.5-44.3H2.6l25.5 44.3z"
        />
        <path
          id="bottomGlassOuterEdge"
          className="mix-blend-screen fill-white"
          d="m2.6 33.1 25.5 44.3h83.8l25.5-44.3H136l-24.9 43.1H28.9L4 33.1z"
        />
        <path
          id="topGlassOuterEdge"
          className="mix-blend-screen fill-white"
          d="M4 33.1 21 3.8h98l17 29.3h1.4L119.8 2.6H20.2L2.6 33.1z"
        />
        <path
          id="highlight"
          className="mix-blend-screen fill-[#efefef] opacity-75"
          d="M119 3.8H21L4 33.1C41.2 15.7 70.3 9.6 124 12.4"
        />
        <g fill="none" mask="url(#predictionMask)">
          <JumbotronPredictionMeter flipTeams={flipTeams} />
          <path
            id="predictionMeterOutline"
            className="stroke-white opacity-80"
            strokeWidth="3"
            d="m108.1 72 6.9-12H25l6.9 12z"
          />
        </g>
        <path
          id="predictionMeterOutline"
          className="stroke-black"
          fill="none"
          d="m108.1 72 6.9-12H25l6.9 12z"
        />
      </svg>

      <div
        id="jumbotronTopGlass"
        className="absolute top-[-8px] left-1/2 "
        style={{
          transform: 'translateX(-50%) rotateX(0deg)',
          perspective: '28px',
        }}
      >
        <div
          className="game-time tracking-tight pt-4 pb-4 px-1 flex h-6 w-32 text-center items-center justify-between gap-1 opacity-75 mix-blend-multiply"
          style={{
            transform: 'rotateX(16deg) scale(.9)',
            transformStyle: 'preserve-3d',
          }}
        >
          <QueenEggTracker queenID={flipTeams ? 2 : 1} />
          <GameTimeBox className="relative -left-px text-black" />
          <QueenEggTracker queenID={flipTeams ? 1 : 2} className="-scale-x-100" />
        </div>
      </div>
      <div
        id="jumbotronTopGlass"
        className="absolute top-5 left-1/2 z-20"
        style={{
          transform: 'translateX(-50%) rotateX(0deg)',
          perspective: '35px',
        }}
      >
        <div
          className="tracking-tight pt-4 pb-4 px-3 flex h-9 w-32 gap-1 text-center items-center justify-between text-black opacity-80 mix-blend-multiply"
          style={{
            transform: 'rotateX(-16deg)',
            transformStyle: 'preserve-3d',
          }}
        >
          <BerriesRemainingBox />
          <SnailCompletion />
        </div>
      </div>
    </div>
  );
}
