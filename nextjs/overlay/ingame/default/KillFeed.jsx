import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import KillFeed from 'overlay/components/KillFeed';

const useStyles = makeStyles(theme => ({
  killFeed: {
    marginTop: '50px',
    maxHeight: '200px',
    overflow: 'hidden',
    '& .assists': {
      marginTop: '18px',
      position: 'absolute',
      left: '55px',
    },
    '& .kill-feed-entry': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      margin: '5px 0',
      '& .kill-time': {
        color: 'white',
        textAlign: 'right',
        marginRight: '8px',
        width: '40px',
      },
      '& .player-icon': {
        height: '24px',
        width: '24px',
        margin: '0 5px',
        '&.assist': {
          height: '12px',
          width: '12px',
          margin: '0 2px',
        },
      },
      '& .sword-icon': {
        color: 'white',
        transform: 'scaleX(-1)',
      },
    },
  },
}));

export default function KillFeedStyled({ ...props }) {
  const classes = useStyles();

  return (
    <KillFeed className={classes.killFeed} />
  );
}
