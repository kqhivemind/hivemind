import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { useOverlaySettings } from 'overlay/Settings';
import TeamName from 'overlay/components/TeamName';
import TeamScore from 'overlay/components/TeamScore';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'row' : 'row-reverse'),
  },
  section: {
    borderWidth: '5px',
    borderStyle: 'outset',
    borderColor: ({ team }) => theme.palette[team].light2,
    backgroundColor: ({ team }) => theme.palette[team].main,
    padding: '0 5px',
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontSize: '28px',
    fontWeight: 'bold',
    color: 'black',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
  teamName: {
    flexBasis: 0,
    flexGrow: 1,
  },
  score: {
    width: '80px',
    flexBasis: '80px',
    flexGrow: 0,
  },
}));

export default function TeamNameBox({ team, className }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ team, settings });
  const match = useMatch();
  const colorOnChange = { higher: '#550', duration: 1200 };
  const score = match?.score[team];

  return (
    <div className={clsx(classes.container, className, 'team-name-container')}>
      <div className={clsx(classes.teamName, classes.section, 'team-name')}>
        <TeamName team={team} />
      </div>
      {match.showScore && (
        <div className={clsx(classes.score, classes.section, 'score', `score-${score}`)}>
          <TeamScore team={team} colorOnChange={colorOnChange} />
        </div>
      )}
    </div>
  );
}

TeamNameBox.propTypes = {
  team: PropTypes.string.isRequired,
  className: PropTypes.string,
};
