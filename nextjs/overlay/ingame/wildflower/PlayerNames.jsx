import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from 'overlay/components/PlayerIcon';
import PlayerName from 'overlay/components/PlayerName';
import PlayerNamesBlock from 'overlay/components/PlayerNamesBlock';

const useStyles = makeStyles(theme => ({
  block: {
    width: '300px',
    display: 'flex',
    flexDirection: 'column',
    gap: '2px',
  },
  position: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    flexDirection: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'row' : 'row-reverse'),
    gap: '5px',
    flexBasis: 0,
    flexGrow: 1,
    textAlign: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'left' : 'right'),
  },
  icon: {
    height: '34px',
    padding: '2px',
    opacity: '0.2',
    '&.active': {
      opacity: '1',
    },
  },
  name: {
    flexBasis: 0,
    flexGrow: 1,
    color: 'black',
    fontFamily: 'Lato',
    overflow: 'hidden',
    visibility: 'hidden',
    '&.active': {
      visibility: 'visible',
    },
  },
}));

export default function PlayerNames({ team }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ team, settings });

  return (
    <PlayerNamesBlock className={clsx(classes.block, 'player-names', team)}>
      {POSITIONS_BY_TEAM[team].map(pos => (
        <div className={clsx(classes.position, 'position')} key={pos.ID}>
          <PlayerIcon className={clsx(classes.icon, 'icon')} position={pos} />
          <PlayerName className={clsx(classes.name, 'name')} position={pos} />
        </div>
      ))}
    </PlayerNamesBlock>
  );
}
