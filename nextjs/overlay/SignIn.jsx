import React, { createContext, useState, useContext } from 'react';

import { SIGN_IN_ACTIONS, CABINET_POSITIONS } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { useOverlaySettings } from 'overlay/Settings';

export const SignInContext = createContext({});

export function SignInProvider({ cabinetId, children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const [initialized, setInitialized] = useState(false);
  const playerName = {};
  const setPlayerName = {};
  for (const pos of Object.values(CABINET_POSITIONS)) {
    [playerName[pos.ID], setPlayerName[pos.ID]] = useState(null);
  }
  const ws = useWebSocket('/ws/signin');

  ws.onJsonMessage(message => {
    if (settings?.cabinet?.id == message.cabinetId) {
      if (message.action == SIGN_IN_ACTIONS.SIGN_IN) {
        setPlayerName[message.playerId](message.userName);
      }

      if (message.action == SIGN_IN_ACTIONS.SIGN_OUT) {
        setPlayerName[message.playerId](null);
      }
    }
  }, [settings?.cabinet?.id]);

  if (settings?.cabinet && !initialized) {
    setInitialized(true);
    axios.get(`/api/game/cabinet/${settings.cabinet.id}/signin/`).then(response => {
      for (const player of response.data.signedIn) {
        setPlayerName[player.playerId](player.userName);
      }
    });
  }

  return (
    <SignInContext.Provider value={playerName}>
      {children}
    </SignInContext.Provider>
  );
}

export function useSignIn() {
  return useContext(SignInContext);
}
