import React from 'react';
import clsx from 'clsx';
import Icon from '@mdi/react';
import { mdiSword } from '@mdi/js';
import { motion, AnimatePresence } from 'framer-motion';

import { POSITIONS_BY_ID } from 'util/constants';
import { useGameStats } from 'overlay/GameStats';
import { formatUTC } from 'util/dates';

export default function KillFeed({ className }) {
  const stats = useGameStats();

  return (
    <div className={clsx(className, "kill-feed")}>
      <AnimatePresence initial={false}>
      {stats?.killFeed?.map(kill => (
        <motion.div
          layout
          key={kill.id}
          className="kill-feed-entry"
          positionTransition
          initial={{ opacity: 0, x: 50, scale: 0.5 }}
          animate={{ opacity: 1, x: 0, scale: 1 }}
          exit={{ opacity: 0, scale: 0.5, transition: { duration: 0.3 } }}
        >
          {kill.gameTime && (
            <div className="kill-time">{formatUTC(kill.gameTime * 1000, 'm:ss')}</div>
          )}
          <div className="assists">
            {kill.assists.map(assist => (
              <img key={assist} className="player-icon assist" src={POSITIONS_BY_ID[assist].ICON} />
            ))}
          </div>
          <img className="player-icon killer" src={POSITIONS_BY_ID[kill.killer]?.ICON} />
          <Icon className="sword-icon" path={mdiSword} size={1} />
          <img className="player-icon victim" src={POSITIONS_BY_ID[kill.victim]?.ICON} />
        </motion.div>
      ))}
      </AnimatePresence>
    </div>
  );
}
