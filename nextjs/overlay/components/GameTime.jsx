import { useGameStats } from 'overlay/GameStats';
import { formatUTC } from 'util/dates';

export default function GameTime({ className }) {
  const stats = useGameStats();
  const gameTime = formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss');

  return <span className={className}>{gameTime}</span>;
}
