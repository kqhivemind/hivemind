import clsx from 'clsx';
import Head from 'next/head';
import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import medievalClasses from '../ingame/medieval/medieval.module.css';
import EventNameBox from './default/EventNameBox';
import TeamNameBox from './default/TeamNameBox';

export default function MedievalMatchPreview({}) {
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap"
          rel="stylesheet"
        ></link>
        <style type="text/css">
          {`
           .font-medieval { font-family: 'MedievalSharp', sans-serif; letter-spacing: 2px; }
           .carved-text { color: #898989; text-shadow: 1px 1px 3px black, 1px 1px 0px black, -1px -1px 0px white, -2px -2px 1px rgba(255,255,255,.5); }
           .engraved-text { color: #444; text-shadow: -1px -1px 3px rgba(0,0,0,.8), -1px -1px 0px black, 1px 1px 0px white, 2px 2px 3px rgba(255,255,255,0.5); }
           .shadow-text { text-shadow: 1px 1px 2px black; }
          `}
        </style>
      </Head>
      <div
        id="matchPreviewOverlay"
        className={clsx(
          'absolute aspect-video top-0 left-0 w-full  ',
          active ? 'visible' : 'invisible',
        )}
      >
        <div
          className={clsx(
            'aspect-video w-[75vw] top-[3.125vw] left-[12.5vw] absolute overflow-hidden flex flex-col items-start',
          )}
        >
          <EventNameBox
            className={clsx(
              'px-6 py-3 rounded bg-black/50 mx-auto',
              'event-name text-5xl text-white font-medieval',
              medievalClasses.ridgeBorder,
              medievalClasses.carvedText,
            )}
          />
          <div className=" grid grid-cols-2 w-full px-12 py-6 gap-12">
            <TeamNameBox
              className={clsx(
                'px-8 py-4 flex-grow',
                'team',
                'left-team',
                medievalClasses.ridgeBorder,
              )}
              team={leftTeam}
            />
            <TeamNameBox
              className={clsx(
                'px-8 py-4 flex-grow',
                'team',
                'right-team',
                medievalClasses.ridgeBorder,
              )}
              team={rightTeam}
            />
          </div>
          <img
            src="/static/medieval/slab.webp"
            className="absolute -z-10 inset-0 aspect-video w-full h-full "
          />
        </div>
      </div>
    </>
  );
}

MedievalMatchPreview.themeProps = {
  name: 'Medieval',
  displayName: 'Medieval',
  description: `Medieval match preview`,
};
