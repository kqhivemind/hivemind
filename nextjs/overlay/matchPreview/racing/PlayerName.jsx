import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Icon from '@mdi/react';

import { roleIcon } from '@/components/whiteboard';

const useStyles = makeStyles(theme => ({
  player: ({ team }) => ({
    flexGrow: 1,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 4px',
  }),
  image: {
    width: '115px',
    height: '115px',
    overflow: 'hidden',
    flexGrow: 0,
    flexBasis: '100px',
    '& img': {
      width: '100px',
      height: '115px',
      objectFit: 'scale-down',
    },
    '.player-names.pics &': {
      display: 'block',
    },
    marginLeft: "10px",

    "&.sixPlayers": {
      height: "100px",
      width: "100px",

      '& img': {
        width: '90px',
        height: '100px',
        objectFit: 'scale-down',
      },
    },
  },
  text: {
    flexGrow: '1',
    flexBasis: '0',
    display: 'flex',
    flexDirection: 'column',
    padding: '0 0 0 20px',
  },
  playerAndSceneRow: {
    display: 'flex',
    direction: 'row',
  },
  playerName: {
    fontSize: '24px',
    lineHeight: '24px',
    display: 'flex',
  },
  pronouns: {
    fontSize: '24px',
    lineHeight: '24px',
    mixWidth: '200px',
    margin: '0 10px 0 auto',
    padding: 0,
    display: 'flex',
    flexDirection: 'row',
  },
  pronounsText: {
    width: "100%",
    textAlign: "right",
    textTransform: "capitalize",
  },
  tidbit: {
    fontSize: '16px',
    textAlign: 'left',
    margin: '6px 0 0 0',
    paddingTop: "8px",
    maxHeight: "80px",
    minHeight: "80px",
    overflow: "hidden",
    maxWidth: "600px",
    textOverflow: "ellipsis",

    "&.sixPlayers": {
      maxHeight: "58px",
      minHeight: "58px",
    },
  },
}));

function makePronounStylingConsistent(pronouns) {
  if (!pronouns || pronouns.trim() === "") return pronouns;

  try {
    if (pronouns.indexOf('/') > -1) {
      return pronouns.toLowerCase().split(/\s*\/\s*/).join(" / ");
    } else {
      return pronouns.toLowerCase().split(/\s+/).join(" / ");
    }
  } catch(e) {
    return pronouns;
  }
}

export default function PlayerName({ player, team, className, sixPlayers }) {
  const classes = useStyles({ team });

  // TODO: Change to official missing avatar once I have it

  const sixPlayerClass = sixPlayers ? "sixPlayers" : "";

  return (
    <div className={clsx(classes.player, 'player', className)}>
      <div className={clsx(classes.image, 'player-image', sixPlayerClass)}>
        {player.image && !player.doNotDisplay ? (
          <img src={player.image}/>
        ) : (
          <img src={`/static/racing/kqsf-logo-icon.png`}/>
        )}
      </div>
      {player?.role && (
        <div className={clsx(classes.role, 'role')}>
          <Icon path={roleIcon[player.role]} size={2} />
        </div>
      )}
      <div className={clsx(classes.text, 'text')}>
        <div className={clsx(classes.playerAndSceneRow, 'player-scene-row')}>
          <div className={clsx(classes.playerName, 'player-name')}>
            {player.name}
            {player.scene ? " [ " + player.scene + " ]" : null}
          </div>
          <div className={clsx(classes.pronouns, 'pronouns')}>
            <div className={clsx(classes.pronounsText, "pronouns-text")}>
              {player.pronouns ? makePronounStylingConsistent(player.pronouns) : ''}
            </div>
          </div>
        </div>
        {player.tidbit && <div className={clsx(classes.tidbit, 'tidbit', sixPlayerClass)}>{player.tidbit}</div>}
      </div>
    </div>
  );
}
