import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import EventName from 'overlay/components/EventName';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  eventName: {
    padding: '12px 20px',
  }
}));

export default function EventNameBox({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();

  return (
    <div className={clsx(classes.container, className, 'event-container')}>
      <div className={clsx(classes.eventName)}>
        <EventName />
      </div>
    </div>
  );
}
