import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';

import {useMatch} from 'overlay/Match';
import TeamName from 'overlay/components/TeamName';
import PlayerName from './PlayerName';
import {GOLD_TEAM} from "@/util/constants";

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    gap: '1px',
  },
  containerLarge: {
    overflow: 'hidden',
    alignItems: 'center',
  },
  containerSmall: {
    height: '82px',
  },
  teamNameContainer: {
    flexDirection: ({team}) => team === GOLD_TEAM ? 'row-reverse' : 'row',
    display: "flex",

    width: '100%',
    padding: "30px 20px",

    height: "110px",

    borderRadius: "8px",
    border: "4px solid transparent",

    "&.blue": {
      background: "url('/static/racing/blue-pregame-team-banner.svg') padding-box center center, linear-gradient(to right, #010147, #010147) padding-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
    },

    "&.gold": {
      background: "url('/static/racing/gold-pregame-team-banner.svg') padding-box center center, linear-gradient(to right, #201006, #201006) padding-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
      textAlign: "right",
    },
  },
  teamNameWrapper: {
    // TODO: Test banana works
    fontSize: '48px',
    lineHeight: '40px',
    whiteSpace: 'nowrap',

    fontFamily: 'Orbitron, sans-serif',
    fontWeight: "bold",
    textTransform: 'uppercase',
    letterSpacing: '1px',

    maxWidth: "100%",

    flexGrow: 1,
    flexBasis: 0,

    overflow: "hidden",
    textOverflow: "ellipsis",

    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",
  },
  vsTextSpace: {
    minWidth: "65px",
    display: 'flex',
  },
  playerNames: {
    margin: '0 0 0 0',
    flexGrow: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    gap: '1px',
    fontFamily: 'Orbitron, sans-serif',
    fontWeight: "bold",
  },
  playerName: {
    borderRadius: "8px",
    border: "4px solid transparent",

    "&.blue": {
      background: "linear-gradient(to right, #010147, #010147) content-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
    },

    "&.gold": {
      background: "linear-gradient(to right, #201006, #201006) content-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
    },
  },
  insideBox: {
    color: 'white',
    minHeight: 'fit-content',
  },
}));

export default function TeamNameBox({team, className, teamColor}) {
  const classes = useStyles({team});
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  if (players?.length > 0) {
    return (
      <div className={clsx(classes.container, classes.containerLarge, className, 'team-container')}>
        <div className={clsx(classes.teamNameContainer, 'team-name', classes.insideBox, teamColor)}>
          <div className={clsx(classes.teamNameWrapper, "team-name-wrapper")}>
            <TeamName team={team}/>
          </div>
          <div className={clsx(classes.vsTextSpace)}></div>
        </div>

        <div className={clsx(classes.playerNames, 'player-names', {pics})}>
          {players.map(player => (
            <PlayerName
              key={player.id}
              className={clsx(classes.playerName, 'player-name', classes.insideBox, teamColor)}
              team={team}
              player={player}
              sixPlayers={players.length === 6}
            />
          ))}
        </div>
      </div>
    );
  }

  return (
    <div
      className={clsx(classes.container, classes.containerSmall, className, 'team-container-small')}
    >
      <div className={clsx(classes.teamNameContainer, 'team-name')}>
        <div className={clsx(classes.teamNameWrapper, "team-name-wrapper")}>
          <TeamName team={team}/>
        </div>
        <div className={clsx(classes.vsTextSpace)}></div>
      </div>
    </div>
  );
}
