import clsx from 'clsx';

import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import EventNameBox from './wildflower/EventNameBox';
import TeamNameBox from './wildflower/TeamNameBox';

import styles from './Wildflower.module.css';

export default function WildflowerOverlay({}) {
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <div id="matchPreviewOverlay" className={clsx(styles.container, { active })}>
      <EventNameBox className={clsx(styles.eventName, 'event-name', active)} />
      <TeamNameBox
        className={clsx(styles.teamContainer, 'left', 'team', leftTeam)}
        team={leftTeam}
      />
      <TeamNameBox
        className={clsx(styles.teamContainer, 'right', 'team', rightTeam)}
        team={rightTeam}
      />
    </div>
  );
}

WildflowerOverlay.themeProps = {
  name: 'wildflower',
  displayName: 'Wildflower (Light)',
  description: `Wildflower match preview`,
};
