import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Icon from '@mdi/react';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { roleIcon } from '@/components/whiteboard';

const useStyles = makeStyles(theme => ({
  player: ({ team }) => ({
    flexBasis: 0,
    flexGrow: 1,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: team == GOLD_TEAM ? 'row-reverse' : 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 20px',
    '&:not(:last-of-type)': {
      borderBottomWidth: '1px',
      borderBottomStyle: 'solid',
      borderBottomColor: theme.palette[team].dark2,
    },
  }),
  image: {
    display: 'none',
    width: '125px',
    height: '125px',
    overflow: 'hidden',
    flexGrow: 0,
    flexBasis: '125px',
    '& img': {
      width: '125px',
      height: '125px',
      objectFit: 'cover',
    },
    '.player-names.pics &': {
      display: 'block',
    },
  },
  text: {
    flexGrow: '1',
    flexBasis: '0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 20px',
    height: '100%',
  },
  playerName: {
    fontSize: '36px',
  },
  pronouns: {
    fontSize: '16px',
    fontStyle: 'italic',
  },
  tidbit: {
    fontSize: '18px',
    textAlign: 'center',
    margin: '10px 0 0 0',
    flexGrow: '1',
    flexBasis: '0',
    maxHeight: 'calc(100% - 60px)',
  },
}));

export default function PlayerName({ player, team, className }) {
  const classes = useStyles({ team });

  return (
    <div className={clsx(classes.player, 'player')}>
      <div className={clsx(classes.image, 'player-image')}>
        {player.image && !player.doNotDisplay && <img src={player.image} />}
      </div>
      {player?.role && (
        <div className={clsx(classes.role, 'role')}>
          <Icon path={roleIcon[player.role]} size={2} />
        </div>
      )}
      <div className={clsx(classes.text, 'text')}>
        <div className={clsx(classes.playerName, 'player-name')}>
          {player.scene ? `${player.name} [${player.scene}]` : player.name}
        </div>
        {player.pronouns && (
          <div className={clsx(classes.pronouns, 'pronouns')}>{player.pronouns}</div>
        )}
        {player.tidbit && <div className={clsx(classes.tidbit, 'tidbit')}>{player.tidbit}</div>}
      </div>
    </div>
  );
}
