import Bb5 from './Bb5MatchPreview';
import Default from './Default';
import FFVII from './FFVII';
import Manuka from './Manuka';
import MedievalMatchPreview from './MedievalMatchPreview';
import Racing from './Racing';
import Wildflower from './Wildflower';
import WildflowerDark from './WildflowerDark';

const allMatchPreviewThemes = [
  Default,
  Manuka,
  FFVII,
  MedievalMatchPreview,
  Racing,
  Wildflower,
  WildflowerDark,
  Bb5,
];

export function getMatchPreviewTheme(name) {
  for (const theme of allMatchPreviewThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allMatchPreviewThemes };
