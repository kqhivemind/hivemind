import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatch } from 'overlay/Match';
import TeamName from 'overlay/components/TeamName';
import PlayerName from './PlayerName';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    opacity: '0.9',
    background: ({ team }) => (team === 'blue' ? 'bg-blue-main' : 'bg-gold-main'),
  },

  teamName: {
    padding: '10px 0',
    minWidth: '100%',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '42px',
    whiteSpace: 'nowrap',
  },
}));

export default function TeamNameBox({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  return (
    <div className={clsx(classes.container, className, 'preview-team-container')}>
      <div
        className={clsx(
          classes.teamName,
          'preview-team-name font-custom ',
          team === 'blue' && ' text-white text-shadow-thicc',
        )}
      >
        <TeamName team={team} />
      </div>
      {players?.length > 0 && (
        <div className={clsx(classes.playerNames, 'preview-player-names py-2', { pics })}>
          {players.map(player => (
            <PlayerName
              key={team + player.id}
              className={clsx(classes.playerName, 'preview-player-name')}
              team={team}
              player={player}
            />
          ))}
        </div>
      )}
    </div>
  );
}
