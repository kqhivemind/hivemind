import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { GOLD_TEAM } from '@/util/constants';
import { useMatch } from 'overlay/Match';
import TeamName from 'overlay/components/TeamName';
import PlayerName from './PlayerName';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    gap: '1px',
  },
  containerLarge: {
    overflow: 'hidden',
    alignItems: 'center',
  },
  containerSmall: {
    height: '82px',
  },
  teamNameContainer: {
    flexDirection: ({ team }) => (team === GOLD_TEAM ? 'row-reverse' : 'row'),
    display: 'flex',

    width: '100%',
    padding: '30px 20px',

    height: '110px',

    borderRadius: '8px',
    border: '4px solid transparent',

    '&.blue': {
      // background:
      //   "url('/static/bb5/blue-pregame-team-banner.svg') padding-box center center, var(--bb5-blue-player-box-bg, linear-gradient(to right, #010147, #010147)) padding-box, var(--bb5-border-gradient)",
    },

    '&.gold': {
      // background:
      //   "url('/static/bb5/gold-pregame-team-banner.svg') padding-box center center, var(--bb5-blue-player-box-bg, linear-gradient(to right, #201006, #201006)) padding-box, var(--bb5-border-gradient)",
      // textAlign: 'right',
    },
  },
  teamNameWrapper: {
    // TODO: Test banana works
    fontSize: '48px',
    lineHeight: '40px',
    whiteSpace: 'nowrap',
    textTransform: 'uppercase',
    letterSpacing: '1px',

    maxWidth: '100%',

    flexGrow: 1,
    flexBasis: 0,

    overflow: 'hidden',
    textOverflow: 'ellipsis',

    color: 'black',
  },
  vsTextSpace: {
    minWidth: '65px',
    display: 'flex',
  },
  playerNames: {
    margin: '0 0 0 0',
    flexGrow: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    gap: '1px',
    fontWeight: 'bold',
  },
  playerName: {
    borderRadius: '8px',
    border: '4px solid transparent',

    '&.blue': {
      // background:
      //   'var(--bb5-blue-player-box-bg, linear-gradient(to right, #010147, #010147)) content-box, var(--bb5-border-gradient)',
    },

    '&.gold': {
      // background:
      //   'var(--bb5-blue-player-box-bg, linear-gradient(to right, #201006, #201006)) content-box, var(--bb5-border-gradient)',
    },
  },
  insideBox: {
    color: 'white',
    minHeight: 'fit-content',
  },
}));

export default function TeamNameBox({ team, className, teamColor }) {
  const classes = useStyles({ team });
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  if (players?.length > 0) {
    return (
      <div className={clsx(classes.container, classes.containerLarge, className, 'team-container')}>
        <div
          className={clsx(
            classes.teamNameContainer,
            'team-name bb5-font',
            classes.insideBox,
            teamColor,
          )}
        >
          <div
            className={clsx(
              classes.teamNameWrapper,
              'team-name-wrapper [text-shadow:0_3px_3px_rgba(0,0,0,.5)]',
            )}
            style={{ color: teamColor === 'gold' ? '#ffb330' : '#0625bc' }}
          >
            <TeamName team={team} />
          </div>
        </div>

        <div className={clsx(classes.playerNames, 'player-names', { pics })}>
          {players.map(player => (
            <PlayerName
              key={player.id}
              className={clsx(classes.playerName, 'player-name', classes.insideBox, teamColor)}
              team={team}
              player={player}
              sixPlayers={players.length === 6}
            />
          ))}
        </div>
      </div>
    );
  }

  return (
    <div
      className={clsx(classes.container, classes.containerSmall, className, 'team-container-small')}
    >
      <div className={clsx(classes.teamNameContainer, 'team-name')}>
        <div className={clsx(classes.teamNameWrapper, 'team-name-wrapper')}>
          <TeamName team={team} />
        </div>
        <div className={clsx(classes.vsTextSpace)}></div>
      </div>
    </div>
  );
}
