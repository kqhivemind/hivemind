import TournamentRegistrationForm from 'components/forms/TournamentRegistrationForm';
import { getAxios } from 'util/axios';

export default function TournamentRegistrationPage({
  tournament,
  scenes,
  questions,
  forSaleItems,
}) {
  return (
    <div className="flex items-start justify-center min-h-[calc(100vh-50px)] p-0 md:p-10 md:pb-12">
      <div className="bg-white box-shadow-xl md:rounded p-6 md:p-10 pb-10 w-full max-w-4xl mx-auto">
        <div
          className="-mx-6 -mt-6 md:-mx-10 md:-mt-10 md:rounded-t relative h-48 mb-8 flex items-center justify-center after:content-[''] after:bg-gradient-to-br after:mix-blend-overlay after:from-gray-500 after:to-gray-700 after:inset-0 after:absolute"
          style={
            tournament.registrationBanner
              ? {
                  backgroundImage: `url(${tournament.registrationBanner})`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                }
              : {
                  color: tournament.scene.foreGroundColor,
                  backgroundColor: tournament.scene.backgroundColor,
                }
          }
        >
          {!Boolean(tournament.registrationBanner) && (
            <hgroup className="text-center relative z-10 ">
              <div className="border-0 text-sm border-b border-solid border-b-current opacity-90 px-4 pb-1.5 mb-2">
                KQ <span className="uppercase ">{tournament.scene.name}</span> Presents
              </div>
              <h1 className="font-bold text-4xl my-0">{tournament.name}</h1>
            </hgroup>
          )}
        </div>
        {tournament && scenes && questions && forSaleItems && (
          <TournamentRegistrationForm
            tournament={tournament}
            scenes={scenes}
            questions={questions}
            forSaleItems={forSaleItems}
          />
        )}
      </div>
    </div>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  response = await axios.get(`/api/tournament/player-info-field/?tournament_id=${params.id}`);
  const questions = response.data.results;

  response = await axios.getAllPages(`/api/tournament/scene/`);
  const scenes = response;

  response = await axios.getAllPages(`/api/tournament/payment-option/`, {
    params: { tournamentId: tournament.id },
  });
  const forSaleItems = response;
  return {
    props: {
      tournament,
      title: `${tournament.name} - Tournament Registration`,
      description: tournament.description,
      hideContainer: true,
      questions,
      scenes,
      forSaleItems,
    },
  };
}
