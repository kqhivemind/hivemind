import React, { useState } from 'react';
import { CircularProgress, Breadcrumbs, Grid, Link } from '@material-ui/core';

import { getAxios } from 'util/axios';
import Title from 'components/Title';
import TeamSheetPlayerList from 'components/tables/TeamSheetPlayerList';

export default function TeamSheetPage({ tournament }) {
  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/t/${tournament.sceneName}/${tournament.slug}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={`${tournament.name} - Team Sheet`} />

      <Grid container direction="row" spacing={2}>
        <Grid xs={8}></Grid>

        <Grid xs={4}>
          <TeamSheetPlayerList
            title="Free Agents"
            players={tournament.players.filter(p => p.team === null)}
          />
        </Grid>
      </Grid>
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  tournament.teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: tournament.id },
  });
  tournament.players = await axios.getAllPages(`/api/tournament/player/`, {
    params: { tournamentId: tournament.id },
  });

  response = await axios.get(`/api/tournament/bracket/`, {
    params: { tournamentId: tournament.id },
  });
  tournament.brackets = [];

  for (const bracket of response.data.results) {
    bracket.matches = await axios.getAllPages(`/api/tournament/match/`, {
      params: { bracketId: bracket.id },
    });
    bracket.matches.sort((a, b) => a?.roundName?.localeCompare(b?.roundName) || a.id - b.id);
    tournament.brackets.push(bracket);
  }

  return {
    props: { tournament, title: tournament.name, description: tournament.description },
  };
}
