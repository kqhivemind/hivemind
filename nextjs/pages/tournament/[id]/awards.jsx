import { Breadcrumbs, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { getAxios } from 'util/axios';
import Title from 'components/Title';
import Code from 'components/Code';
const useStyles = makeStyles(theme => ({}));

export default function TournamentAwardsPage({ tournament, users }) {
  const classes = useStyles();

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/t/${tournament.sceneName}/${tournament.slug}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Code>{JSON.stringify(tournament.awards)}</Code>

      <Title title={`${tournament.name}: Awards`} />
    </div>
  );
}

export async function getStaticProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  tournament.teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: tournament.id },
  });

  return {
    props: { tournament, title: tournament.name, description: tournament.description },
    revalidate: 60,
  };
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking',
  };
}
