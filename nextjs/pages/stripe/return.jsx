import React from 'react';

import isServer from 'util/isServer';

export default function StripeReturn({ }) {
  if (!isServer()) window.close();

  return (
    <>
    </>
  );
}
