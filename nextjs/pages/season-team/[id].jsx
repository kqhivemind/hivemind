import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { formatUTC } from 'util/dates';
import Title from 'components/Title';
import PlayerListTable from 'components/tables/PlayerListTable';
import MatchListTable from 'components/tables/MatchListTable';
import BYOTeamForm from 'components/forms/BYOTeamForm';

const useStyles = makeStyles(theme => ({
}));

export default function BYOTeamPage({ team }) {
  const classes = useStyles();
  const completedMatches = team.matches.filter(match => match.isComplete);
  const remainingMatches = team.matches.filter(match => !match.isComplete);

  const form = (<BYOTeamForm team={team} />);

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${team.season.scene.name}`}>{team.season.scene.displayName}</Link>
        <Link href={`/season/${team.season.id}`}>{team.season.name}</Link>
        <Link href={`/season-team/${team.id}`}>{team.name}</Link>
      </Breadcrumbs>

			<Title title={team.name} form={form} />

      <PlayerListTable data={team.players} />

      {remainingMatches.length > 0 && (
        <MatchListTable title="Remaining Matches" data={remainingMatches} />
      )}

      {completedMatches.length > 0 && (
        <MatchListTable title="Completed Matches" data={completedMatches} />
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/byoteam/${params.id}`);
  const team = response.data;

  if (!team) return { notFound: true };

  response = await axios.get('/api/league/match/', { params: { byoteamId: team.id } });
  team.matches = response.data.results;

  response = await axios.get('/api/league/player/', { params: { byoteamId: team.id } });
  team.players = response.data.results;

  response = await axios.get(`/api/league/season/${team.season}`);
  team.season = response.data;

  response = await axios.get(`/api/game/scene/${team.season.scene}`);
  team.season.scene = response.data;

  return {
    props: {
      team,
      title: team.name,
      description: `${team.name}, a team playing in ${team.season.name} in ${team.season.scene.displayName}.`,
    },
  };
}

