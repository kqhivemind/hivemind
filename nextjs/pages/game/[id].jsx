import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Breadcrumbs, Link, Grid, Box, Tooltip } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_ID, POSITIONS_BY_TEAM } from 'util/constants';
import AchievementIcon from 'components/AchievementIcon';
import { SummaryTable, SummaryTableRow } from 'components/tables/SummaryTable';
import GameSummaryTable from 'components/tables/GameSummaryTable';
import PlayerStatsTable from 'components/tables/PlayerStatsTable';
import TeamStatsTable from 'components/tables/TeamStatsTable';
import SnailProgressChart from 'components/charts/SnailProgressChart';
import BerryChart from 'components/charts/BerryChart';
import WarriorChart from 'components/charts/WarriorChart';
import QueenDeathsChart from 'components/charts/QueenDeathsChart';
import BerriesRemainingChart from 'components/charts/BerriesRemainingChart';
import WinProbabilityChart from 'components/charts/WinProbabilityChart';
import GameEventsTable from 'components/tables/GameEventsTable';

import { mdiTrophy } from '@mdi/js';
import { Icon } from '@mdi/react';

const useStyles = makeStyles(theme => ({
  table: {
    // padding: theme.spacing(1),
    // marginTop: theme.spacing(3),
  },
  tableTitle: {
    // margin: theme.spacing(2),
    // marginBottom: theme.spacing(2)
  },
  backgroundBlue: {
    background: theme.gradients.blue.light,
  },
  backgroundGold: {
    background: theme.gradients.gold.light,
  },
  winnerBlue: {
    background: theme.gradients.blue.main,
  },
  winnerGold: {
    background: theme.gradients.gold.main,
  },
  gameSummary: {
    // margin: theme.spacing(2, 0),
    // padding: theme.spacing(1),
  },
  signedInTable: {
    flexWrap: 'nowrap',
  },
  signedInColumn: {
    flexBasis: 0,
    flexGrow: 1,
    marginLeft: '-1px',
  },
  signedInPlayer: {
    height: '50px',
    alignItems: 'center',
    // border: '1px solid #fff',
    // marginTop: '-1px',
    gap: '8px',
  },
  signedInIcon: {
    width: '48px',
    flexGrow: 0,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signedInName: {
    flexGrow: 1,
    flexBasis: 0,
    lineHeight: '50px',
  },
  signedInLink: {
    color: 'black',
  },
  topPlayerColumn: {
    //   margin: theme.spacing(0.5, 0),
    //   [theme.breakpoints.down('sm')]: {
    //     marginTop: theme.spacing(2),
    //   },
  },
  topPlayerRow: {
    alignItems: 'center',
    // padding: theme.spacing(1, 2),
    height: '36px',
  },
  topPlayerValue: {
    textAlign: 'right',
    '& .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  topPlayerIcons: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexGrow: 1,
  },
  topPlayerIcon: {
    width: '2rem',
    height: '2rem',
    marginLeft: '4px',
    '& img': {
      width: '2rem',
      height: '2rem',
      // border: '1px solid #555',
    },
  },
  gameAwards: {
    display: 'flex',
    flexDirection: 'row',
    gap: '16px',
    padding: '0 100px',
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      padding: '0 20px',
    },
  },
  gameAward: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    gap: '8px',
    background: 'linear-gradient(#eee, #ddd)',
    padding: '16px',
    borderRadius: '16px',
    fontSize: '18px',
    maxWidth: '400px',
  },
  playerIcon: {
    height: '24px',
    width: '24px',
  },
  teamIcon: {
    height: '24px',
    width: '24px',
  },
}));

function GameAward({ award }) {
  const classes = useStyles();
  return (
    <Tooltip arrow title={award.description}>
      <div className={classes.gameAward}>
        {award.players?.map(player => (
          <img className={classes.playerIcon} key={player} src={POSITIONS_BY_ID[player].ICON} />
        ))}
        {award.teams?.map(team => (
          <div
            className={classes.teamIcon}
            style={{ backgroundColor: team === 'blue' ? '#99ccff' : '#ffb330' }}
          />
        ))}
        <span>{award.title}</span>
      </div>
    </Tooltip>
  );
}

export default function GamePage({ game }) {
  const classes = useStyles({ game });
  const startTime = format(new Date(game.startTime), 'EEE, MMM dd, yyyy h:mmaaa');

  function GamePagePaper({ className, children, title, ...props }) {
    return (
      <div className={clsx(classes.table, className)} elevation={4} {...props}>
        {title && (
          <Typography variant="h2" className={clsx(classes.tableTitle, 'mb-4')}>
            {title}
          </Typography>
        )}

        {children}
      </div>
    );
  }

  function TopPlayerRow({ title, value }) {
    if (!value) return null;
    return (
      <Grid item container direction="row" className={classes.topPlayerRow}>
        <Grid item xs={5} className={classes.topPlayerTitle}>
          <Typography className="font-bold">{title}</Typography>
        </Grid>

        <Grid item container direction="row" xs={7} className={clsx(classes.topPlayerIcons)}>
          {value?.players &&
            value.players.map(position => (
              <Grid item className={classes.topPlayerIcon} key={position}>
                <img src={POSITIONS_BY_ID[position].ICON} />
              </Grid>
            ))}
          <Typography className="h-8 px-3 bg-gray-200 font-bold text-lg leading-8 rounded-r">
            {value?.count}
          </Typography>
        </Grid>
        {/* <Grid item xs={1} className={classes.topPlayerValue}>
        </Grid> */}
      </Grid>
    );
  }

  function loggedInPlayerGrid({ team, bgClass }) {
    return (
      <Grid
        item
        container
        direction="column"
        className={clsx(classes.signedInColumn, 'space-y-px')}
      >
        {POSITIONS_BY_TEAM[team].map(pos => (
          <Grid
            item
            container
            direction="row"
            key={pos.ID}
            className={clsx(
              classes.signedInPlayer,
              game.signedIn[pos.ID] ? bgClass : 'bg-gray-100',
            )}
          >
            <Grid item className={classes.signedInIcon}>
              <img src={pos.IMAGE} style={{ transform: team === 'gold' && 'scaleX(-1)' }} />
            </Grid>
            <Grid item className={classes.signedInName}>
              {game.signedIn[pos.ID] && game.signedIn[pos.ID].isProfilePublic && (
                <>
                  <Link
                    className={classes.signedInLink}
                    href={`/user/${game.signedIn[pos.ID]?.id}`}
                  >
                    {game.signedIn[pos.ID]?.name}
                  </Link>

                  {game.userAchievements.results
                    .filter(a => a.user == game.signedIn[pos.ID]?.id)
                    .map(userAchievement => (
                      <AchievementIcon
                        key={userAchievement.id}
                        team={team}
                        achievement={userAchievement}
                      />
                    ))}
                </>
              )}
              {game.signedIn[pos.ID] && !game.signedIn[pos.ID].isProfilePublic && (
                <>{game.signedIn[pos.ID].name}</>
              )}
            </Grid>
          </Grid>
        ))}
      </Grid>
    );
  }

  return (
    <div className="space-y-12">
      <Breadcrumbs>
        <Link href={`/scene/${game.sceneName}`}>{game.sceneDisplayName}</Link>
        <Link href={`/cabinet/${game.sceneName}/${game.cabinetName}`}>
          {game.cabinetDisplayName}
        </Link>
        {game.match && (
          <Link href={`/season/${game.match.event.season.id}`}>{game.match.event.season.name}</Link>
        )}
        {game.match && (
          <Link href={`/event/${game.match.event.id}`}>
            {format(new Date(game.match.event.eventDate), 'MMMM d, yyyy')}
          </Link>
        )}
        {game.match && (
          <Link href={`/match/${game.match.id}`}>
            {game.match.blueTeam.name} vs. {game.match.goldTeam.name}
          </Link>
        )}
        {game.tournamentMatch && (
          <Link
            href={`/t/${game.tournamentMatch.tournament.sceneName}/${game.tournamentMatch.tournament.slug}`}
          >
            {game.tournamentMatch.tournament.name}
          </Link>
        )}
        {game.tournamentMatch && (
          <Link href={`/tournament-match/${game.tournamentMatch.id}`}>
            {game.tournamentMatch.blueTeam?.name ?? '(unknown team)'} vs.{' '}
            {game.tournamentMatch.goldTeam?.name ?? '(unknown team'}
          </Link>
        )}
      </Breadcrumbs>

      <header className="space-y-4">
        <div className="flex gap-4 justify-between items-center">
          <Typography
            className="flex flex-wrap md:flex-nowrap justify-between flex-grow"
            variant="h1"
            suppressHydrationWarning
          >
            Game {game.gameId}{' '}
            <span suppressHydrationWarning className="whitespace-nowrap font-normal text-gray-700">
              {startTime}
            </span>
          </Typography>
          <div
            className={clsx(
              'w-10 h-10 rounded flex justify-center items-center text-white md:order-first',
              game.winningTeamDisplay === 'Blue' ? classes.winnerBlue : classes.winnerGold,
            )}
          >
            <Icon path={mdiTrophy} size={1.25} />
          </div>
        </div>

        <GamePagePaper className={clsx(classes.gameSummary, 'rounded bg-gray-100  ')}>
          <GameSummaryTable
            game={game}
            showGameVersion
            className="md:columns-2 text-lg md:text-xl"
          />
        </GamePagePaper>
      </header>

      {game.awards && (
        <GamePagePaper className={classes.gameAwards}>
          {game.awards.map(award => (
            <GameAward key={award} award={award} />
          ))}
        </GamePagePaper>
      )}

      <GamePagePaper className={classes.gameStats} title="Game Stats">
        <Grid container direction="row" spacing={2}>
          <TeamStatsTable stats={game} xs={12} md={6} />

          <Grid
            item
            container
            direction="column"
            className={clsx(classes.topPlayerColumn, 'space-y-1')}
            xs={12}
            md={6}
          >
            <TopPlayerRow title="Most Kills" value={game.mostKills} />
            <TopPlayerRow title="Most Military Kills" value={game.mostMilitaryKills} />
            <TopPlayerRow title="Most Deaths" value={game.mostDeaths} />
            <TopPlayerRow title="Most Berries" value={game.mostBerries} />
            <TopPlayerRow title="Most Warrior Uptime" value={game.mostWarriorUptime} />
          </Grid>
        </Grid>
      </GamePagePaper>

      {Object.keys(game.signedIn).length > 0 && (
        <GamePagePaper className={clsx(classes.signedIn)} title="Players Signed In">
          <Grid
            container
            direction="row"
            className={clsx(classes.signedInTable, 'rounded overflow-hidden space-x-px')}
          >
            {loggedInPlayerGrid({ team: BLUE_TEAM, bgClass: classes.backgroundBlue })}
            {loggedInPlayerGrid({ team: GOLD_TEAM, bgClass: classes.backgroundGold })}
          </Grid>
        </GamePagePaper>
      )}

      <GamePagePaper className={classes.playerStats} title="Player Stats">
        <PlayerStatsTable game={game} />
      </GamePagePaper>

      <GamePagePaper className={clsx(classes.charts)} title="Charts">
        <div className="bg-gray-100 rounded p-4 space-y-4">
          <SnailProgressChart game={game} />
          <BerryChart game={game} />
          <WarriorChart game={game} />
          <QueenDeathsChart game={game} />
          <BerriesRemainingChart game={game} />
          <WinProbabilityChart game={game} />
        </div>
      </GamePagePaper>

      <GamePagePaper className={classes.events} title="Full Play-By-Play Game Events">
        <GameEventsTable game={game} />
      </GamePagePaper>
    </div>
  );
}

GamePage.propTypes = {
  game: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/game/${params.id}/`);
  let game = response.data;

  if (!game) return { notFound: true };

  response = await axios.get(`/api/game/game/${params.id}/stats`);
  game = { ...game, ...response.data };

  if (game.match) {
    response = await axios.get(`/api/league/match/${game.match}/`);
    game.match = response.data;

    response = await axios.get(`/api/league/season/${game.match.event.season}/`);
    game.match.event.season = response.data;
  }

  if (game.tournamentMatch) {
    response = await axios.get(`/api/tournament/match/${game.tournamentMatch}/`);
    game.tournamentMatch = response.data;

    if (game.tournamentMatch.blueTeam) {
      response = await axios.get(`/api/tournament/team/${game.tournamentMatch.blueTeam}/`);
      game.tournamentMatch.blueTeam = response.data;
    }

    if (game.tournamentMatch.goldTeam) {
      response = await axios.get(`/api/tournament/team/${game.tournamentMatch.goldTeam}/`);
      game.tournamentMatch.goldTeam = response.data;
    }
  }

  response = await axios.get(`/api/achievement/user-achievement/`, { params: { gameId: game.id } });
  game.userAchievements = response.data;

  const title = `Game ${game.id}`;
  const description = `${game.winningTeamDisplay} ${game.winConditionDisplay} victory on ${
    game.map
  }. Game played on ${format(new Date(game.startTime), 'MMM d')} at ${format(
    new Date(game.startTime),
    'hh:mm aa',
  )} at ${game.cabinet.displayName} in ${game.sceneDisplayName}.`;

  return { props: { game, title, description } };
}
