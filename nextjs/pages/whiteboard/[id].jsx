import { Breadcrumbs, Link } from '@material-ui/core';

import { getAxios } from '@/util/axios';
import { Whiteboard } from '@/components/whiteboard';
import Title from '@/components/Title';

export default function WhiteboardPage({ session, scene }) {
  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${scene.name}`}>{scene.displayName}</Link>
      </Breadcrumbs>

      <Title title={`Whiteboard Session: ${scene.displayName}`} />

      <div>
        Sign up for automatically generated competitive matches. Select what role(s) you'd like to
        play, and you will be placed onto a team for one set of games.
      </div>

      <Whiteboard session={session} />
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/whiteboard/session/${params.id}/`);
  const session = response.data;

  if (!session) return { notFound: true };

  response = await axios.get(`/api/game/scene/${session.scene}/`);
  const scene = response.data;

  return {
    props: {
      session,
      scene,
      title: `Whiteboard Session: ${scene.displayName}`,
      description: scene.description,
    },
  };
}
