import React, { useState, useEffect, useMemo } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress, Grid, Link } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiEmoticonSad } from '@mdi/js';
import { format } from 'date-fns';

import { mapColor } from 'theme/colors';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import {
  Recap,
  RecapItem,
  RecapMainItem,
  RecapColumn,
  RecapSubItem,
  RecapList,
  RecapListItem,
  RecapBarChart,
  RecapPieChart,
  RecapShareImage,
} from 'components/recap';
import { CABINET_POSITIONS, POSITIONS_BY_ID, STAT_FORMATTING_TYPE } from 'util/constants';
import { formatInterval } from 'util/dates';
import { formatPercent, formatStatValue } from 'util/formatters';
import SceneColorBox from 'components/SceneColorBox';

const useStyles = makeStyles(theme => ({
  shareImg: {
    height: '945px',
    width: '756px',
  },
}));

export default function UserRecapPage() {
  const router = useRouter();
  const requestUser = useUser();
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();

  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  const [generating, setGenerating] = useState(false);
  const [recapData, setRecapData] = useState(null);

  const winPct = v => (v?.games > 0 ? v.wins / v.games : null);
  const colorByMap = mapName => mapColor[mapName];

  const getUserInfo = async () => {
    if (!router.isReady) return;

    let response = await axios.get(`/api/user/user/${router.query.id}/`);
    setUser(response.data);

    response = await axios.get(`/api/stats/user-recap/`, {
      params: { userId: router.query.id, year: router.query.year },
    });

    if (!response?.data?.count) {
      if (generating) {
        setTimeout(getUserInfo, 1000);
        return;
      }

      if (requestUser?.id == router.query.id) {
        response = await axios.post(`/api/stats/user-recap/generate/`, { year: router.query.year });
        if (response?.data?.success) {
          setError(null);
          setGenerating(true);
          setTimeout(getUserInfo, 1000);
        } else {
          setError(response?.data?.error ?? 'unknown error');
        }

        return;
      }

      setError('recap not found');
      return;
    }

    if (response.data.results[0] && !response.data.results[0].data) {
      setGenerating(true);
      setTimeout(getUserInfo, 1000);
      return;
    }

    setError(null);
    setGenerating(false);
    setRecapData({
      shareImage: response.data.results[0].shareImage,
      ...response.data.results[0].data,
    });
  };

  useEffect(() => {
    getUserInfo();
  }, [router.isReady, requestUser?.id]);

  const byPosition = useMemo(() => {
    if (!recapData?.byPosition) return {};

    const positionStats = {};
    for (const pos of Object.values(CABINET_POSITIONS)) {
      if (pos.ID in recapData.byPosition) {
        positionStats[pos.POSITION] = positionStats[pos.POSITION] ?? {};
        for (const [k, v] of Object.entries(recapData.byPosition[pos.ID])) {
          positionStats[pos.POSITION][k] = v + (positionStats[pos.POSITION][k] ?? 0);
        }
      }
    }

    return positionStats;
  }, [recapData]);

  const allDroneStats = useMemo(() => {
    if (!recapData?.statTotals || !byPosition?.queen) return {};

    const droneStats = {};
    for (const [k, v] of Object.entries(recapData.statTotals)) {
      droneStats[k] = v - (byPosition?.queen[k] ?? 0);
    }

    return droneStats;
  }, [recapData, byPosition]);

  const favoritePosition = useMemo(() => {
    if (!byPosition) return null;
    return Object.keys(byPosition).sort((a, b) => byPosition[b].games - byPosition[a].games)[0];
  }, [byPosition]);

  const favoriteMap = useMemo(() => {
    if (!recapData?.byMap) return null;

    return Object.values(recapData.byMap).sort((a, b) => winPct(b) - winPct(a))[0];
  }, [recapData?.byMap]);

  const worstMap = useMemo(() => {
    if (!recapData?.byMap) return null;

    return Object.values(recapData.byMap).sort((a, b) => winPct(a) - winPct(b))[0];
  }, [recapData?.byMap]);

  const totalStandardMapGames = useMemo(() => {
    if (!recapData?.byMap) return null;

    return Object.values(recapData.byMap)
      .map(i => i.games)
      .reduce((a, b) => a + b, 0);
  }, [recapData?.byMap]);

  const favoriteDronePosition = useMemo(() => {
    if (!byPosition) return null;
    return Object.keys(byPosition)
      .filter(i => i !== 'queen')
      .sort((a, b) => byPosition[b].games - byPosition[a].games)[0];
  }, [byPosition]);

  const mostPlayedWith = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedWith)
      .sort((a, b) => b.count - a.count)
      .slice(0, 5);
  }, [recapData?.playedWith]);

  const mostPlayedAgainst = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedAgainst).sort((a, b) => b.count - a.count)[0];
  }, [recapData?.playedAgainst]);

  const nemesis = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedAgainst)
      .filter(i => i.count >= 10)
      .sort((a, b) => b.losses / b.count - a.losses / a.count)[0];
  }, [recapData?.playedAgainst]);

  const combo = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedWith)
      .filter(i => i.count >= 10)
      .sort((a, b) => b.wins / b.count - a.wins / a.count)[0];
  }, [recapData?.playedWith]);

  if (error !== null) {
    return <>Could not load recap: {error}.</>;
  }

  if (user === null) {
    return <CircularProgress />;
  }

  if (generating) {
    return (
      <Recap title={`${user.name}'s ${router.query.year} Recap`}>
        <RecapItem>
          <RecapMainItem>
            <CircularProgress /> Generating your recap...
          </RecapMainItem>
        </RecapItem>
      </Recap>
    );
  }

  if (recapData === null) {
    return <CircularProgress />;
  }

  if (recapData?.statTotals?.games === 0) {
    return (
      <Recap title={`${user.name}'s ${recapData.year} Recap`}>
        <RecapItem>
          <RecapMainItem>Worst year ever?</RecapMainItem>

          <RecapSubItem>
            {user.name} didn't play any games in {recapData.year}.{' '}
            <Icon path={mdiEmoticonSad} size={1} />
          </RecapSubItem>
        </RecapItem>
      </Recap>
    );
  }

  return (
    <Recap title={`${user.name}'s ${recapData.year} Recap`}>
      <RecapItem>
        <RecapMainItem>
          You played <b>{recapData.statTotals.games?.toLocaleString() || 0}</b> total games in{' '}
          {recapData.year}
        </RecapMainItem>

        <RecapSubItem>
          Your team won <b>{recapData.statTotals.wins?.toLocaleString() || 0}</b> games and lost{' '}
          <b>{recapData.statTotals.losses?.toLocaleString() || 0}</b>.
        </RecapSubItem>

        <RecapSubItem>
          That's a total of <b>{formatInterval(recapData.statTotals.timePlayed)}</b> of game time.
        </RecapSubItem>

        <RecapSubItem>
          You spent <b>{formatInterval(recapData.statTotals.warriorUptime)}</b> of that time as a
          warrior.
        </RecapSubItem>

        <RecapSubItem>
          Your favorite cab position was <b>{favoritePosition}</b>, accounting for{' '}
          <b>{formatPercent(byPosition[favoritePosition].games / recapData.statTotals.games)}</b> of
          your games.
        </RecapSubItem>

        {recapData.famines && (
          <RecapSubItem>
            <b>{recapData.famines.toLocaleString()}</b> of your games went to famine.
          </RecapSubItem>
        )}

        <RecapList>
          <RecapColumn>
            <RecapListItem name="Kills" value={recapData.statTotals.kills?.toLocaleString() || 0} />
            <RecapListItem
              name="Deaths"
              value={recapData.statTotals.deaths?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Queen Kills"
              value={recapData.statTotals.queenKills?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Military Kills"
              value={recapData.statTotals.militaryKills?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Military Deaths"
              value={recapData.statTotals.militaryDeaths?.toLocaleString() || 0}
            />
          </RecapColumn>
          <RecapColumn>
            <RecapListItem
              name="Bump Assists"
              value={recapData.statTotals.bumpAssists?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Berries Deposited"
              value={recapData.statTotals.berries?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Berries Kicked In"
              value={recapData.statTotals.berriesKicked?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Snail Distance"
              value={`${recapData.statTotals.snailMeters?.toLocaleString() || 0} m`}
            />
            <RecapListItem
              name="Times Eaten By Snail"
              value={recapData.statTotals.eatenBySnail?.toLocaleString() || 0}
            />
          </RecapColumn>
        </RecapList>
      </RecapItem>

      {recapData?.gamesByScene?.length > 1 && (
        <RecapItem>
          <RecapMainItem>
            You played games at <b>{recapData.gamesByScene.length}</b> different scenes
          </RecapMainItem>

          <RecapPieChart
            title=""
            data={recapData.gamesByScene}
            getLabel={i => i.name}
            getValue={i => i.games}
            getColor={i => i.color}
          />
        </RecapItem>
      )}

      {recapData?.gamesByMonth && (
        <RecapItem>
          <RecapMainItem>Your games by month</RecapMainItem>

          <RecapBarChart
            title=""
            data={recapData.gamesByMonth.data}
            stacks={recapData.gamesByMonth.scenes}
            labelKey="month"
            stackLabelKey="name"
            stackColorKey="backgroundColor"
          />
        </RecapItem>
      )}

      {recapData?.playedWith && (
        <RecapItem>
          <RecapMainItem>
            You played with <b>{Object.keys(recapData.playedWith).length}</b> other HiveMind users
          </RecapMainItem>

          {combo && (
            <RecapSubItem>
              You played best when paired with <b>{combo.name}</b>, winning{' '}
              <b>{formatPercent(combo.wins / combo.count, 0)}</b> of your games together.
            </RecapSubItem>
          )}

          {mostPlayedAgainst && (
            <RecapSubItem>
              Your most common opponent was <b>{mostPlayedAgainst.name}</b>. You won{' '}
              {mostPlayedAgainst.wins} of your <b>{mostPlayedAgainst.count}</b> games against them.
            </RecapSubItem>
          )}

          {nemesis && (
            <RecapSubItem>
              Your nemesis was <b>{nemesis.name}</b>, who defeated you{' '}
              <b>{formatPercent(nemesis.losses / nemesis.count, 0)}</b> of the time.
            </RecapSubItem>
          )}

          <RecapList title={`Your ${recapData.year} BeeFFs`}>
            {mostPlayedWith.map(player => (
              <RecapListItem
                key={player.playerId}
                name={`${player.name} ${player.scene ? `[${player.scene}]` : ''}`}
                value={player.count}
              />
            ))}
          </RecapList>
        </RecapItem>
      )}

      {recapData?.byMap && favoriteMap && worstMap && totalStandardMapGames && (
        <RecapItem>
          <RecapMainItem>
            Your best map was <b>{favoriteMap.mapName}</b>
          </RecapMainItem>

          <RecapSubItem>
            You won <b>{formatPercent(favoriteMap.wins / favoriteMap.games, 1)}</b> of your games
            there.
          </RecapSubItem>

          <RecapSubItem>
            Your worst map was <b>{worstMap.mapName}</b>, with a winning percentage of{' '}
            <b>{formatPercent(worstMap.wins / worstMap.games, 1)}</b>.
          </RecapSubItem>

          <RecapPieChart
            title="Games Played by Map"
            data={Object.values(recapData.byMap)}
            getLabel={i => i.mapName}
            getValue={i => i.games}
            getColor={i => colorByMap(i.mapName)}
            getTooltipValue={i =>
              `${i.toLocaleString()} (${formatPercent(i / totalStandardMapGames, 1)})`
            }
          />
        </RecapItem>
      )}

      {byPosition?.queen?.games > 0 && (
        <RecapItem>
          <RecapMainItem>
            You played <b>{byPosition.queen.games?.toLocaleString() || 0}</b> games as Queen
          </RecapMainItem>

          <RecapSubItem>
            That's <b>{formatPercent(byPosition.queen.games / recapData.statTotals.games, 1)}</b> of
            your total games for the year.
          </RecapSubItem>

          <RecapSubItem>
            Your team won <b>{byPosition.queen.wins?.toLocaleString() || 0}</b> games and lost{' '}
            <b>{byPosition.queen.losses?.toLocaleString() || 0}</b>.
          </RecapSubItem>

          <RecapSubItem>
            You played Queen for a total of <b>{formatInterval(byPosition.queen.timePlayed)}</b>.
          </RecapSubItem>

          <RecapList>
            <RecapListItem name="Kills" value={byPosition.queen.kills?.toLocaleString() || 0} />
            <RecapListItem name="Deaths" value={byPosition.queen.deaths?.toLocaleString() || 0} />
            <RecapListItem
              name="Queen Kills"
              value={byPosition.queen.queenKills?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Military Kills"
              value={byPosition.queen.militaryKills?.toLocaleString() || 0}
            />

            <RecapListItem
              name="Bump Assists"
              value={byPosition.queen.bumpAssists?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Berries Kicked In"
              value={byPosition.queen.berriesKicked?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Military Kills per Minute"
              value={formatStatValue(
                recapData.statTotals.queenKpm,
                STAT_FORMATTING_TYPE.THOUSANDTHS,
              )}
            />
            {console.log(byPosition.queen)}
          </RecapList>
        </RecapItem>
      )}

      {recapData?.bestGameMil?.gameId && (
        <RecapItem>
          <RecapMainItem>Your best military game</RecapMainItem>

          <RecapSubItem>
            Your best military game was{' '}
            <Link href={`/game/${recapData.bestGameMil.gameId}`}>
              Game {recapData.bestGameMil.gameId}
            </Link>
            , played on <b>{format(new Date(recapData.bestGameMil.startTime), 'MMM d')}</b> at{' '}
            <b>{format(new Date(recapData.bestGameMil.startTime), 'h:mm a')}</b>.
          </RecapSubItem>

          <RecapSubItem>
            This game was played at {recapData.bestGameMil.cabinetName} in{' '}
            {recapData.bestGameMil.sceneName}.
          </RecapSubItem>
          <RecapSubItem>
            You played as <b>{POSITIONS_BY_ID[recapData.bestGameMil.playerId].NAME}</b>.
          </RecapSubItem>
          <RecapList>
            <RecapListItem
              name="Kills"
              value={recapData.bestGameMil.kills?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Deaths"
              value={recapData.bestGameMil.deaths?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Military Kills"
              value={recapData.bestGameMil.militaryKills?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Military Deaths"
              value={recapData.bestGameMil.militaryDeaths?.toLocaleString() || 0}
            />
            <RecapListItem
              name="Queen Kills"
              value={recapData.bestGameMil.queenKills?.toLocaleString() || 0}
            />
          </RecapList>
        </RecapItem>
      )}

      {recapData?.tournaments && (
        <RecapItem>
          <RecapMainItem>
            You played in <b>{recapData.tournaments.length}</b> tournaments this year.
          </RecapMainItem>

          <RecapList>
            {recapData.tournaments.map(tournament => (
              <RecapListItem
                key={tournament.name}
                name={
                  <>
                    <SceneColorBox color={tournament.sceneColor} /> {tournament.name}
                  </>
                }
                value={tournament.date}
              />
            ))}
          </RecapList>

          <RecapSubItem>
            <i>Only tournaments with player registration through HiveMind are available here.</i>
          </RecapSubItem>
        </RecapItem>
      )}

      {allDroneStats?.games > 0 && (
        <RecapItem>
          <RecapMainItem>
            You played <b>{allDroneStats.games?.toLocaleString() || 0}</b> games as a drone
          </RecapMainItem>

          <RecapSubItem>
            Your team won <b>{allDroneStats.wins?.toLocaleString() || 0}</b> games and lost{' '}
            <b>{allDroneStats.losses?.toLocaleString() || 0}</b>.
          </RecapSubItem>

          <RecapSubItem>
            That's a total of <b>{formatInterval(allDroneStats.timePlayed)}</b> of game time.
          </RecapSubItem>

          <RecapSubItem>
            You spent <b>{formatPercent(allDroneStats.warriorUptime / allDroneStats.timePlayed)}</b>{' '}
            of that time as a warrior.
          </RecapSubItem>

          <RecapSubItem>
            When playing as a drone, you picked <b>{favoriteDronePosition}</b> most often (
            <b>{formatPercent(byPosition[favoriteDronePosition].games / allDroneStats.games)}</b> of
            your drone games).
          </RecapSubItem>

          <RecapList>
            <RecapColumn>
              <RecapListItem name="Kills" value={allDroneStats.kills?.toLocaleString() || 0} />
              <RecapListItem name="Deaths" value={allDroneStats.deaths?.toLocaleString() || 0} />
              <RecapListItem
                name="Queen Kills"
                value={allDroneStats.queenKills?.toLocaleString() || 0}
              />
              <RecapListItem
                name="Military Kills"
                value={allDroneStats.militaryKills?.toLocaleString() || 0}
              />
              <RecapListItem
                name="Military Deaths"
                value={allDroneStats.militaryDeaths?.toLocaleString() || 0}
              />
            </RecapColumn>
            <RecapColumn>
              <RecapListItem
                name="Bump Assists"
                value={allDroneStats.bumpAssists?.toLocaleString() || 0}
              />
              <RecapListItem
                name="Berries Deposited"
                value={allDroneStats.berries?.toLocaleString() || 0}
              />
              <RecapListItem
                name="Berries Kicked In"
                value={allDroneStats.berriesKicked?.toLocaleString() || 0}
              />
              <RecapListItem
                name="Snail Distance"
                value={`${allDroneStats.snailMeters?.toLocaleString() || 0} m`}
              />
              <RecapListItem
                name="Times Eaten By Snail"
                value={allDroneStats.eatenBySnail?.toLocaleString() || 0}
              />
            </RecapColumn>
          </RecapList>
        </RecapItem>
      )}

      <RecapShareImage {...{ user, recapData, mostPlayedWith, favoritePosition }} />
    </Recap>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/stats/user-recap/`, {
    params: { userId: params.id, year: params.year },
  });

  const props = {};
  if (response?.data?.count) {
    const recap = response.data.results[0];

    response = await axios.get(`/api/user/user/${params.id}/public-data/`);
    const user = response.data;

    if (user) {
      props.title = `${user.name}'s ${params.year} Recap`;
      props.description = `Check out ${user.name}'s ${params.year} recap on kqhivemind.com!`;
    }

    if (recap.shareImage) {
      props.image = recap.shareImage;
    }
  }

  return { props };
}
