import React from 'react';
import { Breadcrumbs, Link, Typography, List, ListItem } from '@material-ui/core';

import Title from 'components/Title';
import CreateSceneFromRequestForm from 'components/forms/CreateSceneFromRequestForm';

export default function SceneRequestPage({ sceneRequestId }) {
  return (
    <>
      <Breadcrumbs>
        <Link href={`/admin/scene-requests`}>New Scene Registrations</Link>
      </Breadcrumbs>

      <Title title="New Scene Registration" />

      <CreateSceneFromRequestForm sceneRequestId={sceneRequestId} />
    </>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { sceneRequestId: params.id },
  };
}
