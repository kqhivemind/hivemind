import React from 'react';
import PropTypes from 'prop-types';

import App from 'overlay/App';
import theme from 'theme/overlay';
import { GameStatsProvider } from 'overlay/GameStats';
import { OverlaySettingsProvider, IngameOverlay } from 'overlay/Settings';
import { SignInProvider } from 'overlay/SignIn';
import { MatchProvider } from 'overlay/Match';
import { MatchStatsProvider } from 'overlay/MatchStats';

export default function InGameOverlayPage({ id }) {
  return (
    <OverlaySettingsProvider id={id}>
      <GameStatsProvider>
        <SignInProvider>
          <MatchProvider>
            <IngameOverlay />
          </MatchProvider>
        </SignInProvider>
      </GameStatsProvider>
    </OverlaySettingsProvider>
  );
}

InGameOverlayPage.propTypes = {
  id: PropTypes.string.isRequired,
};

InGameOverlayPage.App = App;

export async function getServerSideProps({ params }) {
  return {
    props: params,
  };
}
