import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import { useAuth } from '@/util/auth';
import Markdown from 'components/Markdown';
import Title from 'components/Title';
import SeasonsTable from 'components/tables/SeasonsTable';
import TournamentTable from 'components/tables/TournamentTable';
import CabinetTable from 'components/tables/CabinetTable';
import GameTable from 'components/tables/GameTable';
import SceneForm from 'components/forms/SceneForm';
import { CreateSessionButton } from 'components/whiteboard';

const useStyles = makeStyles(theme => ({
  description: {
    padding: theme.spacing(1, 2),
    margin: theme.spacing(2, 0),
    borderRadius: '5px',
    background: theme.palette.grey['100'],
  },
}));

export default function ScenePage({ scene }) {
  const classes = useStyles();
  const { isAdminOf } = useAuth();

  const form = isAdminOf(scene.id) ? (
    <Grid container direction="row" spacing={2}>
      <Grid item>
        <CreateSessionButton scene={scene} />
      </Grid>

      <Grid item>
        <SceneForm scene={scene} />
      </Grid>

      <Grid item>
        <Button variant="contained" color="secondary" href={`/scene/${scene.name}/permissions`}>
          Permissions
        </Button>
      </Grid>
    </Grid>
  ) : null;

  return (
    <div className="">
      <header className="mb-12">
        <Title title={scene.displayName} form={form} />

        {scene.description && (
          <div className={classes.description}>
            <Markdown contents={scene.description} />
          </div>
        )}
      </header>
      <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-12 ">
        <TournamentTable scene={scene} />
        <SeasonsTable scene={scene} />
      </div>
      <CabinetTable scene={scene} />
      <GameTable filters={{ scene_id: scene.id }} />
    </div>
  );
}

ScenePage.propTypes = {
  scene: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  const response = await axios.get(`/api/game/scene/`, { params: { name: params.name } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  return {
    props: { scene, title: scene.displayName, description: scene.description },
  };
}
