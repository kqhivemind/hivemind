import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { formatUTC } from 'util/dates';
import MatchResultTable from 'components/tables/MatchResultTable';
import { GameSummaryTableList } from 'components/tables/GameSummaryTable';

const useStyles = makeStyles(theme => ({
}));

export default function MatchPage({ match }) {
  const classes = useStyles({ match });

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${match.event.season.scene.name}`}>{match.event.season.scene.displayName}</Link>
        <Link href={`/season/${match.event.season.id}`}>{match.event.season.name}</Link>
        <Link href={`/event/${match.event.id}`}>{formatUTC(match.event.eventDate, 'MMMM d, yyyy')}</Link>
        <Link href={`/match/${match.id}`}>Match {match.matchNumber}</Link>
      </Breadcrumbs>


      <Typography variant="h1">{match.blueTeam.name} vs. {match.goldTeam.name}</Typography>


      <Typography variant="h2">Match Results</Typography>
      <MatchResultTable match={match} />

      <Typography variant="h2">Game Results</Typography>

      {match.games.length === 0
        ? <Typography className={classes.noGames}>Game details not available for this match.</Typography>
        : <GameSummaryTableList games={match.games} />
      }
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/match/${params.id}`);
  const match = response.data;

  if (!match) return { notFound: true };

  match.games = [];
  response = await axios.get('/api/game/game/', { params: { matchId: match.id } });

  for (const game of response.data.results) {
    response = await axios.get(`/api/game/game/${game.id}/stats`);
    match.games.push(response.data);
  }

  response = await axios.get(`/api/league/season/${match.event.season}`);
  match.event.season = response.data;

  response = await axios.get(`/api/game/scene/${match.event.season.scene}`);
  match.event.season.scene = response.data;

  return {
    props: {
      match,
      title: `${match.blueTeam.name} vs. ${match.goldTeam.name}`,
      description: `${match.blueTeam.name} vs. ${match.goldTeam.name} on ${formatUTC(match.event.eventDate, 'MMMM d, yyyy')}, an event in ${match.event.season.name} in ${match.event.season.scene.displayName}.`,
    },
  };
}

