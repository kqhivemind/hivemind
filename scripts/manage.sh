#!/bin/bash
cd /data/hivemind
/usr/bin/docker compose -f prod/docker-compose.worker.yaml run --rm worker python /src/server/manage.py "$@"
