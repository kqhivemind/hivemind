#!/bin/bash

for domain in kqhivemind.com dev.kqhivemind.com worker.kqhivemind.com mail.kqhivemind.com logs.kqhivemind.com baltimorekillerqueen.com kqbmore.com killerqueenarcade.wiki; do
    /usr/bin/certbot certonly -n --renew-by-default --dns-digitalocean \
                     -m contact@kqhivemind.com --agree-tos \
                     --dns-digitalocean-credentials ~/certbot-creds.ini \
                     -d "${domain}"

done

cd /etc/letsencrypt/live
for dir in *; do
    if [ -d "$dir" ]; then
        find "$dir" -type f,l | xargs -I{} /usr/local/bin/b2 upload-file kqhivemind-certs "{}" "{}"
    fi
done

cp -rL /etc/letsencrypt/live/* /data/certs/
