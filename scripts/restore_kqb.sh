#!/bin/bash

basedir="/data/backups"
volumes="-v $(pwd)/backups:${basedir}"
outputdir="${basedir}/$( date +%Y%m%d_%H%M%S )"
tmpfile=$( mktemp ./backups/objlist.XXXXXX )

source app.env
docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               pg_restore -l "${basedir}/$1" | grep "public kqb_" > "$tmpfile"

docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               pg_restore -h db --user postgres --dbname postgres \
               --use-list "${basedir}/$( basename ${tmpfile} )" \
               --format directory "${basedir}/$1"

rm "$tmpfile"
