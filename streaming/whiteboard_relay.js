#!/usr/bin/env node
const http = require('http');
const request = require('request');
const WebSocket = require('ws');
const redis = require('redis');
const { log, apiFetch, sendMessage } = require('./util');

const cachedSubchannels = ['players', 'status'];

function heartbeat() {
  this.isAlive = true;
}

const server = http.createServer(function (request, response) {
  response.writeHead(404);
  response.end();
});

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
});
const dataBySessionId = {};

const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (!ws.isAlive) return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

wss.on('connection', async (ws, request) => {
  log(`New connection to ${request.url}`);

  const [sessionId] = request.url.split('/').reverse();

  if (!(sessionId in dataBySessionId)) {
    log(`Subscribing to Redis channel whiteboard.${sessionId}.*`);
    dataBySessionId[sessionId] = {
      subscriptions: new Set(),
    };
    redisClient.psubscribe(`whiteboard.${sessionId}.*`);
  }

  if (dataBySessionId[sessionId]?.status) {
    sendMessage(ws, 'status', dataBySessionId[sessionId].status);
  } else {
      apiFetch(`whiteboard/session/${sessionId}/status/`).then(data => {
        dataBySessionId[sessionId].status = { ...data };
        sendMessage(ws, 'status', dataBySessionId[sessionId].status);
      }).catch(err => {
        log(`Couldn't fetch current status for session ${sessionId}`);
        log(err);
      });
  }

  if (dataBySessionId[sessionId]?.players) {
    sendMessage(ws, 'players', dataBySessionId[sessionId].players);
  } else {
    apiFetch(`whiteboard/session/${sessionId}/players/`).then(data => {
      dataBySessionId[sessionId].players = [...data];
      sendMessage(ws, 'players', dataBySessionId[sessionId].players);
    }).catch(err => {
      log(`Couldn't fetch players for session ${sessionId}`);
      log(err);
    });
  }

  dataBySessionId[sessionId].subscriptions.add(ws);
  ws.isAlive = true;
  ws.on('pong', heartbeat);

  ws.on('close', () => {
    log(`Connection to ${request.url} closed.`);
    dataBySessionId[sessionId].subscriptions.delete(ws);
  });
});

redisClient.on('pmessage', async (pattern, channel, message) => {
  const [subchannel, sessionId] = channel.split('.').reverse();
  const messageData = JSON.parse(message);

  if (cachedSubchannels.includes(subchannel)) {
    dataBySessionId[sessionId][subchannel] = messageData;
  }

  if (subchannel === 'player') {
    dataBySessionId[sessionId].players = [
      ...dataBySessionId[sessionId]?.players.filter(i => i.id !== messageData.id),
      messageData,
    ]
  }

  for (const ws of dataBySessionId[sessionId].subscriptions) {
    try {
      for (const subchannel of cachedSubchannels) {
        sendMessage(ws, subchannel, dataBySessionId[sessionId][subchannel]);
      }
    } catch (err) {
      log(err);
      ws.close();
    }
  }
});

server.listen(8080);
