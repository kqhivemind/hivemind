#!/usr/bin/env node
const http = require('http');
const request = require('request');
const WebSocket = require('ws');
const redis = require('redis');
const axios = require('axios');

function log(message) {
  console.log((new Date()) + ' ' + message);
}

function heartbeat() {
  this.isAlive = true;
}

const server = http.createServer(function(request, response) {
  response.writeHead(404);
  response.end();
});

const redisClient = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});
const subscriptions = {};

const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if(!ws.isAlive)
      return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

wss.on('connection', async (ws, request) => {
  log(`New connection to ${request.url}`);

  const [cabinetName, sceneName] = request.url.split('/').reverse();

  let response = await axios.get('/game/scene/', {
    baseURL: process.env.API_BASE_URL,
    params: { name: sceneName },
  });
  if (!(response?.data?.results?.length > 0)) {
    ws.close();
    return;
  }

  const scene = response.data.results[0];

  response = await axios.get('/game/cabinet/', {
    baseURL: process.env.API_BASE_URL,
    params: { scene: scene.id, name: cabinetName },
  });
  if (!(response?.data?.results?.length > 0)) {
    ws.close();
    return;
  }

  const cabinet = response.data.results[0];

  if (!(cabinet.id in subscriptions)) {
    log(`Subscribing to Redis channel events.cabinet.${cabinet.id}`);
    subscriptions[cabinet.id] = new Set();
    redisClient.subscribe(`events.cabinet.${cabinet.id}`);
  }

  subscriptions[cabinet.id].add(ws);

  ws.isAlive = true;
  ws.on('pong', heartbeat);

  ws.on('close', () => {
    log(`Connection to ${request.url} closed.`);
    subscriptions[cabinet.id].delete(ws);
  });
});

redisClient.on('message', async (channel, messageText) => {
  if(!channel.startsWith('events.cabinet.'))
    return;

  const cabinetID = channel.replace('events.cabinet.', '');

  for (const ws of subscriptions[cabinetID]) {
    try {
      ws.send(messageText);
    } catch (err) {
      log(err);
      ws.close();
    }
  }
});

server.listen(8080);
