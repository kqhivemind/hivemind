#!/usr/bin/env node
const http = require("http");
const WebSocket = require("ws");
const redis = require("redis");

function log(message) {
    console.log((new Date()) + " " + message);
}

function heartbeat() {
    this.isAlive = true;
}

const server = http.createServer(function(request, response) {
    response.writeHead(404);
    response.end();
});

const redisClient = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});
redisClient.subscribe("signin");

const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
        if(!ws.isAlive)
            return ws.terminate();

        ws.isAlive = false;
        ws.ping(() => {});
    });
}, 30000);

wss.on("connection", ws => {
    ws.isAlive = true;
    ws.on("pong", heartbeat);
});

redisClient.on("message", function(channel, messageString) {
    wss.clients.forEach(function each(ws) {
        ws.send(messageString);
    });
});

server.listen(8080);
