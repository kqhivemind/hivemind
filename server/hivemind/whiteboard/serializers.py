from hivemind.user.serializers import UserPublicSerializer
from rest_framework import serializers

from .models import PlayerSignup


class PlayerSignupSerializer(serializers.ModelSerializer):
    user = UserPublicSerializer(source="player.user")

    class Meta:
        model = PlayerSignup
        fields = ["id", "player_id", "roles", "is_ready", "requeue", "user", "priority"]
