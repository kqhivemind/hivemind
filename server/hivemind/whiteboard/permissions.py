from rest_framework.permissions import BasePermission

from ..permissions import SceneAdminOrReadOnly


class WhiteboardSessionPermission(SceneAdminOrReadOnly):
    pass


class PlayerPermission(BasePermission):
    def has_permission(self, request, view):
        return False

    def has_object_permission(self, request, view, obj):
        if request.user and request.user.is_authenticated:
            return obj.user.id == request.user.id

        return False
