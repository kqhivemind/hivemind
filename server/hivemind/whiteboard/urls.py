from django.urls import include, path
from rest_framework import routers, serializers, viewsets

from . import views

router = routers.DefaultRouter()
router.register("session", views.WhiteboardSessionViewSet)
router.register("player", views.PlayerViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
