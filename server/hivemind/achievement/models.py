from datetime import datetime, timedelta
from itertools import groupby

from django.db import models
from django.utils import timezone
from tqdm import tqdm

from ..constants import CabinetTeam
from ..game.models import CabinetPosition, Game, GameMap, GameStat
from ..model import BaseModel
from ..stats.stattype import StatType, player_has_speed
from ..user.models import Message, User, UserGame
from ..whiteboard.models import RatingHistory


class Achievement(object):
    title = ""
    description = ""
    icon_url = ""
    include_in_tournament_awards = False
    is_repeatable = False
    requires_stats = False

    all_achievements = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

        if cls.title is not None:
            cls.all_achievements.append(cls)

    @classmethod
    def choices(cls):
        return [(i.__name__, i.title) for i in cls.all_achievements]

    @classmethod
    def test(cls, usergame, stats):
        return False

    @classmethod
    def check(cls, usergame, stats):
        if not cls.is_repeatable and UserAchievement.objects.filter(user=usergame.user, achievement=cls.__name__).count() > 0:
            return

        if cls.test(usergame, stats):
            UserAchievement(achievement=cls.__name__, user=usergame.user, game=usergame.game,
                            timestamp=usergame.game.get_end_time_local()).save()
            Message(user=usergame.user, url="/user/{}#achievements".format(usergame.user_id),
                    message_text="Achievement unlocked: {}".format(cls.title)).save()

    @classmethod
    def check_all(cls, game):
        stats = game.get_postgame_stats()

        for usergame in game.usergame_set.all():
            for achievement in cls.all_achievements:
                achievement.check(usergame, stats)

    @classmethod
    def check_history(cls, progress=False, regenerate=False, **filters):
        games = Game.objects.filter(usergame__isnull=False, **filters).distinct().order_by("id")
        if progress:
            games = tqdm(games)

        for game in games:
            stats = game.get_postgame_stats(regenerate=regenerate) if cls.requires_stats else None
            for usergame in game.usergame_set.all():
                cls.check(usergame, stats)

    @classmethod
    def user_achievements(cls):
        return UserAchievement.objects.filter(achievement=cls.__name__)


class FirstGame(Achievement):
    title = "One Of Us"
    description = "Log your first game on HiveMind."

    @classmethod
    def test(cls, usergame, stats):
        return True


class AllLoggedIn(Achievement):
    title = "Fully Assimilated"
    description = "Play a game with all ten players logged in to HiveMind."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.usergame_set.count() >= 10


class OrganizedEvent(Achievement):
    title = "Organized"
    description = "Play in a league night or tournament match."

    @classmethod
    def test(cls, usergame, stats):
        return (usergame.game.match is not None or \
                usergame.game.qp_match is not None or \
                usergame.game.tournament_match is not None)


class SecondLocation(Achievement):
    title = "Cross Pollination"
    description = "Sign in to HiveMind on more than one cabinet."

    @classmethod
    def test(cls, usergame, stats):
        return (usergame.user.usergame_set.exclude(game__cabinet_id=usergame.game.cabinet_id).count() > 0)


class SameTeammate(Achievement):
    title = "BeeFFs"
    description = "Play 100 games with the same teammate."

    @classmethod
    def test(cls, usergame, stats):
        user_games = set((i.game.id, i.player_id % 2) for i in UserGame.objects.filter(user_id=usergame.user_id))
        teammates = [i for i, j in CabinetPosition.choices if i % 2 == usergame.player_id % 2 and i != usergame.player_id]
        for teammate in usergame.game.usergame_set.filter(player_id__in=teammates).exclude(user=usergame.user):
            teammate_games = set((i.game.id, i.player_id % 2) for i in UserGame.objects.filter(user_id=teammate.user_id))
            if len(user_games.intersection(teammate_games)) >= 100:
                return True


class HatTrick(Achievement):
    title = "You Have Selected Regicide"
    description = "Kill the enemy queen three times in the same game."
    include_in_tournament_awards = True
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        return stats["by_player"]["queen_kills"].get(usergame.player_id, 0) >= 3


class GordieHowe(Achievement):
    title = "Gordie Howe Hat Trick"
    description = "In one game, record a queen kill, a bump assist, and a berry kick-in."
    include_in_tournament_awards = True
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        for stat_type in ["queen_kills", "bump_assists", "berries_kicked"]:
            if stats["by_player"][stat_type].get(usergame.player_id, 0) == 0:
                return False

        return True


class LastBerryKickIn(Achievement):
    title = "Pelé"
    description = "Win a game by kicking in the last berry."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        if usergame.game.win_condition != "economic":
            return False

        if usergame.game.winning_team != CabinetPosition(usergame.player_id).team.value:
            return False

        last_berry = usergame.game.gameevent_set.filter(event_type__in=["berryDeposit", "berryKickIn"]) \
                                                .order_by("-timestamp").first()

        if last_berry is None:
            return False

        return (last_berry.event_type == "berryKickIn" and last_berry.values[2] == str(usergame.player_id) and \
                last_berry.values[3] == "True")


class SnailBonusMilitaryVictory(Achievement):
    title = "The Hard Way"
    description = "Win with a military victory on the snail bonus map."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.map_name == GameMap.BONUS_SNAIL and usergame.game.win_condition == "military" and \
            usergame.game.winning_team == CabinetPosition(usergame.player_id).team.value


class TwilightSnailVictory(Achievement):
    title = 'You Had Me At "Meat Tornado"'
    description = "Win with a snail victory on Twilight."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.map_name in ["map_twilight", "map_twilight2"] \
            and usergame.game.win_condition == "snail" \
            and usergame.game.winning_team == CabinetPosition(usergame.player_id).team.value


class SaveTeammateFromSnail(Achievement):
    title = "Drones Are Friends, Not Food"
    description = "Save a teammate from being eaten by the snail three times in the same game."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        count = 0

        off_snail_events = usergame.game.gameevent_set.filter(event_type="getOffSnail")
        kill_events = usergame.game.gameevent_set.filter(event_type="playerKill", values__4="Worker")
        if len(kill_events) == 0:
            return False

        for escape in usergame.game.gameevent_set.filter(event_type="snailEscape"):
            off_snail_event = sorted(off_snail_events, key=lambda i: abs(i.timestamp - escape.timestamp))[0]
            kill_event = sorted(kill_events.filter(values__3=off_snail_event.values[3]), key=lambda i: abs(i.timestamp - escape.timestamp))[0]
            if int(kill_event.values[2]) == usergame.player_id:
                count += 1

        return count >= 3


class DayBerries(Achievement):
    title = "It's A Viable Strategy"
    description = "Win with berries on day map."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.map_name == "map_day" and usergame.game.win_condition == "economic" and \
            usergame.game.winning_team == CabinetPosition(usergame.player_id).team.value


class BuzzerBeater(Achievement):
    title = "Buzzer Beater"
    description = "Win by killing the enemy queen after their team deposits their last berry."
    include_in_tournament_awards = True
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        if usergame.game.win_condition != "military" or usergame.game.winning_team != CabinetPosition(usergame.player_id).team.value:
            return False

        if stats["berries"][CabinetPosition(usergame.player_id).opponent.value] < 12:
            return False

        queen_kill = usergame.game.gameevent_set.filter(event_type="playerKill", values__4="Queen").order_by("-timestamp").first()
        return int(queen_kill.values[2]) == usergame.player_id


class DepositTwelveBerries(Achievement):
    title = "Single Drone Economy"
    description = "Deposit twelve berries in one game."
    include_in_tournament_awards = True
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        return stats["by_player"]["berries"].get(usergame.player_id, 0) + \
            stats["by_player"]["berries_kicked"].get(usergame.player_id, 0) == 12


class FastSnailWin(Achievement):
    title = "Go Directly To Snail"
    description = "Win a snail victory in under one minute on a normal map."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.win_condition == "snail" \
            and usergame.game.end_time - usergame.game.start_time < timedelta(minutes=1) \
            and usergame.game.winning_team == CabinetPosition(usergame.player_id).team.value \
            and not usergame.game.get_map().is_bonus


class BerryParkour(Achievement):
    title = "Parkour!"
    description = "Deposit a berry after bouncing off two or more enemy players."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        for berry_event in usergame.game.gameevent_set.filter(event_type="berryDeposit", values__2=str(usergame.player_id)):
            glanced = set()
            for glance in usergame.game.gameevent_set.filter(timestamp__gt=berry_event.timestamp - timedelta(seconds=5),
                                                             timestamp__lte=berry_event.timestamp,
                                                             event_type="glance"):
                if str(usergame.player_id) in glance.values[2:4]:
                    glanced.update(glance.values[2:4])

            if len(glanced) >= 3:
                return True

        return False


class SurviveFamine(Achievement):
    title = "Happy New Year!"
    description = "As a warrior, survive the full length of a famine."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        berry_count = usergame.game.get_total_berry_count()
        if berry_count is None or berry_count == 0:
            return False

        up_down_events = usergame.game.gameevent_set.filter(
            models.Q(event_type="useMaiden", values__2="maiden_wings", values__3=str(usergame.player_id)) |
            models.Q(event_type="playerKill", values__3=str(usergame.player_id), values__4="Soldier")
        )

        berry_events = usergame.game.gameevent_set.filter(event_type__in=["berryDeposit", "berryKickIn", "useMaiden"]).order_by("timestamp")
        famine_events = berry_events[berry_count-1::berry_count]
        for famine_event in famine_events:
            if famine_event.timestamp > usergame.game.end_time - timedelta(seconds=90):
                continue

            last_evt = up_down_events.filter(timestamp__lte=famine_event.timestamp).order_by("-timestamp").first()
            if last_evt and last_evt.event_type == "useMaiden":
                deaths = up_down_events.filter(timestamp__gte=famine_event.timestamp, timestamp__lte=famine_event.timestamp + timedelta(seconds=90),
                                               event_type="playerKill")
                if deaths.count() == 0:
                    return True


class OverNineThousand(Achievement):
    title = "There's No Way That Can Be Right"
    description = "Log over nine thousand total kills."

    @classmethod
    def test(cls, usergame, stats):
        query = GameStat.objects.filter(user_id=usergame.user_id, stat_type=GameStat.StatType.KILLS).aggregate(models.Sum("value"))
        return query["value__sum"] and query["value__sum"] > 9000


class AfterMidnight(Achievement):
    title = "Midnight Snail Going Anywhere"
    description = "Play a game that ends between midnight and 5 AM local time."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        end_time = usergame.game.get_end_time_local()
        return end_time.hour < 6


class FourTwenty(Achievement):
    title = "Blaze It"
    description = "Play a game that lasts 4:20."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        game_time = usergame.game.end_time - usergame.game.start_time
        return (game_time >= timedelta(minutes=4, seconds=20) and game_time < timedelta(minutes=4, seconds=21))


class GateQueue(Achievement):
    title = "Guacamole Is Extra"
    description = "Have three players on your team use the same warrior gate within seven seconds."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        gate_uses = usergame.game.gameevent_set.filter(
            event_type="useMaiden",
            values__2="maiden_wings",
            values__3__in=CabinetPosition(usergame.player_id).team.positions,
        )

        event_loc = lambda i: (i.values[0], i.values[1])
        for gate, uses in groupby(sorted(gate_uses, key=event_loc), event_loc):
            uses_sorted = sorted(uses, key=lambda i: i.timestamp)
            for evt1, evt2 in zip(uses_sorted, uses_sorted[2:]):
                if evt2.timestamp - evt1.timestamp < timedelta(seconds=7):
                    return True


class EconOwnGoal(Achievement):
    title = "I Meant To Do That"
    description = "Score an own goal on the economic bonus map."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.map_name == GameMap.BONUS_ECON and usergame.game.gameevent_set.filter(
            event_type="berryKickIn",
            values__2=usergame.player_id,
            values__3=False,
        ).count() > 0


class DepositAndKickIn(Achievement):
    title = "Maximized Worker Productivity"
    description = "Deposit a berry while kicking in another."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        for deposit in usergame.game.gameevent_set.filter(event_type="berryDeposit", values__2=usergame.player_id):
            kicks = usergame.game.gameevent_set.filter(
                event_type="berryKickIn",
                values__2=usergame.player_id,
                values__3=True,
                timestamp__gte=deposit.timestamp - timedelta(seconds=1),
                timestamp__lte=deposit.timestamp + timedelta(seconds=1),
            )

            if kicks.count() > 0:
                return True


class FiveSceneTeam(Achievement):
    title = "United Federation"
    description = "Play on a team with players from five different scenes."
    include_in_tournament_awards = True

    @classmethod
    def test(cls, usergame, stats):
        teammates = CabinetPosition(usergame.player_id).team.positions
        team_usergames = usergame.game.usergame_set.filter(player_id__in=teammates)
        scenes = set([i["user__scene"] for i in team_usergames.values("user__scene") if i is not None])

        return len(scenes) == 5


class GameStartsNow(Achievement):
    title = "Game Starts Now"
    description = "As Queen on your last life, survive for five minutes and win the game."
    include_in_tournament_awards = True
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        if CabinetPosition(usergame.player_id) not in [CabinetPosition.GOLD_QUEEN, CabinetPosition.BLUE_QUEEN]:
            return False
        if usergame.game.winning_team != CabinetPosition(usergame.player_id).team.value:
            return False
        if stats["by_player"][StatType.DEATHS].get(usergame.player_id, 0) != 2:
            return False
        if usergame.game.end_time < usergame.game.start_time + timedelta(minutes=5):
            return False

        death_event = usergame.game.gameevent_set.filter(event_type="playerKill", values__3=str(usergame.player_id)) \
            .order_by("-timestamp") \
            .first()

        return (usergame.game.end_time - death_event.timestamp) > timedelta(minutes=5)


class Snailathon(Achievement):
    title = "Snailathon"
    description = "Ride the snail for 42,195 snail meters."

    @classmethod
    def test(cls, usergame, stats):
        query = GameStat.objects.filter(
            user_id=usergame.user_id,
            stat_type=StatType.SNAIL_METERS,
            game__start_time__lte=usergame.game.start_time,
        ).aggregate(models.Sum("value"))

        return query["value__sum"] and query["value__sum"] >= 42195


class Cyberbully(Achievement):
    title = "Cyberbully"
    description = "Kill the same player three times within ten seconds."

    @classmethod
    def test(cls, usergame, stats):
        kills = usergame.game.gameevent_set.filter(event_type="playerKill", values__2=str(usergame.player_id))
        for kill in kills:
            if kills.filter(
                    values__3=kill.values[3],
                    timestamp__gt=kill.timestamp,
                    timestamp__lte=kill.timestamp + timedelta(seconds=10),
            ).count() >= 2:
                return True

        return False


class LastBerryEcon(Achievement):
    title = "District 1"
    description = "Win an economic victory with the last berry on the map."
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.winning_team == CabinetPosition(usergame.player_id).team.value and \
            usergame.game.win_condition == "economic" and \
            usergame.game.get_map().total_berries > 1 and \
            stats["berries_used"] % usergame.game.get_map().total_berries == 0


class TwilightFamine(Achievement):
    title = "Meat's Back on the Menu"
    description = "Reach famine on Twilight."
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.map_name == GameMap.TWILIGHT and \
            stats["berries_used"] >= usergame.game.get_map().total_berries


class TripleCrown(Achievement):
    title = "Triple Crown"
    description = "Win a military victory while also having 11 berries deposited and the snail at 90 percent or more to your goal."
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        berry_threshold = 11
        player_team = CabinetPosition(usergame.player_id).team
        snail_threshold_percent = 0.9
        snail_track_width = usergame.game.get_map().snail_track_width or 900

        snail_distance_threshold = snail_threshold_percent * snail_track_width
        snail_distance = 0
        for position, distance in stats["by_player"].get("snail_distance", {}).items():
            snail_distance += distance * (1 if CabinetPosition(position).team == player_team else -1)

        snail_is_past_threshold = snail_distance >= snail_distance_threshold

        won_game = usergame.game.winning_team == CabinetPosition(usergame.player_id).team.value
        is_bonus = usergame.game.get_map().is_bonus
        military_victory = usergame.game.win_condition == "military"
        berries_are_past_threshold = stats["berries"][player_team] >= berry_threshold

        return won_game and \
            not is_bonus and \
            military_victory and \
            berries_are_past_threshold and \
            snail_is_past_threshold


class VanillaNaturalHatTrick(Achievement):
    title = "Soft Serve"
    description = "As a vanilla warrior, kill the enemy queen three times without dying."
    requires_stats = True

    @classmethod
    def test(cls, usergame, stats):
        if CabinetPosition(usergame.player_id) in [CabinetPosition.GOLD_QUEEN, CabinetPosition.BLUE_QUEEN]:
            return False

        if stats["by_player"][StatType.QUEEN_KILLS].get(usergame.player_id, 0) != 3:
            return False

        kills = list(usergame.game.gameevent_set.filter(
            event_type="playerKill",
            values__2=str(usergame.player_id),
            values__4="Queen",
        ).order_by("timestamp"))

        for kill in kills:
            if player_has_speed(usergame.game, usergame.player_id, kill.timestamp):
                return False

        deaths = usergame.game.gameevent_set.filter(
            event_type="playerKill",
            values__3=str(usergame.player_id),
            timestamp__gt=kills[0].timestamp,
            timestamp__lt=kills[-1].timestamp,
        )

        return deaths.count() == 0


class FirstWhiteboardSet(Achievement):
    title = "Dry Erase"
    description = "Play your first whiteboard set."

    @classmethod
    def test(cls, usergame, stats):
        return usergame.game.whiteboard_match is not None


class WhiteboardRoles(Achievement):
    title = "Remarkable Adaptability"
    description = "Play all three roles in whiteboard sets."

    @classmethod
    def test(cls, usergame, stats):
        if usergame.game.whiteboard_match is None:
            return False

        histories = RatingHistory.objects.filter(
            player__user_id=usergame.user_id,
            match__created_time__lte=usergame.game.whiteboard_match.created_time,
        )
        return len(set([i.role for i in histories])) == 3


class MilestoneGame:
    game_number = None

    @classmethod
    def test(cls, usergame, stats):
        return Game.objects.filter(end_time__isnull=False, end_time__lt=usergame.game.end_time).count() == cls.game_number - 1


class Game250K(MilestoneGame, Achievement):
    game_number = 250_000
    title = "Quarter Million"
    description = "Play in HiveMind's 250,000th completed game."


class Game500K(MilestoneGame, Achievement):
    game_number = 500_000
    title = "Half Million"
    description = "Play in HiveMind's 500,000th completed game."


class Game1M(MilestoneGame, Achievement):
    game_number = 1_000_000
    title = "One Million"
    description = "Play in HiveMind's 1,000,000th completed game."


class UserAchievement(BaseModel):
    achievement = models.CharField(max_length=30, choices=Achievement.choices())
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)

    def get_achievement(self):
        for subclass in Achievement.all_achievements:
            if subclass.__name__ == self.achievement:
                return subclass
