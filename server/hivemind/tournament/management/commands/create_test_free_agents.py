import random

import lipsum
from django.core.management.base import BaseCommand
from django.utils import timezone
from hivemind.tests import HiveMindTest
from hivemind.tournament.models import (Tournament, TournamentPlayer,
                                        TournamentTeam)
from hivemind.user.models import User


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--tournament", help="Tournament ID to add players to.")
        parser.add_argument("--count", help="Number of players to create.")

    def handle(self, *args, **kwargs):
        count = int(kwargs.get("count"))

        if count is None:
            return

        tournament = Tournament.objects.get(id=kwargs.get("tournament"))
        users = list(User.objects.filter(social_auth__isnull=True).exclude(name=""))

        random.shuffle(users)

        for i in range(count):
            try:
                user = users.pop()
            except IndexError:
                break

            TournamentPlayer.objects.create(
                tournament=tournament,
                team=None,
                user=user,
                name=user.name,
                scene=user.scene,
                image=user.image,
                pronouns=user.pronouns,
                tidbit=lipsum.generate_sentences(1),
                email=user.email,
                registration_time=timezone.now(),
            )
