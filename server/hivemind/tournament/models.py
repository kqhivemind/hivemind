import hashlib
import itertools
import json
import logging
import math
import os
from datetime import datetime, timedelta
from urllib.parse import urlencode

from django.contrib.postgres.fields import ArrayField
from django.db import models, transaction
from django.db.models import Count, Q
from django.utils import timezone

from .. import bracket_tools
from ..client import redis_client, redis_get, redis_set
from ..constants import (CabinetPosition, CabinetTeam, FieldType, MatchType,
                         TournamentAutoWarmup, TournamentStageType)
from ..game.models import Cabinet, Game, Scene
from ..model import BaseModel, Deleteable
from ..stats.statmodel import AggregatedStatModel
from ..stats.stattype import stat_types
from ..user.models import User
from ..worker import app
from . import challonge
from .tiebreak import StageTiebreaker, standings_by_tiebreakers

ROUND_NAMES = ("Octofinals", "Quarterfinals", "Semifinals", "Finals")

logger = logging.getLogger(__name__)

def generate_token():
    return hashlib.sha512(os.urandom(2048)).hexdigest()[:32]


class TournamentNotValidated(Exception):
    pass


class Tournament(Deleteable):
    class LinkType(models.TextChoices):
        MANUAL = "manual", "Manual"
        HIVEMIND = "hivemind", "HiveMind Brackets"
        CHALLONGE = "challonge", "Challonge"

    name = models.CharField(max_length=200)
    slug = models.SlugField(null=True)
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    date = models.DateField(default=timezone.now)
    is_active = models.BooleanField(default=False)
    location = models.TextField(default="", null=True, blank=True)
    description = models.TextField(default="", null=True, blank=True)
    registration_message = models.TextField(default="", null=True, blank=True)
    payment_message = models.TextField(default="", null=True, blank=True)
    allow_registration = models.BooleanField(default=False)
    require_player_photo = models.BooleanField(default=False)
    registration_close_date = models.DateField(null=True)
    registration_banner = models.ImageField(null=True, blank=True, upload_to="tournament/registration-banners/")
    accept_payments = models.BooleanField(default=False)
    payment_account = models.ForeignKey("tournament.PaymentAccount", null=True, blank=True,
                                        on_delete=models.SET_NULL)
    awards = models.JSONField(null=True)
    assigned_cabinets = models.ManyToManyField(Cabinet, blank=True)
    link_type = models.TextField(max_length=20, choices=LinkType.choices, default=LinkType.CHALLONGE)
    draft_status = models.JSONField(null=True)

    class Meta:
        unique_together = ("scene", "slug")


    def get_active_matches(self):
        bracket_ids = [i.id for i in self.tournamentbracket_set.all()]
        matches = TournamentMatch.objects.filter(active_cabinet__isnull=False, bracket_id__in=bracket_ids)

        return matches

    def get_active_match_by_cabinet(self, cabinet_id):
        bracket_ids = [i.id for i in self.tournamentbracket_set.all()]
        try:
            return TournamentMatch.objects.get(active_cabinet_id=cabinet_id, bracket_id__in=bracket_ids)
        except TournamentMatch.DoesNotExist:
            return None

    def get_available_matches(self):
        if self.link_type == self.LinkType.CHALLONGE:
            for bracket in self.tournamentbracket_set.all():
                if bracket.is_valid:
                    bracket.get_matches()

        bracket_ids = [i.id for i in self.tournamentbracket_set.order_by("id").all()]
        matches = TournamentMatch.objects.filter(is_complete=False, bracket_id__in=bracket_ids) \
            .filter(blue_team__isnull=False, gold_team__isnull=False) \
            .order_by("round_name", "id")

        if self.link_type == self.LinkType.HIVEMIND:
            matches = matches.filter(blue_team__isnull=False, gold_team__isnull=False)

        return matches

    def get_queue(self, cabinet=None):
        if cabinet:
            queue = cabinet.matchqueue_set.filter(tournament=self, auto_created=True).first()
        else:
            queue = self.matchqueue_set.annotate(Count("cabinets")) \
                                       .filter(cabinets__count=0, auto_created=True) \
                                       .first()
        return queue

    @property
    def stripe_account_id(self):
        if self.payment_account is None:
            return None

        return self.payment_account.stripe_account_id

    def get_general_queue(self):
        return self.matchqueue_set.annotate(Count("cabinets")).filter(cabinets__count=0, auto_created=True).first()

    @transaction.atomic
    def select_next_match(self, cabinet):
        if TournamentMatch.objects.filter(active_cabinet=cabinet).count() > 0:
            return None

        queue = self.get_queue(cabinet=cabinet)
        match = queue.get_next_match(cabinet)

        if match:
            dequeued_from = MatchQueue.dequeue(match)

            # Set warmup if either team has not played yet
            if match.bracket.auto_warmup == TournamentAutoWarmup.FIRST_SET and match.bracket.tournamentmatch_set.filter(is_complete=True).filter(Q(blue_team=match.blue_team) | Q(gold_team=match.blue_team)).count() == 0:
                match.is_warmup = True
            if match.bracket.auto_warmup == TournamentAutoWarmup.FIRST_SET and match.bracket.tournamentmatch_set.filter(is_complete=True).filter(Q(blue_team=match.gold_team) | Q(gold_team=match.gold_team)).count() == 0:
                match.is_warmup = True
            if match.bracket.auto_warmup == TournamentAutoWarmup.ALWAYS:
                match.is_warmup = True

            active_matches = self.get_active_matches()
            active_teams = set(
                [match.blue_team_id for match in active_matches] +
                [match.gold_team_id for match in active_matches]
            )

            if match.blue_team is None or match.gold_team is None or \
               match.blue_team_id in active_teams or \
               match.gold_team_id in active_teams:

                # match is not available yet, get the next one instead
                next_match = self.select_next_match(cabinet)

                # then re-queue this one at the front
                match_list = dequeued_from.get_list()
                match_list.insert(0, match)
                dequeued_from.set_list(match_list)

                return next_match

            if match.active_cabinet is not None or match.is_complete:
                # match already started somewhere, get the next one instead
                # it's already dequeued so just run the function again
                return self.select_next_match(cabinet)

            match.active_cabinet = cabinet
            match.save()
            match.report_start()

        self.publish_match_state()

    def publish_match_state(self):
        for cabinet in self.scene.cabinet_set.all():
            current_match = None
            on_deck = None

            match = self.get_active_match_by_cabinet(cabinet.id)
            if match:
                current_match = {
                    "id": match.id,
                    "blue_team": match.blue_team.name,
                    "blue_score": match.blue_score,
                    "blue_team_image": match.blue_team.get_image_url(),
                    "gold_team": match.gold_team.name,
                    "gold_score": match.gold_score,
                    "gold_team_image": match.gold_team.get_image_url(),
                    "is_warmup": match.is_warmup,
                    "rounds_per_match": match.bracket.rounds_per_match,
                    "wins_per_match": match.bracket.wins_per_match,
                    "bracket_url": match.bracket.bracket_url(),
                }

                if match.round_name:
                    current_match["round_name"] = "{} - {}".format(match.bracket.name, match.round_name)
                else:
                    current_match["round_name"] = match.bracket.name

            on_deck_match = self.get_queue(cabinet).get_next_match(cabinet)
            if on_deck_match:
                on_deck = {
                    "id": on_deck_match.id,
                    "blue_team": on_deck_match.blue_team.name if on_deck_match.blue_team else "(unknown team)",
                    "gold_team": on_deck_match.gold_team.name if on_deck_match.gold_team else "(unknown team)",
                }

            redis_client.publish("matchstate", json.dumps({
                "type": "match",
                "match_type": "tournament",
                "scene_name": self.scene.name,
                "cabinet_name": cabinet.name,
                "cabinet_id": cabinet.id,
                "current_match": current_match,
                "on_deck": on_deck,
            }))

    @property
    def api_module(self):
        if self.link_type == self.LinkType.CHALLONGE:
            return challonge


class PlayerInfoField(BaseModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    field_slug = models.CharField(max_length=30, blank=True, null=True)
    field_description = models.TextField(null=True, blank=True)
    field_type = models.CharField(max_length=30, choices=FieldType.choices)
    is_required = models.BooleanField(default=False)
    choices = ArrayField(models.CharField(max_length=120), null=True, blank=True)
    order = models.IntegerField(null=True)

    def save(self, **kwargs):
        if not self.field_slug:
            self.field_slug = None

        super().save(**kwargs)

    @property
    def field_name(self):
        return "question{}".format(self.id)

    class Meta:
        unique_together = (
            ("tournament", "field_description"),
            ("tournament", "field_slug"),
        )


class TournamentTemplate(Deleteable):
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    data = models.JSONField()

    class Meta:
        unique_together = (
            ("scene", "name"),
        )


class PaymentOption(BaseModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=9, decimal_places=2, null=True)



class SelectedPaymentOption(BaseModel):
    payment_option = models.ForeignKey(PaymentOption, on_delete=models.CASCADE, null=True)
    quantity = models.IntegerField(default=1)
    custom_price = models.DecimalField(max_digits=9, decimal_places=2, null=True)


class TournamentBracket(BaseModel):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    linked_bracket_id = models.CharField(max_length=200, null=True, blank=True, db_index=True)
    link_token = models.CharField(max_length=50, default=generate_token)
    linked_org = models.CharField(max_length=50, null=True, blank=True)
    is_valid = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    rounds_per_match = models.IntegerField(null=True)
    wins_per_match = models.IntegerField(null=True)
    matches_per_opponent = models.IntegerField(null=True)
    report_as_sets = models.BooleanField(default=False)
    add_tiebreaker = models.BooleanField(default=False)
    stage_type = models.CharField(max_length=20, null=True, choices=TournamentStageType.choices)
    auto_warmup = models.CharField(max_length=10, default=TournamentAutoWarmup.NEVER,
                                   choices=TournamentAutoWarmup.choices)
    queue_timer = models.PositiveIntegerField(default=30)
    randomize_sides = models.BooleanField(default=False)
    third_place_match = models.BooleanField(default=False)
    stage_tiebreakers = ArrayField(
        models.CharField(max_length=30, blank=True, choices=StageTiebreaker.choices),
        null=True,
    )

    def get_teams(self):
        if not self.is_valid:
            return

        teams = {}
        for id, name in self.tournament.api_module.get_participants(self.linked_bracket_id, org=self.linked_org).items():
            team = self.tournament.tournamentteam_set.filter(linked_team_ids__contains=[str(id)]).first()
            if team:
                teams[id] = team
                continue

            team = self.tournament.tournamentteam_set.filter(name=name).first()
            if team is None:
                team = TournamentTeam(tournament=self.tournament, name=name, linked_team_ids=[])

            team.linked_team_ids.append(id)
            team.save()
            teams[id] = team

        return teams

    def set_teams(self, teams):
        if self.tournament.link_type == Tournament.LinkType.CHALLONGE:
            if not self.is_valid:
                return

            for id, name in self.tournament.api_module.get_participants(self.linked_bracket_id, org=self.linked_org).items():
                team = self.tournament.tournamentteam_set.filter(linked_team_ids__contains=[str(id)]).first()
                if team:
                    team.linked_team_ids.remove(str(id))

            ids = self.tournament.api_module.set_participants(self.linked_bracket_id, teams, org=self.linked_org)

            for team, id in zip(teams, ids):
                if team.linked_team_ids is None:
                    team.linked_team_ids = []

                team.linked_team_ids.append(id)

                TournamentTeam.objects.bulk_update(teams, ["linked_team_ids"])

        if self.tournament.link_type == Tournament.LinkType.HIVEMIND:
            StagePlacement.objects.filter(dest_bracket=self).delete()
            for place, team in enumerate(teams):
                StagePlacement.objects.create(dest_bracket=self, team=team, dest_place=place+1)

    def get_round_name(self, match, matches):
        if not hasattr(self, "_max_rounds_per_stage"):
            self._max_rounds_per_stage = {}
            for stage in set([match["stage"] for match in matches]):
                self._max_rounds_per_stage[stage] = max([match["round"] for match in matches if match["stage"] == stage])

        max_rounds_per_stage = self._max_rounds_per_stage
        num_losers_rounds = abs(min([match.get("round", 0) for match in matches])) if matches else 0

        rnd = match["round"]
        stage = match["stage"]

        if self.tournament_info["tournament_type"] == "single elimination" \
           and max_rounds_per_stage[stage] - rnd < len(ROUND_NAMES) \
           and self.tournament_info.get("state") != "group_stages_underway":
            return ROUND_NAMES[rnd - max_rounds_per_stage[stage] - 1]

        if self.tournament_info["tournament_type"] == "double elimination" \
           and self.tournament_info.get("state") != "group_stages_underway":

            if rnd == max_rounds_per_stage[stage]:
                return "Grand Finals"

            if rnd > 0 and max_rounds_per_stage[stage] - rnd <= len(ROUND_NAMES):
                return "Winners' {}".format(ROUND_NAMES[rnd - max_rounds_per_stage[stage]])

            if rnd < 0 and num_losers_rounds + rnd < len(ROUND_NAMES):
                return "Losers' {}".format(ROUND_NAMES[::-1][rnd + num_losers_rounds])

            if rnd > 0:
                return "Winners' Round {}".format(rnd)

            if rnd < 0:
                return "Losers' Round {}".format(abs(rnd))

        return "Round {}".format(rnd)

    def get_matches(self):
        if not self.is_valid:
            return

        teams_by_id = self.get_teams()
        matches = self.tournament.api_module.get_matches(self.linked_bracket_id, org=self.linked_org)

        for match in matches:
            stage = match["stage"]
            rnd = match["round"]
            if match["state"] in ["open", "pending"]:
                try:
                    tm = TournamentMatch.objects.get(linked_match_id=match["id"])
                except TournamentMatch.DoesNotExist:
                    tm = TournamentMatch(
                        is_flipped=bool(
                            int(hashlib.sha512(match["id"].to_bytes(16, 'little')).hexdigest()[-1], 16) % 2
                        ) if self.randomize_sides else False
                    )

                tm.bracket = self
                tm.linked_match_id = match["id"]
                tm.match_num = match.get("matchNum")
                if tm.is_flipped:
                    tm.gold_team = teams_by_id.get(match["team1"], None)
                    tm.blue_team = teams_by_id.get(match["team2"], None)
                else:
                    tm.blue_team = teams_by_id.get(match["team1"], None)
                    tm.gold_team = teams_by_id.get(match["team2"], None)

                tm.round_name = self.get_round_name(match, matches)

                tm.save()

    @property
    def tournament_info(self):
        if not hasattr(self, "_tournament_info"):
            if self.tournament.link_type == Tournament.LinkType.MANUAL:
                return True

            self._tournament_info = self.tournament.api_module.get_tournament(self.linked_bracket_id, org=self.linked_org)

        return self._tournament_info

    def bracket_url(self):
        if self.tournament.link_type == Tournament.LinkType.CHALLONGE:
            if self.linked_org:
                return f"https://{self.linked_org}.challonge.com/{self.linked_bracket_id}"

            return f"https://challonge.com/{self.linked_bracket_id}"

        if self.tournament.link_type == Tournament.LinkType.HIVEMIND:
            return f"https://kqhivemind.com/t/{self.tournament.scene.name}/{self.tournament.slug}"

    def check_permissions(self):
        if self.tournament.link_type == Tournament.LinkType.MANUAL:
            return True

        if self.tournament.link_type == Tournament.LinkType.CHALLONGE:
            # Get both account and org tournies
            account_ids = [i["url"] for i in self.tournament.api_module.list_tournaments(org=None)]
            org_ids = [i["url"] for i in self.tournament.api_module.list_tournaments(org=self.linked_org)]
            ids = account_ids + org_ids
            return (self.linked_bracket_id in ids)

        return False

    def validate(self):
        if self.tournament.link_type == Tournament.LinkType.MANUAL:
            return True

        if self.tournament.link_type == Tournament.LinkType.CHALLONGE:
            if not self.linked_bracket_id:
                return False

            if self.link_token in self.tournament_info["description"]:
                self.is_valid = True
                self.save()

                return True

        return False

    def reset_tournament(self):
        self.tournament.api_module.reset_tournament(self.linked_bracket_id, org=self.linked_org)

    def start_tournament(self):
        self.tournament.api_module.start_tournament(self.linked_bracket_id, org=self.linked_org)

    def get_placed_teams(self):
        places = StagePlacement.objects.filter(dest_bracket_id=self.id).order_by("dest_place")
        teams = []

        for place in places.order_by("dest_place"):
            if place.team:
                teams.append(place.team)

            if place.team is None and place.src_bracket.is_complete:
                standings = place.src_bracket.standings()
                teams.append(TournamentTeam.objects.get(id=standings[place.src_place-1]["id"]))

            if place.team is None and not place.src_bracket.is_complete:
                return

        return teams

    def generate_matches(self):
        if self.tournamentmatch_set.count() > 0:
            return

        generate_fn = {
            TournamentStageType.ROUND_ROBIN: bracket_tools.generate_round_robin_matches,
            TournamentStageType.SINGLE_ELIM: bracket_tools.generate_single_elim_matches,
            TournamentStageType.DOUBLE_ELIM: bracket_tools.generate_double_elim_matches,
            TournamentStageType.LADDER: bracket_tools.generate_ladder_matches,
            TournamentStageType.CUSTOM: lambda n: None,
        }[self.stage_type]

        teams = self.get_placed_teams()
        if teams is None:
            return

        matches = []
        match_entries = generate_fn(len(teams))

        # Special case for third-place match in a single elimination
        if self.stage_type == TournamentStageType.SINGLE_ELIM and self.third_place_match:
            # Change the semifinals
            for entry in match_entries:
                if entry.winner_to == len(match_entries) - 1:
                    entry.loser_place = None
                    entry.loser_to = entry.winner_to  # Will be replaced with the third-place game
                    entry.winner_to += 1              # New match number of the finals

            third_place_match = bracket_tools.MatchEntry(
                round_num=0,
                match_type=MatchType.THIRD_PLACE,
                winner_place=3,
                loser_place=4,
            )

            match_entries.insert(-1, third_place_match)

        for i, match_entry in enumerate(match_entries):
            match = TournamentMatch(
                bracket=self,
                blue_team=teams[match_entry.blue_team] if match_entry.blue_team is not None else None,
                gold_team=teams[match_entry.gold_team] if match_entry.gold_team is not None else None,
                round_num=match_entry.round_num,
                round_name=self.round_name(match_entry, match_entries),
                match_type=match_entry.match_type,
                winner_place=match_entry.winner_place,
                loser_place=match_entry.loser_place,
                match_num=i+1,
            )

            matches.append(match)

        TournamentMatch.objects.bulk_create(matches)

        to_update = set()
        blue_is_assigned = set()
        for i, match_entry in enumerate(match_entries):
            if match_entry.winner_to:
                if match_entry.winner_to in blue_is_assigned or matches[match_entry.winner_to].blue_team is not None:
                    matches[i].winner_to_team = CabinetTeam.GOLD
                else:
                    blue_is_assigned.add(match_entry.winner_to)
                    matches[i].winner_to_team = CabinetTeam.BLUE

                matches[i].winner_to_match = matches[match_entry.winner_to]
                to_update.add(matches[i])

            if match_entry.loser_to:
                if match_entry.loser_to in blue_is_assigned:
                    matches[i].loser_to_team = CabinetTeam.GOLD
                else:
                    blue_is_assigned.add(match_entry.loser_to)
                    matches[i].loser_to_team = CabinetTeam.BLUE

                matches[i].loser_to_match = matches[match_entry.loser_to]
                to_update.add(matches[i])

        TournamentMatch.objects.bulk_update(list(to_update), [
            "winner_to_match",
            "winner_to_team",
            "loser_to_match",
            "loser_to_team",
        ])

    @classmethod
    def standings_sort_key(cls, row):
        return (
            row["game_wins"] / (row["game_wins"] + row["game_losses"]) if row["game_wins"] + row["game_losses"] else 0,
            row["match_wins"] / (row["match_wins"] + row["match_losses"]) if row["match_wins"] + row["match_losses"] else 0,
            row.get("tiebreak"),
        )

    def teams(self):
        return set(
            [i.blue_team for i in self.tournamentmatch_set.all()] +
            [i.gold_team for i in self.tournamentmatch_set.all()]
        )

    def standings(self):
        return {
            TournamentStageType.ROUND_ROBIN: self.round_robin_standings,
            TournamentStageType.SINGLE_ELIM: self.bracket_standings,
            TournamentStageType.DOUBLE_ELIM: self.bracket_standings,
        }.get(self.stage_type, lambda: [])()

    def bracket_standings(self):
        standings = []

        for match in self.tournamentmatch_set.all():
            if match.is_complete:
                if match.winner_place is not None:
                    standings.append({
                        "id": match.winning_team().id,
                        "name": match.winning_team().name,
                        "place": match.winner_place,
                    })
                if match.loser_place is not None:
                    standings.append({
                        "id": match.losing_team().id,
                        "name": match.losing_team().name,
                        "place": match.loser_place,
                    })

        return sorted(standings, key=lambda i: i["place"])

    def round_robin_standings(self):
        standings = standings_by_tiebreakers(self)

        team_results = {
            team: {
                "id": team.id,
                "name": team.name,
                "match_wins": 0,
                "match_losses": 0,
                "game_wins": 0,
                "game_losses": 0,
            }
            for team in self.teams() if team is not None
        }

        for match in self.tournamentmatch_set.all():
            team_results[match.blue_team]["game_wins"] += match.blue_score
            team_results[match.blue_team]["game_losses"] += match.gold_score
            team_results[match.gold_team]["game_wins"] += match.gold_score
            team_results[match.gold_team]["game_losses"] += match.blue_score

            if match.is_complete and match.blue_score > match.gold_score:
                team_results[match.blue_team]["match_wins"] += 1
                team_results[match.gold_team]["match_losses"] += 1

            if match.is_complete and match.gold_score > match.blue_score:
                team_results[match.gold_team]["match_wins"] += 1
                team_results[match.blue_team]["match_losses"] += 1

        return [
            {
                **team_results[row["team"]],
                "tiebreak": row["tiebreak"],
                "tiebreak_desc": row["tiebreak_desc"],
            }
            for row in standings
        ]

    def round_name(self, match_entry, match_entries):
        if self.stage_type == TournamentStageType.ROUND_ROBIN:
            return f"Round {match_entry.round_num}"

        if self.stage_type == TournamentStageType.SINGLE_ELIM:
            if match_entry.match_type == MatchType.THIRD_PLACE:
                return "Third-Place Match"

            try:
                round_name = ROUND_NAMES[-match_entry.round_num]
            except IndexError:
                round_name = f"Round of {2**match_entry.round_num}"

            return round_name

        if self.stage_type == TournamentStageType.DOUBLE_ELIM:
            if match_entry.match_type == MatchType.WINNERS:
                max_round = max([i.round_num for i in match_entries if i.match_type == MatchType.WINNERS])
                round_num = max_round - match_entry.round_num + 1
                return f"Championship Round {round_num} of {max_round}"
            if match_entry.match_type == MatchType.LOSERS:
                max_round = max([i.round_num for i in match_entries if i.match_type == MatchType.LOSERS])
                round_num = max_round - match_entry.round_num + 1
                return f"Elimination Round {round_num} of {max_round}"
            if match_entry.match_type == MatchType.FIRST_FINAL:
                return "Grand Finals (First Match)"
            if match_entry.match_type == MatchType.SECOND_FINAL:
                return "Grand Finals (Second Match)"

        if self.stage_type == TournamentStageType.LADDER:
            return f"{ordinal(match_entry.round_num)} Place Match"

class TournamentTeam(BaseModel):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    linked_team_ids = ArrayField(models.CharField(max_length=200), null=False, default=list)
    image = models.ImageField(null=True, blank=True, upload_to="tournament/team-images/")

    def get_image_url(self):
        try:
            return self.image.url
        except ValueError:
            return None


class StagePlacement(BaseModel):
    dest_bracket = models.ForeignKey(TournamentBracket, on_delete=models.CASCADE, related_name="+")
    dest_place = models.IntegerField()
    src_bracket = models.ForeignKey(TournamentBracket, on_delete=models.CASCADE, null=True, related_name="+")
    src_place = models.IntegerField(null=True)
    team = models.ForeignKey(TournamentTeam, null=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = (
            ("dest_bracket", "dest_place"),
        )


class HomeScene(BaseModel):
    name = models.CharField(max_length=10, unique=True)
    display_name = models.CharField(max_length=200)
    background_color = models.CharField(max_length=10)
    foreground_color = models.CharField(max_length=10)


class TournamentPlayer(BaseModel):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    team = models.ForeignKey(TournamentTeam, null=True, blank=True, on_delete=models.SET_NULL)
    scene = models.CharField(max_length=10, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to="tournament/player-images/")
    do_not_display = models.BooleanField(default=False)
    pronouns = models.CharField(max_length=30, null=True, blank=True)
    tidbit = models.TextField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    user = models.ForeignKey("user.User", null=True, on_delete=models.SET_NULL)
    registration_time = models.DateTimeField(null=True)
    check_in_time = models.DateTimeField(null=True)
    selected_payment_options = models.ManyToManyField(SelectedPaymentOption, blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    paid = models.BooleanField(default=False)
    stripe_session_id = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "{} [{}]".format(self.name, self.scene)

    @transaction.atomic
    def collect_stats(self):
        player_games = [(i.game, i.player_id) for i in self.tournamentplayergame_set.all()]
        PlayerStat.calculate_aggregates(player_games, player_id=self.id)

    def get_stats(self):
        return PlayerStat.get_stats(player_id=self.id)


class PlayerInfo(BaseModel):
    player = models.ForeignKey(TournamentPlayer, on_delete=models.CASCADE)
    field = models.ForeignKey(PlayerInfoField, on_delete=models.CASCADE)
    value = models.CharField(max_length=2000)

    class Meta:
        unique_together = ("player", "field")


class TournamentPlayerGame(BaseModel):
    tournament_player = models.ForeignKey(TournamentPlayer, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)

    def get_stats(self):
        stats_dict = {i.stat_type: i for i in self.playerstat_set.all()}
        stats = []

        for stat_type in itertools.chain(AggregateStatType, StatType, HiddenStatType):
            stats.append({
                "name": stat_type.value,
                "label": stat_type.label,
                "value": stat.value if (stat := stats_dict.get(stat_type)) else 0,
            })

        return stats

    class Meta:
        unique_together = ("player_id", "game")


class PlayerStat(AggregatedStatModel):
    player = models.ForeignKey(TournamentPlayer, on_delete=models.CASCADE)

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        return [(tpg.game, tpg.player_id) for tpg in TournamentPlayerGame.objects.filter(
            tournament_player_id=filters.get("player_id"),
        )]

    class Meta:
        unique_together = ("player", "stat_type")


class TournamentMatch(BaseModel):
    bracket = models.ForeignKey(TournamentBracket, on_delete=models.CASCADE)
    linked_match_id = models.CharField(max_length=200, unique=True, null=True)
    blue_team = models.ForeignKey(TournamentTeam, null=True, on_delete=models.SET_NULL, related_name="+")
    gold_team = models.ForeignKey(TournamentTeam, null=True, on_delete=models.SET_NULL, related_name="+")
    match_type = models.CharField(max_length=20, null=True, choices=MatchType.choices)
    match_num = models.IntegerField(null=True)
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    round_num = models.IntegerField(null=True)
    round_name = models.CharField(max_length=50, null=True)
    active_cabinet = models.OneToOneField(Cabinet, null=True, on_delete=models.SET_NULL)
    is_flipped = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    is_warmup = models.BooleanField(default=False)
    video_link = models.URLField(null=True)
    winner_to_match = models.ForeignKey("TournamentMatch", null=True, on_delete=models.CASCADE, related_name="+")
    loser_to_match = models.ForeignKey("TournamentMatch", null=True, on_delete=models.CASCADE, related_name="+")
    winner_to_team = models.CharField(max_length=5, null=True, choices=CabinetTeam.choices)
    loser_to_team = models.CharField(max_length=5, null=True, choices=CabinetTeam.choices)
    winner_place = models.IntegerField(null=True)
    loser_place = models.IntegerField(null=True)

    @property
    def stage_name(self):
        return self.bracket.name

    def blue_team_name(self):
        return self.blue_team.name

    def gold_team_name(self):
        return self.gold_team.name

    def winning_team(self):
        if self.is_complete:
            if self.blue_score > self.gold_score:
                return self.blue_team
            if self.gold_score > self.blue_score:
                return self.gold_team

    def losing_team(self):
        if self.is_complete:
            if self.blue_score < self.gold_score:
                return self.blue_team
            if self.gold_score < self.blue_score:
                return self.gold_team

    def team_side(self, team):
        if self.blue_team == team:
            return CabinetTeam.BLUE
        if self.gold_team == team:
            return CabinetTeam.GOLD

    def team_wins(self, team):
        if self.blue_team == team:
            return self.blue_score
        if self.gold_team == team:
            return self.gold_score

    def team_losses(self, team):
        if self.blue_team == team:
            return self.gold_score
        if self.gold_team == team:
            return self.blue_score

    def round_count_reached(self):
        if self.bracket.add_tiebreaker and self.blue_score == self.gold_score:
            return False

        if self.bracket.rounds_per_match and self.blue_score + self.gold_score >= self.bracket.rounds_per_match:
            return True

        if self.bracket.wins_per_match and max(self.blue_score, self.gold_score) >= self.bracket.wins_per_match:
            return True

        return False

    def end_time(self):
        game = self.game_set.filter(end_time__isnull=False).order_by("-end_time").first()
        if game:
            return game.end_time

        if self.active_cabinet_id is not None:
            return timezone.now()

        return None

    @transaction.atomic
    def set_complete(self):
        cabinet = self.active_cabinet

        self.is_complete = True
        self.publish_match_end()
        self.active_cabinet = None
        self.save()

        if self.winner_to_match:
            if self.winner_to_team == CabinetTeam.BLUE:
                self.winner_to_match.blue_team = self.winning_team()
            if self.winner_to_team == CabinetTeam.GOLD:
                self.winner_to_match.gold_team = self.winning_team()

            self.winner_to_match.save()
            app.send_task("hivemind.tournament.tasks.publish_match", countdown=1, args=[self.winner_to_match.id])

        if self.loser_to_match:
            if self.loser_to_team == CabinetTeam.BLUE:
                self.loser_to_match.blue_team = self.losing_team()
            if self.loser_to_team == CabinetTeam.GOLD:
                self.loser_to_match.gold_team = self.losing_team()

            self.loser_to_match.save()
            app.send_task("hivemind.tournament.tasks.publish_match", countdown=1, args=[self.loser_to_match.id])

        # Double-elimination grand finals reset
        if self.bracket.tournament.link_type == Tournament.LinkType.HIVEMIND and \
           self.bracket.stage_type == TournamentStageType.DOUBLE_ELIM and \
           self.match_type == MatchType.FIRST_FINAL:

            reset_required = True
            next_match = self.bracket.tournamentmatch_set.filter(match_type=MatchType.SECOND_FINAL).first()
            for other_match in self.bracket.tournamentmatch_set.filter(
                    Q(blue_team=self.losing_team()) | Q(gold_team=self.losing_team())):
                if other_match.losing_team() == self.losing_team() and self != other_match:
                    reset_required = False

            if reset_required:
                self.winner_to_match = self.loser_to_match = next_match
                next_match.blue_team = self.gold_team
                next_match.gold_team = self.blue_team
                next_match.winner_place = 1
                next_match.loser_place = 2
                self.save()
                next_match.save()
                app.send_task("hivemind.tournament.tasks.publish_match", countdown=1, args=[next_match.id])
            else:
                next_match.delete()
                self.winner_place = 1
                self.loser_place = 2
                self.save()

        if self.bracket.tournamentmatch_set.filter(is_complete=False).count() == 0:
            self.bracket.is_complete = True
            self.bracket.save()

            if self.bracket.tournament.link_type == Tournament.LinkType.HIVEMIND:
                for bracket in self.bracket.tournament.tournamentbracket_set.filter(is_complete=False):
                    bracket.generate_matches()

        self.bracket.tournament.publish_match_state()

        if cabinet:
            app.send_task(
                "hivemind.tournament.tasks.select_next_match",
                countdown=self.bracket.queue_timer,
                args=[self.bracket.tournament.id, cabinet.id],
            )

    def report_result(self):
        app.send_task("hivemind.tournament.tasks.publish_match", args=[self.id])
        app.send_task("hivemind.tournament.tasks.publish_standings", args=[self.bracket_id])

        if not self.bracket.is_valid:
            return

        self.bracket.tournament.api_module.report_result(self)

    def report_start(self):
        app.send_task("hivemind.tournament.tasks.publish_match", countdown=1, args=[self.id])
        app.send_task("hivemind.notifications.tasks.notify_tournament_match_started", countdown=5, args=[self.id])

        if not self.bracket.is_valid:
            return

        self.bracket.tournament.api_module.report_start(
            self.linked_match_id,
            self.bracket.linked_bracket_id,
            org=self.bracket.linked_org,
        )

    def report_stop(self):
        if not self.bracket.is_valid:
            return

        self.bracket.tournament.api_module.report_stop(
            self.linked_match_id,
            self.bracket.linked_bracket_id,
            org=self.bracket.linked_org,
        )

    def publish_match_end(self):
        redis_client.publish("matchstate", json.dumps({
            "type": "matchend",
            "match_id": self.id,
            "cabinet_id": self.active_cabinet_id,
            "blue_team": self.blue_team.name,
            "blue_score": self.blue_score,
            "gold_team": self.gold_team.name,
            "gold_score": self.gold_score,
        }))

    def stats_cache_key(self):
        return "tournament_match_stats.{}".format(self.id)

    def get_match_stats(self, regenerate=False):
        if not regenerate:
            cached_stats = redis_get(self.stats_cache_key())
            if cached_stats:
                return cached_stats

        data = self.collect_match_stats()
        redis_set(self.stats_cache_key(), data, expire=60*60*24)
        return data

    def collect_match_stats(self):
        match_stats = {
            "length_sec": 0,
            "by_player": {},
        }

        by_player = match_stats["by_player"]

        for game in self.game_set.all():
            game_stats = game.generate_postgame_stats()

            match_stats["length_sec"] += game_stats["length_sec"]

            # Sum the by-team stats
            for stat_name in ["kills", "military_kills", "berries", "gate_control_sec"]:
                match_stats[stat_name] = match_stats.get(stat_name, {})
                for team, value in game_stats.get(stat_name, {}).items():
                    match_stats[stat_name][team] = match_stats[stat_name].get(team, 0) + value

            # Sum everything in the by_player dataset
            for stat_name, stat_values in game_stats.get("by_player", {}).items():
                by_player[stat_name] = by_player.get(stat_name, {})
                for player, value in stat_values.items():
                    by_player[stat_name][player] = by_player[stat_name].get(player, 0) + value


        # Get gate control percentage
        # This assumes each map has three warrior gates
        if "gate_control_sec" in match_stats:
            match_stats["gate_control"] = {
                k: "{:.01f}%".format(100 * v / match_stats["length_sec"] / 3)
                for k, v in match_stats["gate_control_sec"].items()
            }

        match_stats["by_player"] = by_player
        return match_stats


class MatchQueue(BaseModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, null=True, blank=True)
    cabinets = models.ManyToManyField(Cabinet, blank=True)
    enabled = models.BooleanField(default=False)
    auto_created = models.BooleanField(default=False)

    @classmethod
    def dequeue(cls, match):
        entry = MatchQueueEntry.objects.filter(match=match).first()
        if entry:
            queue = entry.queue
            entry.delete()
            return queue

    @transaction.atomic
    def add(self, match):
        last_entry = self.matchqueueentry_set.order_by("order").last()
        order = last_entry.order + 1 if last_entry else 1
        MatchQueueEntry.objects.create(queue=self, match=match, order=order)

    @transaction.atomic
    def set_list(self, match_list):
        self.matchqueueentry_set.all().delete()

        for idx, match in enumerate(match_list):
            MatchQueueEntry.objects.create(queue=self, match=match, order=idx+1)

    def get_list(self):
        return [i.match for i in self.matchqueueentry_set.order_by("order")]

    def get_next_match(self, cabinet):
        if not self.enabled:
            return None

        next_entry = self.matchqueueentry_set.order_by("order").first()
        if next_entry is not None:
            return next_entry.match

        # if this was the general queue, exit now
        if self.cabinets.count() == 0:
            return None

        # nothing in this queue, find the next-most-specific one
        next_queue = cabinet.matchqueue_set.filter(tournament=self.tournament, enabled=True) \
                                           .annotate(Count("cabinets")).filter(
                                               Q(cabinets__count=self.cabinets.count(), id__gt=self.id) |
                                               Q(cabinets__count__gt=self.cabinets.count())
                                           ).order_by("cabinets__count", "id").first()

        if next_queue:
            return next_queue.get_next_match(cabinet)

        # now try the tournament's general queue
        return self.tournament.get_general_queue().get_next_match(cabinet)

    class Meta:
        unique_together = ("tournament", "name")


class MatchQueueEntry(BaseModel):
    queue = models.ForeignKey(MatchQueue, on_delete=models.CASCADE)
    match = models.OneToOneField(TournamentMatch, on_delete=models.CASCADE)
    order = models.IntegerField(null=False)

    class Meta:
        unique_together = ("queue", "order")


class Video(BaseModel):
    class VideoSite(models.TextChoices):
        YOUTUBE = "youtube", "YouTube"

    tournament = models.ForeignKey(Tournament, null=True, on_delete=models.SET_NULL)
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    service = models.TextField(max_length=20, choices=VideoSite.choices, null=True)
    video_id = models.CharField(max_length=200)
    start_time = models.DateTimeField()
    length = models.DurationField()

    class Meta:
        unique_together = ("service", "video_id")

    def games(self):
        return Game.objects.filter(
            cabinet=self.cabinet,
            start_time__gte=self.start_time,
            start_time__lt=self.start_time + self.length,
        ).order_by("start_time")

    def get_match_data(self):
        matches = {}

        for game in self.games():
            if game.tournament_match:
                match = game.tournament_match
                matches[match.id] = {
                    "match": match,
                    "start_time": min(
                        matches.get(match.id, {}).get("start_time", timedelta.max),
                        game.start_time - self.start_time - timedelta(seconds=5),
                    ),
                    "end_time": max(
                        matches.get(match.id, {}).get("end_time", timedelta.min),
                        game.end_time - self.start_time + timedelta(seconds=5),
                    ),
                }

        return matches

    def tag_matches(self):
        matches = self.get_match_data()

        for match in matches.values():
            match["match"].video_link = self.get_embed_url(match["start_time"], match["end_time"])
            match["match"].save()

    def match_timestamps(self):
        matches = self.get_match_data()

        return os.linesep.join([
            "{} - {}: {} vs. {}".format(
                str(m["start_time"]).split(".")[0],
                m["match"].round_name,
                m["match"].blue_team.name,
                m["match"].gold_team.name,
            )
            for m in sorted(matches.values(), key=lambda m: m["start_time"])
        ])

    def url(self):
        if self.service == self.VideoSite.YOUTUBE:
            return "https://www.youtube.com/watch?v={}".format(self.video_id)

        return self.video_id

    def get_embed_url(self, start_time, end_time=None):
        query = {
            "start": int(start_time.total_seconds()),
        }

        if end_time:
            query["end"] = int(end_time.total_seconds())

        if self.service == self.VideoSite.YOUTUBE:
            return "https://www.youtube.com/embed/{}?{}".format(self.video_id, urlencode(query))


class PaymentAccount(Deleteable):
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    stripe_account_id = models.TextField(max_length=50, null=True, blank=True)
    created_timestamp = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)


class TournamentLink(BaseModel):
    tournament = models.ForeignKey(Tournament, null=True, on_delete=models.SET_NULL)
    url = models.URLField()
    title = models.TextField(max_length=100)
    image = models.ImageField(null=True, blank=True, upload_to="tournament/links/")
    order = models.IntegerField(null=True)
