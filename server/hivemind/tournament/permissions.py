from datetime import datetime

from django.core.exceptions import BadRequest
from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod, PermissionType
from ..game.models import Cabinet, Game, Scene
from ..permissions import (SceneAdminOnly, SceneAdminOrReadOnly,
                           TournamentOfficialOrReadOnly)
from .models import (MatchQueue, Tournament, TournamentBracket, TournamentTeam,
                     TournamentTemplate)


class TournamentPermission(SceneAdminOrReadOnly):
    can_delete = True


class PlayerInfoFieldPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid tournament")


class TournamentTemplatePermission(SceneAdminOnly):
    can_delete = True


class PaymentOptionPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid tournament")


class TournamentBracketPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid tournament")


class TournamentTeamPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid tournament")


class StagePlacementPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.dest_bracket.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            bracket = TournamentBracket.objects.get(id=request.data.get("dest_bracket"))
            return bracket.tournament.scene.id
        except TournamentBracket.DoesNotExist:
            raise BadRequest("Invalid tournament")


class TournamentPlayerPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid tournament")


class TournamentMatchPermission(TournamentOfficialOrReadOnly):
    write_permissions = [PermissionType.ADMIN, PermissionType.TOURNAMENT]

    def get_scene_from_object(self, obj):
        return obj.bracket.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            bracket = TournamentBracket.objects.get(id=request.data.get("bracket"))
            return bracket.tournament.scene.id
        except TournamentBracket.DoesNotExist:
            raise BadRequest("Invalid bracket")


class MatchQueuePermission(TournamentOfficialOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid cabinet")


class VideoPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.cabinet.scene

    def get_scene_id_from_request(self, request):
        try:
            cabinet = Cabinet.objects.get(id=request.data.get("cabinet"))
            return cabinet.scene.id
        except Cabinet.DoesNotExist:
            raise BadRequest("Invalid cabinet")


class VideoCreatePermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False

        try:
            game = Game.objects.get(id=request.data.get("game"))
            return request.user.is_admin_of(game.cabinet.scene)
        except Game.DoesNotExist:
            raise BadRequest("Invalid game")


class PaymentAccountPermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False

        if request.method not in SAFE_METHODS:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        return request.user.is_admin_of(obj.scene)


class TournamentLinkPermission(SceneAdminOrReadOnly):
    can_delete = True

    def get_scene_from_object(self, obj):
        return obj.tournament.scene

    def get_scene_id_from_request(self, request):
        try:
            tournament = Tournament.objects.get(id=request.data.get("tournament"))
            return tournament.scene.id
        except Tournament.DoesNotExist:
            raise BadRequest("Invalid tournament")
