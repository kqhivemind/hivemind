# Generated by Django 3.2.7 on 2023-09-22 02:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0064_auto_20230916_2224'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournamentmatch',
            name='round_num',
            field=models.IntegerField(null=True),
        ),
    ]
