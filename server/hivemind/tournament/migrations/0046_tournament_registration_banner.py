# Generated by Django 3.2.7 on 2023-04-10 05:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0045_tournamentplayer_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='registration_banner',
            field=models.ImageField(blank=True, null=True, upload_to='tournament/registration-banners/'),
        ),
    ]
