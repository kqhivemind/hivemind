import json
import logging

from celery import shared_task

from ..achievement.models import Achievement
from ..client import redis_client
from ..constants import AggregateStatType, StatType
from ..game.models import Cabinet, Game, GameStat
from .models import MatchQueue, Tournament, TournamentMatch, TournamentPlayer, TournamentBracket
from .serializers import MatchQueueSerializer, TournamentMatchSerializer

logger = logging.getLogger(__name__)

@shared_task
def generate_awards(tournament_id, start_date=None, end_date=None):
    tournament = Tournament.objects.get(id=tournament_id)

    games = Game.objects.filter(
        cabinet__scene_id=tournament.scene.id,
        end_time__isnull=False,
    )

    if start_date:
        games = games.filter(start_time__gte=start_date, start_time__lt=end_date,
                             cabinet__scene_id=tournament.scene.id)
    else:
        games = games.filter(tournament_match__bracket__tournament_id=tournament_id)

    data = {
        "tournament_id": tournament_id,
        "start_date": start_date,
        "end_date": end_date,
        "awards": [],
        "achievements": [],
    }

    highest_stats = [
        (AggregateStatType.GAMES, "Most Games Played"),
        (StatType.KILLS, "Most Kills"),
        (StatType.QUEEN_KILLS, "Most Queen Kills"),
        (StatType.BUMP_ASSISTS, "Most Bump Assists"),
        (StatType.BERRIES, "Most Berries Deposited"),
        (StatType.BERRIES_KICKED, "Most Berries Kicked In"),
        (StatType.EATEN_BY_SNAIL, "Tastiest Drone"),
    ]

    totals_by_user = {i[0]: {} for i in highest_stats}
    names_by_user = {}
    for game in games:
        for user_game in game.usergame_set.all():
            if user_game.user.id not in names_by_user:
                if user_game.user.scene:
                    names_by_user[user_game.user_id] = "{} [{}]".format(user_game.user.name, user_game.user.scene)
                else:
                    names_by_user[user_game.user_id] = user_game.user.name

            totals_by_user[AggregateStatType.GAMES][user_game.user_id] = 1 + \
                totals_by_user[AggregateStatType.GAMES].get(user_game.user.id, 0)

            for game_stat in game.gamestat_set.filter(player_id=user_game.player_id):
                if game_stat.stat_type in totals_by_user:
                    totals_by_user[game_stat.stat_type][user_game.user_id] = game_stat.value + \
                        totals_by_user[game_stat.stat_type].get(user_game.user.id, 0)

    for stat_type, title in highest_stats:
        stat_totals = totals_by_user[stat_type]

        data["awards"].append({
            "title": title,
            "values": [
                {"user_id": i, "user_name": names_by_user[i], "value": stat_totals[i]}
                for i in sorted(stat_totals, key=lambda i: stat_totals[i], reverse=True)[:5]
            ],
        })

    achievements_earned = {}
    user_names = {}

    for game in games:
        stats = game.get_postgame_stats()

        for achievement in Achievement.__subclasses__():
            if not achievement.include_in_tournament_awards:
                continue

            if achievement.__name__ not in achievements_earned:
                achievements_earned[achievement.__name__] = {}

            for user_game in game.usergame_set.all():
                if user_game.user_id in achievements_earned[achievement.__name__]:
                    continue

                try:
                    if achievement.test(user_game, stats):
                        achievements_earned[achievement.__name__][user_game.user_id] = game.id
                except Exception as err:
                    logger.exception("Failed processing {} for game {}".format(achievement.__name__, game.id))
                    continue

    for achievement_name, users in achievements_earned.items():
        for user_id, game_id in users.items():
            data["achievements"].append({
                "achievement": achievement_name,
                "user_id": user_id,
                "user_name": names_by_user[user_id],
                "game_id": game_id,
            })

    tournament.awards = data
    tournament.save()

    return data

@shared_task
def get_available_matches(tournament_id):
    tournament = Tournament.objects.get(id=tournament_id)
    for bracket in tournament.tournamentbracket_set.all():
        if bracket.is_valid:
            bracket.get_matches()

    publish_matches(tournament.id)

@shared_task
def publish_match(match_id):
    match = TournamentMatch.objects.get(id=match_id)
    serialized = json.dumps(TournamentMatchSerializer(match).data)
    redis_client.publish("tournament.{}.match".format(match.bracket.tournament_id), serialized)

@shared_task
def publish_matches(tournament_id):
    tournament = Tournament.objects.get(id=tournament_id)

    matches = TournamentMatch.objects.filter(is_complete=False, bracket__tournament_id=tournament_id) \
                                     .order_by("bracket__id", "round_name", "id")

    matches_serialized = json.dumps(TournamentMatchSerializer(matches, many=True).data)
    redis_client.publish("tournament.{}.matches".format(tournament_id), matches_serialized)

@shared_task
def collect_player_stats(player_id):
    player = TournamentPlayer.objects.get(id=player_id)
    player.collect_stats()

@shared_task
def select_next_match(tournament_id, cabinet_id):
    tournament = Tournament.objects.get(id=tournament_id)
    cabinet = Cabinet.objects.get(id=cabinet_id)

    tournament.select_next_match(cabinet)

    publish_matches(tournament.id)
    for queue in tournament.matchqueue_set.all():
        publish_queue(queue.id)

@shared_task
def publish_queue(queue_id):
    queue = MatchQueue.objects.get(id=queue_id)
    data = json.dumps(MatchQueueSerializer(queue).data)

    redis_client.publish("tournament.{}.queue".format(queue.tournament_id), data)

@shared_task
def publish_standings(tournament_bracket_id):
    bracket = TournamentBracket.objects.get(id=tournament_bracket_id)
    data = json.dumps({
        "bracket_id": tournament_bracket_id,
        "standings": bracket.standings(),
    })

    redis_client.publish("tournament.{}.standings".format(bracket.tournament_id), data)
