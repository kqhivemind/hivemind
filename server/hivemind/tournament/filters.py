from datetime import timedelta

from django.db.models import Q
from django.utils import timezone
from django_filters import rest_framework as filters

from .models import PlayerInfo, TournamentMatch


class TournamentFilter(filters.FilterSet):
    scene_id = filters.NumberFilter(field_name="scene_id")
    slug = filters.CharFilter(field_name="slug")
    is_future = filters.BooleanFilter(method="is_future_filter")

    ordering = filters.OrderingFilter(fields=["date"])

    def is_future_filter(self, queryset, name, value):
        return queryset.filter(date__gt=timezone.now() - timedelta(days=4))

    fields = [
        "scene_id",
        "slug",
        "is_future",
    ]


class TournamentMatchFilter(filters.FilterSet):
    team_id = filters.NumberFilter(method="team_id_filter")

    def team_id_filter(self, queryset, name, value):
        return queryset.filter(Q(blue_team_id=value) | Q(gold_team_id=value))

    class Meta:
        model = TournamentMatch
        fields = [
            "bracket_id",
            "team_id",
            "active_cabinet_id",
        ]


class PlayerInfoFilter(filters.FilterSet):
    tournament_id = filters.NumberFilter(field_name="field__tournament_id")

    class Meta:
        model = PlayerInfo
        fields = [
            "player_id",
            "field_id",
            "tournament_id",
        ]
