import bisect
from datetime import timedelta

from django.db import models

from ..constants import (SNAIL_PX_PER_SEC, SNAIL_PX_PER_SEC_SPEED,
                         CabinetPosition, StatFormattingType)


def stat_types(**filters):
    """ Returns a list of stat types, optionally filtered. """
    subclasses = BaseStatType.all_stat_types

    for k, v in filters.items():
        subclasses = list(filter(lambda c: getattr(c, k) == v, subclasses))

    return subclasses

def get_stat_type_class(name):
    """ Finds a stat type class by stat name. """
    return {stat.name: stat for stat in stat_types()}[name]

def calculate_game_stats(game):
    """ Calculates all stats for a game. """
    stats = {}

    for stat_type in stat_types():
        if not stat_type.is_aggregate:
            stats[stat_type.name] = {k: v for k, v in stat_type.calculate(game).items() if v > 0}

    return stats

def get_stat_events_by_position(game):
    """ For each player position, get a list of all events that counted towards a stat. """
    stat_events = {}

    for stat_type in stat_types():
        if not stat_type.is_aggregate:
            try:
                stat_events[stat_type.name] = stat_type.events_by_position(game)
            except NotImplementedError:
                pass

    return stat_events

def calculate_aggregates(games):
    """ Calculates aggregate stats across a list of games. Accepts a list of (game, position_id) tuples. """
    stats = {}

    for stat_type in stat_types():
        stats[stat_type.name] = stat_type.calculate_aggregate(games)

    return stats

def add_game_to_aggregates(aggregates, game, position_id):
    """ Adds all stats from a game to an existing aggregate. Returns the new aggregated values. """
    stats = {}

    for stat_type in stat_types():
        stats[stat_type.name] = stat_type.add_game_to_aggregate(aggregates, game, position_id)

    return stats

def initialize_dict(start_value=0):
    """ Helper method to initialize a dict of {position: start_value} for each position. """

    return {i.value: start_value() if callable(start_value) else start_value for i in CabinetPosition}

def get_event_count_by_player(game, event_type, position_idx, event_filter=None):
    """ Shortcut to calculate a stat if it's a simple count of events """
    counts = initialize_dict()

    for event in game.get_events(lambda evt: evt.event_type == event_type):
        if event_filter is not None and not event_filter(event):
            continue

        position_id = int(event.values[position_idx])
        counts[position_id] += 1

    return counts

def warrior_up_intervals(game, position):
    """ Returns a list of (start, end) tuples representing times when the player was a warrior. """

    warrior_up_events = game.get_events(
        lambda evt: evt.event_type == "useMaiden" and \
            evt.values[2] == "maiden_wings" and \
            int(evt.values[3]) == int(position),
    )

    death_events = game.get_events(
        lambda evt: evt.event_type == "playerKill" and int(evt.values[3]) == int(position),
    )

    death_timestamps = [evt.timestamp for evt in death_events]

    intervals = []
    for up_event in warrior_up_events:
        death_event_idx = bisect.bisect_right(death_timestamps, up_event.timestamp)
        if death_event_idx >= len(death_events):
            death_timestamp = game.end_time
        else:
            death_timestamp = death_events[death_event_idx].timestamp

        intervals.append((up_event.timestamp, death_timestamp))

    return intervals

def speed_intervals(game, position):
    """ Returns a list of (start, end) tuples representing times when the player had speed. """

    speed_up_events = game.get_events(
        lambda evt: evt.event_type == "useMaiden" and \
            evt.values[2] == "maiden_speed" and \
            int(evt.values[3]) == int(position),
    )

    death_events = game.get_events(
        lambda evt: evt.event_type == "playerKill" and int(evt.values[3]) == int(position),
    )

    death_timestamps = [evt.timestamp for evt in death_events]

    intervals = []
    for up_event in speed_up_events:
        death_event_idx = bisect.bisect_right(death_timestamps, up_event.timestamp)
        if death_event_idx >= len(death_events):
            death_timestamp = game.end_time
        else:
            death_timestamp = death_events[death_event_idx].timestamp

        intervals.append((up_event.timestamp, death_timestamp))

    return intervals

def player_is_warrior(game, position, timestamp):
    """ Helper method to determine if player was a warrior at the given timestamp. """

    for interval in warrior_up_intervals(game, position):
        if interval[0] < timestamp and timestamp < interval[1]:
            return True

    return False

def player_has_speed(game, position, timestamp):
    """ Helper method to determine if player had speed at the given timestamp. """

    for interval in speed_intervals(game, position):
        if interval[0] < timestamp and timestamp < interval[1]:
            return True

    return False


class BaseStatType:
    name = None
    display_name = None
    description = None
    formatting = StatFormattingType.INTEGER

    is_hidden = False                 # If hidden, stat will not display in lists
    is_aggregate = False              # Aggregate stats are only shown when aggregating multiple games
    include_in_leaderboards = False   # Should this stat type be shown on the leaderboards?
    event_type = None                 # If triggered by an event, set this to the event_type
    event_position_idx = None         # The index of the player position in the event's values array
    event_filter = lambda evt: True   # Function to filter events

    all_stat_types = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

        if cls.name is not None:
            cls.all_stat_types.append(cls)

    @classmethod
    def events(cls, game):
        """
        Return a list of all events that are counted for this stat.
        If not implemented, event_type must be set.
        """

        if cls.event_type is None:
            raise NotImplementedError

        return game.get_events(lambda evt: evt.event_type == cls.event_type and cls.event_filter(evt))

    @classmethod
    def events_by_position(cls, game):
        """
        Return a list of all events that are counted for this stat, grouped by player position.
        If not implemented, event_position_idx must be set, and either events or event_type must be set.
        Should return a {position_id: [events]} dict.
        """

        if cls.event_position_idx is None:
            raise NotImplementedError

        by_position = {i.value: [] for i in CabinetPosition}

        for event in cls.events(game):
            position_id = int(event.values[cls.event_position_idx])
            by_position[position_id].append(event)

        return by_position

    @classmethod
    def calculate(cls, game):
        """
        Calculate stats for all players for a single game.
        If not implemented, event_type and event_position_idx must be set.
        Should return a {position_id: value} dict.
        """

        return {k: len(v) for k, v in cls.events_by_position(game).items()}

    @classmethod
    def calculate_aggregate(cls, games):
        """
        Calculates aggregate value of a stat across a list of games.
        Accepts a list of (game, position_id) tuples.
        Returns the aggregated stat value.
        """

        value = 0

        for game, position_id in games:
            value += cls.calculate(game).get(position_id, 0)

        return value

    @classmethod
    def add_game_to_aggregate(cls, aggregates, game, position_id):
        """
        Adds a game to an existing set of aggregates.
        Override this for any value that can't be aggregated by simply adding the game's value to the
        existing aggregate.
        Returns the new aggregated stat value.
        """

        value = aggregates.get(cls.name, 0)
        value += cls.calculate(game).get(position_id, 0)

        return value


class BaseRateStat(BaseStatType):
    """
    Parent class for stats that are calculated as rates over time.
    """

    value_class = None
    time_class = None
    multiplier = 60_000_000    # By default, assume times are in ms, and return value per 1000 minutes
    formatting = StatFormattingType.THOUSANDTHS

    @classmethod
    def calculate(cls, game):
        values = cls.value_class.calculate(game)
        times = cls.time_class.calculate(game)

        return {k: int(values.get(v, 0) * cls.multiplier / v) for k, v in times.items() if v > 0}

    @classmethod
    def calculate_aggregate(cls, games):
        total_value = cls.value_class.calculate_aggregate(games)
        total_time = cls.time_class.calculate_aggregate(games)

        if total_time:
            return int(total_value * cls.multiplier / total_time)

        return None

    @classmethod
    def add_game_to_aggregate(cls, aggregates, game, position_id):
        total_value = aggregates.get(cls.value_class.name, 0)
        total_value += cls.value_class.calculate(game).get(position_id, 0)

        total_time = aggregates.get(cls.time_class.name, 0)
        total_time += cls.time_class.calculate(game).get(position_id, 0)

        if total_time:
            return int(total_value * cls.multiplier / total_time)

        return None


class Games(BaseStatType):
    name = "games"
    display_name = "Games"
    is_aggregate = True
    include_in_leaderboards = True

    @classmethod
    def calculate(cls, game):
        return {pos.value: 1 for pos in CabinetPosition}


class Wins(BaseStatType):
    name = "wins"
    display_name = "Wins"
    is_aggregate = True

    @classmethod
    def calculate(cls, game):
        return {pos.value: 1 if pos.team == game.winning_team else 0 for pos in CabinetPosition}


class Losses(BaseStatType):
    name = "losses"
    display_name = "Losses"
    is_aggregate = True

    @classmethod
    def calculate(cls, game):
        return {pos.value: 0 if pos.team == game.winning_team else 1 for pos in CabinetPosition}


class TimePlayed(BaseStatType):
    name = "time_played"
    display_name = "Time Played"
    is_aggregate = True
    formatting = StatFormattingType.TIME

    @classmethod
    def calculate(cls, game):
        return {pos.value: int(game.game_length().total_seconds() * 1000) for pos in CabinetPosition}


class Kills(BaseStatType):
    name = "kills"
    display_name = "Total Kills"
    event_type = "playerKill"
    event_position_idx = 2
    include_in_leaderboards = True


class Deaths(BaseStatType):
    name = "deaths"
    display_name = "Total Deaths"
    event_type = "playerKill"
    event_position_idx = 3


class QueenKills(BaseStatType):
    name = "queen_kills"
    display_name = "Queen Kills"
    event_type = "playerKill"
    event_position_idx = 2
    event_filter = lambda evt: evt.values[4] == "Queen"


class MilitaryKills(BaseStatType):
    name = "military_kills"
    display_name = "Military Kills"
    event_type = "playerKill"
    event_position_idx = 2
    event_filter = lambda evt: evt.values[4] != "Worker"


class MilitaryDeaths(BaseStatType):
    name = "military_deaths"
    display_name = "Military Deaths"
    event_type = "playerKill"
    event_position_idx = 3
    event_filter = lambda evt: evt.values[4] != "Worker"


class BumpAssists(BaseStatType):
    name = "bump_assists"
    display_name = "Bump Assists"
    description = "Bumping a warrior or queen, who is then killed within two seconds."
    include_in_leaderboards = True

    @classmethod
    def events(cls, game):
        kills = game.get_events(lambda evt: evt.event_type == "playerKill" and evt.values[4] != "Worker")
        bumps = game.get_events(lambda evt: evt.event_type == "glance")
        bump_timestamps = [i.timestamp for i in bumps]

        assisted_kills = []

        for kill in kills:
            # Find all glances in previous two seconds
            start_idx = bisect.bisect_left(bump_timestamps, kill.timestamp - timedelta(seconds=2))
            end_idx = bisect.bisect_right(bump_timestamps, kill.timestamp)
            victim = kill.values[3]

            for bump_idx in range(start_idx, end_idx):
                bump = bumps[bump_idx]
                if victim in bump.values[2:4]:
                    assisted_kills.append(kill)
                    break

        return assisted_kills

    @classmethod
    def events_by_position(cls, game):
        assists = initialize_dict(start_value=lambda: [])
        kills = cls.events(game)
        bumps = game.get_events(lambda evt: evt.event_type == "glance")
        bump_timestamps = [i.timestamp for i in bumps]

        for kill in kills:
            assist_positions = set()

            start_idx = bisect.bisect_left(bump_timestamps, kill.timestamp - timedelta(seconds=2))
            end_idx = bisect.bisect_right(bump_timestamps, kill.timestamp)
            victim = kill.values[3]

            for bump_idx in range(start_idx, end_idx):
                bump = bumps[bump_idx]
                if victim in bump.values[2:4]:
                    # Add both players involved in the bump, discard the victim later
                    assist_positions |= set(bump.values[2:4])

            assist_positions.discard(victim)
            for pos in assist_positions:
                assists[int(pos)].append(kill)

        return assists


class Berries(BaseStatType):
    name = "berries"
    display_name = "Berries Deposited"
    event_type = "berryDeposit"
    event_position_idx = 2
    include_in_leaderboards = True


class BerriesKicked(BaseStatType):
    name = "berries_kicked"
    display_name = "Berries Kicked In"
    event_type = "berryKickIn"
    event_position_idx = 2
    event_filter = lambda evt: len(evt.values) < 4 or evt.values[3] == "True"
    include_in_leaderboards = True


class SnailDistance(BaseStatType):
    name = "snail_distance"
    display_name = "Snail Distance"
    is_hidden = True

    @classmethod
    def calculate(cls, game):
        distance = initialize_dict()

        snail_events = game.get_events(lambda evt: evt.event_type in ["getOnSnail", "getOffSnail", "snailEat"])

        last_on = None

        for event in snail_events:
            if event.event_type == "getOnSnail":
                last_on = event

            if event.event_type == "getOffSnail":
                if last_on is not None:
                    distance[int(last_on.values[2])] += abs(int(event.values[0]) - int(last_on.values[0]))

                last_on = None

            if event.event_type == "snailEat":
                if last_on is not None:
                    distance[int(last_on.values[2])] += abs(int(event.values[0]) - int(last_on.values[0]))

                last_on = event

        # Add estimated distance if player is still on snail at end of game
        if last_on is not None:
            speed = SNAIL_PX_PER_SEC_SPEED if player_has_speed(game, last_on.values[2], last_on.timestamp) \
                else SNAIL_PX_PER_SEC

            est_time = None
            if last_on.event_type == "getOnSnail":
                est_time = game.end_time - last_on.timestamp
            if last_on.event_type == "snailEat":
                # If last snail event is snailEat, snail won't start moving until the playerKill
                kill_events = game.get_events(
                    lambda evt: evt.event_type == "playerKill" and \
                        evt.values[2] == last_on.values[2] and \
                        evt.values[3] == last_on.values[3] and \
                        evt.timestamp > last_on.timestamp,
                )

                if len(kill_events) > 0:
                    est_time = game.end_time - kill_events[0].timestamp

            if est_time is not None:
                est_distance = speed * est_time.total_seconds()
                distance[int(last_on.values[2])] += int(est_distance)

        return distance


class SnailMeters(BaseStatType):
    name = "snail_meters"
    display_name = "Snail Meters"
    description = "21 pixels, or approximately 1 second as a vanilla snail rider"
    include_in_leaderboards = True

    @classmethod
    def calculate(cls, game):
        return {k: int(v/21) for k, v in SnailDistance.calculate(game).items()}


class SnailDistancePct(BaseStatType):
    name = "snail_distance_pct"
    display_name = "Snail Distance (%)"
    description = "Snail distance calculated as percentage of track length"
    is_hidden = True

    @classmethod
    def calculate(cls, game):
        snail_track_width = game.get_map().snail_track_width
        if snail_track_width:
            return {k: int(100 * v / snail_track_width) for k, v in SnailDistance.calculate(game).items()}

        return {}


class EatenBySnail(BaseStatType):
    name = "eaten_by_snail"
    display_name = "Eaten by Snail"
    event_type = "snailEat"
    event_position_idx = 3


class WarriorUptime(BaseStatType):
    name = "warrior_uptime"
    display_name = "Warrior Uptime"
    formatting = StatFormattingType.TIME

    @classmethod
    def calculate(cls, game):
        uptimes = initialize_dict()

        for position in CabinetPosition:
            for interval in warrior_up_intervals(game, position.value):
                uptimes[position.value] += (interval[1] - interval[0]).total_seconds()

        return {k: int(v * 1000) for k, v in uptimes.items()}


class VanillaWarriorUptime(BaseStatType):
    name = "vanilla_warrior_uptime"
    display_name = "Vanilla Warrior Uptime"
    formatting = StatFormattingType.TIME

    @classmethod
    def calculate(cls, game):
        uptimes = initialize_dict()

        for position in CabinetPosition:
            for interval in warrior_up_intervals(game, position.value):
                if not player_has_speed(game, position.value, interval[0]):
                    uptimes[position.value] += (interval[1] - interval[0]).total_seconds()

        return {k: int(v * 1000) for k, v in uptimes.items()}


class KillsAsVanilla(BaseStatType):
    name = "kills_as_vanilla"
    display_name = "Kills as Vanilla Warrior"
    description = "Total military kills as a vanilla warrior"
    event_position_idx = 2
    is_hidden = True

    @classmethod
    def events(cls, game):
        kills = []

        for event in game.get_events(lambda evt: evt.event_type == "playerKill" and evt.values[4] != "Worker"):
            pos = int(event.values[2])
            if not CabinetPosition(pos).is_queen and not player_has_speed(game, pos, event.timestamp):
                kills.append(event)

        return kills


class SpeedWarriorUptime(BaseStatType):
    name = "speed_warrior_uptime"
    display_name = "Speed Warrior Uptime"
    formatting = StatFormattingType.TIME

    @classmethod
    def calculate(cls, game):
        uptimes = initialize_dict()

        for position in CabinetPosition:
            for interval in warrior_up_intervals(game, position.value):
                if player_has_speed(game, position.value, interval[0]):
                    uptimes[position.value] += (interval[1] - interval[0]).total_seconds()

        return {k: int(v * 1000) for k, v in uptimes.items()}


class QueenTime(BaseStatType):
    name = "queen_time"
    display_name = "Time as Queen"
    formatting = StatFormattingType.TIME
    is_hidden = True

    @classmethod
    def calculate(cls, game):
        return {k: v for k, v in TimePlayed.calculate(game).items() if CabinetPosition(k).is_queen}

    @classmethod
    def calculate_aggregate(cls, games):
        return TimePlayed.calculate_aggregate(filter(lambda i: CabinetPosition(i[1]).is_queen, games))


class KillsAsSpeed(BaseStatType):
    name = "kills_as_speed"
    display_name = "Kills as Speed Warrior"
    description = "Total military kills as a speed warrior"
    event_position_idx = 2
    is_hidden = True

    @classmethod
    def events(cls, game):
        kills = []

        for event in game.get_events(lambda evt: evt.event_type == "playerKill" and evt.values[4] != "Worker"):
            pos = int(event.values[2])
            if not CabinetPosition(pos).is_queen and player_has_speed(game, pos, event.timestamp):
                kills.append(event)

        return kills


class KillsAsQueen(BaseStatType):
    name = "kills_as_queen"
    display_name = "Kills as Queen"
    description = "Total military kills as a queen"
    event_type = "playerKill"
    event_position_idx = 2
    event_filter = lambda evt: CabinetPosition(int(evt.values[2])).is_queen and evt.values[4] != "Worker"
    is_hidden = True


class QueenKPM(BaseRateStat):
    name = "queen_kpm"
    display_name = "Queen Kills/Min"
    description = "Military kills per minute when playing queen"
    is_aggregate = True
    value_class = KillsAsQueen
    time_class = QueenTime


class VanillaKPM(BaseRateStat):
    name = "vanilla_kpm"
    display_name = "Vanilla Kills/Min"
    description = "Military kills per minute as a vanilla warrior"
    is_aggregate = True
    value_class = KillsAsVanilla
    time_class = VanillaWarriorUptime


class SpeedKPM(BaseRateStat):
    name = "speed_kpm"
    display_name = "Speed Kills/Min"
    description = "Military kills per minute as a speed warrior"
    is_aggregate = True
    value_class = KillsAsSpeed
    time_class = SpeedWarriorUptime


StatType = models.TextChoices("StatType", {
    s.name.upper(): (s.name, s.display_name) for s in stat_types()
})
