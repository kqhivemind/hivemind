import itertools
import logging
import math
from datetime import date, datetime, timedelta, timezone

from celery import chain, shared_task
from django.db import models
from django.db.models import Count, ExpressionWrapper, F
from django.db.models.functions import Trunc, TruncMonth
from tqdm import tqdm

from ..client import redis_get, redis_set
from ..constants import CabinetPosition, CabinetTeam
from ..game.models import Game, GameMap, GameStat, Scene
from ..user.models import User, UserGame
from .models import SignInLog, UserRecap
from .statmodel import AggregatedStatModel
from .stattype import (StatType, calculate_aggregates, get_stat_type_class,
                       stat_types)

logger = logging.getLogger(__name__)

@shared_task
def generate_site_recap(year):
    lock_key = f"recap.{year}.is_generating"
    is_running = redis_get(lock_key)
    if is_running:
        return

    redis_set(lock_key, True)

    redis_key = f"recap.{year}"
    year_start = datetime(year, 1, 1, tzinfo=timezone.utc)
    year_end = datetime(year+1, 1, 1, tzinfo=timezone.utc)

    data = {
        "year": year,
    }

    games = Game.objects.filter(end_time__isnull=False, start_time__gte=year_start, start_time__lt=year_end)
    user_games = UserGame.objects.filter(game__start_time__gte=year_start, game__start_time__lt=year_end)
    nfcs = NFC.objects.filter(date_registered__gte=year_start, date_registered__lt=year_end)
    all_maps = {i.name: i for i in GameMap.objects.all()}

    top_month = games.annotate(month=TruncMonth("start_time")) \
                     .values("month") \
                     .annotate(count=models.Count("id")) \
                     .order_by("-count") \
                     .first()
    top_map = games.values("map_name").annotate(count=models.Count("id")).order_by("-count").first()

    data["games"] = {
        "total": games.count(),
        "game_time_days": games.annotate(game_time=models.Sum(ExpressionWrapper((F("end_time") - F("start_time")), output_field=models.DateTimeField()))).aggregate(models.Sum("game_time"))["game_time__sum"].days,
        "scenes": games.values("cabinet__scene_id").annotate(total=models.Count("id")).count(),
        "cabinets": games.values("cabinet_id").annotate(total=models.Count("id")).count(),
        "top_month": {
            "month": top_month["month"].strftime("%B"),
            "count": top_month["count"],
        },
        "top_map": {
            "name": GameMap.objects.get(name=top_map["map_name"]).display_name,
            "count": top_map["count"],
        },
        "top_scenes": [
            {
                "scene": i["cabinet__scene__display_name"],
                "background_color": i["cabinet__scene__background_color"],
                "games": i["total"],
            }
            for i in games.values("cabinet__scene__display_name", "cabinet__scene__background_color").annotate(total=models.Count("id")).order_by("-total")[:5]
        ],
        "winning_team": {k: games.filter(winning_team=k).count() for k in ["blue", "gold"]},
    }

    position_counts = {i["player_id"]: i["total"] for i in user_games.values("player_id").annotate(total=models.Count("id"))}
    total_user_games = sum(position_counts.values())
    queen_pct = (position_counts[1] + position_counts[2]) / total_user_games
    data["logged_in"] = {
        "games": user_games.values("game_id").annotate(totals=models.Count("id")).count(),
        "users": user_games.values("user_id").annotate(totals=models.Count("id")).count(),
        "scenes": user_games.values("game__cabinet__scene_id").annotate(totals=models.Count("id")).count(),
        "queen_pct": queen_pct,
        "position_counts": position_counts,
        "nfc_tags": nfcs.count(),
    }

    tournament_game_counts = [
        {
            "name": i["tournament_match__bracket__tournament__name"],
            "scene": i["cabinet__scene__display_name"],
            "background_color": i["cabinet__scene__background_color"],
            "games": i["count"],
        }
        for i in games.filter(tournament_match__isnull=False).values(
                "tournament_match__bracket__tournament_id",
                "tournament_match__bracket__tournament__name",
                "cabinet__scene__display_name",
                "cabinet__scene__background_color",
        ).annotate(count=models.Count("id")).order_by("-count")
    ]

    data["tournaments"] = {
        "tournaments": len(tournament_game_counts),
        "games": sum([i["games"] for i in tournament_game_counts]),
        "sign_in_pct": user_games.filter(game__tournament_match__isnull=False).count() / games.filter(tournament_match__isnull=False).values("player_count").aggregate(models.Sum("player_count"))["player_count__sum"],
        "scenes": games.filter(tournament_match__isnull=False).values("cabinet__scene_id").annotate(totals=models.Count("id")).count(),
        "top_tournaments": tournament_game_counts[:5],
        "famines": 0,
    }

    famines_by_map = {}
    data["multi_famine"] = 0
    for game in games:
        if game.get_map().is_bonus:
            continue
        if not game.get_map().total_berries:
            continue
        if game.berries_used is None:
            continue

        famines = math.floor(game.berries_used / game.get_map().total_berries)
        if famines > 0:
            famines_by_map[game.map_name] = famines_by_map.get(game.map_name, 0) + famines

            if game.tournament_match is not None:
                data["tournaments"]["famines"] += famines
            if famines > 1:
                data["multi_famine"] += 1

    data["by_map"] = [
        {
            "id": all_maps[i["map_name"]].id,
            "name": all_maps[i["map_name"]].display_name or i["map_name"],
            "beta_of": all_maps[i["map_name"]].beta_of_id,
            "is_bonus": all_maps[i["map_name"]].is_bonus,
            "games": i["games"],
            "famines": famines_by_map.get(i["map_name"], 0),
        }
        for i in games.values("map_name").annotate(games=models.Count("id")).order_by("-games")
    ]

    query = games.annotate(month=Trunc("start_time", "month")) \
                 .values("month", "cabinet__scene") \
                 .annotate(Count("id")) \
                 .order_by("month", "cabinet__scene")
    data["by_month"] = {
        "scenes": [],
        "data": [],
    }

    for month, group in itertools.groupby(query, lambda row: row["month"]):
        by_scene = {row["cabinet__scene"]: row["id__count"] for row in group}
        data["by_month"]["data"].append({
            "month": month.strftime("%b"),
            **by_scene
        })

    for scene_id, _ in itertools.groupby(query.order_by("cabinet__scene"), lambda row: row["cabinet__scene"]):
        scene = Scene.objects.get(id=scene_id)
        data["by_month"]["scenes"].append({
            "id": scene.id,
            "name": scene.display_name,
            "background_color": scene.background_color,
        })

    data["new_scenes"] = [
        {
            "id": i.id,
            "name": i.display_name,
            "background_color": i.background_color,
            "first_game": i.first_game.isoformat(),
        }
        for i in Scene.objects.annotate(first_game=models.Min("cabinet__game__start_time")).filter(first_game__gte=year_start, first_game__lt=year_end).order_by("first_game")
    ]

    redis_set(redis_key, data)
    redis_set(lock_key, False)


@shared_task
def generate_recap(user_id, year):
    logger.info("Starting recap for user {}".format(user_id))
    start_time = datetime.now()

    user = User.objects.get(id=user_id)
    all_stat_types = stat_types()
    year_start = datetime(year=year, month=1, day=1, tzinfo=timezone.utc)
    year_end = datetime(year=year+1, month=1, day=1, tzinfo=timezone.utc)
    all_user_games = user.usergame_set.filter(
        game__start_time__gte=year_start,
        game__start_time__lt=year_end,
    )

    data = {
        "year": year,
        "stat_totals": {},
    }

    data["played_against"] = {}
    data["played_with"] = {}
    data["best_game_mil"] = {}

    all_games = [(ug.game, ug.player_id) for ug in all_user_games]
    data["stat_totals"] = calculate_aggregates(all_games)
    logger.info("Done calculating aggregates")

    data["by_map"] = {}
    for game_map in GameMap.standard_maps():
        map_games = [(ug.game, ug.player_id) for ug in all_user_games.filter(game__map_name=game_map.name)]
        data["by_map"][game_map.name] = calculate_aggregates(map_games)
        data["by_map"][game_map.name]["map_name"] = game_map.display_name
    logger.info("Done calculating data by map")

    data["by_position"] = {}
    for pos in CabinetPosition:
        position_games = [(ug.game, ug.player_id) for ug in all_user_games.filter(player_id=pos)]
        data["by_position"][pos] = calculate_aggregates(position_games)
    logger.info("Done calculating data by position")

    for user_game in all_user_games:
        game_stats = {}
        for game_stat in user_game.game.gamestat_set.filter(player_id=user_game.player_id):
            game_stats[game_stat.stat_type] = game_stat.value

        for other_user_game in UserGame.objects.filter(game_id=user_game.game_id).exclude(user_id=user.id):
            if CabinetPosition(user_game.player_id).team == CabinetPosition(other_user_game.player_id).team:
                target = data["played_with"]
            else:
                target = data["played_against"]

            if other_user_game.user_id not in target:
                target[other_user_game.user_id] = {
                    "id": other_user_game.user.id,
                    "name": other_user_game.user.name or "(no name set)",
                    "scene": other_user_game.user.scene,
                    "count": 0,
                    "wins": 0,
                    "losses": 0,
                }

            value = target[other_user_game.user_id]
            value["count"] += 1

            if CabinetPosition(user_game.player_id).team == user_game.game.winning_team:
                value["wins"] += 1
            else:
                value["losses"] += 1

        # Kinda arbitrary, but based on these relative totals for 2022:
        # kills = 3,117,000
        # military kills = 1,087,522
        # queen kills = 460,428
        game_score_mil = game_stats.get(StatType.KILLS, 0) \
            + 1.86 * game_stats.get(StatType.MILITARY_KILLS, 0) \
            + 3.91 * game_stats.get(StatType.QUEEN_KILLS, 0) \
            - game_stats.get(StatType.DEATHS, 0) \
            - (5.77 if user_game.player_id in [CabinetPosition.GOLD_QUEEN, CabinetPosition.BLUE_QUEEN] else 1.86) * game_stats.get(StatType.MILITARY_DEATHS, 0)

        if game_score_mil > data["best_game_mil"].get("game_score", 0) \
                and not user_game.game.get_map().is_bonus:
            data["best_game_mil"] = {
                "game_id": user_game.game_id,
                "player_id": user_game.player_id,
                "start_time": user_game.game.start_time.isoformat(),
                "scene_name": user_game.game.cabinet.scene.display_name,
                "cabinet_name": user_game.game.cabinet.display_name,
                "game_score": game_score_mil,
                **game_stats,
            }

    logger.info("Done with best game and BeeFFs")

    first_game = user.usergame_set.order_by("game__start_time").first()
    if first_game and first_game.game.start_time >= year_start and first_game.game.start_time < year_end:
        data["first_game"] = {
            "id": first_game.game_id,
            "start_time": first_game.game.start_time.isoformat(),
            "scene_name": first_game.game.cabinet.scene.display_name,
            "cabinet_name": first_game.game.cabinet.display_name,
            "position": first_game.player_id,
        }

    data["games_by_scene"] = list(
        all_user_games.values(
            scene_id=F("game__cabinet__scene_id"),
            color=F("game__cabinet__scene__background_color"),
            name=F("game__cabinet__scene__display_name"),
        ) \
        .annotate(games=models.Count("id"))
    )
    logger.info("Done with games by scene")

    data["games_by_month"] = {
        "scenes": [],
        "data": [],
    }

    query = all_user_games \
        .annotate(month=TruncMonth("game__start_time")) \
        .values("month", "game__cabinet__scene") \
        .annotate(Count("id")) \
        .order_by("month", "game__cabinet__scene")

    games_by_month = {(month+1): {"month": date(year, month+1, 1).strftime("%b")} for month in range(12)}

    for month, group in itertools.groupby(query, lambda row: row["month"]):
        by_scene = {row["game__cabinet__scene"]: row["id__count"] for row in group}
        games_by_month[month.month].update(by_scene)

    for month in sorted(games_by_month.keys()):
        data["games_by_month"]["data"].append(games_by_month[month])

    for scene_id, _ in itertools.groupby(query.order_by("game__cabinet__scene"),
                                         lambda row: row["game__cabinet__scene"]):
        scene = Scene.objects.get(id=scene_id)
        data["games_by_month"]["scenes"].append({
            "id": scene.id,
            "name": scene.display_name,
            "background_color": scene.background_color,
        })

    logger.info("Done with games by month")

    tournament_players = user.tournamentplayer_set.filter(
        tournament__date__gt=year_start,
        tournament__date__lt=year_end,
    ).order_by("tournament__date")

    if tournament_players.count() > 0:
        data["tournaments"] = [
            {
                "id": row.tournament.id,
                "name": row.tournament.name,
                "date": row.tournament.date.strftime("%B %d"),
                "scene_name": row.tournament.scene.name,
                "scene_color": row.tournament.scene.background_color,
                "team_name": row.team.name if row.team else None,
            }
            for row in tournament_players
        ]
    logger.info("Done with tournament data")

    data["famines"] = 0
    for user_game in all_user_games:
        berries_used = user_game.game.gameevent_set \
                                     .filter(event_type__in=["berryDeposit", "berryKickIn", "useMaiden"]) \
                                     .count()

        if berries_used >= user_game.game.get_map().total_berries:
            data["famines"] += 1

    logger.info("Done getting famine data")

    obj, _ = UserRecap.objects.get_or_create(user=user, year=year)
    obj.data = data
    obj.time_to_generate_ms = (datetime.now() - start_time).total_seconds() * 1000
    obj.save()

    return data

@shared_task
def check_sign_in_timeouts():
    sign_ins = SignInLog.objects.filter(
        is_current=True,
        action=SignInLog.Action.SIGN_IN,
        timestamp__lt=datetime.now() - timedelta(minutes=60),
    )

    cabinet_ids = set([i.cabinet_id for i in sign_ins])
    for cabinet_id in cabinet_ids:
        game_count = Game.objects.filter(
            cabinet_id=cabinet_id,
            start_time__gt=datetime.now() - timedelta(minutes=60),
        ).count()

        if game_count > 0:
            continue

        for sign_in in SignInLog.objects.filter(
                cabinet_id=cabinet_id,
                is_current=True,
                action=SignInLog.Action.SIGN_IN,
                timestamp__lt=datetime.now() - timedelta(minutes=60),
        ):
            sign_in.sign_out()


@shared_task
def generate_map_data():
    min_date = datetime.now() - timedelta(days=90)

    for game_map in GameMap.objects.all():
        games = Game.objects.filter(
            start_time__gt=min_date,
            end_time__isnull=False,
            map_name=game_map.name,
            player_count__gte=8,
        )

        mapdata = {
            "games_played": games.count(),
        }


        redis_set("mapdata.{}".format(game_map.name), mapdata)

@shared_task
def recalculate_gamestat_batch(stat_type_name, start_id, end_id):
    stat_type = get_stat_type_class(stat_type_name)
    games = Game.objects.filter(end_time__isnull=False, id__gte=start_id, id__lte=end_id)
    for game in games:
        for position_id, value in stat_type.calculate(game).items():
            if value > 0:
                GameStat.objects.create(
                    game=game,
                    player_id=position_id,
                    stat_type=stat_type_name,
                    value=value,
                )

@shared_task
def recalculate_single_stat(stat_type_name, batch_size=1000):
    stat_type = get_stat_type_class(stat_type_name)

    if not stat_type.is_aggregate:
        GameStat.objects.filter(stat_type=stat_type_name).delete()
        games = Game.objects.filter(end_time__isnull=False).order_by("id")
        batches = range(0, games.count(), batch_size)
        signatures = []
        for start_idx in batches:
            batch = games[start_idx:start_idx+batch_size]
            signatures.append(recalculate_gamestat_batch.si(stat_type_name, batch[0].id, batch[len(batch)-1].id))

        batch_chain = chain(signatures)
        batch_chain()

    for subclass in AggregatedStatModel.__subclasses__():
        subclass.objects.filter(stat_type=stat_type_name).delete()

        if subclass.Meta and subclass.Meta.abstract:
            continue

        distinct_values = subclass.objects.values(*subclass.aggregate_on()).distinct()
        for row in distinct_values:
            games = subclass.get_games_in_aggregate(**row)
            value = stat_type.calculate_aggregate(games)
            subclass.objects.create(stat_type=stat_type_name, value=value, **row)
