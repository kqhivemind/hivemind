from rest_framework.permissions import SAFE_METHODS, BasePermission


class NotificationConfigPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        return request.user == obj.user
