from django.urls import include, path
from rest_framework import routers, viewsets

from . import views

router = routers.DefaultRouter()
router.register(r"notification-config", views.NotificationConfigViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
