import json
import logging
import math
import random
import re
import secrets
import string
import uuid
from datetime import datetime, timedelta
from decimal import Decimal

import pytz
from django.contrib.postgres.fields import ArrayField
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models, transaction
from django.db.models import Q
from django.utils import timezone
from drf_extra_fields.fields import Base64ImageField

from ..client import redis_client, redis_get, redis_set
from ..constants import (SNAIL_PX_PER_SEC, SNAIL_PX_PER_SEC_SPEED,
                         CabinetPosition, CabinetTeam, HeatmapType,
                         HiddenStatType, StatType)
from ..model import BaseModel
from ..stats.stattype import calculate_game_stats
from ..worker import app
from .awards import Award

PLAYERS_PER_MATCH = 10

logger = logging.getLogger(__name__)

def timedelta_format(td):
    return "{}:{:02}".format(math.floor(td.total_seconds() / 60), td.seconds % 60)

def timedelta_parse(text):
    return timedelta(seconds=sum([60**int(x) * int(y) for x, y in enumerate(text.split(":")[::-1])]))

def generate_device_id():
    while True:
        token = secrets.token_hex(2)
        if ClientDevice.objects.filter(id=token).count() == 0:
            return token


class GameMap(BaseModel):
    DAY = "map_day"
    NIGHT = "map_night"
    DUSK = "map_dusk"
    TWILIGHT = "map_twilight"
    BONUS_MIL = "map_bonus1"
    BONUS_SNAIL = "map_bonus3snail"
    BONUS_ECON = "map_bonusEcon"

    name = models.CharField(max_length=50, unique=True)
    display_name = models.CharField(max_length=200)
    is_bonus = models.BooleanField(default=False)
    beta_of = models.ForeignKey("GameMap", null=True, on_delete=models.SET_NULL)
    total_berries = models.IntegerField(default=0)
    snail_track_width = models.IntegerField(default=0)
    snail_track_y_loc = models.IntegerField(null=True)
    image = models.ImageField(null=True, blank=True, upload_to="map-images/")

    @classmethod
    def standard_maps(cls):
        return cls.objects.filter(name__in=[cls.DAY, cls.NIGHT, cls.DUSK, cls.TWILIGHT])

    def __str__(self):
        return self.name


class Heatmap(BaseModel):
    game_map = models.ForeignKey(GameMap, on_delete=models.CASCADE)
    event_type = models.CharField(max_length=20, choices=HeatmapType.choices)
    image = models.ImageField(null=True, blank=True, upload_to="heatmaps/")
    date_generated = models.DateTimeField(default=timezone.now)


class Scene(BaseModel):
    name = models.CharField(max_length=20, unique=True)
    display_name = models.CharField(max_length=200)
    description = models.TextField(default="", blank=True)
    background_color = models.CharField(max_length=20, null=True)
    total_games = models.BigIntegerField(default=0)
    last_game_time = models.DateTimeField(null=True)

    def all_games(self):
        return Game.objects.filter(cabinet__scene_id=self.id).filter(end_time__isnull=False).order_by("-end_time")

    def last_game(self):
        return self.all_games().first()

    def current_event(self):
        for season in self.season_set.all():
            for event in season.event_set.all():
                if event.is_active:
                    return event

    def current_season(self):
        pass


class Cabinet(BaseModel):
    class ClientStatus(models.TextChoices):
        ONLINE = "online", "Online"
        OFFLINE = "offline", "Offline"


    match_max_choices = (
        (0, 'Unlimited Pips'),
        (1, 'Bo1'),
        (2, 'Bo3'),
        (3, 'Bo5'),
        (4, 'Bo7')
    )

    name = models.CharField(max_length=20)
    scene = models.ForeignKey(Scene, null=True, on_delete=models.SET_NULL)
    display_name = models.CharField(max_length=200)
    address = models.TextField(default="", blank=True)
    description = models.TextField(default="", blank=True)
    token = models.CharField(max_length=200, default=secrets.token_urlsafe)
    time_zone = models.CharField(max_length=50, null=True, choices=[(i, i) for i in pytz.all_timezones])
    allow_qr_signin = models.BooleanField(default=True)
    allow_nfc_signin = models.BooleanField(default=False)
    allow_tournament_player = models.BooleanField(default=False)
    client_status = models.TextField(max_length=20, default=ClientStatus.OFFLINE, choices=ClientStatus.choices)
    last_connection = models.DateTimeField(null=True)
    last_connection_ip = models.GenericIPAddressField(null=True)
    last_game_time = models.DateTimeField(null=True)
    twitch_channel_name = models.TextField(null=True, blank=True)

    class Meta:
        unique_together = ("scene", "name")


    @classmethod
    def by_name(cls, scene_name, cabinet_name):
        try:
            scene = Scene.objects.get(name=scene_name)
        except Scene.DoesNotExist:
            raise cls.DoesNotExist("Scene does not exist.")

        return Cabinet.objects.get(scene=scene, name=cabinet_name)

    def total_games(self):
        games = redis_get("cabinet.{}.total_games".format(self.id))
        if games is None:
            games = self.game_set.filter(end_time__isnull=False).count()
            redis_set("cabinet.{}.total_games".format(self.id), games, expire=60*60)

        return games

    def last_game(self):
        return self.game_set.filter(end_time__isnull=False).order_by("-end_time").first()

    def get_activity(self, day_of_week, hour):
        if not hasattr(self, "_activity"):
            self._activity = {}
            for row in self.cabinetactivity_set.all():
                self._activity[(row.day_of_week, row.hour)] = row.activity

            if len(self._activity) > 0:
                self._max_activity = min(3600, max(self._activity.values()))
            else:
                self._max_activity = 1

        return min(3600, self._activity.get((day_of_week, hour), 0)) / self._max_activity

    def publish_register_nfc_request(self, reader_id, user=None, tournament_player=None):
        data = {
            "type": "nfc_register",
            "scene_name": self.scene.name,
            "cabinet_name": self.name,
            "reader_id": reader_id,
        }

        if user:
            data["user"] = user.id
        if tournament_player:
            data["tournament_player"] = tournament_player.id

        redis_client.publish("signin", json.dumps(data))

    @property
    def max_marker_count(self):
        # For some reason possibly doesn't seralize to int?
        return self.overlay_match_win_max if int(self.overlay_match_win_max) else 4


class CabinetLegacyName(BaseModel):
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, unique=True)


class CabinetActivity(BaseModel):
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    day_of_week = models.IntegerField()
    hour = models.IntegerField()
    activity = models.IntegerField()

    class Meta:
        unique_together = ("cabinet", "day_of_week", "hour")


class Game(BaseModel):
    WIN_CONDITIONS = (
        ("military", "Military"),
        ("economic", "Economic"),
        ("snail", "Snail"),
    )

    class GameStatus(models.TextChoices):
        INCOMPLETE = "incomplete", "Game not completed"
        READY = "ready", "Game complete, ready for processing"
        PROCESSING = "processing", "Game complete, stats processing"
        COMPLETE = "complete", "Game complete"
        ERROR = "error", "Error in postgame processing"


    match = models.ForeignKey("league.Match", null=True, on_delete=models.SET_NULL)
    qp_match = models.ForeignKey("league.QPMatch", null=True, on_delete=models.SET_NULL)
    tournament_match = models.ForeignKey("tournament.TournamentMatch", null=True, on_delete=models.SET_NULL)
    whiteboard_match = models.ForeignKey("whiteboard.Match", null=True, on_delete=models.SET_NULL)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.SET_NULL)
    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True, db_index=True)
    status = models.TextField(max_length=20, default=GameStatus.INCOMPLETE, choices=GameStatus.choices)
    win_condition = models.CharField(max_length=10, null=True, choices=WIN_CONDITIONS)
    winning_team = models.CharField(max_length=5, null=True, choices=CabinetTeam.choices)
    map_name = models.CharField(max_length=50)
    player_count = models.IntegerField(null=True)
    berries_used = models.IntegerField(null=True)
    cabinet_version = models.CharField(max_length=50, null=True)
    uuid = models.UUIDField(serialize=False, null=True, unique=True, editable=False)

    @property
    def scene(self):
        return self.cabinet.scene

    def get_events(self, event_filter=None):
        if not hasattr(self, "_events"):
            self._events = list(self.gameevent_set.filter(timestamp__gt=self.start_time).order_by("timestamp"))

        if event_filter:
            return list(filter(event_filter, self._events))

        return self._events

    def get_map(self):
        if not hasattr(self, "_game_map"):
            self._game_map, _ = GameMap.objects.get_or_create(name=self.map_name)

        return self._game_map

    def get_map_name_display(self):
        return self.get_map().display_name or f"Unknown ({self.map_name})"

    def get_total_berry_count(self):
        return self.get_map().total_berries

    def get_start_time_local(self):
        if self.cabinet and self.cabinet.time_zone:
            return self.start_time.astimezone(pytz.timezone(self.cabinet.time_zone))

        return self.start_time.astimezone(pytz.utc)

    def get_end_time_local(self):
        if self.cabinet and self.cabinet.time_zone:
            return self.end_time.astimezone(pytz.timezone(self.cabinet.time_zone))

        return self.end_time.astimezone(pytz.utc)

    def get_winning_team(self):
        if self.match is None:
            return None

        if self.winning_team == CabinetTeam.BLUE:
            return self.match.blue_team
        if self.winning_team == CabinetTeam.GOLD:
            return self.match.gold_team

    def get_winning_team_name(self):
        if self.match:
            return self.get_winning_team().name

        if self.qp_match:
            return self.get_winning_team_display()

        if self.tournament_match:
            if self.winning_team == CabinetTeam.BLUE:
                return self.tournament_match.blue_team
            if self.winning_team == CabinetTeam.GOLD:
                return self.tournament_match.gold_team

    @property
    def gold_on_left(self):
        try:
            return self.gameevent_set.filter(event_type="gamestart").first().values[1] == "True"
        except AttributeError:
            return False

    @property
    def team_on_left(self):
        return CabinetTeam.GOLD if self.gold_on_left else CabinetTeam.BLUE

    def get_event_count_by_player(self, event_type, value_idx, event_filter=None):
        counts = {}
        for event in self.gameevent_set.filter(event_type=event_type, timestamp__gt=self.start_time):
            if event_filter is not None and not event_filter(event):
                continue

            player_id = int(event.values[value_idx])
            counts[player_id] = counts.get(player_id, 0) + 1

        return counts

    def get_most_events(self, event_type, value_idx, event_filter=None):
        counts = self.get_event_count_by_player(event_type, value_idx, event_filter)
        if not counts:
            return {}

        return {
            "count": max(counts.values()),
            "players": list(filter(lambda i: counts[i] == max(counts.values()), counts.keys())),
        }

    def get_team_event_count(self, event_type, value_idx, event_filter=None):
        count = {"blue": 0, "gold": 0}
        for event in self.gameevent_set.filter(event_type=event_type, timestamp__gt=self.start_time):
            if event_filter is not None and not event_filter(event):
                continue

            player_id = int(event.values[value_idx])
            if player_id % 2:
                count["gold"] += 1
            else:
                count["blue"] += 1

        return count

    def game_length(self):
        return self.end_time - self.start_time

    def get_game_length_str(self):
        if self.start_time and self.end_time:
            return timedelta_format(self.end_time - self.start_time)

        return ""

    def get_basic_info(self, user_id=None):
        game_length = self.end_time - self.start_time
        data = {
            "game_id": self.id,
            "start_time": self.start_time,
            "end_time": self.end_time,
            "winning_team": self.winning_team,
            "winning_team_display": self.get_winning_team_display(),
            "win_condition": self.win_condition,
            "win_condition_display": self.get_win_condition_display(),
            "win_description": "{} Victory - {}".format(self.get_winning_team_display(), self.get_win_condition_display()),
            "map": self.get_map_name_display(),
            "length": timedelta_format(game_length),
            "length_sec": game_length.total_seconds(),
            "player_count": self.player_count,
            "scene_name": self.cabinet.scene.name,
            "scene_display_name": self.cabinet.scene.display_name,
            "cabinet_name": self.cabinet.name,
            "cabinet_display_name": self.cabinet.display_name,
        }

        if user_id is not None:
            user_game = self.usergame_set.filter(user_id=user_id).first()
            if user_game is not None:
                data["position"] = user_game.player_id
                data["position_name"] = CabinetPosition(user_game.player_id).label

        return data

    def stats_cache_key(self):
        return "postgame_stats.{}".format(self.id)

    def get_postgame_stats(self, regenerate=False):
        if not self.winning_team:
            return {}

        if not regenerate:
            cached_stats = redis_get(self.stats_cache_key())
            if cached_stats:
                return cached_stats

        data = self.generate_postgame_stats()
        redis_set(self.stats_cache_key(), data, expire=60*60*24)
        return data

    def clear_cached_stats(self):
        redis_client.delete(self.stats_cache_key())

    def collect_player_stats(self, regenerate=False):
        stats = self.get_postgame_stats(regenerate=regenerate)
        user_ids = {i.player_id: i.user_id for i in self.usergame_set.all()}
        tournament_player_ids = {i.player_id: i.tournament_player_id for i in self.tournamentplayergame_set.all()}

        if self.tournament_match:
            for user_game in self.usergame_set.all():
                if user_game.player_id in tournament_player_ids:
                    continue

                player = user_game.user.tournamentplayer_set.filter(
                    tournament_id=self.tournament_match.bracket.tournament_id,
                ).first()

                if player:
                    tournament_player_ids[user_game.player_id] = player.id

        if "by_player" not in stats:
            return

        with transaction.atomic():
            for stat_type, _ in StatType.choices + HiddenStatType.choices:
                for player_id, value in stats["by_player"].get(stat_type, {}).items():
                    game_stat, _ = GameStat.objects.get_or_create(game=self, player_id=player_id, stat_type=stat_type)
                    game_stat.user_id = user_ids.get(player_id)
                    game_stat.tournament_player_id = tournament_player_ids.get(int(player_id))
                    game_stat.value = value
                    game_stat.save()

        for player_id in tournament_player_ids.values():
            app.send_task("hivemind.tournament.tasks.collect_player_stats", args=[player_id])

    def snail_chart(self):
        """ Generates data used by the snail charts. """

        if self.map_name == GameMap.BONUS_SNAIL:
            all_snails = [[{"x": 0, "y": 0}], [{"x": 0, "y": 0}], [{"x": 0, "y": 0}]]
            players_on_snails = [None, None, None]
            get_on_snail_locs = [None, None, None]
            get_on_snail_times = [None, None, None]
        else:
            snail_data = [{"x": 0, "y": 0}]

        snail_distance_by_player = {}
        get_on_snail_loc = None
        last_snail_loc = None
        last_snail_time = None
        player_on_snail = None
        initial_position = 960
        initial_position_on_bonus = [960, 520, 1400]
        has_speed = set()

        snail_track_width = self.get_map().snail_track_width or 900
        get_on_snail_loc = 960

        snail_events = self.gameevent_set.filter(
            timestamp__gt=self.start_time,
            event_type__in=["getOnSnail", "getOffSnail", "snailEat", "useMaiden", "playerKill"],
        ).order_by("timestamp")

        for snail_event in snail_events:
            if snail_event.event_type == "useMaiden":
                if snail_event.values[2] == "maiden_speed":
                    has_speed.add(int(snail_event.values[3]))

                continue

            if snail_event.event_type == "playerKill":
                has_speed.discard(int(snail_event.values[3]))
                continue

            event_text = ""
            if snail_event.event_type == "getOnSnail":
                event_text = "{} got on snail at {}".format(
                    CabinetPosition(int(snail_event.values[2])).label,
                    timedelta_format(snail_event.timestamp - self.start_time),
                )

            if snail_event.event_type == "getOffSnail":
                event_text = "{} got off snail at {}".format(
                    CabinetPosition(int(snail_event.values[3])).label,
                    timedelta_format(snail_event.timestamp - self.start_time),
                )

            if snail_event.event_type == "snailEat":
                event_text = "Snail ate {} at {}".format(
                    CabinetPosition(int(snail_event.values[3])).label,
                    timedelta_format(snail_event.timestamp - self.start_time),
                )

            if self.map_name == GameMap.BONUS_SNAIL:
                # Top snail is at x = 731
                # Left and right snails are at x = 371, on the left and right halves of the screen
                if int(snail_event.values[1]) == 731:
                    snail_num = 0
                elif int(snail_event.values[0]) < 960:
                    snail_num = 1
                else:
                    snail_num = 2

                snail_data = all_snails[snail_num]
                initial_position = initial_position_on_bonus[snail_num]

                if snail_event.event_type == "getOnSnail":
                    get_on_snail_locs[snail_num] = initial_position - int(snail_event.values[0])
                    get_on_snail_times[snail_num] = snail_event.timestamp
                    players_on_snails[snail_num] = int(snail_event.values[2])

                if snail_event.event_type == "getOffSnail":
                    players_on_snails[snail_num] = None

                if snail_event.event_type == "snailEat":
                    get_on_snail_locs[snail_num] = initial_position - int(snail_event.values[0])
                    get_on_snail_times[snail_num] = snail_event.timestamp

            snail_data.append({
                "x": (snail_event.timestamp - self.start_time).total_seconds(),
                "y": initial_position - int(snail_event.values[0]),
                "text": event_text,
            })

            last_snail_time = snail_event.timestamp

            if snail_event.event_type == "getOnSnail":
                get_on_snail_loc = last_snail_loc = int(snail_event.values[0])
                last_snail_time = snail_event.timestamp
                player_on_snail = int(snail_event.values[2])

            if snail_event.event_type == "getOffSnail":
                player_id = int(snail_event.values[3])
                snail_distance_by_player[player_id] = snail_distance_by_player.get(player_id, 0) + abs(int(snail_event.values[0]) - get_on_snail_loc)
                last_snail_loc = int(snail_event.values[0])
                player_on_snail = None

            if snail_event.event_type == "snailEat":
                player_id = int(snail_event.values[2])
                if get_on_snail_loc:
                    snail_distance_by_player[player_id] = snail_distance_by_player.get(player_id, 0) + abs(int(snail_event.values[0]) - get_on_snail_loc)

                get_on_snail_loc = last_snail_loc = int(snail_event.values[0])

        if self.map_name == GameMap.BONUS_SNAIL:
            final_position = [snail_data[-1]["y"] for snail_data in all_snails]
            winning_snail = None

            # It doesn't tell us which snail won, so take a guess
            if self.win_condition == "snail":
                mod_value = 1 if self.winning_team == "gold" else 0
                snail_direction = 1 if self.winning_team == self.team_on_left else -1
                min_speed = None

                # Find snails that had players from the winning team riding them
                for snail_num, player_id in enumerate(players_on_snails):
                    if player_id and player_id % 2 == mod_value:
                        # Figure out how fast the snail would have to be going to win the game
                        # Assume the one with the lowest required speed is the one that won
                        if get_on_snail_times[snail_num] < self.end_time:
                            snail_speed = (snail_track_width - get_on_snail_locs[snail_num] * snail_direction) / \
                                (self.end_time - get_on_snail_times[snail_num]).total_seconds()

                            if min_speed is None or snail_speed < min_speed:
                                winning_snail = snail_num
                                min_speed = snail_speed

                if winning_snail is not None:
                    final_position[winning_snail] = (snail_track_width if self.winning_team == self.team_on_left else -snail_track_width)
                    snail_distance_by_player[players_on_snails[winning_snail]] = snail_distance_by_player.get(players_on_snails[winning_snail], 0) + \
                        abs(final_position[winning_snail] - get_on_snail_locs[winning_snail])

            for snail_num, snail_data in enumerate(all_snails):
                if snail_num == winning_snail:
                    end_loc = (snail_track_width if self.winning_team == self.team_on_left else -snail_track_width)
                    snail_data.append({
                        "x": (self.end_time - self.start_time).total_seconds(),
                        "y": end_loc,
                        "text": "{} rode snail to goal at {}".format(
                            CabinetPosition(int(players_on_snails[snail_num])).label,
                            timedelta_format(self.end_time - self.start_time),
                        ),
                    })
                else:
                    snail_data.append({
                        "x": (self.end_time - self.start_time).total_seconds(),
                        "y": final_position[snail_num],
                        "text": "End of game",
                    })

        else:
            if player_on_snail:
                if self.win_condition == "snail":
                    snail_direction = -1 if self.winning_team == self.team_on_left else 1
                    end_loc = initial_position + snail_direction * snail_track_width
                    event_text = "{} rode snail to goal at {}".format(
                            CabinetPosition(int(player_on_snail)).label,
                            timedelta_format(snail_event.timestamp - self.start_time),
                        ),

                else:
                    pixels_per_sec = SNAIL_PX_PER_SEC_SPEED if int(player_on_snail) in has_speed else SNAIL_PX_PER_SEC
                    if CabinetPosition(int(player_on_snail)).team != self.team_on_left:
                        pixels_per_sec *= -1

                    end_loc = last_snail_loc + int((self.end_time - last_snail_time).total_seconds() * pixels_per_sec)
                    event_text = "Estimated position at end of game"

                snail_data.append({
                    "x": (self.end_time - self.start_time).total_seconds(),
                    "y": initial_position - end_loc,
                    "text": event_text,
                })
                snail_distance_by_player[player_on_snail] = snail_distance_by_player.get(player_on_snail, 0) + abs(960 - end_loc - get_on_snail_loc)

            else:
                snail_data.append({
                    "x": (self.end_time - self.start_time).total_seconds(),
                    "y": snail_data[-1]["y"],
                    "text": "End of game",
                })

        if self.map_name == GameMap.BONUS_SNAIL:
            return all_snails
        else:
            return [snail_data]


    def generate_postgame_stats(self):
        data = self.get_basic_info()

        data["human_players"] = sorted(self.count_players())
        data["player_count"] = len(data["human_players"])
        data["kills"] = self.get_team_event_count("playerKill", 2)
        data["military_kills"] = self.get_team_event_count("playerKill", 2, lambda event: event.values[4] != "Worker")
        data["berries"] = self.get_team_event_count("berryDeposit", 2)
        data["berries_used"] = self.gameevent_set.filter(event_type__in=["berryDeposit", "berryKickIn", "useMaiden"]).count()

        for event in self.gameevent_set.filter(event_type="berryKickIn"):
            if len(event.values) < 4:
                continue

            own_team = (event.values[3] == "True")
            player_id = int(event.values[2])
            if (player_id % 2) ^ own_team:
                data["berries"]["blue"] += 1
            else:
                data["berries"]["gold"] += 1

        data["most_kills"] = self.get_most_events("playerKill", 2)
        data["most_military_kills"] = self.get_most_events("playerKill", 2, lambda event: event.values[4] != "Worker")
        data["most_deaths"] = self.get_most_events("playerKill", 3)
        data["most_berries"] = self.get_most_events("berryDeposit", 2)

        data["game_map"] = {k: getattr(self.get_map(), k)
                            for k in ["name", "display_name", "is_bonus", "total_berries", "snail_track_width"]}

        data["team_on_left"] = str(self.team_on_left)

        warrior_uptime = {}
        warrior_up_events = []
        for up_event in self.gameevent_set.filter(event_type="useMaiden", values__2="maiden_wings", timestamp__gt=self.start_time).all():
            warrior_up_events.append({"player_id": int(up_event.values[3]), "up_timestamp": up_event.timestamp})
        if self.map_name == GameMap.BONUS_MIL:
            spawned = set((1, 2))
            for up_event in self.gameevent_set.filter(event_type="spawn").all():
                if int(up_event.values[0]) not in spawned:
                    warrior_up_events.append({"player_id": int(up_event.values[0]), "up_timestamp": max(up_event.timestamp, self.start_time)})
                    spawned.add(int(up_event.values[0]))

        for up_event in warrior_up_events:
            up_timestamp = up_event["up_timestamp"]
            player_id = up_event["player_id"]

            down_event = self.gameevent_set.filter(event_type="playerKill", values__3=player_id, timestamp__gt=up_timestamp) \
                                           .order_by("timestamp").first()

            if down_event is None:
                down_time = self.end_time
            else:
                down_time = down_event.timestamp

            warrior_uptime[player_id] = warrior_uptime.get(player_id, timedelta(0)) + (down_time - up_timestamp)

        if warrior_uptime:
            data["most_warrior_uptime"] = {
                "count": timedelta_format(max(warrior_uptime.values())),
                "players": list(filter(lambda i: warrior_uptime[i] == max(warrior_uptime.values()), warrior_uptime.keys())),
            }

        warrior_gates = set()
        for event in self.gameevent_set.filter(event_type="useMaiden", values__2="maiden_wings", timestamp__gt=self.start_time):
            warrior_gates.add(tuple(event.values[0:2]))

        gate_control = {"blue": timedelta(0), "gold": timedelta(0)}
        for gate in warrior_gates:
            last_time = self.start_time
            controlled_by = None
            gate_events = self.gameevent_set.filter(
                event_type="blessMaiden",
                timestamp__gt=self.start_time,
                values__0=gate[0],
                values__1=gate[1],
            ).order_by("timestamp")

            for event in gate_events:
                if controlled_by:
                    gate_control[controlled_by] += event.timestamp - last_time

                controlled_by = "blue" if event.values[2] == "Blue" else "gold"
                last_time = event.timestamp

            if controlled_by:
                gate_control[controlled_by] += self.end_time - last_time

        if len(warrior_gates) > 0:
            data["gate_control_sec"] = {k: v.total_seconds() for k, v in gate_control.items()}
            data["gate_control"] = {k: "{:.01f}%".format(100 * v / (self.end_time - self.start_time) / len(warrior_gates)) for k, v in gate_control.items()}

        speed_gates = set()
        for event in self.gameevent_set.filter(event_type="useMaiden", values__2="maiden_speed", timestamp__gt=self.start_time - timedelta(seconds=10)):
            speed_gates.add(tuple(event.values[0:2]))

        speed_gate_control = {"blue": timedelta(0), "gold": timedelta(0)}
        for gate in speed_gates:
            last_time = self.start_time
            controlled_by = None
            gate_events = self.gameevent_set.filter(
                event_type="blessMaiden",
                timestamp__gt=self.start_time - timedelta(seconds=10),
                values__0=gate[0],
                values__1=gate[1],
            ).order_by("timestamp")

            for event in gate_events:
                if controlled_by and event.timestamp > self.start_time:
                    speed_gate_control[controlled_by] += event.timestamp - last_time

                controlled_by = "blue" if event.values[2] == "Blue" else "gold"
                last_time = max(event.timestamp, self.start_time)

            if controlled_by:
                speed_gate_control[controlled_by] += self.end_time - last_time

        if len(speed_gates) > 0:
            data["speed_gate_control_sec"] = {k: v.total_seconds() for k, v in speed_gate_control.items()}
            data["speed_gate_control"] = {k: "{:.01f}%".format(100 * v / (self.end_time - self.start_time) / len(speed_gates)) for k, v in speed_gate_control.items()}

        data["snail_data"] = self.snail_chart()

        berry_data = {"blue": [{"x": 0, "y": 0}], "gold": [{"x": 0, "y": 0}]}
        for berry_event in self.gameevent_set.filter(timestamp__gt=self.start_time, event_type__in=["berryDeposit", "berryKickIn"]).order_by("timestamp"):
            if berry_event.event_type == "berryKickIn" and len(berry_event.values) >= 4:
                own_team = (berry_event.values[3] == "True")
            else:
                own_team = True

            if (int(berry_event.values[2]) % 2) ^ own_team:
                berry_chart = berry_data["blue"]
            else:
                berry_chart = berry_data["gold"]

            event_text = "Berry {} by {} at {}".format(
                "deposited" if berry_event.event_type == "berryDeposit" else "kicked in",
                CabinetPosition(int(berry_event.values[2])).label,
                timedelta_format(berry_event.timestamp - self.start_time),
            )

            previous_value = berry_chart[-1]["y"]
            berry_chart.append({"x": (berry_event.timestamp - self.start_time).total_seconds(), "y": previous_value + 1, "text": event_text})

        berry_data["blue"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": berry_data["blue"][-1]["y"], "text": "End of game"})
        berry_data["gold"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": berry_data["gold"][-1]["y"], "text": "End of game"})
        data["berry_data"] = berry_data

        warrior_data = {"blue": [{"x": 0, "y": 0}], "gold": [{"x": 0, "y": 0}]}
        spawned = set((1, 2))

        warrior_events = self.gameevent_set.filter(
            timestamp__gt=self.start_time-timedelta(seconds=12),
            event_type__in=["useMaiden", "playerKill", "spawn"],
        ).order_by("timestamp")

        for warrior_event in warrior_events:
            if warrior_event.event_type == "useMaiden" and warrior_event.values[2] != "maiden_wings":
                continue
            if warrior_event.event_type == "playerKill" and warrior_event.values[4] != "Soldier":
                continue

            if warrior_event.event_type == "spawn":
                player_id = int(warrior_event.values[0])
                if self.map_name != GameMap.BONUS_MIL or int(warrior_event.values[0]) in spawned:
                    continue

                spawned.add(player_id)
            else:
                player_id = int(warrior_event.values[3])

            if player_id % 2:
                warrior_chart = warrior_data["gold"]
            else:
                warrior_chart = warrior_data["blue"]

            previous_value = warrior_chart[-1]["y"]
            if warrior_event.event_type in ("useMaiden", "spawn"):
                new_value = previous_value + 1
                event_text = "{} up at {}".format(CabinetPosition(player_id).label, timedelta_format(warrior_event.timestamp - self.start_time))
            else:
                new_value = previous_value - 1
                event_text = "{} killed by {} at {}".format(
                    CabinetPosition(player_id).label,
                    CabinetPosition(int(warrior_event.values[2])).label,
                    timedelta_format(warrior_event.timestamp - self.start_time),
                )

            warrior_chart.append({"x": max((warrior_event.timestamp - self.start_time).total_seconds(), 0), "y": new_value, "text": event_text})

        warrior_data["blue"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": warrior_data["blue"][-1]["y"], "text": "End of game"})
        warrior_data["gold"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": warrior_data["gold"][-1]["y"], "text": "End of game"})
        data["warrior_data"] = warrior_data

        queen_death_data = {"blue": [], "gold": []}
        for death_event in self.gameevent_set.filter(timestamp__gt=self.start_time, event_type="playerKill", values__4="Queen"):
            data_list = queen_death_data["blue"] if death_event.values[3] == "2" else queen_death_data["gold"]
            data_list.append({
                "x": (death_event.timestamp - self.start_time).total_seconds(),
                "y": 1 if death_event.values[3] == "2" else -1,
                "text": "{} killed by {} at {}".format(
                    CabinetPosition(int(death_event.values[3])).label,
                    CabinetPosition(int(death_event.values[2])).label,
                    timedelta_format(death_event.timestamp - self.start_time),
                ),
            })

        data["queen_death_data"] = queen_death_data

        data["berries_remaining_data"] = []
        total_berries = self.get_total_berry_count()
        if total_berries:
            data["berries_remaining_data"].append({"x": 0, "y": total_berries})
            for event in self.gameevent_set.filter(event_type__in=["berryDeposit", "berryKickIn", "useMaiden"]).order_by("timestamp"):
                if event.event_type in ["berryDeposit", "berryKickIn"]:
                    event_text = "Berry {} by {} at {}".format(
                        "deposited" if event.event_type == "berryDeposit" else "kicked in",
                        CabinetPosition(int(event.values[2])).label,
                        timedelta_format(event.timestamp - self.start_time),
                    )
                if event.event_type == "useMaiden":
                    event_text = "{} gate used by {} at {}".format(
                        "Warrior" if event.values[2] == "maiden_wings" else "Speed",
                        CabinetPosition(int(event.values[3])).label,
                        timedelta_format(event.timestamp - self.start_time),
                    )

                data["berries_remaining_data"].append({
                    "x": (event.timestamp - self.start_time).total_seconds(),
                    "y": data["berries_remaining_data"][-1]["y"] - 1,
                    "text": event_text,
                })

                if data["berries_remaining_data"][-1]["y"] == 0 and event.timestamp < self.end_time - timedelta(seconds=90):
                    data["berries_remaining_data"].append({
                        "x": data["berries_remaining_data"][-1]["x"] + 90,
                        "y": total_berries,
                        "text": "Berries respawn at {}".format(timedelta_format(event.timestamp + timedelta(seconds=90) - self.start_time)),
                    })

            data["berries_remaining_data"].append({
                "x": (self.end_time - self.start_time).total_seconds(),
                "y": data["berries_remaining_data"][-1]["y"],
                "text": "End of game",
            })

        # Full stats by player
        data["by_player"] = calculate_game_stats(self)

        # Coordinates for end of the game
        data["game_end_coordinates"] = {}

        if self.win_condition == "military":
            end_event = self.gameevent_set.filter(
                timestamp__gte=self.start_time,
                event_type="playerKill",
                values__4="Queen",
            ).order_by("-timestamp").first()

            if end_event:
                data["game_end_coordinates"].update({"x": int(end_event.values[0]), "y": int(end_event.values[1])})

        if self.win_condition == "economic":
            end_event = self.berry_events_by_team(self.winning_team).order_by("-timestamp").first()
            if end_event:
                data["game_end_coordinates"].update({"x": int(end_event.values[0]), "y": int(end_event.values[1])})

        if self.win_condition == "snail":
            # Get first snail event, then add the track width in the proper direction
            first_snail_event = self.gameevent_set \
                                    .filter(event_type="getOnSnail", timestamp__gte=self.start_time) \
                                    .order_by("timestamp") \
                                    .first()

            if first_snail_event:
                snail_track_width = self.get_map().snail_track_width or 900
                data["game_end_coordinates"].update({
                    "x": int(first_snail_event.values[0]) + snail_track_width * (-1 if self.winning_team == "gold" else 1),
                    "y": int(first_snail_event.values[1]),
                })

        # KQuity win probability values
        data["win_probability"] = [{
            "x": 0,
            "y": 0,
            "text": "Start of game",
        }]

        for event in self.gameevent_set.filter(timestamp__gte=self.start_time, timestamp__lt=self.end_time, win_probability__isnull=False).order_by("timestamp"):
            data["win_probability"].append({
                "x": (event.timestamp - self.start_time).total_seconds(),
                "y": int((event.win_probability - Decimal(0.5)) * 1000),
                "text": "{} ({:4.2f}%)".format(
                    "Blue" if event.win_probability > 0.5 else "Gold",
                    (abs(event.win_probability - Decimal(0.5)) + Decimal(0.5)) * 100,
                ),
            })

        data["win_probability"].append({
            "x": (self.end_time - self.start_time).total_seconds(),
            "y": 500 if self.winning_team == "blue" else -500,
            "text": data["win_description"],
        })

        # Players signed in
        data["signed_in"] = {}
        for usergame in self.usergame_set.all():
            data["signed_in"][usergame.player_id] = {
                "id": usergame.user.id,
                "name": usergame.user.name,
                "is_profile_public": usergame.user.is_profile_public,
            }

        # Get awards
        data["awards"] = [
            {
                "title": s.award.get_title_for_game(self, data),
                "description": s.award.description,
                "players": [p.value for p in s.players] if s.players else None,
                "teams": s.teams if s.teams else None,
            } for s in Award.pick_some(self, data, 3)
        ]

        self.berries_used = data["berries_used"]
        self.save()

        return data

    @classmethod
    def get_video_timestamps(cls, first_game_id, first_game_start_time, video_length):
        first_game = Game.objects.get(id=first_game_id)
        cabinet = first_game.cabinet
        video_start_time = first_game.start_time - timedelta_parse(first_game_start_time)
        video_end_time = video_start_time + timedelta_parse(video_length)

        return [
            (game, timedelta_format(game.start_time - video_start_time))
            for game
            in cabinet.game_set.filter(start_time__gt=video_start_time, start_time__lt=video_end_time)
            .order_by("start_time")
        ]

    @classmethod
    def process_gamestate_log(cls, filename):
        with open(filename, "r") as fh:
            contents = fh.read()
            contents = re.sub(r"(\w+)\:", r'"\1":', contents)
            contents = re.sub(r"\:\s*\'(.*?)\'", r': "\1"', contents)
            contents = re.sub(r"}", r"},", contents)
            contents = "[" + contents.strip(",\n ") + "]"

            entries = json.loads(contents)

            team_names = {}
            game_ids = set()
            games = []

            for entry in entries:
                if entry.get("type") == "overlaysettings":
                    team_names[entry["cabinet_name"]] = {"blue": entry["blue_team"], "gold": entry["gold_team"]}

                if entry.get("type") == "gameend":
                    if int(entry["game_id"]) in game_ids:
                        continue

                    game_ids.add(int(entry["game_id"]))
                    game = cls.objects.get(id=int(entry["game_id"]))

                    games.append({
                        "game": game,
                        "blue": team_names.get(game.cabinet.name, {}).get("blue", "(unknown)"),
                        "gold": team_names.get(game.cabinet.name, {}).get("gold", "(unknown)"),
                    })

            for game in sorted(games, key=lambda i: (i["game"].cabinet_id, i["game"].id)):
                yield "{:10} {:%d %b %H:%M:%S}  {:15} vs. {:15} ({:8} {:4} {:>9})  https://kqhivemind.com/game/{}".format(
                    game["game"].cabinet.display_name,
                    game["game"].get_start_time_local(),
                    game["blue"],
                    game["gold"],
                    game["game"].get_map_name_display(),
                    game["game"].winning_team,
                    game["game"].win_condition,
                    game["game"].id,
                )


    def detect_bonus_map(self):
        # Game currently sends the wrong map name for bonus maps, need to detect the correct map
        events = self.gameevent_set.order_by("timestamp").all()

        # Economic bonus map: 7 queen kills
        if self.win_condition == "military":
            queen_kills = max(
                self.gameevent_set.filter(event_type="playerKill", values__3="1").count(),
                self.gameevent_set.filter(event_type="playerKill", values__3="2").count(),
            )

            if queen_kills == 7:
                return GameMap.BONUS_ECON

        # Economic bonus map: one berry kick-in, no deposits, and economic victory
        if self.win_condition == "economic" and \
           self.gameevent_set.filter(event_type="berryKickIn").count() == 1 and \
           self.gameevent_set.filter(event_type="berryDeposit").count() == 0:
            return GameMap.BONUS_ECON

        # Military bonus map: a kill (by or on a warrior) happens before any useMaiden or carryFood events
        for event in events:
            if event.event_type == "playerKill":
                if event.values[2] not in ["1", "2"] or event.values[4] == "Soldier":
                    return GameMap.BONUS_MIL

            if event.event_type in ("useMaiden", "carryFood"):
                break

        # Also military bonus map: interacting with a gate at (400, 20), (1520, 20), or (960, 580)
        for event in events:
            if event.event_type in ("blessMaiden", "reserveMaiden", "unreserveMaiden", "useMaiden"):
                if tuple(event.values[:2]) in (("400", "20"), ("1520", "20"), ("960", "580")):
                    return GameMap.BONUS_MIL

        # Snail bonus map: snail events at y=371 or y=731
        for event in events:
            if event.event_type in ("getOnSnail", "getOffSnail"):
                if event.values[1] in ("371", "731"):
                    return GameMap.BONUS_SNAIL

                break

    def count_players(self):
        players = set()
        spawn_events = self.gameevent_set.filter(event_type="spawn", values__1="False")

        # Make sure they've actually done something after spawning (don't count just being tapped in)
        action_types = [
            {"event_type": "berryDeposit", "player_index": 2},
            {"event_type": "getOnSnail", "player_index": 2},
            {"event_type": "playerKill", "player_index": 2},
            {"event_type": "useMaiden", "player_index": 3},
        ]

        for event in spawn_events:
            # On a bonus map, anyone who tapped in counts
            if self.get_map().is_bonus:
                players.add(event.values[0])
                continue

            # Queens always count
            if event.values[0] in ["1", "2"]:
                players.add(event.values[0])
                continue

            for action in action_types:
                filter_args = {
                    "timestamp__gt": event.timestamp,
                    "event_type": action["event_type"],
                    "values__{}".format(action["player_index"]): event.values[0],
                }

                if self.gameevent_set.filter(**filter_args).count() > 0:
                    players.add(event.values[0])
                    break

        self.player_count = len(players)

        return players

    def berry_events_by_team(self, team):
        return self.gameevent_set.filter(timestamp__gte=self.start_time).filter(
            Q(event_type="berryDeposit", values__2__in=CabinetTeam(team).positions) |
            Q(event_type="berryKickIn", values__2__in=CabinetTeam(team).positions, values__3="True") |
            Q(event_type="berryKickIn", values__2__in=CabinetTeam(team).opponent.positions, values__3="False")
        )

    def print_events(self, **filters):
        events = self.gameevent_set
        if filters:
            events = events.filter(**filters)

        for event in events.order_by("timestamp"):
            print(event)


class GameEvent(BaseModel):
    uuid = models.UUIDField(default=uuid.uuid4, serialize=False, unique=True, editable=False)
    game = models.ForeignKey(Game, on_delete=models.SET_NULL, null=True)
    game_uuid = models.UUIDField(null=True, db_index=True)
    timestamp = models.DateTimeField()
    event_type = models.CharField(max_length=50)
    values = ArrayField(models.CharField(max_length=50))
    win_probability = models.DecimalField(null=True, max_digits=5, decimal_places=4)

    def __str__(self):
        return "{}  {:%H:%M:%S.%f}  {:20}  {}".format(self.uuid, self.timestamp, self.event_type, ", ".join(self.values))


class GameStat(BaseModel):
    StatType = StatType
    HiddenStatType = HiddenStatType

    stat_type_choices = StatType.choices + HiddenStatType.choices

    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)
    user = models.ForeignKey("user.User", null=True, on_delete=models.SET_NULL)
    tournament_player = models.ForeignKey("tournament.TournamentPlayer", null=True, on_delete=models.SET_NULL)
    stat_type = models.TextField(max_length=20, choices=stat_type_choices)
    value = models.IntegerField(default=0)


class ClientDevice(BaseModel):
    id = models.CharField(max_length=10, primary_key=True, default=generate_device_id)
    name = models.CharField(max_length=30, null=True, blank=True)
    owner = models.ForeignKey(Scene, null=True, on_delete=models.SET_NULL)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.SET_NULL)
    token = models.CharField(max_length=200, default=secrets.token_urlsafe)

    def publish(self):
        data = {
            "device_id": self.id,
            "scene_name": self.cabinet.scene.name,
            "cabinet_name": self.cabinet.name,
            "cabinet_id": self.id,
        }

        redis_client.publish("client_devices", json.dumps(data))
