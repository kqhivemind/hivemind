import pytz
import qrcode
import qrcode.image.svg
from django.db import transaction
from django.db.models import Count, Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from ..constants import HeatmapType, HTTPMethod, PermissionType
from ..model import BaseViewSet
from ..overlay.models import Overlay
from ..stats.models import SignInLog
from ..tournament.models import MatchQueue, Tournament, TournamentMatch
from ..tournament.serializers import TournamentMatchSerializer
from ..user.serializers import ScenePermissionSerializer
from .filters import CabinetActivityFilter, GameFilter
from .models import (Cabinet, CabinetActivity, ClientDevice, Game, GameEvent,
                     GameMap, Heatmap, Scene)
from .permissions import (CabinetPermission, ClientDevicePermission,
                          GamePermission, ScenePermission)
from .serializers import (CabinetAdminSerializer, CabinetSerializer,
                          ClientDeviceAdminSerializer, ClientDeviceSerializer,
                          GameSerializer, SceneSerializer)


class SceneViewSet(BaseViewSet):
    queryset = Scene.objects.all().order_by("display_name")
    serializer_class = SceneSerializer
    permission_classes = [ScenePermission]
    filterset_fields = ["name"]

    @action(detail=True, url_path="payment-accounts")
    def payment_accounts(self, request, pk=None):
        scene = get_object_or_404(Scene, id=pk)
        if not (self.request.user.is_authenticated and self.request.user.is_admin_of(scene)):
            raise PermissionDenied()

        accounts = []
        account_ids = {}
        for tournament in Tournament.objects.filter(scene=scene, stripe_account_id__isnull=False).order_by("-date"):
            if tournament.stripe_account_id in account_ids:
                continue

            accounts.append({
                "stripe_account_id": tournament.stripe_account_id,
                "last_tournament": tournament.name,
            })

        return Response(accounts)

    @action(detail=True)
    def permissions(self, request, pk=None):
        scene = get_object_or_404(Scene, id=pk)
        if not (self.request.user.is_authenticated and self.request.user.is_admin_of(scene)):
            raise PermissionDenied()

        scene_permissions = scene.permissions.order_by("id")
        page = self.paginate_queryset(scene_permissions)
        if page is not None:
            serializer = ScenePermissionSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = ScenePermissionSerializer(scene_permissions, many=True)
        return Response(serializer.data)


class CabinetViewSet(BaseViewSet):
    queryset = Cabinet.objects.all().order_by("name")
    serializer_class = CabinetSerializer
    filterset_fields = ["scene_id", "name"]
    permission_classes = [CabinetPermission]

    def get_serializer_class(self):
        if not (self.detail and self.request.user.is_authenticated):
            return CabinetSerializer

        cabinet = self.get_object()
        if self.request.user.is_authenticated and self.request.user.is_admin_of(cabinet.scene):
            return CabinetAdminSerializer

        return CabinetSerializer

    @transaction.atomic
    def create(self, request):
        response = super().create(request)

        Overlay.objects.create(
            cabinet_id=response.data["id"],
            is_default=True,
        )

        return response

    @action(detail=False)
    def timezones(self, request):
        return Response({"results": pytz.all_timezones})

    @action(detail=True, methods=[HTTPMethod.GET])
    def signin(self, request, pk=None):
        cabinet = get_object_or_404(Cabinet, id=pk)
        signed_in = SignInLog.objects.filter(cabinet=cabinet, action=SignInLog.Action.SIGN_IN, is_current=True)

        return Response({
            "id": cabinet.id,
            "signed_in": [i.get_publish_data() for i in signed_in],
        })

    @action(detail=True, methods=[HTTPMethod.GET], url_path="queued-matches")
    def queued_matches(self, request, pk=None):
        cabinet = get_object_or_404(Cabinet, id=pk)

        # Find the current tournament
        current_match = TournamentMatch.objects.filter(active_cabinet=cabinet).first()
        if current_match:
            tournament = current_match.bracket.tournament
        else:
            last_game = Game.objects.filter(
                end_time__isnull=False,
                tournament_match__isnull=False,
                cabinet=cabinet,
            ).order_by("-end_time").first()
            if last_game is None:
                return Response({ "cabinet_id": cabinet.id, "matches": [] })

            tournament = last_game.tournament_match.bracket.tournament

        queues = cabinet.matchqueue_set.filter(tournament=tournament, enabled=True) \
            .annotate(Count("cabinets")) \
            .order_by("cabinets__count", "id")

        general_queue = MatchQueue.objects.filter(tournament=tournament) \
            .annotate(Count("cabinets")) \
            .filter(cabinets__count=0)

        matches = []
        for queue in list(queues) + list(general_queue):
            matches.extend(queue.get_list())

        return Response({ "cabinet_id": cabinet.id, "matches": TournamentMatchSerializer(matches, many=True).data })

class GameViewSet(BaseViewSet):
    queryset = Game.objects.order_by("id")
    serializer_class = GameSerializer
    filterset_class = GameFilter
    permission_classes = [GamePermission]

    @action(detail=False)
    def recent(self, request):
        games = self.filter_queryset(self.get_queryset()) \
                    .filter(end_time__isnull=False) \
                    .order_by("-start_time")

        page = self.paginate_queryset(games)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(games, many=True)
        return Response(serializer.data)

    @action(detail=True)
    def stats(self, request, pk=None):
        game = Game.objects.get(id=pk)
        return Response(game.get_postgame_stats())

    @action(detail=True, methods=[HTTPMethod.GET])
    def qr(self, request, pk=None):
        response = HttpResponse()
        response["Content-Type"] = "image/svg+xml"

        image = qrcode.make("https://kqhivemind.com/game/{}".format(pk),
                            image_factory=qrcode.image.svg.SvgPathImage)
        image.save(response)

        return response


class GameEventViewSet(BaseViewSet):
    queryset = GameEvent.objects.all().order_by("timestamp")
    serializer_class = GameEvent.serializer()
    filterset_fields = ["game_id", "event_type"]


class CabinetActivityPagination(PageNumberPagination):
    page_size = 7 * 24


class CabinetActivityViewSet(BaseViewSet):
    queryset = CabinetActivity.objects.all()
    serializer_class = CabinetActivity.serializer()
    filterset_class = CabinetActivityFilter
    pagination_class = CabinetActivityPagination


class GameMapViewSet(BaseViewSet):
    queryset = GameMap.objects.all()
    serializer_class = GameMap.serializer()
    filterset_fields = ["name"]

    @action(detail=False, url_path="(?P<map_name>.+)/at-version/(?P<cabinet_version>[0-9\.]+)")
    def at_version(self, request, map_name, cabinet_version):
        game_map = get_object_or_404(GameMap, name=map_name)
        return Response(GameMap.serializer()(game_map).data)

    @action(detail=True)
    def heatmaps(self, request, pk=None):
        heatmaps = [
            Heatmap.objects.filter(game_map_id=pk, event_type=event_type).order_by("-date_generated").first()
            for event_type in HeatmapType
        ]

        serializer = Heatmap.serializer()(heatmaps, many=True)
        return Response(serializer.data)


class ClientDeviceViewSet(BaseViewSet):
    queryset = ClientDevice.objects.all()
    serializer_class = ClientDeviceSerializer()
    permission_classes = [ClientDevicePermission]

    def get_queryset(self):
        user = self.request.user
        if user and user.is_authenticated:
            scenes = [p.scene_id for p in user.permissions.filter(permission=PermissionType.ADMIN)]
            return ClientDevice.objects.filter(owner_id__in=scenes)

        if self.kwargs.get("pk"):
            return ClientDevice.objects.filter(id=self.kwargs.get("pk"))

        raise PermissionDenied()

    def get_serializer_class(self):
        if self.detail:
            device = self.get_object()
            if not (self.request.user.is_authenticated and self.request.user.is_admin_of(device.owner)):
                return ClientDeviceSerializer

        return ClientDeviceAdminSerializer

    def update(self, request, pk=None):
        device = get_object_or_404(ClientDevice, id=pk)
        if device.cabinet_id != request.data.get("cabinet"):
            new_cabinet = get_object_or_404(Cabinet, id=request.data.get("cabinet"))
            if not request.user.is_admin_of(new_cabinet.scene):
                if request.data.get("cabinet_token") != new_cabinet.token:
                    raise PermissionDenied()

        response = super().update(request, pk)
        device.publish()
        return response

    @action(detail=True, methods=[HTTPMethod.GET])
    def signin(self, request, pk=None):
        device = get_object_or_404(ClientDevice, id=pk)
        signed_in = SignInLog.objects.filter(cabinet=device.cabinet, action=SignInLog.Action.SIGN_IN,
                                             is_current=True)

        return Response({
            "id": device.id,
            "signed_in": [i.get_publish_data() for i in signed_in],
        })
