import logging

from django.core.exceptions import BadRequest
from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod, PermissionType
from ..permissions import SceneAdminOrReadOnly, TournamentOfficialOrReadOnly
from .models import Cabinet, Scene


class ScenePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS or request.method == HTTPMethod.PUT:
            return True

        if request.method == HTTPMethod.POST and request.user.is_authenticated and request.user.is_site_admin:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if not request.user.is_authenticated:
            return False

        if request.method == HTTPMethod.PUT and request.user.is_admin_of(obj):
            return True

        return False


class CabinetPermission(SceneAdminOrReadOnly):
    pass


class GamePermission(TournamentOfficialOrReadOnly):
    def get_scene_from_object(self, obj):
        return obj.cabinet.scene

    def get_scene_id_from_request(self, request):
        return False


class ClientDevicePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True

        if not request.user.is_authenticated:
            return False

        try:
            scene = Scene.objects.get(id=request.data.get("owner"))
            cabinet = Cabinet.objects.get(id=request.data.get("cabinet"))
            return request.user.is_admin_of(scene) and request.user.is_admin_of(cabinet.scene)
        except (Scene.DoesNotExist, Cabinet.DoesNotExist):
            raise BadRequest("Invalid scene or cabinet")

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if not request.user.is_authenticated:
            return False

        return request.user.is_admin_of(obj.owner)
