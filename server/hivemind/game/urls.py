from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r"scene", views.SceneViewSet)
router.register(r"cabinet", views.CabinetViewSet)
router.register(r"game", views.GameViewSet)
router.register(r"game-event", views.GameEventViewSet)
router.register(r"cabinet-activity", views.CabinetActivityViewSet)
router.register(r"map", views.GameMapViewSet)
router.register(r"client-device", views.ClientDeviceViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
