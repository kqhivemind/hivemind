# Generated by Django 3.2.7 on 2023-09-20 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0026_auto_20230906_1849'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gamestat',
            name='stat_type',
            field=models.TextField(choices=[('kills', 'Kills'), ('deaths', 'Deaths'), ('queen_kills', 'Queen Kills'), ('military_kills', 'Military Kills'), ('military_deaths', 'Military Deaths'), ('bump_assists', 'Bump Assists'), ('berries', 'Berries Deposited'), ('berries_kicked', 'Berries Kicked In'), ('snail_distance', 'Snail Distance'), ('eaten_by_snail', 'Eaten by Snail'), ('warrior_uptime', 'Warrior Uptime'), ('snail_distance_pct', 'Snail Distance (%)')], max_length=20),
        ),
    ]
