from enum import Enum

from django.db import models


class HTTPMethod(str, Enum):
    GET = "GET"
    HEAD = "HEAD"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    CONNECT = "CONNECT"
    OPTIONS = "OPTIONS"
    TRACE = "TRACE"
    PATCH = "PATCH"


class PermissionType(models.TextChoices):
    ADMIN = "admin", "Scene Admin"
    TOURNAMENT = "tournament", "Tournament Manager"


class CabinetTeam(models.TextChoices):
    BLUE = "blue", "Blue"
    GOLD = "gold", "Gold"

    @property
    def positions(self):
        if self.value == self.BLUE:
            return [CabinetPosition(i) for i in [4, 6, 2, 8, 10]]
        if self.value == self.GOLD:
            return [CabinetPosition(i) for i in [3, 5, 1, 7, 9]]

    @property
    def opponent(self):
        if self.value == self.BLUE:
            return self.GOLD
        if self.value == self.GOLD:
            return self.BLUE


class CabinetPosition(models.IntegerChoices):
    GOLD_QUEEN = 1, "Gold Queen"
    BLUE_QUEEN = 2, "Blue Queen"
    GOLD_STRIPES = 3, "Gold Stripes"
    BLUE_STRIPES = 4, "Blue Stripes"
    GOLD_ABS = 5, "Gold Abs"
    BLUE_ABS = 6, "Blue Abs"
    GOLD_SKULLS = 7, "Gold Skulls"
    BLUE_SKULLS = 8, "Blue Skulls"
    GOLD_CHECKS = 9, "Gold Checks"
    BLUE_CHECKS = 10, "Blue Checks"

    @property
    def image(self):
        IMAGES = {
            1: "/static/players/gold-queen.png",
            2: "/static/players/blue-queen.png",
            3: "/static/players/gold-drone-stripes.png",
            4: "/static/players/blue-drone-stripes.png",
            5: "/static/players/gold-drone-abs.png",
            6: "/static/players/blue-drone-abs.png",
            7: "/static/players/gold-drone-skulls.png",
            8: "/static/players/blue-drone-skulls.png",
            9: "/static/players/gold-drone-checks.png",
            10: "/static/players/blue-drone-checks.png",
        }

        return IMAGES[self.value]

    @property
    def warrior_image(self):
        WARRIOR_IMAGES = {
            1: "/static/players/gold-queen.png",
            2: "/static/players/blue-queen.png",
            3: "/static/players/gold-warrior-stripes.png",
            4: "/static/players/blue-warrior-stripes.png",
            5: "/static/players/gold-warrior-abs.png",
            6: "/static/players/blue-warrior-abs.png",
            7: "/static/players/gold-warrior-skulls.png",
            8: "/static/players/blue-warrior-skulls.png",
            9: "/static/players/gold-warrior-checks.png",
            10: "/static/players/blue-warrior-checks.png",
        }

        return WARRIOR_IMAGES[self.value]

    @property
    def icon(self):
        ICONS = {
            1: "/static/players/gold-icon-queen.png",
            2: "/static/players/blue-icon-queen.png",
            3: "/static/players/gold-icon-stripes.png",
            4: "/static/players/blue-icon-stripes.png",
            5: "/static/players/gold-icon-abs.png",
            6: "/static/players/blue-icon-abs.png",
            7: "/static/players/gold-icon-skulls.png",
            8: "/static/players/blue-icon-skulls.png",
            9: "/static/players/gold-icon-checks.png",
            10: "/static/players/blue-icon-checks.png",
        }

        return ICONS[self.value]

    @property
    def team(self):
        if self.value % 2 == 0:
            return CabinetTeam.BLUE
        else:
            return CabinetTeam.GOLD

    @property
    def opponent(self):
        return self.team.opponent

    @property
    def is_queen(self):
        return self in [CabinetPosition.GOLD_QUEEN, CabinetPosition.BLUE_QUEEN]


# Stats calculated for every game
class StatType(models.TextChoices):
    KILLS = "kills", "Kills"
    DEATHS = "deaths", "Deaths"
    QUEEN_KILLS = "queen_kills", "Queen Kills"
    MILITARY_KILLS = "military_kills", "Military Kills"
    MILITARY_DEATHS = "military_deaths", "Military Deaths"
    BUMP_ASSISTS = "bump_assists", "Bump Assists"
    BERRIES = "berries", "Berries Deposited"
    BERRIES_KICKED = "berries_kicked", "Berries Kicked In"
    SNAIL_DISTANCE = "snail_distance", "Snail Distance"
    EATEN_BY_SNAIL = "eaten_by_snail", "Eaten by Snail"
    WARRIOR_UPTIME = "warrior_uptime", "Warrior Uptime"


# Stats calculated for every game but not shown in lists
class HiddenStatType(models.TextChoices):
    SNAIL_DISTANCE_PCT = "snail_distance_pct", "Snail Distance (%)"


# Stats that only need to be shown as aggregates over multiple games
class AggregateStatType(models.TextChoices):
    GAMES = "games", "Games"
    WINS = "wins", "Wins"
    LOSSES = "losses", "Losses"
    TIME_PLAYED = "time_played", "Time Played"


class StatFormattingType(models.TextChoices):
    INTEGER = "integer", "Integer"
    TIME = "time", "Time"
    THOUSANDTHS = "thousandths", "Thousandths"


class FieldType(models.TextChoices):
    TEXT = "text", "Text"
    CHECKBOX = "checkbox", "Checkbox"
    SINGLE_CHOICE = "single_choice", "Single Choice (Dropdown)"
    MULTI_CHOICE = "multi_choice", "Multiple Choice (Checkboxes)"

class TournamentStageType(models.TextChoices):
    ROUND_ROBIN = "round_robin", "Round Robin (Group Stage)"
    SINGLE_ELIM = "single_elim", "Single Elimination"
    DOUBLE_ELIM = "double_elim", "Double Elimination"
    LADDER = "ladder", "Ladder"
    CUSTOM = "custom", "Custom (Manually Scheduled)"

class TournamentAutoWarmup(models.TextChoices):
    NEVER = "never", "Never"
    FIRST_SET = "first_set", "First Set"
    ALWAYS = "always", "Always"

class MatchType(models.TextChoices):
    WINNERS = "winners", "Winners Bracket"
    LOSERS = "losers", "Losers Bracket"
    FIRST_FINAL = "first_final", "Finals (First Match)"
    SECOND_FINAL = "second_final", "Finals (Second Match)"
    THIRD_PLACE = "third_place", "Third Place Match"


class PlayerRole(models.TextChoices):
    QUEEN = "queen", "Queen"
    WARRIOR = "warrior", "Warrior"
    OBJECTIVE = "objective", "Objective"


class HeatmapType(models.TextChoices):
    QUEEN = "queen", "Queen"
    WARRIOR = "warrior", "Warrior"
    WORKER = "worker", "Worker"
    SNAIL = "snail", "Snail Rider"


# https://discord.com/channels/850091759824535552/877059615924301845/1021890909996064788
SNAIL_PX_PER_SEC = 20.896215463
SNAIL_PX_PER_SEC_SPEED = 28.209890875
