from celery import Celery, signals
from django.conf import settings


@signals.setup_logging.connect
def setup_celery_logging(**kwargs):
    pass

task_modules = [
    "hivemind.achievement.tasks",
    "hivemind.stats.tasks",
    "hivemind.game.tasks",
    "hivemind.notifications.tasks",
    "hivemind.tournament.challonge",
    "hivemind.tournament.tasks",
    "hivemind.user.tasks",
    "hivemind.whiteboard.tasks",
]

if settings.REDIS_PASSWORD:
    broker_url = f"redis://:{settings.REDIS_PASSWORD}@{settings.REDIS_HOST}:6379/0"
else:
    broker_url = f"redis://{settings.REDIS_HOST}:6379/0"

app = Celery("hivemind.worker", broker=broker_url, include=task_modules)
app.conf.result_backend = broker_url
app.conf["worker_concurrency"] = 4
app.conf.beat_schedule = {
    "hivemind.stats.tasks.check_sign_in_timeouts": {
        "task": "hivemind.stats.tasks.check_sign_in_timeouts",
        "schedule": 60*60,
    },
    "hivemind.whiteboard.tasks.deactivate_stale_sessions": {
        "task": "hivemind.whiteboard.tasks.deactivate_stale_sessions",
        "schedule": 60*60,
    },
}
