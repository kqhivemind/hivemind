from rest_framework.decorators import action
from rest_framework.response import Response

from ..constants import HTTPMethod
from ..model import BaseViewSet
from .filters import MatchFilter, PlayerFilter
from .models import (BYOTeam, Event, Match, Player, QPMatch, QPPlayer, Season,
                     SeasonPlace, Team)
from .permissions import (BYOTeamPermission, EventPermission, PlayerPermission,
                          SeasonPermission, TeamPermission)
from .serializers import (EventSerializer, MatchSerializer, PlayerSerializer,
                          QPMatchSerializer, SeasonPlaceSerializer,
                          SeasonSerializer, TeamSerializer)

TeamSerializer = Team.default_serializer()
BYOTeamSerializer = BYOTeam.default_serializer()


def serialize_players(player_list):
    return [PlayerSerializer(Player.objects.get(id=i)).data for i in player_list]


class SeasonViewSet(BaseViewSet):
    queryset = Season.objects.all().order_by("-start_date", "-id")
    serializer_class = SeasonSerializer
    permission_classes = [SeasonPermission]
    filterset_fields = ["scene_id"]

    @action(detail=True)
    def standings(self, request, pk=None):
        season = Season.objects.get(id=pk)

        if season.season_type == season.SeasonType.SHUFFLE:
            standings = season.player_results()
        if season.season_type == season.SeasonType.BYOT:
            standings = season.team_results()

            for row in standings:
                row["team"] = BYOTeamSerializer(row["team"]).data
                row["team"]["players"] = serialize_players(row["team"]["players"])

        return Response(standings)


class EventViewSet(BaseViewSet):
    queryset = Event.objects.all().order_by("event_date", "id")
    serializer_class = EventSerializer
    permission_classes = [EventPermission]
    filterset_fields = ["season_id", "cabinet_id", "is_active"]

    @action(detail=True, methods=[HTTPMethod.PUT])
    def publish(self, request, pk=None):
        event = Event.objects.get(id=pk)
        current_match = event.publish_match_state()

        return Response(current_match)

    @action(detail=True)
    def standings(self, request, pk=None):
        event = Event.objects.get(id=pk)

        if event.is_quickplay:
            standings = event.qp_player_results()

            for row in standings:
                row["player"] = PlayerSerializer(row["player"]).data

        else:
            standings = event.team_results()

            for row in standings:
                row["team"] = TeamSerializer(row["team"]).data
                row["team"]["players"] = serialize_players(row["team"]["players"])

        return Response(standings)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="generate-teams")
    def generate_teams(self, request, pk=None):
        event = Event.objects.get(id=pk)

        if event.is_quickplay:
            if event.qpmatch_set.count() > 0:
                return Response({"success": False, "error": "Teams have already been generated for this event."})

            event.generate_qp_match()
            return Response({
                "success": True,
            })

        else:
            if event.team_set.count() > 0:
                return Response({"success": False, "error": "Teams have already been generated for this event."})

            event.generate_teams(request.data.get("num_teams"), request.data.get("teams_to_include"))

            return Response({
                "success": True,
                "teams": [TeamSerializer(team).data for team in event.team_set.all()],
            })

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="delete-teams")
    def delete_teams(self, request, pk=None):
        event = Event.objects.get(id=pk)

        event.match_set.all().delete()
        for team in event.team_set.all():
            for player in team.players.all():
                if team.byoteam is None or player not in team.byoteam.players.all():
                    event.unassigned_players.add(player)

            team.delete()

        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="select-teams")
    def select_teams(self, request, pk=None):
        event = Event.objects.get(id=pk)

        teams_to_include = {int(i) for i in filter(lambda i: request.data[i], request.data.keys())}
        event.generate_teams(None, teams_to_include=teams_to_include)

        return Response({"success": True})


class MatchViewSet(BaseViewSet):
    queryset = Match.objects.all().order_by("match_order")
    serializer_class = MatchSerializer
    filterset_class = MatchFilter


class QPMatchViewSet(BaseViewSet):
    queryset = QPMatch.objects.all()
    serializer_class = QPMatchSerializer
    filterset_fields = ["event_id"]

    @action(detail=True)
    def players(self, request, pk=None):
        data = {}
        qp_match = QPMatch.objects.get(id=pk)
        for qp_player in qp_match.qpplayer_set.all():
            team_players = data[qp_player.team] = data.get(qp_player.team, [])
            team_players.append(PlayerSerializer(qp_player.player).data)

        return Response(data)


class SeasonPlaceViewSet(BaseViewSet):
    queryset = SeasonPlace.objects.all().order_by("place")
    serializer_class = SeasonPlaceSerializer
    filterset_fields = ["season_id"]


class PlayerViewSet(BaseViewSet):
    queryset = Player.objects.all().order_by("name")
    serializer_class = PlayerSerializer
    filterset_class = PlayerFilter
    permission_classes = [PlayerPermission]

    @action(detail=True)
    def results(self, request, pk=None):
        player = Player.objects.get(id=pk)
        standings = player.results_by_season()

        for season_row in standings:
            season_row["season"] = SeasonSerializer(season_row["season"]).data
            for event_row in season_row["events"]:
                event_row["event"] = EventSerializer(event_row["event"]).data
                if "team" in event_row:
                    event_row["team"] = TeamSerializer(event_row["team"]).data
                if "player" in event_row:
                    event_row["player"] = PlayerSerializer(event_row["player"]).data

        return Response(standings)


class TeamViewSet(BaseViewSet):
    queryset = Team.objects.all().order_by("id")
    serializer_class = TeamSerializer
    permission_classes = [TeamPermission]


class BYOTeamViewSet(BaseViewSet):
    queryset = BYOTeam.objects.all().order_by("id")
    serializer_class = BYOTeamSerializer
    permission_classes = [BYOTeamPermission]
    filterset_fields = ["season_id"]
