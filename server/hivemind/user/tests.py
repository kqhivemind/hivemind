import http.client

from ..constants import PermissionType
from ..game.models import Scene
from ..tests import HiveMindTest
from .models import Message, Permission, User


class UserTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._public_user = User(username="Public User", email="public@user.com", is_profile_public=True)
        self._public_user.save()

        self._private_user = User(username="Private User", email="private@user.com", is_profile_public=False)
        self._private_user.save()

    def test_user_endpoint(self):
        response = self.client.get("/api/user/user/{}/".format(self._user.id))
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/user/user/{}/".format(self._public_user.id))
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/user/user/{}/".format(self._private_user.id))
        self.assertEqual(response.status_code, http.client.FORBIDDEN)


class PermissionTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._scene2 = Scene(name="TEST2", display_name="Test Scene 2")
        self._scene2.save()

        self._user2 = User(username="test ID 2", email="test2@test.com", is_site_admin=False)
        self._user2.save()

        Permission.objects.create(user=self._user2, scene=self._scene2, permission=PermissionType.ADMIN)

        self._user3 = User(username="test ID 3", email="test3@test.com", is_site_admin=False)
        self._user3.save()

        Permission.objects.create(user=self._user3, scene=self._scene, permission=PermissionType.ADMIN)

        self._basic_user = User(username="test ID 4", email="test4@test.com", is_site_admin=False)
        self._basic_user.save()

    def test_permission_list_no_user(self):
        self.client.force_authenticate(None)

        response = self.client.get("/api/user/permission/")
        self.assertEqual(response.status_code, http.client.UNAUTHORIZED)

    def test_permission_list(self):
        response = self.client.get("/api/user/permission/")
        self.assertEqual(response.status_code, http.client.OK)

        users = [i["user"] for i in response.data["results"]]
        self.assertIn(self._user.email, users)
        self.assertNotIn(self._user2.email, users)

    def test_permission_add(self):
        data = {
            "user": self._user2.email,
            "scene": self._scene.id,
            "permission": PermissionType.ADMIN,
        }

        response = self.client.post("/api/user/permission/", data)
        self.assertEqual(response.status_code, http.client.CREATED)

        response = self.client.get("/api/user/permission/")
        users = [i["user"] for i in response.data["results"]]
        self.assertIn(self._user2.email, users)

    def test_permission_add_other_scene(self):
        data = {
            "user": self._user.email,
            "scene": self._scene2.id,
            "permission": PermissionType.ADMIN,
        }

        response = self.client.post("/api/user/permission/", data)
        self.assertEqual(response.status_code, http.client.FORBIDDEN)

    def test_permission_delete(self):
        response = self.client.get("/api/user/permission/")
        self.assertEqual(response.status_code, http.client.OK)

        for permission in response.data["results"]:
            if permission["user"] == self._user3.email:
                permission_id = permission["id"]

        response = self.client.delete("/api/user/permission/{}/".format(permission_id))
        self.assertEqual(response.status_code, http.client.NO_CONTENT)

        response = self.client.get("/api/user/permission/")
        self.assertEqual(response.status_code, http.client.OK)

        users = [i["user"] for i in response.data["results"]]
        self.assertIn(self._user.email, users)
        self.assertNotIn(self._user3.email, users)

    def test_permission_delete_other_scene(self):
        self.client.force_authenticate(self._user2)

        response = self.client.get("/api/user/permission/")
        self.assertEqual(response.status_code, http.client.OK)

        for permission in response.data["results"]:
            if permission["user"] == self._user2.email and permission["scene"] == self._scene2.id:
                permission_id = permission["id"]

        self.client.force_authenticate(self._user)

        response = self.client.delete("/api/user/permission/{}/".format(permission_id))
        self.assertEqual(response.status_code, http.client.NOT_FOUND)


class MessageTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._user2 = User(username="test ID 2", email="test2@test.com", is_site_admin=False)
        self._user2.save()

        Message.objects.create(message_text="Test 1", url="/", user=self._user)
        Message.objects.create(message_text="Test 2", url="/", user=self._user2)

    def test_message_list_no_user(self):
        self.client.force_authenticate(None)

        response = self.client.get("/api/user/message/")
        self.assertEqual(response.status_code, http.client.UNAUTHORIZED)

    def test_message_list(self):
        self.client.force_authenticate(self._user)

        response = self.client.get("/api/user/message/")
        self.assertEqual(response.status_code, http.client.OK)

        message_texts = [i["message_text"] for i in response.data["results"]]
        self.assertIn("Test 1", message_texts)
        self.assertNotIn("Test 2", message_texts)

    def test_message_update(self):
        self.client.force_authenticate(self._user)

        response = self.client.get("/api/user/message/")
        self.assertEqual(response.status_code, http.client.OK)

        data = response.data["results"][0]

        data["message_text"] = "editing message"
        data["is_read"] = True

        response = self.client.put("/api/user/message/{}/".format(data["id"]), data)
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/user/message/")
        self.assertEqual(response.status_code, http.client.OK)

        self.assertEqual(response.data["results"][0]["message_text"], "Test 1")
        self.assertEqual(response.data["results"][0]["is_read"], True)


class SceneRequestTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._user.is_site_admin = True
        self._user.save()

        self._user2 = User(username="test ID 2", email="test2@test.com", is_site_admin=False)
        self._user2.save()

        self._scene_name = "scene2"

    def test_submit_scene_request(self):
        self.client.force_authenticate(self._user2)

        data = {
            "name": self._scene_name,
            "display_name": "New Test Scene",
            "user_name": "Testy McTestface",
            "comments": "Comments",
        }

        response = self.client.post("/api/user/scene-request/", data)
        self.assertEqual(response.status_code, http.client.CREATED)
