import random

from django.core.management.base import BaseCommand

from hivemind.constants import CabinetPosition
from hivemind.game.models import Cabinet
from hivemind.stats.models import SignInLog
from hivemind.user.models import User


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        users = list(User.objects.filter(social_auth__isnull=True).exclude(name=""))

        for cabinet in Cabinet.objects.filter(scene__name="test"):
            for position in CabinetPosition:
                sign_in = SignInLog(
                    user=random.choice(users),
                    method=SignInLog.SignInMethod.NFC,
                    action=SignInLog.Action.SIGN_IN,
                    cabinet=cabinet,
                    player_id=position,
                )

                sign_in.set_current()
                sign_in.save()
                sign_in.publish()
