# Generated by Django 5.0.6 on 2024-08-06 16:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0021_permissioninvite'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='patreon_amount',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
