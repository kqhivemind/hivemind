import http.client
import json
import os
import random
import warnings

import requests
from django.urls import resolve
from hivemind.constants import PermissionType
from hivemind.game.models import Cabinet, Scene
from hivemind.league.models import Player
from hivemind.user.models import Permission, User
from rest_framework.test import APIRequestFactory, APITestCase


class HiveMindTest(APITestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        warnings.filterwarnings(
            "ignore", r"DateTimeField .* received a naive datetime",
            RuntimeWarning, r"django\.db\.models\.fields",
        )

    def setUp(self):
        scene = Scene(name="test", display_name="Test Scene")
        scene.save()

        test_user = User(username="test ID", email="test@test.com", is_site_admin=False)
        test_user.save()

        Permission.objects.create(user=test_user, scene=scene, permission=PermissionType.ADMIN)

        cabinet = Cabinet(name="test", display_name="Test Cabinet", scene=scene)
        cabinet.save()

        self._user = test_user
        self._scene = scene
        self._cabinet = cabinet

        self.client.force_authenticate(test_user)

    def get_player_names(self, name_count):
        return random.sample(self.PLAYER_NAMES, k=name_count)

    def get_team_names(self, name_count):
        return random.sample(self.TEAM_NAMES, k=name_count)

    def add_players_to_event(self, event, player_count):
        for player_name in self.get_player_names(player_count):
            player, created = Player.objects.get_or_create(name=player_name, scene=event.season.scene)
            event.unassigned_players.add(player)

        event.save()

    PLAYER_NAMES = [
        "Adam Banallen",
        "Alex Turzeon",
        "Alex Zoseph",
        "Alexnder Refe",
        "Allan Chantz",
        "Anatoli Smorin",
        "Andre Tackett",
        "Andujar Ersulak",
        "Andy Pmith",
        "Anthony Gwindell",
        "Armando Nartinez",
        "Arturs Nuller",
        "Barry Rankford",
        "Benito Labo",
        "Bill Erochia",
        "Bill Nay",
        "Bip Karr",
        "Birry Odereitt",
        "Biry Dedorov",
        "Bob Gurkett",
        "Bobby Krarsa",
        "Bobby Levason",
        "Bobby Raminiti",
        "Bobson Dugnutt",
        "Brad Klark",
        "Brian Silkins",
        "Bryan Make",
        "Carlos Drown",
        "Chris Whitmey",
        "Claude McShee",
        "Cliff Revis",
        "Craig Goleman",
        "Curt Dandiotti",
        "Danny Estacio",
        "Darren Sryper",
        "Darrin Clerk",
        "Dave Cozlov",
        "Dave Quitter",
        "Dave Sweemey",
        "Delino Jole",
        "Dennis Leynoso",
        "Dimitri Ysedaert",
        "Don Gianfrocc",
        "Don Poulder",
        "Doug Pernandez",
        "Doug Pomlin",
        "Dwight Blavine",
        "Dwigt Rortugal",
        "Eddie Dallagher",
        "Elvis Crushel",
        "Eric Pollins",
        "Garry Gubinsky",
        "Greg Mibbard",
        "Greg Roung",
        "Gregg Klark",
        "Henry Ancaviglia",
        "Igor Karbon",
        "Jack Dozon",
        "Jacky Milmanov",
        "Jamie Shiasson",
        "Jan Svobota",
        "Jarvis Fell",
        "Jay Bastillo",
        "Jay Butierrez",
        "Jeff Bottenkield",
        "Jeff Enthony",
        "Jerald Kordero",
        "Jim Dallach",
        "Jim Reclair",
        "Jimmy Durakowsky",
        "Joe Sedeno",
        "John Armstarong",
        "John Newksbury",
        "Jose Bitrangelo",
        "Jose Every",
        "Josef Lelfour",
        "Ken Barris",
        "Ken Nurphy",
        "Kevin Bogarty",
        "Kevin Faite",
        "Kevin Liver",
        "Kevin Rohnson",
        "Kirt Magnozzi",
        "Krik McDain",
        "Luis Drowhing",
        "Luke Vrisebois",
        "Mac Baglianeti",
        "Mark Lourque",
        "Mark Reschyshyn",
        "Mickey Ofterman",
        "Mike Lichardson",
        "Mike Mteen",
        "Mike Sernandez",
        "Mike Truk",
        "Orel Nulholland",
        "Orestes Narkin",
        "Ozzie Thompsen",
        "Pat Leichel",
        "Paul Caramnov",
        "Paul Williarms",
        "Pedro Packson",
        "Pele Lodriguez",
        "Ryan Loper",
        "Sammy Nereker",
        "Scott Balgneault",
        "Sergei Kavallini",
        "Sid Srabek",
        "Sleve McDichael",
        "Stan Nurphy",
        "Stephan McSim",
        "Stephane Brok",
        "Steve Thompton",
        "Steven Czerpaws",
        "Ted Balloon",
        "Tim Wakedield",
        "Todd Bonzalez",
        "Todd Willicams",
        "Tom Vellows",
        "Tommy Nartinez",
        "Tony Ban Slyke",
        "Vincent Fearson",
        "Vincent Memenov",
        "Wally Balk",
        "Willie Dustice",
    ]

    TEAM_NAMES = [
        "Akron RubberDucks",
        "Albuquerque Isotopes",
        "Amarillo Sod Poodles",
        "Asheville Tourists",
        "Beloit Snappers",
        "Biloxi Shuckers",
        "Binghamton RumblePonies",
        "Brevard County Manatees",
        "Charleston RiverDogs",
        "Clinton LumberKings",
        "Down East Wood Ducks",
        "Fort Myers Mighty Mussels",
        "Greensboro Grasshoppers",
        "Hickory Crawdads",
        "Jacksonville Jumbo Shrimp",
        "Kannapolis Intimidators",
        "Lansing Lugnuts",
        "Lehigh Valley IronPigs",
        "Modesto Nuts",
        "Montgomery Biscuits",
        "Pensacola Blue Wahoos",
        "Quad Cities River Bandits",
        "Richmond Flying Squirrels",
        "Rocket City Trash Pandas",
        "Salt Lake Bees",
        "Savannah Sand Gnats",
        "Sugar Land Space Cowboys",
        "Toledo Mud Hens",
        "Vermont Lake Monsters",
    ]
